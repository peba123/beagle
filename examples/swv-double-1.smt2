(set-logic LIA)

(set-info
  :source |
Verification Example
====================

def f(i: Int) =
@pre: i ≥ 0
@post: res = 2·i
    if (i == 0)
       0
    else
       f(i-1) + 2

Partial correctness:

Proof of postcondition by induction, assuming precondition:
∀ i:Int
  i ≥ 0 →				// @pre
  let res =				// res = "translation of body of f"
      if i = 0 then
        0
      else
        let res1 =			// induction hypotheses:
            let i = i-1			// assume @post holds for f(i-1),
             in 2·i 			// i.e. f(i-1) = 2·(i-1) =: res1
	 in res1 + 2			// It follows res := f(i) = res1 + 2
   in res = 2·i                         // show that @post holds for f(i), i.e., res = 2·i

Proof precondition of induction hypotheses:
∀ i:Int
  i ≥ 0 →
  if i = 0 then
    true				// nothing to show for base case
  else
    let i = i-1
     in i ≥ 0				// @pre for induction hypotheses
|)

;; first obligation
(assert (not
 (forall ((i Int))
	 (=> (>= i 0)
	     (let ((res
		    (ite (= i 0)
			 0
			 (let ((res1
				(let ((i (- i 1))) (* 2 i))))
			   (+ res1 2)))))
	       (= res (* 2 i)))))))
		
(check-sat)
