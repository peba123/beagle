(set-logic UFLIA)

(set-info
  :source |
Binary seach algorithm
======================

(declare-datatypes () ((Tree empty (node (val Int) (left Tree) (right Tree)))))

@pre: searchtree(t)
@post: res ↔ in(v, t)
def binSearch(t: Tree v: Int) =
  if (t == empty)
     false
  else {
     if (v == val(t))
        true
     else if (v < val(t))
        binSearch(left(t), v)
     else 
        binSearch(right(t), v)
  }

Partial correctness:

Proof of postcondition by induction, assuming precondition:
∀ t:Tree, v:Int
  searchtree(t) →
  let res =
      if t == empty then
         false
      else
         if v = val(t) then
            true
         else if v < val(t) then
                 let res1 = // res1 is result of recursive call, by I.H.
                     let t = left(t)
                      in in(v, t) // I.H.
                  in res1 // result res is just res1
              else
                 let res1 = // res1 is result of recursive call, by I.H.
                     let t = right(t)
                      in in(v, t) // I.H.
                  in res1 // result res is just res1
   in res ↔ in(v, t)


Proof precondition of induction hypotheses:
∀ t:Tree, v:Int
  searchtree(t) →
  if t = empty then
     true
  else
     if v = val(t) then
        true
     else if v < val(t) then
             let t = left(t)
              in searchtree(t) 
          else
             let t = right(t)
              in searchtree(t)
|)

;; translation of the above into concrete SMT-LIB syntax

(declare-datatypes () ((Tree empty (node (val Int) (left Tree) (right Tree)))))
;; (in v t) is true iff v occurs in t. The tree t does not need to be ordered
(declare-fun in (Int Tree) Bool)
(assert
 (forall ((v Int) (t Tree))
	 (= (in v t)
	    (ite (= t empty)
		 false
		 (or (= v (val t))
		     (in v (left t))
		     (in v (right t)))))))

;; (searchtree t) is true iff t is a binary search tree
(declare-fun searchtree (Tree) Bool)
(assert
 (forall ((t Tree))
	 (= (searchtree t)
	    (ite (= t empty)
		 true
		 (and
		  (searchtree (left t))
		  (searchtree (right t))
		  (forall ((v Int))
			  (=> (in v (left t))
			      (<= v (val t))))
		  (forall ((v Int))
			  (=> (in v (right t))
			      (> v (val t)))))))))

;; second proof obligation
(assert
 (not
  (forall ((t Tree) (v Int))
	  (=> (searchtree t)
	      (ite (= t empty)
		   true
		   (ite (= v (val t))
			true
			(ite (< v (val t))
			     (let ((t (left t)))
			       (searchtree t))
			     (let ((t (right t)))
			       (searchtree t)))))))))

(check-sat)
