(set-logic AUFLIA)

(set-info
  :source |
Binary seach algorithm
======================

@pre: 0 ≤ l ∧ u < length(a) ∧ sorted(a, 0, length(a)-1)
@post: res ↔ ∃ i (l ≤ i ∧ i ≤ u ∧ a[i] = e)
def binSearch(a: Array[Int], l: Int, u: Int, e: Int) =
  if (l > u)
     false
  else {
     val m = (l + u) / 2
     if (a[m] == e)
        true
     else if (a[m] < e)
        binSearch(a, m+1, u, e)
     else 
        binSearch(a, l, m-1, e)
  }

Partial correctness:

Proof of postcondition by induction, assuming precondition:
∀ l:Int, u:Int, a:Array[Int], e:Int
  0 ≤ l ∧ u < length(a) ∧ sorted(a, 0, length(a)-1) →
  let res =
      if l > u then
        false
      else
        let m = (l + u) / 2 
         in if a[m] = e then
               true
            else if a[m] < e
               let res1 =         // res1 is result of recursive call, by I.H.
                   let l = m+1
                    in ∃ i:Int (l ≤ i ∧ i ≤ u ∧ a[i] = e) // I.H.
                in res1 // result res is just res1
            else
               let res1 =        // res1 is result of recursive call, by I.H.
                   let u = m-1
                    in ∃ i:Int (l ≤ i ∧ i ≤ u ∧ a[i] = e) // I.H.
                in res1 // result res is just res1
   in res ↔ ∃ i (l ≤ i ∧ i ≤ u ∧ a[i] = e) // prove postcondition

Proof precondition of induction hypotheses:
∀ l:Int, u:Int, a:Array[Int], e:Int
  0 ≤ l ∧ u < length(a) ∧ sorted(a, 0, length(a)-1) →
  if l > u then
    true
  else
    let m = (l + u) / 2 
     in if a[m] = e then
           true
        else if a[m] < e
           let l = m+1
            in 0 ≤ l ∧ u < length(a) ∧ sorted(a, 0, length(a)-1)
        else
           let u = m-1
            in 0 ≤ l ∧ u < length(a) ∧ sorted(a, 0, length(a)-1)
|)

;; translation of the above into concrete SMT-LIB syntax

;; division by 2
(declare-fun div2 (Int) Int)
(assert
 (forall ((a Int) (res Int))
	 (= (and (<= (* 2 res) a)
		 (> (* 2 (+ res 1)) a))
	    (= (div2 a) res)
	    )))
	 
(declare-fun length ((Array Int Int)) Int)
(assert
 (forall ((a (Array Int Int)))
	 (>= (length a) 0)))

;; whether the array a is sorted
(define-fun sorted ((a (Array Int Int)) (l Int) (u Int)) Bool
  (forall ((i Int) (j Int))
	  (=> (and (<= l i)
		   (< i j)
		   (<= j u))
	      (<= (select a i) (select a j)))))


;; second proof obligation
(assert (not
  (forall ((l Int) (u Int) (a (Array Int Int)) (e Int))
	  (=> 
	   (and (<= 0 l)
		(< u (length a))
		(sorted a 0 (- (length a) 1)))
	   (ite (> l u)
		true
		(let ((m (div2 (+ l u))))
		  (ite (= (select a m) e)
		       true
		       (ite (< (select a m) e)
			    (let ((l (+ m 1)))
			      (and (<= 0 l)
				   (< u (length a))
				   (sorted a 0 (- (length a) 1))))
			    (let ((u (- m 1)))
			      (and (<= 0 l)
				   (< u (length a))
				   (sorted a 0 (- (length a) 1))))))))))))

(check-sat)
;; (get-model)
