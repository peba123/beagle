TODO for IDX commit
===================
On branch idx_dev.
See files [indexing results](idx-sww-only-summary) and [default results](sww-only-baseline-summary).

Problem Files
=============

Results via IDX:
----------------
	Solved	: 153
	Error	: 3
	No Soln	: 18
	Timeout	: 49
	Unsound	: 0

Results baseline:
-----------------
	Solved	: 110
	Error	: 3
	No Soln	: 18
	Timeout	: 92
	Unsound	: 0

No unsoundness, so that's great...

Bug (SWW643=2):
---------------
These timeout when using indexing but are solved without indexing
* /home/jbax/TPTP-v6.1.0/Problems/SWW/SWW474_1.p, solved in 32s
* /home/jbax/TPTP-v6.1.0/Problems/SWW/SWW643=2.p, solved in 18s

474_1 done in 36.58 regular t/o after 55s
643 done in 18.2s regular

Subsumption disabled: done in 7s! (Same for 474_1: done in 22s)
Side by side comparison on 643 shows that for this clause

     ¬(length1(A_1468, X1_1470) -> -1) 

idx isSubsumed = false while norm isSubsumed = true

So idx is missing a matcher, for non-boolean `length1(A_1468, X1_1470)` which results in
missing a subsumption between BG-sorted unit clauses.

FIX: by adding units which have either an equation with BG sort or a BG predicate back to the second test.
TODO: could also manually test those using bgsubsume?

Next problem:

	tb2t1(t2tb1(#skF_12)) ->_Z #skF_14

idx isSubsumed = true while norm isSubsumed = false
Now idx subsumes too many clauses.

Problem:
a clause f(c) = d is abstracted to f(X) = d
this was incorrectly subsumed by f(Y) = Y because the matcher was constructed in 2 steps.
The first step matched f(Z) from the index to f(X) and the second step extended [Z -> X] to match
X to d by adding X -> d.
But of course this is not possible as the matcher can only apply to the first (subsuming) clause.

FIX
by checking that the matcher does not act on any of the subsumed clauses terms.
Note that it is not correct to use `Subst.actsOn` here because that is true for `x` where the substitution is `[x->x]`.
This behaviour is relied upon in `matchTo`.
So the fix was implemented by applying the substitution and checking whether the term is actually altered.
Otherwise, could define a custom `actsOn` method just for this section.

Bug (SWW598=2)
--------------
Timeout idx but reg solved in 13s
Seems to get stuck in Cooper, caused by positive unit simplification.

      mem(#skF_3, #skF_1) simplified to mem(#skF_3, #skF_1) by idx and $$true by normal

It appears that `mem(#skF_3, #skF_1)` is abstracted to `mem(X_15348ᵃ, #skF_1) v X_15348ᵃ != #skF_3`
Then it is simplified by demod with `mem(#skF_3, #skF_1) = T` followed by tautology deletion
FIX: call `IndexSimp.reduceBool` with `cl.unabstrCautious` instead (for Pos. case).

Final problem appeared to be inadequate filtering of tautological clauses before demodulation...
This fix created a bug, because unabstrAggressive is generally unsound so cannot be used for that kind of simplification.
Tautology checks are now done elsewhere.

Bug (NUM878=1.p)
-----------------
Missing subsumption.
Caused by uninitialised indexes (i.e. posUnitsOpt = None) note being forcibly initialised when adding a clause for the first time.
FIX: by using the gaurded accessor methods for indexes in ListClauseSet instead of operating on the Opt versions.

NLPP demodulation
-----------------
Bug (fixed in ce7e010): demodNLPP had a variable pattern match so *never* applied. Fixed by changing pattern.
This affected both -idx and regular versions.

However, tt seems that the following

   /home/jbax/TPTP-v6.1.0/Problems/SWW/SWW573=2.p
   /home/jbax/TPTP-v6.1.0/Problems/SWW/SWW574=2.p
   /home/jbax/TPTP-v6.1.0/Problems/SWW/SWW575=2.p
   
can be solved quickly by disabling NLPP demodulation entirely!
False! I think this was an artefact of a bug in -idx producing unsat clauses from nlpp clauses.

	  default (nlpp on) nlpp delay	 -x
SWW573=2  T  	  	    T	 	 T
SWW574=2  T		    T		 T
SWW575=2  16		    T		 T

(tested on a056cb3)
It appears that delayed NLPP has no effect on SWW573-5 

However it does help for SWW591, but it hinders SWW662 and SWW663.
Perhaps use this as a heuristic in -auto mode?

Bug: from NUM882
----------------
When indexing two terms which are variants up to abstraction variables, the insertion algorithm ignores the distinction between variable types:

DiscrTreeIndex:176 val i = sub.indexWhere(_.label == ts.head)

This is because VarOp subclasses do not define a specialised equals method and so inherit their method from the superclass, in which they are indistinguishable.
This leads to unsoundness since the matching algorithm DOES distinguish variable types, as does substitution application, so the result is
terms which still contain normalisation variables.

A quick fix would be to enforce proper equality on abstraction/general vars, but this will mean an increase in storage space...
TODO: It could be advantageous to ignore the difference between variables and only check after matching.

Commit a056cb3
---------------------
The following performed worse with the index:

* SWW591=2: Solved after 17s by restricting NLPP demod! Not solved by either idx or regular otherwise.
* SWW650=2: Solved after 3s (no -idx) by using strong BG taut simplification.
* SWW662=2: Solved in 3.5s by turning NLPP demod on (no -x or -idx)
* SWW663=2: Solved in 3.5s by turning NLPP demod on (no -x or -idx)
* SWW584=2: Only a few seconds worse, seems to be improved by -x

BG Taut. performance
====================

Enabled using -x. Use std. abstraction to get as much information as possible about a clause then check whether BG part is unsatisfiable
using Cooper. If so, delete clause.
For some problems this is a huge improvement, for others not so much.

Improves:
* SWW650=2
* SWW584=2
* SWW666=2
* SWW661=2

Worsens ?

From SWW666=2:

Lots similar to
     (I_2271ᵃ < L_115ᵃ) ∨ ¬(I_2271ᵃ < U_116ᵃ) ∨ (L_115ᵃ < U_116ᵃ)
     (6 < L_115ᵃ) ∨ ¬(6 < U_116ᵃ) ∨ (L_115ᵃ < U_116ᵃ)
     (7 < L_115ᵃ) ∨ ¬(7 < U_116ᵃ) ∨ (L_115ᵃ < U_116ᵃ)

Conjecture: It should not make a difference if pure BG clauses are deleted or not.
It appears that doing exempting that decreases BG taut checking time, increases Cooper time and slightly increases overall time (200ms...)
(On SWW666=2).

More interesting
*** ¬(B_3066 < X_3075) ∨ ¬(X_3075 ≈_ℤ (B_1195 + X_3067ᵃ)) ∨ (Y_3069ᵃ < X_3067ᵃ) ∨ ¬(B_3066 ≈_ℤ (B_1195 + Y_3069ᵃ)) simplified by BG taut. deletion
*** ¬(B_3066 < 0) ∨ (Y_3069ᵃ < X_3067ᵃ) ∨ ¬(B_3066 ≈_ℤ (N_1192 + Y_3069ᵃ)) ∨ ¬(X_3067ᵃ ≈_ℤ -N_1192) simplified by BG taut. deletion
Some with general vars

*** (1 < L_115ᵃ) ∨ ¬(1 < U_116ᵃ) ∨ (X_4672ᵃ < U_116ᵃ) ∨ (L_115ᵃ < U_116ᵃ) ∨ ¬(#skF_8(t2tb1(#skF_19), t2tb(3), int, M_114, L_115ᵃ, U_116ᵃ) →_ℤ X_4672ᵃ) simplified by BG taut. deletion
Some with a FG term

Changing to lazy val does not affect time for DAT073=1.

SWW598=2 ok for both -x and no -x
SWW591=2 unsolvable in any configuration
SWW661=2 great example... (needs mult_lemmas) -x with no std-abstract dies