Installation:

1) move the lemmas dir and beagle-0.9.1.jar to a directory of your liking.
2) export a shell variable $BEAGLEDIR pointing to that directory,
   or make a script for calling beagle that sets BEAGLEDIR. Something like:

#!/bin/sh
export BEAGLE="`dirname $0`"
export BEAGLEDIR="/path/to/dir/with/lemmas-directory"
# Can use this if lemmas are in $BEAGLE:
# export BEAGLEDIR="`dirname $0`"
exec java -Dfile.encoding=UTF-8 -Xms512M -Xmx4G -Xss10M -jar $BEAGLE/beagle-0.9.1.jar "$@"
#

   Beagle expects BEAGLEDIR to be set to the directory containin the
   lemmas directory.

3) For the TPTP "automatic mode" applications please feed in the -auto 
   and -t options in the above call before the "$@"

  -t Integer
  Timeout, in seconds. 0 means 7 days, default: 0

  -auto
  Turn on automatic strategy selection. If on, beagle first tries to find a proof within the timelimit T/2,
  where T is the Timeout in use, cf the -t flag. If the result is inconclusive after reaching this timelimit or before,
  beagle spends the remaining time on finding a proof with the -genvars and -chaining flags on.
  This flag is effective only when -genvars is not given at the command line.

The Beagle source is a at https://bitbucket.org/peba123/smttotptp/src/master/
