
lazy val apacheSimplex = Seq(
  libraryDependencies += "org.apache.commons" % "commons-math3" % "3.6.1"
)

//select sub-directories of base that are not src/test
def srcDirs(base: File) = ( (base / "src") * ("*" -- "test") ) filter { _.isDirectory }

lazy val root = (project in file(".")).
  settings(
    name := "beagle",
    version := "0.9.52",
    scalaVersion := "2.12.19",
    scalaSource in Compile := baseDirectory.value / "src",                           //SBT default is to have source files in src/main/scala
    unmanagedSourceDirectories in Compile := srcDirs(baseDirectory.value).get,
    unmanagedSources in Compile ++= ((baseDirectory.value / "src") * "*.scala").get, //manually add any *.scala source in src/
    unmanagedSourceDirectories in Test += baseDirectory.value / "src" / "test",      //re-add src/test directory when testing
    mainClass in (Compile, run) := Some("beagle.main"),
    retrieveManaged := true,                                                         //default is false
    // Version 2.12:
    // libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",
    libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "2.4.0",
    // assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    mappings in (Compile, packageDoc) := Seq(),
    assemblyJarName in assembly := s"beagle-${version.value}.jar",
    mainClass in assembly := Some("beagle.main"),
    scalacOptions ++= Seq(
      "-unchecked",
      "-feature",
      "-deprecation",
      "-language:implicitConversions",
      "-language:postfixOps",
    ),
  ).
  // settings(scalaCheck:_*).
  // settings(scalaTest:_*).
  settings(apacheSimplex:_*)

