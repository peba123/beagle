package beagle.datastructures

import beagle._
import util._
import calculus._
import fol._
import collection.mutable.ListBuffer
import collection.mutable.PriorityQueue
import indexing._

// import collection.mutable.{ Set => MSet }

/** Thrown if BG clauses are inconsistent.*/
case object BGClauseExtensionFail extends Exception

abstract class ClauseSet {
  type Collection <: Iterable[ConsClause]

  // Abstract members
  val clauses: Collection // All clauses in the clause set, use mutable collections
  /** Destructively add a clause to clauses */
  def addToClauses(cl: ConsClause): Unit 
  /** Shallow copy */
  def klone(): ClauseSet
  
  val maxWeight = if (flags.weightBound.value == 0) 20000 else flags.weightBound.value

  // Derived notions

  /** Also checks that no clauses heavier than maxWeight are added */
  def add(cl: ConsClause) {
    // if (cl.weight > maxWeight || (clauses exists { (_ ~ cl) })) return
    // if (cl.weight > maxWeight || (clauses exists { (_.lits == cl.lits) })) return
    if (cl.weight > maxWeight) return
    addToClauses(cl)
  }

  def add(cls: Iterable[ConsClause]) {
    cls foreach { add(_) }
  }

  def size = clauses.size
  def isEmpty = clauses.isEmpty

  // Can't use lazy val, because clauses is a mutable data structure
  def unitClauses: Iterable[ConsClause]

  def show() {
    clauses foreach { println(_) }
  }

}

/** Augments a ClauseSet with discrimination tree indexes for unit clauses.
  * 
  * This index store has mutable state although the indexing datastructures
  * are immutable.
  * Each of the accessor methods checks whether indexes have been initialised
  * otherwise initialises them using the current set of clauses.
  * It is up to the implementing class to ensure that the indexes are correctly
  * maintained by calling `indexUpdateOnInserted` and `indexUpdateOnRemoved` methods.
  * In addition, the `setIndexes` method can be used to seed the indexes of a new ClauseSet
  * instance with the indexes of an existing ClauseSet, particularly useful in the
  * `klone()` method.
  * 
  * Note that `indexUpdate*` methods do nothing if `util.flags.indexing` is not set. 
  * 
  * Also note that the indexes also store unit clauses produced by unabstraction of regular 
  * clauses.*/
trait UnitClauseIndex {

  val clauses: Iterable[ConsClause] //defined in the ClauseSet

  private var posUnitsOpt: Option[DiscrTreeIndex[ClInfo]] = None
  private var negUnitsOpt: Option[DiscrTreeIndex[ClInfo]] = None
  private var negBoolUnitsOpt: Option[DiscrTreeIndex[ClInfo]] = None
  private var posBoolUnitsOpt: Option[DiscrTreeIndex[ClInfo]] = None

  /** Indexes initialised by adding all relevant clauses from existing set of unit clauses */
  private def initIndexes {

    negBoolUnitsOpt = Option(DTree.empty[ClInfo])
    posBoolUnitsOpt = Option(DTree.empty[ClInfo])
    negUnitsOpt = Option(DTree.empty[ClInfo])
    posUnitsOpt = Option(DTree.empty[ClInfo])

    if (util.flags.indexing.value)
      clauses foreach { indexUpdateOnInserted(_) }
  }

  /** Drop current indexes, thereby forcing a rebuild next time they are accessed. */
  protected def invalidateIndexes: Unit = {
    negBoolUnitsOpt = None
    posBoolUnitsOpt = None
    negUnitsOpt = None
    posUnitsOpt = None
    //unitClausesOpt = None
  }

  protected def indexUpdateOnInserted(cl: ConsClause, nonUnit: Boolean = false): Unit =
    if (util.flags.indexing.value) {
      if (cl.isUnitClause) {
        if (!cl(0).isTrivial) {
          cl(0) match {
            case Lit(true, PredEqn(p)) =>
              posBoolUnitsOpt = Some(posBoolUnits.insert(p, ClInfo(TT, cl, nonUnit)))
            case Lit(false, PredEqn(p)) =>
              negBoolUnitsOpt = Some(negBoolUnits.insert(p, ClInfo(TT, cl, nonUnit)))
            case Lit(false, e @ Eqn(t1, t2)) => {
              negUnitsOpt = Some(negUnits.insert(t1, ClInfo(t2, cl, nonUnit)))
              if (!e.isOrdered)
                negUnitsOpt = Some(negUnits.insert(t2, ClInfo(t1, cl, nonUnit)))
            }
            case Lit(true, eqn @ Eqn(t1, t2)) => {
              //from makeEqnForDemod:
              // never use demodulators of the form X -> t
              // if unordered, use rhs -> lhs as a demodulator
              if (!eqn.isOrdered && !eqn.rhs.isVar) {
                posUnitsOpt = Some(posUnits.insert(t2, ClInfo(t1, cl, nonUnit)))
              }

              if (!eqn.lhs.isVar) { //uses lhs -> rhs
                posUnitsOpt = Some(posUnits.insert(t1, ClInfo(t2, cl, nonUnit)))
              }
            }
            case _ => ()
          }
        }
      } else if (cl.unabstrAggressive.isUnitClause) {
        val c = cl.unabstrAggressive
        //util.stats.idxNonUnit += 1
        indexUpdateOnInserted(c, true) //since c is a unit, this is well-founded.
      }
    }

  protected def indexUpdateOnRemoved(cl: ConsClause, nonUnit: Boolean = false): Unit =
    if (util.flags.indexing.value) {
      if (cl.isUnitClause) {
        cl(0) match {
          case Lit(true, PredEqn(p)) =>
            posBoolUnitsOpt = Some(posBoolUnits.deleteAll(p))
          case Lit(false, PredEqn(p)) =>
            negBoolUnitsOpt = Some(negBoolUnits.deleteAll(p))
          case Lit(false, e @ Eqn(t1,t2)) =>
            //correct?
            negUnitsOpt = Some(negUnits.delete(t1, ClInfo(t2, cl, nonUnit)))
            if (!e.isOrdered)
              negUnitsOpt = Some(negUnits.delete(t2, ClInfo(t1, cl, nonUnit)))
          case Lit(true, eqn @ Eqn(t1, t2)) => {
            //guarded by makeEqnForDemod:
            if (!eqn.isOrdered && !eqn.rhs.isVar) //res ::= (eqn.rhs, eqn.lhs, false)
              posUnitsOpt = Some(posUnits.delete(t2, ClInfo(t1, cl, nonUnit)))
            if (!eqn.lhs.isVar) //res ::= (eqn.lhs, eqn.rhs, eqn.isOrdered)
              posUnitsOpt = Some(posUnits.delete(t1, ClInfo(t2, cl, nonUnit)))
          }
          case _ => ()
        }
      } else if (cl.unabstrAggressive.isUnitClause) {
        val c = cl.unabstrAggressive
        indexUpdateOnRemoved(c, true) //since c is a unit, this is well-founded.
      }
    }

  def setIndexes(negBU: DiscrTreeIndex[ClInfo], posBU: DiscrTreeIndex[ClInfo], 
                   negU: DiscrTreeIndex[ClInfo], posU: DiscrTreeIndex[ClInfo]) {
    negBoolUnitsOpt = Option(negBU)
    posBoolUnitsOpt = Option(posBU)
    negUnitsOpt = Option(negU)
    posUnitsOpt = Option(posU)
  }

  def negBoolUnits = {
    negBoolUnitsOpt getOrElse {
      initIndexes
      negBoolUnitsOpt.get
    }
  }

  def posBoolUnits = {
    posBoolUnitsOpt getOrElse {
      initIndexes
      posBoolUnitsOpt.get
    }
  }

  def negUnits = {
    negUnitsOpt getOrElse {
      initIndexes
      negUnitsOpt.get
    }
  }

  def posUnits = {
    posUnitsOpt getOrElse {
      initIndexes
      posUnitsOpt.get
    }
  }

}

import PMI._

class ListClauseSet extends ClauseSet with PMI[ListClauseSet] with UnitClauseIndex {

  type Collection = ListBuffer[ConsClause]

  val clauses = ListBuffer.empty[ConsClause]

  def apply(i: Int) = clauses(i)

  private var termIndexOpt: Option[TermIndex] = None
  // // Mixin PMI
  def termIndex = {
    termIndexOpt getOrElse {
      // Have to build it from scratch
      termIndexOpt = Option(liftIndex(clauses.toList map { _.termIndex }))
      termIndexOpt.get
    }
  }

  private var unitClausesOpt: Option[ListBuffer[ConsClause]] = None

  def unitClauses = {
    unitClausesOpt getOrElse {
      unitClausesOpt = Option(clauses filter { _.isUnitClause })
      unitClausesOpt.get
    }
  }

  private def invalidate = {
    unitClausesOpt = None
    termIndexOpt = None
    invalidateIndexes
  }

  private def updateOnInserted(cl: ConsClause): Unit = {
    //reporter.debug(s"Inserted $cl")
    if (cl.isUnitClause) {
      unitClauses += cl // This works!
    }
    indexUpdateOnInserted(cl)
    //anything for PMI?
  }

  private def updateOnRemoved(cl: ConsClause): Unit = {
    //reporter.debug(s"Removed $cl")
    if (cl.isUnitClause) {
      unitClauses -= cl
    }
    indexUpdateOnRemoved(cl)
  }

  def replaceAt(pos: Pos, t: Term) = {
    val clPos :: inClPos = pos
    updateOnRemoved(clauses(clPos))

    // Destructive!
    clauses(clPos) = clauses(clPos).replaceAt(inClPos, t)
    updateOnInserted(clauses(clPos))

    termIndexOpt = None
    this
  }

  //ignored method in PMI:
  def get(pos: Pos) = ???

  def addToClauses(cl: ConsClause) {
    cl +=: clauses // This prepends cl to clauses
    // todo: could also append to clauses and re-use index
    termIndexOpt = None
    updateOnInserted(cl)
  }

  /** Tacitly assume clauses was interreduced before the last clause was added.
    * Hence can start with last clause added 
    * Nothing is done when the empty clause is generated. That is, 
    * the caller needs to check that. 
    */
  def interreduce() {
    termIndexOpt = None
    //unitClauses is maintained using indexUpdate methods

    if (clauses.isEmpty) return

    val open = ListBuffer(clauses.head) // The list of clauses to be added to clauses
    clauses.trimStart(1)
    while (!open.isEmpty) {
      val next = open.head // chose any
      open.trimStart(1)
      // reduce next using the clauses in closed
      next.reduce(new ImmutableListClauseSet(clauses)) match {
        case Nil => { // Clause 'next' is deleted
          updateOnRemoved(next)
        }
        case List(nextRed) => {
          // We reduce clauses by nextRed and put those that have been simplified
          // into open.
          val oldClauses = clauses.clone() // Rebuild clauses from a copy
          clauses.clear()
          val nextRedCls = new ImmutableListClauseSet(List(nextRed))
          for (cl <- oldClauses) {
            val (clRed, touched) = cl.reduceWithStatus(nextRedCls)
            clRed match {
              case Nil => { // cl is deleted
                updateOnRemoved(cl)
              }
              case List(clRed) => 
                if (touched) {
                  open += clRed 
                  updateOnRemoved(cl)
                } else {
                  // todo: Do we need test for variantship? No, reduce covers that
                  clauses += clRed
                  updateOnInserted(clRed)
                }
            }
          }
          // We need to put the reduced version of next into clauses
          clauses += nextRed
          updateOnInserted(nextRed)
        }
      }
    }
  }

  /** Add a clause `cl` to clauses, thereby keeping only those
    * that are not reducible by `cl` and returning the
    * reduced versions (by cl).
    */
  def addAndBackwardReduce(cl: ConsClause): List[ConsClause] = {
    val rest = clauses.clone() // The clauses to be reduced
    var res = List.empty[ConsClause] // The resulting clauses, those that are simplified
    // Go through rest and separate away the clauses that are reducible by last
    clauses.clear()
    clauses += cl
    updateOnInserted(cl)
    val clCls = new ImmutableListClauseSet(List(cl))
    for (into <- rest) {
	stats.simpBackRed.tried
        val (intoRed, touched) = into.reduceWithStatus(clCls)
        intoRed match {
          case Nil => updateOnRemoved(into) // deleted
          case _ =>
            if (touched) { //  cannot do that: && !intoRed.isPureBG
              res ++= intoRed
              //stats.nrBackRed += 1
              updateOnRemoved(into)
	      stats.simpBackRed.succeed
            }
            else {
              clauses ++= intoRed // not touched - can keep in clauses
              // Test for subsumption before insertion. Costly?
              /*
             val intoRedNonSubsumed =
                intoRed filter { intoRedCl =>
                  !(clauses exists { cl => (cl subsumes intoRedCl).nonEmpty })
                  // !(clauses exists { cl => (cl.lits == intoRedCl.lits) })
                }
              clauses ++= intoRedNonSubsumed // not touched - can keep in clauses
               */
              }
        }
    }
    termIndexOpt = None
    //unitClausesOpt = None
    res
  }

  /** Also copies indexes. */
  def klone() = {
    val h = clauses
    val res = new ListClauseSet {
      override val clauses = h.clone()
    }
    res.setIndexes(negBoolUnits, posBoolUnits, negUnits, posUnits)
    res
  }

  def removeLightest(): ConsClause = {
    require(!clauses.isEmpty, { println("removeLightest: clause set is empty") })
    // println("*** size "  + clauses.size)
    termIndexOpt = None
    var (best, iBest) = (clauses.head, 0)
    for (
      (tails, iTail) ← clauses.tail.tails zip Iterator.from(1);
      if !tails.isEmpty;
      next = tails.head
    ) if (next.isPureBG) {
      clauses.remove(iBest)
      updateOnRemoved(best)
      return best
    } else if (next.weight < best.weight) {
      best = next
      iBest = iTail
    }
    // println("*** iBest "  + iBest)
    clauses.remove(iBest)
    updateOnRemoved(best)

    // best.delete()
    return best
  }
}



object ListClauseSet {

  def apply(cls: ConsClause*) = {
    val cs = new ListClauseSet
    cls foreach { cs.addToClauses(_) }
    cs
  }

  def apply(cls: ListBuffer[ConsClause]) = new ListClauseSet {
    override val clauses = cls
  }
}


private class ImmutableListClauseSet(cls: Iterable[ConsClause]) extends ClauseSet {

  type Collection = List[ConsClause]

  val clauses = cls.toList

  def addToClauses(cl: ConsClause) {
    throw InternalError("ImmutableListClauseSet.addToClauses() should never be called")
  }

  def klone() = {
    throw InternalError("ImmutableListClauseSet.klone() should never be called")
  }

  lazy val unitClauses = clauses filter { _.isUnitClause }

}



/** Clause sets implemented as priority queues */
class PQClauseSet extends ClauseSet {

  object WeightOrdering extends scala.math.Ordering[ConsClause] {

/*    def priority(cl: ConsClause): Int = {
      // The more preferred the lower the result
      if (cl.isPureBG) 0 else 1
      // if (cl.isUnitClause && cl.lits(0).isFlat && !cl.lits(0).isNLPP) return 0
      // return 1
      // if (cl.isUnitClause) return 1
      // if (cl.isUnitClause && cl.isPureBG) return 1
      // if (cl.isPureBG) return 2
      // if (cl.isBG) return 2
      // if (cl.isFlat) return 3
    }
 */
    def compare(a: ConsClause, b: ConsClause) = {
      // val (au, bu) = (a.unabstrAggressive, b.unabstrAggressive)
      val (au, bu) = (a, b)
      // val (pa, pb) = (priority(a), priority(b))
      // if (pa < pb) 1
      // else if (pa > pb) -1
      // else
      // if (au.length < bu.length) 1
      // else if (bu.length < au.length) -1
      // else 
      // if (au.isPureBG && bu.isPureBG && au.length < bu.length) 1
      // else if (au.isPureBG && bu.isPureBG && bu.length < au.length) -1
      // else 
      // if (au.isPureBG && !bu.isPureBG) 1
      // else if (bu.isPureBG && !au.isPureBG) -1
      // else 
        if (au.weight < bu.weight) 1
      else if (bu.weight < au.weight) -1
      else if (au.age < bu.age) 1 // prefer older clauses
      else if (au.age > bu.age) -1
      else if (au.idxRelevant.size < bu.idxRelevant.size) 1
      else if (bu.idxRelevant.size < au.idxRelevant.size) -1
      else 0
/*
        if (a.weight < b.weight) 1
      else if (b.weight < a.weight) -1
      else if (a.age < b.age) 1 // prefer older clauses
      else if (a.age > b.age) -1
      else if (a.idxRelevant.size < b.idxRelevant.size) 1
      else if (b.idxRelevant.size < a.idxRelevant.size) -1
      else 0
 */
    }
  }

  type Collection = PriorityQueue[ConsClause]

  val clauses = PriorityQueue.empty[ConsClause](WeightOrdering)

  private var unitClausesOpt: Option[ListBuffer[ConsClause]] = None
  private def buildUnitClauses() {
    if (unitClausesOpt == None) {
      var cls = ListBuffer.empty[ConsClause]
      clauses foreach { cl =>
        if (cl.isUnitClause) cls += cl
      }
      unitClausesOpt = Some(cls)
    }
  }

  def unitClauses = {
    buildUnitClauses()
    unitClausesOpt.get
  }

  def addToClauses(cl: ConsClause) {
    clauses += cl
    if (cl.isUnitClause) {
      unitClauses += cl
    }
  }

  def klone() = {
    val h = clauses
    val res = new PQClauseSet {
      override val clauses = h.clone()
    }
    res
  }

  def removeLightest() = { 
    val res = clauses.dequeue()
    // res.delete()
    if (res.isUnitClause)
      unitClausesOpt = None
    // println(s"removeLightest() = $res")
    res
  }

  def removeOldest() = { 
  // Select an oldest clause, and among these a lightest one
    require(!clauses.isEmpty, "removeOldest: clause set is empty")
    
    var best = clauses.dequeue() 
    var kept = List.empty[ConsClause] // All clauses but the removed one  
    
    // We remove the clauses one by one, inspecting if we've got a better one,
    // thereby remembering those to be kept
    while (!clauses.isEmpty) {
      val next = clauses.dequeue()
      if (next.age < best.age ||
        (next.age == best.age && next.weight < best.weight)) {
        // found a better best: save the current best and make the new best the current best
        kept ::= best
        best = next
      } else 
        // best preserves, next is top be kept
        kept ::= next
    }
    // clauses is empty at this point, still need to push kept into clauses
    clauses ++= kept
    // best.delete()
    if (best.isUnitClause)
      unitClausesOpt = None
    // println(s"removeOldest() = $best")
    best
  }

  def removeGoaliest() = {
  // Select an oldest clause, and among these a lightest one
    require(!clauses.isEmpty, "removeGoaliest: clause set is empty")
    
    var best = clauses.dequeue() 
    var kept = List.empty[ConsClause] // All clauses but the removed one  
    
    // We remove the clauses one by one, inspecting if we've got a better one,
    // thereby remembering those to be kept
    while (!clauses.isEmpty) {
      val next = clauses.dequeue()
      if (// (next.nonBGLits exists { !_.isPositive }) &&
        (next.info.deltaConjecture < best.info.deltaConjecture ||
        (next.info.deltaConjecture == best.info.deltaConjecture && next.weight < best.weight))) {
        // found a better best: save the current best and make the new best the current best
        kept ::= best
        best = next
      } else 
        // best preserves, next is top be kept
        kept ::= next
    }
    // clauses is empty at this point, still need to push kept into clauses
    clauses ++= kept
    // best.delete()
    if (best.isUnitClause)
      unitClausesOpt = None
    // println(s"removeGoaliest() = $best")
    best
  }

  // Add a clause cl to clauses and perform full interreduction.
  // Assumes that cl is already cheaply reduced
  def addAndInterreduce(cl: ConsClause) {

    val open = ListBuffer(cl) // The list of clauses to be added to clauses
    while (!open.isEmpty) {
      val next = open.head // chose any
      open.trimStart(1)
      // reduce next using the clauses in closed
      next.reduce(new ImmutableListClauseSet(clauses)) match {
        case Nil => () // Clause is deleted
        case List(nextRed) => {
          // We reduce clauses by nextRed and put those that have been simplified
          // into open.
          val oldClauses = clauses.clone() // Rebuild clauses from a copy
          clauses.clear()
          val nextRedCls = new ImmutableListClauseSet(List(nextRed))
          for (cl <- oldClauses) {
            val (clRed, touched) = cl.reduceWithStatus(nextRedCls)
            clRed match {
              case Nil => () // deleted
              case List(clRed) => 
                if (touched) 
                  open += clRed 
                else
                  // todo: Do we need test for variantship? No, reduce covers that
                  clauses += clRed
            }
          }
          // We need to put the reduced version of next into clauses
          clauses += nextRed
        }
      }
    }
    unitClausesOpt = None
  }


  /**
    * Reduce all clauses by the given clause set
    */
  def reduce(newCl: ConsClause) {
    val newClCset = new ImmutableListClauseSet(List(newCl))
    val oldClauses = clauses.toList
    clauses.clear()
    oldClauses foreach {
      cl => {
        clauses ++=
        (cl.reduceWithStatus(newClCset) match {
          case (clRed, true) => {
            // println("*** Reduce with %s\n    %s\n    %s".format(newCl, cl, clRed))
            val h = clRed flatMap { _.simplifyCheap }
            if (h forall {
              c => (// (c.length < cl.length) || 
                (c.idxRelevant subsetOf cl.idxRelevant))
            }) h
            else
              List(cl)
          }
          case (_, false) => List(cl)
        })
      }
    }
    unitClausesOpt = None
  }

  def reduce(cls: ClauseSet, newCl: ConsClause) {
    val newClCset = new ImmutableListClauseSet(List(newCl))
    val oldClauses = clauses.toList
    clauses.clear()
    oldClauses foreach {
      cl => {
        clauses ++=
        (cl.reduceWithStatus(newClCset) match {
          case (clRed, true) => {
            // println("*** Reduce with %s\n    %s\n    %s".format(newCl, cl, clRed))
            // clRed flatMap { _.simplifyCheap }
            val h = clRed flatMap { _.reduce(cls) } flatMap { _.simplifyCheap }
            if (h forall {
              c => ((c.length < cl.length) && (c.idxRelevant subsetOf cl.idxRelevant))
            }) h
            else
              List(cl)
          }
          case (_, false) => List(cl)
        })
      }
    }
    unitClausesOpt = None
  }

}


