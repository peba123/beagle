package beagle.datastructures

import beagle._
import fol._
import calculus._
import datastructures._
import PMI._
import bgtheory.LIA.cooper._

/**
 * The class of literals as appearing in clauses.
 * Also Context literals (CLit). Todo: Separate out Context Literals into State,
 * which would be a more logical place.
 */

case class Lit(isPositive: Boolean, eqn: Eqn) extends Expression[Lit] with PMI[Lit] {

  /*
     * Mixin Expression
     */
  lazy val vars = eqn.vars
  def applySubst(sigma: Subst) = 
    if (sigma actsOn this) {
      Lit(isPositive, sigma(eqn))
      // No - need to apply consLit simplification more often, e.g. when constraint is ground
      // val res = Lit(isPositive, sigma(eqn))
      // if (consLit.isConsLit(this) && !sigma.isRenaming)
      //   consLit.simplify(res)
      // else res
    } else this

  def mgus(that: Lit) =
    if (isPositive == that.isPositive) eqn mgus that.eqn else List.empty
  def matchers(that: Lit, gammas: List[Subst]) =
    if (isPositive == that.isPositive) eqn.matchers(that.eqn, gammas) else List.empty

  lazy val depth = eqn.depth
  lazy val weight = eqn.weight
  lazy val sorts = eqn.sorts
  lazy val operators = eqn.operators
  lazy val kind = eqn.kind
  lazy val symConsts = eqn.symConsts

  /*
   * Mixin PMI
   */
  lazy val termIndex = eqn.termIndex

  def replaceAt(pos: Pos, t: Term) = Lit(isPositive, eqn.replaceAt(pos, t))

  def get(pos: Pos) =
    if (pos.isEmpty) this
    else eqn.get(pos)

  override def toString = util.printer.litToString(this) // + " === " + subTermsWithPos
    /*eqn match {
      case PredEqn(equ) ⇒ (if (isPositive) "" else "¬") + eqn.toString // Works also for PseudoVar and Assert  
      case _ ⇒ (if (isPositive) eqn.toString else "¬(" + eqn.toString + ")")
    }*/

  // Complement
  lazy val compl = Lit(!isPositive, eqn)
  lazy val toPositive = if (isPositive) this else compl

  lazy val isTrivialNeg = (!isPositive) && eqn.isTrivial
  lazy val isTrivialPos =
    (isPositive && eqn.isTrivial) ||
      // such cases should better be detected elsewhere:
      (!isPositive && ((eqn.lhs, eqn.rhs) match {
        case (d1: DomElem[_], d2: DomElem[_]) ⇒ d1.value != d2.value
        case _ ⇒ false
      }))

  def isPredLit = eqn.isPredEqn

  def isFlat = eqn.isFlat
  def isTrivial = eqn.isTrivial
  lazy val isNLPP = eqn.isNLPP

  lazy val minBSFGTerms = eqn.minBSFGTerms
  lazy val maxBSFGTerms = eqn.maxBSFGTerms

  /** i.e. to a Formula literal */
  lazy val toLiteral = if (isPositive) eqn.toAtom else Neg(eqn.toAtom)

  def sort = eqn.sort
  // def isPureBG = eqn.isPureBG

  /**
   * Apply a function to the terms of the equation and produce a new Lit,
   * preserving the sign.
   * Lifts Eqn.map
   */
  def map(f: Term => Term): Lit = Lit(isPositive, eqn.map(f))

  lazy val toMultiSet = if (isPositive) List(eqn.lhs, eqn.rhs) else List(eqn.lhs, eqn.lhs, eqn.rhs, eqn.rhs)

  import fol.Ordering._

  // def compare(that: Lit): OrderingResult = {

  //   // Body of Compare
  //   if (this.eqn == that.eqn) {
  //     // recall that two equations are equal modulo symmetry iff they are equal, as a property
  //     // of determining lhs and rhs canonically
  //     (this.isPositive, that.isPositive) match {
  //       case (true, false) ⇒ Less
  //       case (false, true) ⇒ Greater
  //       case (_, _) ⇒ Equal // same sign, same atoms
  //     }
  //   }
  //   else if (mso_gtr(this.toMultiSet, that.toMultiSet))
  //     Greater
  //   else if (mso_gtr(that.toMultiSet, this.toMultiSet))
  //     Less
  //   else
  //     Unknown
  // }

  // Use cases
  def gtr(that: Lit) =
    if (this.eqn == that.eqn)
      // recall that two equations are equal modulo symmetry iff they are equal, as a property
      // of determining lhs and rhs canonically
      // Need to look at the sign, negative literals are bigger
      (!this.isPositive) && that.isPositive
    else
      // this and that cannot be equal, hence we can use the geq for multisets
      mso_geq(this.toMultiSet, that.toMultiSet)

  // Equality works because of canonical form for equations
  def geq(that: Lit) = {
    val res = (this == that) || (this gtr that)
    // println(s"geq($this, $that) = $res")
    res
  }

  // lazy val toTerm = PFunTerm(Signature.litOp, List(if (isPositive) TT else FF, EqnToTerm(eqn)))
  lazy val toTerm = LitToTerm(this)

  import bgtheory.LIA._

  // The function h is meaningfully applied to the lhs and rhs of an inequation.
  // It moves their monomials around so that only positive signs and addition (not subtraction) results.
  // Could also do ZeroLTPoly and then separate the polynomial in that
  def hLT(lhs: Term, rhs: Term) = {
    val (l, r) = (lhs.asPolynomial, rhs.asPolynomial)
    val h = (r - l) match {
      // Try to factorize as much as possible
      case p @ Polynomial(_, 0, _) => p.factorize
      case p @ Polynomial(_, 1, _) => (p - 1).factorize + 1
      case p @ Polynomial(_, -1, _) => (p - 1).factorize + 1
      case p @ Polynomial(_, k, _) => {
        // try both:
        val h1 = p.factorize
        val h2 = (p - 1).factorize + 1
        var best = p
        if (math.abs(h1.k) < math.abs(best.k)) best = h1
        if (math.abs(h2.k) < math.abs(best.k)) best = h2
        best
      }
    }
    val (lnew, rnew) = h.separate
    (lnew.toTerm, rnew.toTerm)
  }

/*  def hLE(lhs: Term, rhs: Term) = {
    val (l, r) = (lhs.asPolynomial.get, rhs.asPolynomial.get)
    val h = (r - l) match {
        case p @ Polynomial(List(Monomial(c,x)), k, Nil) => {
          val d = math.abs(c)
          if (d > 1) Polynomial(List(Monomial(c/d,x)), (k+1)/d, Nil) else p
        }
        case p @ Polynomial(_, -1, _) => (p + 1).factorize - 1
        case p => p.factorize
      }
    val (lnew, rnew) = h.separate
    (lnew.toTerm, rnew.toTerm)
    // val h = (r - l)
    // ((Polynomial.Zero - h.k).toTerm, (h - h.k).toTerm)
  }
 */

  // Canonical normalizes inequations and equations over Int.
  // Seems to be a better alternative to applying the simplification rules in LIASolver.
  // Strategy is to rewrite everything into <-literals, thereby flipping the sign of ≤-literals
  lazy val canonical: Lit = this match { 
    // Calls elimLinearMult explictly everywhere except for positive equational literals.
    // Cannot do positive equational literals because otherwise a (needed) positive unit clause
    // #nmpp(4711, X) ↦ 4711 · X
    // would be canonicalized into 4711 · X ↦ 4711 · X and hence loose its purpose.

    // Good one:
    case Lit(isPositive, LessEqn(s, t)) ⇒ {
      hLT(s, t) match {
        case (d1:DomElemInt, d2:DomElemInt) =>
          if ((d1.value < d2.value) == isPositive) Lit.TrueLit else Lit.FalseLit
        case (s1, t1) => Lit(isPositive, LessEqn(s1, t1))
      }
    }

    // case Lit(isPositive, LessEqn(s, t)) ⇒ 
    //     Lit(!isPositive, LessEqEqn(t, s)).canonical

    // case Lit(isPositive, LessEqEqn(s, t)) ⇒ {
    //   hLE(s, t) match {
    //     case (d1:DomElemInt, d2:DomElemInt) =>
    //       if ((d1.value <= d2.value) == isPositive) Lit.TrueLit else Lit.FalseLit
    //     case (s1, t1) => Lit(isPositive, LessEqEqn(s1, t1))
    //   }
    // }

    case Lit(isPositive, LessEqEqn(s, t)) ⇒ Lit(!isPositive, LessEqn(t, s)).canonical

      // Very good:
    // case Lit(isPositive, LessEqEqn(s, t)) ⇒ {
    //   hLE(s, t) match {
    //     case (d1:DomElemInt, d2:DomElemInt) =>
    //       if ((d1.value <= d2.value) == isPositive) Lit.TrueLit else Lit.FalseLit
    //     case (s1, t1) => Lit(!isPositive, LessEqn(t1, s1)).canonical
    //       // Preserve sign:
    //     // case (s1, t1) => Lit(isPositive, LessEqn(s1, Sum(OneInt, t1))).canonical
    //   }
    // }


    // case Lit(false, LessEqn(s, t)) ⇒ {
    //   h(s, t) match {
    //     case (d1:DomElemInt, d2:DomElemInt) => 
    //       // Can evaluate
    //       if (!(d1.value < d2.value)) Lit.TrueLit else Lit.FalseLit
    //     case (s1, t1) => Lit(false, LessEqn(s1, t1))
    //   }
    // }
    // case Lit(true, LessEqn(s, t)) ⇒ 
    //   Lit(false, LessEqEqn(t, s)).canonical


   // Good ones
/*    case Lit(false, LessEqEqn(s, t)) ⇒ {
      hLE(s, t) match {
        case (d1:DomElemInt, d2:DomElemInt) => 
          // Can evaluate
          if (!(d1.value <= d2.value)) Lit.TrueLit else Lit.FalseLit
        case (s1, t1) => Lit(false, LessEqEqn(s1, t1))
      }
    }
    case Lit(true, LessEqEqn(s, t)) ⇒ 
      Lit(false, LessEqEqn(Sum(t, OneInt), s)).canonical
 */

    case Lit(isPositive, GreaterEqn(t, s)) ⇒
      Lit(isPositive, LessEqn(s, t)).canonical
    case Lit(isPositive, GreaterEqEqn(t, s)) ⇒
      Lit(isPositive, LessEqEqn(s, t)).canonical

    case Lit(true, Eqn(s, t)) if s.sort == IntSort ⇒
      ZeroEQPoly(t.asPolynomial - s.asPolynomial).toLit match {
        case Lit(true, Eqn(d1: DomElemInt, d2: DomElemInt)) ⇒
          if (d1.value == d2.value) Lit.TrueLit else Lit.FalseLit
        case l => l
      }
    case Lit(false, Eqn(s, t)) if s.sort == IntSort ⇒ 
      ZeroNEPoly(t.asPolynomial - s.asPolynomial).toLit match {
        case Lit(false, Eqn(d1: DomElemInt, d2: DomElemInt)) ⇒
          if (d1.value != d2.value) Lit.TrueLit else Lit.FalseLit
        case l => l
      }
    case _ =>
      // Can still canonicalize the equation
      Lit(isPositive, Eqn(eqn.lhs.canonical, eqn.rhs.canonical))
  }

  /** This, simplified wrt to the BG theory. */
  def simplifyBG = { 
    // println(this)
    if (util.flags.bgsimp.value >= 2) {
      if (main.inPreprocessing)
        // we do a little more, to get rid off all these is_int, to_int etc
        bgtheory.solver.simplify(canonical)
      else if (sorts(IntSort)) {
        // This is in simplifyCheap now:
        // val res = bgtheory.solver.simplifyCheap(canonical)
        // if (canonical != this) println(s"xxx $this.canonical = $canonical")
        canonical
        // res
      // not neccessary - covered above by canonical
      // eqn.lhs match {
      //   case f:FunTerm if (LIAOperators contains f.op) => canonical
      //   case t if t.sort == bgtheory.LIA.IntSort => canonical
      //   case _ => Lit(isPositive, Eqn(eqn.lhs.canonical, eqn.rhs.canonical))
      // }
      } else 
        bgtheory.solver.simplify(this)
    }
    else bgtheory.solver.simplify(this)
  }

  def standardizeVariables(nextUnused: Int, soFar: Subst) = {
    val (eqnStandardized, nu, sf) = eqn.standardizeVariables(nextUnused, soFar)
    (Lit(isPositive, eqnStandardized), nu, sf)
  }

  lazy val toTPOT = me.TPOT(toTerm.preorder)


}


object Lit {

  val TrueLit = Lit(true, PredEqn(TT))
  val FalseLit = Lit(false, PredEqn(TT))

}


// Construct a term from a literal, and
// Deconstruct a term as a literal
object LitToTerm {
  def unapply(t: Term) = t match {
    case PFunTerm(Signature.litOp, List(sign, EqnToTerm(e))) => Some(Lit(sign == TT, e))
    case _ => None
  }
  def apply(l: Lit) = PFunTerm(Signature.litOp, List(if (l.isPositive) TT else FF, EqnToTerm(l.eqn)))
}

