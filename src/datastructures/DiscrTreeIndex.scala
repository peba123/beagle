package beagle.datastructures.indexing

import beagle.fol._
import beagle.util.{stats, Timer}
import beagle.datastructures.{ConsClause, PMI}

/** Indicator vars are used in all terms inside the index. */
private sealed trait IndicatorVar
private class GenIndVar(i: Int, s: Sort) extends GenVar("*", i, s) with IndicatorVar
private class AbstIndVar(i: Int, s: Sort) extends AbstVar("*", i, s) with IndicatorVar

/** Subclasses of info store extra information about terms in the index. */
trait Info {
  /** Apply a normalising subst `s` to any terms in the info item.
    * Usually just applies to the RHS of the equation being indexed. */
  def normalise(s: Subst): Info
}

/** Used for testing */
case object EmptyInfo extends Info {
  def normalise(s: Subst) = this
}

/** Represents information associated with a Boolean unit clause.
  * @deprecated Boolean simplification also needs info stored in ClInfo*/
case class UnitInfo(idxRelevant: Set[Int], age: Int) extends Info {
  def normalise(s: Subst) = this
}

/** @param byUnabstr true if this clause was produced by unabstraction of a non-unit. */
case class ClInfo(rhs: Term, unit: ConsClause, byUnabstr: Boolean = false) extends Info {
  def normalise(s: Subst): ClInfo = ClInfo(s(rhs), unit)
}

case class MatchResult[A](mu: Subst, matchTerm: Term, data: A)

/** Node in a discrimination tree index. Implements a gradual matching lookup on indexed terms. 
  * All queries are evaluated over pre-order traversals of terms which have sort `List[Operator]`.
  * It is assumed that variables in terms are normalised before insertion. */
case class DTreeNode[A](
    label: Operator, 
    sub: List[DTreeNode[A]], 
    leafTerm: Option[Term] = None, 
    leafData: List[A] = Nil) {

  //invariant: only leaves have non-empty leafTerm and leafData fields
  // i.e. if sub is non empty then leafTerm and leafData are empty.

  //invariant 2: leaves which contain no data are not retained
  // i.e. if sub is empty then leafTerm and leafData are non-empty.

  def isLeaf: Boolean = sub.isEmpty

  /** Similar to getAllMatchers except that it return the first indexed term which satisfies `filter`.*/
  def getMatcher(ts: List[Term], mu: Map[Var,Term], filter: (Subst, Term, A) => Boolean): Option[MatchResult[A]] = {
    //println(s"getMatcher $ts $mu $this")
    if (ts.isEmpty) {
      assert(this.isLeaf, "Expected matching to finish at a leaf")
      //otherwise...
      val newMu = new Subst(mu)
      //leafData.flatMap(filter(new Subst(mu), leafTerm.get, _))
      return leafData.find(filter(newMu, leafTerm.get, _)) map { MatchResult(newMu, leafTerm.get, _) }
    } else if (label.sort() != ts.head.sort) {
      return None
    } else if (label.isInstanceOf[DTree.VarOp]) {
      val nextMu = 
        (label.asInstanceOf[DTree.VarOp].asVar, ts.head) match {
          case (x: Var, t: Term) if (mu.isDefinedAt(x)) =>
            if (mu(x) == t) mu 
            else return None
          case (x: AbstVar, t: Term) if (t.isPureBG) => mu + (x -> t)
	  case (x: GenVar, t: Term)                  => mu + (x -> t)
          case _ => return None
        }
      //need to continue with tail...
      if (sub.isEmpty) {
        //leafData.flatMap(filter(new Subst(nextMu), leafTerm.get, _))
        val newMu = new Subst(nextMu)
        return leafData.find(filter(newMu, leafTerm.get, _)) map { MatchResult(newMu, leafTerm.get, _) }
      } else
        sub.toStream.map(_.getMatcher(ts.tail, nextMu, filter)).find(_.nonEmpty).flatten
    } else {
      ts.head match {
        case FunTerm(g, g_args) if (label == g) =>
          if (sub.isEmpty) {
            //leafData.flatMap(filter(new Subst(mu), leafTerm.get, _))
            val newMu = new Subst(mu)
            return leafData.find(filter(newMu, leafTerm.get, _)) map { MatchResult(newMu, leafTerm.get, _) }
          } else
            sub.toStream.map(_.getMatcher(g_args ::: ts.tail, mu, filter)).find(_.nonEmpty).flatten
        case _ => None
      }
    }
  }

  /** @param ts The current state of the term being matched. Subterms of the head are always explored
    * first so that calling this on `List(t)` traverses `t` in pre-order.
    * @param mu The accumulated matcher.
    * @return All indexed equations whose maximal term matches `ts` after application of `mu`*/
  def getAllMatchers(ts: List[Term], mu: Map[Var,Term]): List[MatchResult[A]] = {
    //debug only
    //println(s"getAllMAtchers ts=$ts mu=$mu where node.label=$label : ${label.getClass}")
    // if (leafTerm.nonEmpty) println(s"leaf t=$leafTerm info=$leafData")

    if (ts.isEmpty) {
      assert(this.isLeaf, "Expected matching to finish at a leaf")
      //return the found matcher along with all of the info terms
      return leafData.map(MatchResult(new Subst(mu), leafTerm.get, _))
    } else if (label.sort() != ts.head.sort) {
      //cannot match
      return Nil
    } else if (label.isInstanceOf[DTree.VarOp]) {
      //variable case
      val nextMu =
        (label.asInstanceOf[DTree.VarOp].asVar, ts.head) match {
          case (x: Var, t: Term) if (mu.isDefinedAt(x)) =>
            if (mu(x) == t) mu
            else return Nil
          case (x: AbstVar, t: Term) if (t.isPureBG) => {
            // println(s"binding abst var $x to $t")
            mu + (x -> t)
            }
	  case (x: GenVar, t: Term)                  => {
            // println(s"binding gen var $x to $t")
            mu + (x -> t)
          }
          case _ => return Nil
        }
      // println(s"nextMu=$nextMu")
      //need to continue with tail...
      if (sub.isEmpty)
        leafData.map(MatchResult(new Subst(nextMu), leafTerm.get, _))
      else
        sub.foldLeft(List[MatchResult[A]]())(_ ++ _.getAllMatchers(ts.tail, nextMu))
    } else {
      //non-variable case
      ts.head match {
        case FunTerm(g, g_args) if (label == g) =>
          if (sub.isEmpty)
            leafData.map(MatchResult(new Subst(mu), leafTerm.get, _))
          else
            sub.foldLeft(List[MatchResult[A]]())(_ ++ _.getAllMatchers(g_args ::: ts.tail, mu))
        case _ => Nil
      }
    }
  }

  def contains(ops: List[Operator], info: A): Boolean = ops match {
    case y :: Nil if (label == y) => 
      leafData.contains(info)
    case y :: ops2 if (label == y) =>
      sub.exists(_.contains(ops2, info))
    case _ => 
      false
  }

  /** @return The set of bindings (term -> info) indexed by the tree rooted at this node */
  def flatten: Set[(Term, A)] = {
    if (sub.isEmpty)
      leafData.map((leafTerm.get, _)).toSet
    else
      sub.map(_.flatten).reduce(_ ++ _)
  }

  /** @return The number of Info objects stored at the leaves of the 
    * tree rooted at this node */
  def countLeaves: Int =
    if (sub.isEmpty) leafData.length else sub.map(_.countLeaves).sum

  /** Insert `st` as a subtree in a new DTreeNode. */
  def appendSubTree(st: DTreeNode[A]): DTreeNode[A] = 
    DTreeNode(label, st :: sub)

  /** Replace the ith subtree with `st`.
    * Used to insert a leaf in an existing subtree. */
  def insertSubTree(st: DTreeNode[A], i: Int): DTreeNode[A] = 
    DTreeNode(label, sub.patch(i, st :: Nil, 1))

  /** Insert a new term, as a normalised pre-order traversal ts into the index.
    * @param leafTerm the original term being index
    * @param info the data to be stored against the term */
  def insert(ts: List[Operator], leafTerm: Term, info: A): DTreeNode[A] = {
    if (ts.isEmpty) {
      assert(this.isLeaf, "Expected insertion to finish at a leaf")
      //two versions here: extend or overwrite
      DTreeNode(label, sub, Option(leafTerm), info :: leafData)
    } else {
      val i = sub.indexWhere(_.label == ts.head)
      if (i < 0)
        appendSubTree(DTreeNode(ts.head, List()).insert(ts.tail, leafTerm, info))
      else
        insertSubTree(sub(i).insert(ts.tail, leafTerm, info), i)
    }
  }

  def delete(ts: List[Operator], info: A): Option[DTreeNode[A]] = ts match {
    case y :: Nil if (label == y && sub.isEmpty) => {
      leafData.filterNot(_ == info) match {
        case Nil     => None
        case newData => Option(DTreeNode(label, sub, leafTerm, newData))
      }
    }
    case y :: ops if (label == y) =>
      sub.flatMap(_.delete(ops, info)) match {
        case Nil    => None
        case newSub => Option(DTreeNode(label, newSub, leafTerm, leafData))
      }
    case _ => Option(this)
  }

  def deleteAll(ts: List[Operator]): Option[DTreeNode[A]] = ts match {
    case y :: Nil if (label == y && sub.isEmpty) => None
    case y :: ops if (label == y) =>
      sub.flatMap(_.deleteAll(ops)) match {
        case Nil    => None
        case newSub => Option(DTreeNode(label, newSub, leafTerm, leafData))
      }
    case _ => Option(this)
  }

}

object DTree {

  /** Variables are represented as operators because index requires a common supertype.*/
  sealed trait VarOp { val asVar: Var }
  class GenVarOp(i: Int, s: Sort) extends Operator(FG, "*" + i, Arity0(s)) with VarOp {
    val asVar = GenVar("*", i, s)
    override def equals(that: Any) = {
      super.equals(that) && that.isInstanceOf[GenVarOp]
    }
  }
  class AbstVarOp(i: Int, s: Sort) extends Operator(FG, "*" + i, Arity0(s)) with VarOp {
    val asVar = AbstVar("*", i, s)
    override def equals(that: Any) = {
      super.equals(that) && that.isInstanceOf[AbstVarOp]
    }
  }

  def empty[A <: Info] = new DiscrTreeIndex[A]()

  /** Create a new DTree containing a single term.
    * Essentially map the preorder traversal to a new tree.
    * Assumes that `t` is normalised.
    */
  def apply[A](t: Term, info: A): DTreeNode[A] = {
    toVarOpPreorder(t).foldRight(List[DTreeNode[A]]())({
      case (op, acc) if (acc.isEmpty)       => List(DTreeNode(op, acc, Option(t), info :: Nil))
      case (op, acc)                        => List(DTreeNode(op, acc))
    }).head
  }

  /** Normalise a term using indicator variables.
    * This replaces variables with indicators *_i where variables are numbered by first 
    * occurrence in a prefix traversal of the term tree.
    * 
    * E.g. f(X,Y,X) will go to f(*_1, *_2, *_1)
    */
  def normalise[T <: Term, S <: Info](t: T, info: S): (T, S) = {
    //take the set of first occurrences of variables in t
    val renaming = 
      Term.allSubtermsWithPos(t).filter(_._1.isVar)
	.toList
	//sort the positions by lex order
	.sortWith((x,y) => PMI.lexGtr(y._2,x._2))
	//then collect first occurrences
	.foldLeft(List[Var]())( (acc,v) => if (!acc.contains(v._1)) acc :+ v._1.asInstanceOf[Var] else acc )
	.zipWithIndex
	.map({
	  case (x: GenVar,i) => (x,new GenIndVar(i,x.sort))
	  case (x: AbstVar,i) => (x,new AbstIndVar(i,x.sort))
	})
	.toMap[Var,Term]
    
    //build a substitution to fresh indicator variables
    ((new Subst(renaming))(t).asInstanceOf[T], //ok to cast because substitution is a renaming
      info.normalise(new Subst(renaming)).asInstanceOf[S])
  }

  def normalise[T <: Term](t: T): T =
    normalise(t, UnitInfo(Set(), 0))._1

  /** Expects that `t` has already been normalised to use only Indicator vars*/
  def toVarOpPreorder[T <: Term](t: T): List[Operator] = t.preorder collect {
    case x: GenIndVar  => new GenVarOp(x.index, x.sort)
    case x: AbstIndVar => new AbstVarOp(x.index, x.sort)
    case op: Operator  => op
  }

}

/** Simple index for variables. Duplicate entries are not recorded.
  * Since variables are normalised to indicators, this only stores types. */
class VarIndex[A](val genVars: Map[Sort, Set[A]] = Map[Sort, Set[A]](), 
                          val abstVars: Map[Sort, Set[A]] = Map[Sort, Set[A]]()) {

  //invariant: for every s, genVars(s) is not empty and abstVars(s) is not empty

  private lazy val genIndVars = genVars map { case (s, _) => (s -> new GenIndVar(1, s)) }
  private lazy val abstIndVars = abstVars map { case (s, _) => (s -> new AbstIndVar(1, s)) }

  val isEmpty: Boolean = genVars.isEmpty && abstVars.isEmpty

  /** The number of unique records of the form (x -> info) stored */
  def size: Int = genVars.values.map(_.size).sum + abstVars.values.map(_.size).sum

  /** Whether the index contains the specific binding (x -> info) */
  def contains(x: Var, info: A): Boolean = x match {
    case GenVar(_, _, sort) => genVars.isDefinedAt(sort) && genVars(sort)(info)
    case AbstVar(_, _, sort) => abstVars.isDefinedAt(sort) && abstVars(sort)(info)
  }

  def insert(x: Var, info: A) = 
    if (x.isAbstVar) {
      new VarIndex(
        genVars,
        abstVars + (x.sort -> (abstVars.getOrElse(x.sort, Set()) + info))
      )
    } else {
      new VarIndex(
        genVars + (x.sort -> (genVars.getOrElse(x.sort, Set()) + info)),
        abstVars
      )
    }

  def delete(x: Var, info: A) = 
    if (x.isAbstVar && abstVars.isDefinedAt(x.sort)) {
      val newSet = abstVars(x.sort) - info
      val newVars = if (newSet.isEmpty) abstVars - x.sort else abstVars + (x.sort -> newSet)
      new VarIndex(genVars, newVars)
    } else if (genVars.isDefinedAt(x.sort)) {
      val newSet = genVars(x.sort) - info
      val newVars = if (newSet.isEmpty) genVars - x.sort else genVars + (x.sort -> newSet)
      new VarIndex(newVars, abstVars)
    } else {
      this
    }

  //TODO- this applies its filter to all results anyway...
  def findFirstMatchFilter(
      t: Term, 
      filter: (Subst, Term, A) => Boolean
    ): Option[MatchResult[A]] = {
    if (t.isPureBG && abstVars.isDefinedAt(t.sort)) {
      val xMap = abstIndVars(t.sort)
      val subs = Subst(xMap -> t)
      val res1 = abstVars(t.sort).find(filter(subs, xMap, _)) map { MatchResult(subs, xMap, _) }
      if (res1.nonEmpty) return res1
    }

    if (genVars.isDefinedAt(t.sort)) {
      val xMap = genIndVars(t.sort)
      val subs = Subst(xMap -> t)
      val res2 = genVars(t.sort).find(filter(subs, xMap, _)) map { MatchResult(subs, xMap, _) }
      if (res2.nonEmpty) return res2
    }

    return None //nothing applies
  }

  def findAllMatching(t: Term): List[MatchResult[A]] = {
    (if (t.isPureBG && abstVars.isDefinedAt(t.sort)) {
      val xMap = abstIndVars(t.sort)
      val subs = Subst(xMap -> t)
      abstVars(t.sort).map(MatchResult(subs, xMap, _)).toList
    } else 
      Nil) ++ (if (genVars.isDefinedAt(t.sort)) {
      val xMap = genIndVars(t.sort)
      val subs = Subst(xMap -> t)
      genVars(t.sort).map(MatchResult(subs, xMap, _)).toList
      } else
        Nil)
  }

}

/** Immutable index that builds a tree from pre-order traversal of terms.
  * At the leaves it stores a set of Info objects that typically correspond to 
  * RHS terms of unit clauses that have the indexed term on the LHS.
  * More generally it is assumed that only maximal terms are indexed.
  * Internally, variables are renamed to placeholder variables.
  * Typically a client will only use the insert(), delete() and find*() methods,
  * most other methods are only used for sanity checks. */
class DiscrTreeIndex[A <: Info](
  root: Map[Operator, DTreeNode[A]] = Map[Operator, DTreeNode[A]](),
  varIdx: VarIndex[A] = new VarIndex[A]()) {

  override def toString = {
    val opCounts = root.mapValues(_.countLeaves)
    s"Indexing ${opCounts.values.sum} terms, ${varIdx.size} vars\n"+
    "Indexed Operators:\n"+
    opCounts.map(kv => kv._1+" : "+kv._2).mkString("\n")+"\n"+
    "Indexed Vars: "+
    "Gen:" + varIdx.genVars.mkString("{",",","}")+", "+
    "Abst:" + varIdx.abstVars.mkString("{",",","}")

    // "Gen:" + varIdx.genVars.keySet.mkString("{",",","}")+", "+
    // "Abst:" + varIdx.abstVars.keySet.mkString("{",",","}")
  }

  /** The number of unique records of the form (t -> info) stored */
  def size = root.values.map(_.countLeaves).sum + varIdx.size

  def isEmpty = root.isEmpty && varIdx.isEmpty

  /** A pointer to term and the info is stored at the leaf of this insertion */
  def insert(t: Term, info: A): DiscrTreeIndex[A] = {
    stats.idxInsertions += 1

    Timer.idx_insert.measure { DTree.normalise(t, info) match {
      case (x: Var, inf) => 
        new DiscrTreeIndex(root, varIdx.insert(x, inf))
      case (s @ FunTerm(op, args), inf) if (root.isDefinedAt(op)) => {
        // println(s"*# inserted $t into existing tree with $info")
        new DiscrTreeIndex(root + (op -> root(op).insert(DTree.toVarOpPreorder(s).tail, s, inf)), varIdx)
      }
      case (s @ FunTerm(op, args), inf) => {
        // println(s"*# inserted $t into new tree with $info")
        new DiscrTreeIndex(root + (op -> DTree(s, inf)), varIdx)
      }
    } }
  }

  /** Delete all records held at the leaf of the matching term. 
    * Only used when indexing boolean units
    */
  def deleteAll(t: Term): DiscrTreeIndex[A] = Timer.idx_delete.measure { DTree.normalise(t) match {
    case x: Var =>
      throw new InternalError("delete: variables never occur in top-level index")
    case s @ FunTerm(op, _) => {
      if (root.isDefinedAt(op)) {
        stats.idxDeletions += 1
        root(op).deleteAll(DTree.toVarOpPreorder(s)) map { newTree =>
          new DiscrTreeIndex(root + (op -> newTree))
        } getOrElse {
          new DiscrTreeIndex(root - op)
        }
      } else
        this
    }
  } }

  /** Delete a specific record indicated by info */
  def delete(t: Term, info: A): DiscrTreeIndex[A] = Timer.idx_delete.measure { DTree.normalise(t) match {
    case x: Var => {
      stats.idxDeletions += 1
      new DiscrTreeIndex(root, varIdx.delete(x, info))
    }
    case s @ FunTerm(op, _) if (root.isDefinedAt(op)) => {
      stats.idxDeletions += 1
      root(op).delete(DTree.toVarOpPreorder(s), info) map { newTree =>
        new DiscrTreeIndex(root + (op -> newTree), varIdx)
      } getOrElse {
        new DiscrTreeIndex(root - op, varIdx)
      }
    }
    case _ =>
      this
  } }

  /** Whether the index contains an exact copy of this term when normalised.
    * Mostly useful for testing.
    */
  def contains(t: Term, info: A) = DTree.normalise(t) match {
    case x: Var =>
      varIdx.contains(x, info)
    case s @ FunTerm(op, _) if (root.isDefinedAt(op)) =>
      root(op).contains(DTree.toVarOpPreorder(s), info)
    case _ =>
      false
  }

  /** Simply return the first matching entry. */
  def findFirstMatch(tIn: Term): Option[MatchResult[A]] = 
    findFirstMatchFilter(tIn: Term, (a,b,c) => true)

  /** Useful for 'exists a generalisation' queries e.g. demodulation by boolean
    * units.
    * @param filter is applied to a matcher result before returning */
  def findFirstMatchFilter(
      tIn: Term, 
      filter: (Subst, Term, A) => Boolean
    ): Option[MatchResult[A]] = Timer.idx_match.measure {
    val varRes = varIdx.findFirstMatchFilter(tIn, filter)

    if (varRes.nonEmpty) 
      varRes
    else
      tIn match {
        case t @ FunTerm(op, args) if (root.isDefinedAt(op)) =>
          root(op).getMatcher(t :: Nil, Map(), filter)
        case _ => None
      }
    }

  /** Find all terms in the index which match to `t` */
  def findAllMatching(tIn: Term): List[MatchResult[A]] = Timer.idx_match.measure {
    tIn match {
      case x: Var =>
        varIdx.findAllMatching(x)
      case t @ FunTerm(op, args) if (root.isDefinedAt(op)) =>
        varIdx.findAllMatching(t) ++ root(op).getAllMatchers(t :: Nil, Map())
      case t => //Nil
        varIdx.findAllMatching(t)
  } }

  def findAllUnifiable(t: Term) = ???

  def findAllInstances(t: Term) = ???

  def flatten: Set[(Term, A)] = root.values.map(_.flatten).foldLeft(Set[(Term, A)]())(_ ++ _)

}
