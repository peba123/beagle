package beagle.datastructures

import beagle._
import calculus.{Inference,ClauseOrFormula,simplification}
import fol._
import util._
import bgtheory.LIA.iQE.{QEClause,QEAtom,False}
import PMI._

case class ClsInfo(idxRelevant: Set[Int], age: Int, inference: Inference,
  deltaConjecture: Int)

/* We eliminate the 'Clause' type since we want EVERY method 
 to deal with the possibility of their being a constraint. */

/** A ConsClause is a container of lits with some 'extra information'.
  * 
  * For the purposes of inferencing, the calculus implementation ignores the existence of
  * the field `constraint`, i.e. it doesn't count towards clause weight or length etc.
  * However, it IS passed through inferences and all substitutions that apply to the clause
  * apply to the constraint literals by default.
  * Note that this extends to fresh() and unabstr() which transform the clause using 
  * substitutions.
  * 
  * If a subclass needs different behaviour, it should start by overriding applySubst.
  * 
  * There are roughly three ways to modify a clause while preserving constraints and other info:
  * 1) Explicit passing: from cl1 and cl2 create a new clause using new ConsClause(..., cl1.constraint ++ cl2.constraint,...).applySubst(s)
  * applySubst applies s to both literals and constraints.
  * See paramodulation.scala for an example.
  * 2) Implicit using cl.modified method- lets you selectively replace some fields while
  * preserving others. Useful when a subset of ClsInfo changes.
  *  See simplification.demodulate for an example.
  * 3) Implicit using clause transformations mapLit, append or removeLit.
  *  These function like a specialised modified() method and lift List methods to Clause.
  *  Useful where modifications can be described as a sequence of small changes.
  *  See derivationrules.InfFact for an example.
  */

class ConsClause(val lits: List[Lit], val constraint: List[Lit], val info: ClsInfo)
    extends Expression[ConsClause] with PMI[ConsClause] with ClauseOrFormula {

  /* Abstract members of Expression */

  /** Substitutions apply to constraint literals by default.
    */
  def applySubst(sigma: Subst) = 
    if (sigma actsOn this) 
      new ConsClause(sigma(lits), sigma(constraint), info)
    else
      this

  /** Max depth of any clause literal.*/
  lazy val depth = if (lits.isEmpty) 0 else (lits map { _.depth }).max

  lazy val kind = Expression.lub(lits)

  def matchers(that: ConsClause, gammas: List[Subst]) =
    //that.matchersFrom(this.lits, gammas)
    if (length != that.length)
      List.empty
    else
      Expression.matchers(lits, that.lits, gammas)

  /* Below are versions of `matchers` that allow for hiding of the `lits` field.
   * Currently they are not used.
   */

  def matchersFrom(ls: List[Lit], gammas: List[Subst]) = 
    if (length != ls.length)
      List.empty
    else
      Expression.matchers(ls, lits, gammas)

  def matchersTo(ls: List[Lit], gammas: List[Subst]) = 
    if (length != ls.length)
      List.empty
    else
      Expression.matchers(lits, ls, gammas)

  lazy val maxBSFGTerms = lits.maxBSFGTerms

  // ??? should have the same effect, but it wont be accidentally caught elsewhere.
  def mgus(that: ConsClause) = ??? //throw new InternalError("mgus for clauses are not defined")

  lazy val minBSFGTerms = lits.minBSFGTerms
  lazy val operators = lits.operators
  lazy val sorts = lits.sorts
  lazy val symConsts = lits.symConsts
  lazy val vars = lits.vars

  /* Abstract memebers of PMI */

  lazy val termIndex = liftIndex(lits map { _.termIndex })

  /** Replace a term `t` at position `pos`.
    * 
    * The head of `pos` gives the index of the literal to replace inside.
    * Since we cannot replace a `Lit` with a `Term` then this should be checked
    * for at this level.
    * 
    * @return a new Clause with the terms appropriately replaced. If the term position
    * does not exist, it simply returns `this`.
    * @throws InternalError if the replacement will be invalid. Non-existing positions
    * do not throw this.
    */
  def replaceAt(pos: Pos, t: Term): ConsClause =
    if (pos.isEmpty)
      throw InternalError(s"Attempt to replace a Clause $this with a Term $t in replaceAt")
    else if (pos.tail.isEmpty)
      throw InternalError(s"Attempt to replace a Lit with a Term $t in replaceAt")
    else
      this.modified(lits = PMI.replaceAtList(lits, pos, t))

  def get(pos: Pos) =
    if (pos.isEmpty) this
    else lits(pos.head).get(pos.tail)

  /* Abstract members of ClauseOrFormula */

  // Mixin ClauseOrFormula
  val id: Int = Clause.idCtr.next()
  def inference = info.inference

  /* ------- Clause Methods ----------- */

  /* Overrides */

  override def equals(that: Any) = that match {
    case cl: ConsClause ⇒ 
      info.age == cl.age && idxRelevant == cl.idxRelevant && lits == cl.lits && constraint == cl.constraint
    case _ ⇒ false
  }

  override def fresh() = {
    val r = Term.mkRenaming(lits.vars ++ constraint.vars)
    this.applySubst(r)
  }

  override def freshGenVars() = {
    val r = Term.mkRenamingIntoGenVars(lits.vars ++ constraint.vars)
    this.applySubst(r)
  }

  override def toString = printer.clauseToString(this) 

  /* Lit accessor methods */

  def apply(i: Int) = lits(i)
  def existsLit(p: Lit => Boolean) = lits.exists(p)
  def isEmpty = lits.isEmpty
  lazy val length = lits.length

  /* Info accessor methods */
  val age = info.age
  val idxRelevant = info.idxRelevant

  /* Operators on indices */

  lazy val (iPosLits, iNegLits) = (0 until length) partition { lits(_).isPositive }
  lazy val (iPureBGLits, iNonPureBGLits) = 0 until length partition { lits(_).isPureBG }
  lazy val (iBGLits, iNonBGLits) = 0 until length partition { lits(_).isBG }

  def isPositive = iNegLits.isEmpty
  def isNegative = iPosLits.isEmpty
  def hasPositive = !iPosLits.isEmpty
  def hasNegative = !iNegLits.isEmpty

  lazy val nonPureBGLits = (iNonPureBGLits map { lits(_) }).toList
  lazy val pureBGLits = (iPureBGLits map { lits(_) }).toList

  lazy val nonBGLits = (iNonBGLits map { lits(_) }).toList
  lazy val BGLits = (iBGLits map { lits(_) }).toList

  /** Get all positions in positive clause literals that can be an into-term for superposition */
  def posLitsSubTermsWithPosForSup(t: Term): Iterable[(Term, Pos)] = {
    iPosLits flatMap {
      i => lits(i).subTermsWithPosForSup(t) map {
        case (s, pos) => (s, i :: pos)
      }
    }
  }

  /** Get all positions in negative clause literals that can be an into-term for superposition */
  def negLitsSubTermsWithPosForSup(t: Term): Iterable[(Term, Pos)] = {
    iNegLits flatMap {
      i => lits(i).subTermsWithPosForSup(t) map {
        case (s, pos) => (s, i :: pos)
      }
    }
  }

  /** Stores the result of previous iIsMaximalIn calls */
  private var maxLitCache = Set.empty[Int]

  /** Whether the literal at i is maximal in this clause */
  def iIsMaximalIn(i: Int): Boolean = {
    if (maxLitCache(i)) return true

    //check if there is a greater lit
    var j = 0
    while (j < length) {
      if (j != i && lits(j).gtr(lits(i))) return false
      else j += 1
    }
    //no, so add to cache
    maxLitCache += i
    return true
  }

    /** Whether the literal at i is maximal in this clause, expressed as a list of constraint literals */
  def iIsMaximalInCons(i: Int): List[Lit] = {
    // if (maxLitCache(i)) return true
    val litiToTerm = lits(i).toTerm
    (for (j <- 0 until length; if j != i) yield {
      Lit(true, GEQ(litiToTerm, lits(j).toTerm))
    }).toList
  }

  /** Cache iIsStrictlyMaximalIn calls */
  private var strMaxLitCache = Set.empty[Int]

  def iIsStrictlyMaximalIn(i: Int): Boolean = {
    
    if (strMaxLitCache(i)) return true
    
    var j = 0
    while (j < length) {
      if (j != i && lits(j).geq(lits(i))) return false
      else j += 1 //continue
    }

    strMaxLitCache += i
    return true
  }

    /** Whether the literal at i is strictly maximal in this clause, expressed as a list of constraint literals */
  def iIsStrictlyMaximalInCons(i: Int): List[Lit] = {
    // if (maxLitCache(i)) return true
    val litiToTerm = lits(i).toTerm
    (for (j <- 0 until length; if j != i) yield {
      Lit(true, GTR(litiToTerm, lits(j).toTerm))
    }).toList
  }

  /** Iterator over maximal non-pure BG literals in clauses,
   * those that are subject to inferences.
   */
  lazy val iMaximal = iNonPureBGLits filter { iIsMaximalIn(_) }

  //TODO- these can be improved to exploit the memo-ized iIsMaximal function
  lazy val (iEligible: IndexedSeq[Int], iEligibleIsMax: Boolean) =
    if (flags.negSelection.value) {
      // Try to find a maximal selected negative literal
      (iMaximal find { i ⇒ !lits(i).isPositive && lits(i).isFG }) match {
        case Some(i) ⇒  // got it
          (i to i, false)
        case None ⇒
          // Next best thing: Select any negative literal
          (iNegLits find { i ⇒ lits(i).isFG }) match {
            case Some(i) ⇒ 
              (i to i, false)
            case None ⇒ 
              // No selection of negative literals possible
              (iMaximal, true)
          }
      }
    } else {
      // Some heuristics here:
      if (iMaximal.length == 1)
        (iMaximal, true)
      else {
        // val (iNegFG, iOther) = iMaximal partition { i ⇒ !lits(i).isPositive && lits(i).isFG }
        // println("xxx %s : %d maximal, %d negative literals".format(this, iMaximal.length, iNegFG.length))
        // Select a negative literal if possible
        (iMaximal find { i ⇒ !lits(i).isPositive && lits(i).isFG }) match {
          case Some(i) ⇒  // got it
            (i to i, false)
          case None => 
            (iMaximal, true)
        }
      }
    }

  /* Transformer methods */

  /** Note that this is set-like append */
  // So, append(List(l)) and append(l) may give difference results. Confusing?
  def append(l: Lit): ConsClause =
    if (lits contains l) this else new ConsClause(lits :+ l, constraint, info)

  def append(ls: List[Lit]): ConsClause = 
    new ConsClause(lits ++ ls, constraint, info)

  def appendCons(l: Lit): ConsClause =
    if (constraint contains l)
      this
    else if (consLit.isInconsistentWith(l, constraint))
      new ConsClause(lits, List(Lit.FalseLit), info)
    else
      new ConsClause(lits, constraint :+ l, info)

  def appendCons(ls: List[Lit]): ConsClause =
    if (ls.isEmpty) this else new ConsClause(lits, constraint ++ ls, info)

  // give a list (conjunnction) of constraints ls, strengthen the constraint of this
  // by (conceptually) conjoining \neg ls, which requires to use deMorgan on ls and multiply out
  def excludeCons(ls: List[Lit]): List[ConsClause] = {
    // ls map { l => this.appendCons(consLit.toNeg(l.compl)) } 
    for (l <- ls;
      lc = l.compl // consLit.toPos(l.compl)
      if !consLit.isInconsistentWith(lc, constraint)) yield
      this.appendCons(lc)
  }

  lazy val withoutCons =
    if (constraint.isEmpty) this else new ConsClause(lits, List.empty, info)

  /** asQEClause: list of this' lits as QEAtoms, if appropriate */
  lazy val asQEClause: Option[QEClause] = {
    if (isEmpty)
      Option(QEClause(False(Set.empty, Set(id))))
    else if (isSolverClause)
      Option(QEClause(lits map { QEAtom(_, id) }))
    else None
  }

  lazy val asSolverClauses: Option[(List[ConsClause], List[ConsClause])] = 
    if (isBG) Option(bgtheory.solver.asSolverClauses(this)) else None

  def reduce(cls: ClauseSet) = reduceWithStatus(cls)._1
  def reduceWithStatus(cls: ClauseSet) = 
    simplification.reduceWithStatus(this, cls)

  // Includes call of simplifiedBG below
  lazy val simplifyCheap: List[ConsClause] = simplification.simplifyCheap(this)

  lazy val simplifyBG = {

    Timer.simplificationBG.start()

    //must filter false lits since the continue condition in simplifyCheap requires that
    //the clause length decreases on simplification!

    val res = lits map { _.simplifyBG } filterNot { _ == Lit.FalseLit }

    Timer.simplificationBG.stop()

    // println(s"simplify($cl:%d) = $res:%d".format(cl.id, res.id))
    if (res == lits)
      this // avoid mentioning redundant simplification in proof
    else {
      this.modified(lits = res, 
        inference = calculus.ByBGSimp(this, bgtheory.solver.theoryName),
        deltaConjecture = this.info.deltaConjecture + 1)
    }
    
  }

  lazy val toFormula = (lits map { _.toLiteral }).toOr.closure(Forall)
  lazy val toNegFormula = (lits map { _.compl.toLiteral }).toAnd.closure(Exists)

  /** Produce a formula that includes the constraint sequent style*/
  lazy val toConstrFormula = 
    Implies((constraint map { _.toLiteral }).toAnd, 
      (lits map { _.toLiteral }).toOr).closure(Forall)

  /** Apply a Lit -> Lit map to the clause literals. */
  def mapLits(f: Lit => Lit): ConsClause =
    new ConsClause(lits.map(f), constraint, info)

  def removeLit(i: Int): ConsClause =
    new ConsClause(lits.removeNth(i), constraint, info)

  def setInfo(newInfo: ClsInfo): ConsClause =
    new ConsClause(lits, constraint, newInfo)

  /**
   * It is a good idea to use this instead of Clause(lits,...)
   * or new Clause(...) to construct new clauses.
   * This allows the current clause to have some say in what the
   * state of the new clause is which has two benefits:
   * 1) Extending clauses to be labelled/constrained clauses is easier
   * because only one method needs to be overridden to propagate labels.
   * 2) Later we can use this to store state which may not change or change in
   * a simple way, e.g. keeping a PMI index after a call to fresh().
   */
  def modified(lits: List[Lit] = this.lits,
    constraint: List[Lit] = this.constraint,
    idxRelevant: Set[Int] = this.idxRelevant,
    age: Int = this.age,
    inference: Inference = this.info.inference, 
    deltaConjecture: Int = this.info.deltaConjecture): ConsClause =
      new ConsClause(lits, constraint, ClsInfo(idxRelevant, age, inference, deltaConjecture))

  /* Boolean tests */

  def isDefinition =
    (unabstrAggressive.length == 1) &&
      unabstrAggressive(0).isPositive &&
      unabstrAggressive(0).eqn.lhs.isMinBSFGTerm &&
      unabstrAggressive(0).eqn.rhs.isPureBG

  lazy val isFlat = lits forall { _.isFlat }

  /** extended BGT-fragment: all background-sorted FG-terms are ground.*/
  lazy val isGBTClause = this.maxBSFGTerms forall { _.isGround }

  lazy val isNegUnitClause = isUnitClause && !lits.head.isPositive
  lazy val isPosUnitClause = isUnitClause && lits.head.isPositive

  lazy val isSolverClause = bgtheory.solver.isSolverClause(this)

  /** Very simplistic checks.*/
  lazy val isTautology: Boolean = checkTautology

  private def checkTautology: Boolean = {
    stats.simpTaut.tried
    //TODO- this could exploit clause indexes to find matching pairs
    for {
      i1 <- iPosLits
      l1 = lits(i1)
    } {
      if (l1.isTrivialPos) {
        stats.simpTaut.succeed
        return true
      }
      for (i2 <- iNegLits)
        if (l1.eqn == lits(i2).eqn) {
          stats.simpTaut.succeed
          return true
        }
    }

    // Rather expensive
    // if (!bgc.isConsistentWith(c.lits)) return true
    // also expensive, enable with flag -x
    if (util.flags.experimental.value && sorts.contains(bgtheory.LIA.IntSort)) {
      Timer.bgTaut.start()
      if (lits exists { _.simplifyBG == Lit.TrueLit }) {
        Timer.bgTaut.stop()
        return true
      }
      // test this:
      //if (!this.isPureBG) { //don't bother, shouldn't affect the proof
      if (true) {
        //fully abstract
        //val bgs = stdAbstract.lits filter { _.isBG }
        val bgs = lits filter { _.isBG }
        //test ?x. /\(bgs) -- if unsat, delete
        val in = bgs.map(_.toLiteral.compl).toAnd.closure(Exists)
        //TODO is it better to use ?x ... or !x ... form
        if (bgtheory.LIA.cooper.Solver.QE(in, true) == FalseAtom) {
          // println(s"*** ${this.stdAbstract} simplified by BG taut. deletion")
          Timer.bgTaut.stop()
          return true
        }
      }
      Timer.bgTaut.stop()
    }
    return false
  }
  lazy val isUnitClause = lits.nonEmpty && lits.tail.isEmpty

  /* ---- Clause specific functions ---- */

  lazy val weight = lits.foldLeft(0)(_ + _.weight) // - conjectureMerit

  /* Abstraction/Unabstraction */

  private lazy val stdAbstract = purification.stdAbstractClause(this)
  private lazy val weakAbstract = purification.weakAbstractClause(this)

  /**
   * Do weak abstraction unless standard abstraction is used; which does not require
   * doing anything more as standard abstraction is preserved by the inference rules.
   */
  def abstr = {
    util.Timer.abs.start()
    val res =
      if (util.flags.bgsimp.value > 2)
        unabstrAggressive // this // that's ridiculous
      else if (flags.stdabst.value) stdAbstract
      else weakAbstract
    util.Timer.abs.stop()
    res
  }

  /**Remove equations of the form x != t if x does not occur in t by substitution.
    * The term t should be pure-BG in default mode.
    * Also simplifies trivial negative literals.
    * 
    * @param cautious Whether to also remove general terms t.
    * @return The unabstracted clause and the substitution used to create it.
    */
  private def unabstract(cautious: Boolean) = {
    var CRes = List.empty[Lit]
    var sigma = Subst.empty // The substitution we are building from the disequations of the above form
    var touched = false
    for (l ← lits)
      // Invariant: sigma has been applied to CRes   
      l match {
        case Lit(true, _) => CRes ::= sigma(l)  // nothing to remove; maintain the invariant
        case Lit(false, Eqn(lhs, rhs)) if lhs == rhs =>
          // Removal of trivial negative equations
          touched = true // doing nothing means removing the literal
        case Lit(false, e) => {
          val esigma = sigma(e)
          val delta = // The new substitution
            esigma.asVarTermPair match {
              case Some((x : Var, y: GenVar)) => Subst(y -> x) // To make sure that abstraction variables are not replaced by general variables
                                                               // case Some((x : Var, y: AbstVar)) => Subst(x -> y)
                                                               // Preferred:
              case Some((x : GenVar, y: AbstVar)) => Subst(x -> y)
              case Some((x : Var, t: Term)) if cautious && t.isPureBG && !x.occursIn(t) => Subst(x -> t)
              // case Some((x : Var, t: Term)) if cautious && t.isDomElem => Subst(x -> t)
              // case Some((x : GenVar, t: Term)) if t.isBG && !x.occursIn(t) => Subst(x -> t)
              // Generalizes this:
              // case Some((x : Var, d: DomElem)) => Subst(x -> d)
              // The generalization is much better, in partiuclar in concert with Define,
              // cf PUZ133=2.p, but unfortunately not covered by the redundancy criterion
              // Used up to incl 0.9.19:
              // case Some((x : GenVar, t: Term)) if !cautious && !x.occursIn(t) => Subst(x -> t)
              // However, this is not enough to undo purification during preprocessing.
              // E.g. f(x+1) = f(x)+1 gets f(y) = z+1 \/ y != x+1 \/ z != f(x)
              // and the disequation y != x+1 would not be removed (if y is an abstraction variable).
              case Some((x: Var, t: Term)) if !cautious && !x.occursIn(t) => Subst(x -> t)
              // Too brave:
              // case Some((x : Var, t: Term)) if !x.occursIn(t) => Subst(x -> t)
              case _ => Subst.empty // Not a variable-term pair or cautious or occurs-check problem
            }

          if (delta.isEmpty)
            CRes ::= Lit(false, esigma)
          else {
            sigma ++= delta
            CRes = delta(CRes) // remove disequation and maintain invariant
            touched = true
          }
        }
      } 

    // Like distinct but reverses the list
    def distinctReversed(lits: List[Lit]) = {
      var res = List.empty[Lit]
      for (l ← lits)
        if (!(res contains l)) res ::= l
      res
    }

    if (touched)
      (this.modified(lits = distinctReversed(CRes),
                     constraint = sigma(constraint)), sigma)
    else (this, Subst.empty) // avoid building the clause
  }

  lazy val unabstrCautious = unabstract(cautious = true)._1
  //TODO where is this used?
  lazy val unabstractAggressive = unabstract(cautious = false)
  lazy val unabstrAggressive = unabstractAggressive._1
 
  /* Ordering */
  import fol.Ordering._

  /**
   * Comparing two clauses according to the multiset ordering
   * Currently needed only for diagnostic purposes, to see if
   * simplification really gave a smaller clause
   */
  def compare(that: ConsClause): OrderingResult = {
    if (this.lits mso_equ that.lits)
      Equal
    else if (this.lits.mso_geq(_ gtr _)(that.lits))
      Greater
    else if (that.lits.mso_geq(_ gtr _)(this.lits))
      Less
    else
      Unknown
  }

  // Use cases
  def gtr(that: ConsClause) = (this compare that) == Greater
  def geq(that: ConsClause) = List(Greater, Equal) contains (this compare that)

  /* Inferences */

  /** Instantiate literal with index i */
  def instantiate(i: Int): List[ConsClause] = {
    def mkDomSubsts(xs: List[Var]): List[Subst] =
      xs match {
        case Nil ⇒ List(Subst.empty)
        case x :: rest ⇒
          // Build the cross product
          for (
            gamma ← mkDomSubsts(rest);
            t ← Sigma.domain(x.sort).fresh()
          ) yield gamma + (x -> t)
      }
    for (gamma ← mkDomSubsts(lits(i).vars.toList filter { Sigma.fdSorts contains _.sort })) yield gamma(this)
  }

  /** Split a clause if possible into variable disjoint lists of lits */
  def split: Option[(List[Lit], List[Lit])] = {
    //special cases can be short-circuited
    if (this.isUnitClause || this.isEmpty) 
      return None
    // else if (this.isGround) 
    //   return Some(List(this.lits.head),this.lits.tail)
    
    case class Elem(vars: Set[Var], indices: List[Lit])
    var partitions = List.empty[Elem]

    //can ignore ground lits as they are always separate elements
    var gndLits = List.empty[Elem]
    
    for (l <- lits) {
      if (l.isGround) 
        gndLits ::= Elem(Set.empty, List(l))
      else {
        var varsx = l.vars
        var ix = List(l)
        var newpartitions = List.empty[Elem]
        for (el ← partitions)
          if (el.vars.exists(varsx(_))) {
            varsx ++= el.vars
            ix :::= el.indices
          } else
            newpartitions ::= el
        partitions = Elem(varsx, ix) :: newpartitions
      }
    }

    // If pure BG clauses are not to be split we try to find a maximal subclause
    // of the gndLits
    if (util.flags.split.value == "nopurebgc") {
      val (pureBG, other) = gndLits partition {
        case Elem(_, List(l)) => l.isPureBG
      }
      if (pureBG.nonEmpty && (other.nonEmpty || partitions.nonEmpty)) {
        val right = pureBG flatMap(_.indices)
        val left = (other ::: partitions).flatMap(_.indices)
        return Option((left, right))
      }
    }

    // add ground lit components back
    partitions :::= gndLits
    partitions = (partitions sortWith {
      // discourage/encourage split left with positive literals
      // DEVELOP
      (elems1, elems2) => !(elems1.indices exists { _.isPositive }) &&
        (elems2.indices exists { _.isPositive }) })
    if (partitions.length <= 1)
      return None
    else {
      // println("yy " + partitions)
      val left = partitions.head.indices
      val right = partitions.tail.flatMap(_.indices)
      return Option((left, right))
    }

  }

  /** Proper subsumption or variants, 
    * Result is None if no subsumption, otherwise
    * list of residual clauses (due to constraints not fully covered by subsuming clause.
    */
  def subsumes(that: ConsClause): Option[List[ConsClause]] = {
    stats.simpSubsume.tried
    /* //tail-recursive version of buildClauseMatching
     //seems as though the extra work in maintaining state cancels any advantage from the tail recursion.
    case class LastBCM(gamma: Subst, lits1: List[Lit], other: List[Lit], ts: List[Lit], remGs: List[Subst], oldSaved: Option[LastBCM])

    @annotation.tailrec
    def bcmTR(gamma: Subst, lits1: List[Lit], lits2: List[Lit], oth: List[Lit] = Nil, last: Option[LastBCM] = None): Option[Subst] = {
      if (lits1.isEmpty) {
        Option(gamma) //base case
      } else {
        val l :: remainingLits = lits1
        var other = oth
        val tsIter = lits2.tails.toIterator
        var doContinue = true
        var saved: LastBCM = null
        var lits2exceptk: List[Lit] = null
        var foundG = Subst.empty

        while (tsIter.hasNext && doContinue) {
          val nextL = tsIter.next
          if (nextL.isEmpty) () //skip
          else {
            val (k :: ts) = nextL
              l.matchers(k, List(gamma)) match {
                case Nil => other ::= k
                case g :: remGs => {
                  foundG = g
                  lits2exceptk = other.foldLeft(ts)((acc, k2) => k2 :: acc)
                  saved = LastBCM(gamma, lits1, k :: other, ts, remGs, last)
                  doContinue = false
                }
              }
          }
        }
        if (doContinue) {
          //no matcher found, backtrack with saved state:
          if (last.isEmpty) None
          else
            last.get match {
              case ns @ LastBCM(_, _, _, _, g2 :: gs, _) => bcmTR(g2, lits1, lits2, Nil, Some(ns.copy(remGs = gs))) //restart this level with next gamma
              case LastBCM(gamma, l1, oth, ts, Nil, oldSaved) => bcmTR(gamma, l1, ts, oth, oldSaved) //pop state
            }
          
        } else bcmTR(foundG, remainingLits, lits2exceptk, Nil, Some(saved))
      }
    }
     */
    /**
     * Attempt to extend gamma to a substitution such that for each literal in lits2
     * there is a literal in lits1 for which this substitution is a matcher.
     * Works also with constraint clauses:
      Say, we want that C₁ || D₁ subsumes C₂ || D₂
      First, find a matcher γ as usual, i.e., C₁γ ⊆ C₂ 
      Second, conceptually, split C₂ || D₂ into the two clauses
      C₂ || (D₂ ∧ D₁γ) and C₂ || (D₂ ∧ ¬D₁γ)
      Clearly, this splitting is sound and complete, as it preserves the ground instances of C₂ || D₂ wrt. satisfied constraints
      Finally, C₁ || D₁ classically subsumes C₂ || (D₂ ∧ D₁γ) and hence C₂ || (D₂ ∧ D₁γ) can be deleted,
      but (the clausal form of) C₂ || (D₂ ∧ ¬D₁γ) has to be kept. 
      It represents those instances of C₂ || D₂ that are not subsumed by the instances of C₁ || D₁ wrt. satisfied constraints
     */
    def buildClauseMatching(gamma: Subst, lits1: List[Lit], lits2: List[Lit]): Option[Subst] = {
      //if it must be injective then we can halt whenever lits1.length exceeds lits2.length
      // if (lits1.length > lits2.length)
      //   return None
      // never true -- checked initially in pre-tests and invariant over recursive calls

      // For simplicity stop after the first candidate gamma is found. With constraints, one could consider all possible matchings,
      // as they give rise to different ¬D₁γ
      if (lits1.isEmpty) {
        Option(gamma)
      } else {
        // if (that.constraint containsSlice gamma(this.constraint))
        //   Some(gamma)
        // else
        //   None
        val l :: remainingLits = lits1

        //store those literals not already matched
        //a slightly faster way to get lits2exceptk
        var other = List.empty[Lit]
        for {
          //(k, i) ← lits2.toStream.zipWithIndex
          (k :: ts) <- lits2.tails
          //g ← l.matchers(k, List(gamma)) // if g.isSimpleFor(bgVars)
        } {
          l.matchers(k, List(gamma)) match {
            case Nil => other ::= k
            case ms => for (g <- ms) {
          // Must remove k from lits2 otherwise mapping from lits1 to lits2 might be non-injective
          // causing p(x,y \/ p(y,x) to subsume p(z,z) . Clearly this would be incomplete
          // as the latter is a factor of the former, but of course factors are generally needed

          //however if e.g we have p(x), p(z) match to p(y), p(y) then this reports no match
          //as p(x) is matched to [] on next round
          //therefore remove only the first occurrence of k-- which is i
          //val lits2exceptk = lits2.patch(i, Nil, 1)
          val lits2exceptk = other.foldLeft(ts)((acc, k2) => k2 :: acc)
          val res = buildClauseMatching(g, remainingLits, lits2exceptk) 
          if (res.nonEmpty) return res
          //otherwise continue with next lit in lits1
        }}}
        // After loop now, i.e. all possibilities exhausted
        None
      }
    }

    if (bgtheory.solver.subsumes(this, that)) {
      //println(s"*** $this subsumes $that by BG subsume")
      return Some(List.empty)
    // Cheap pretests below might not apply to the theory case
    }
    if ((length > that.length) ||
      (depth > that.depth) ||
      (hasNegative && that.isPositive) ||
      (hasPositive && that.isNegative) ||
      !(operators subsetOf that.operators))
      return None // cheap pretests apply

    Timer.subsumption.start()
    buildClauseMatching(Subst.empty, lits, that.lits) match {
      case None => return None
      case Some(gamma) => {
        stats.simpSubsume.succeed
        val gammaConstraint = gamma(this.constraint)
        val residue = that.excludeCons(gammaConstraint)
        val res =
          // if (that.constraint containsSlice gammaConstraint)
          //   Some(residue)
          // else 
            if (residue forall {  _.constraint.length < that.constraint.length })
            // length == that.length
            Some(residue)
          else None
/*        val res =
          if (that.constraint containsSlice gammaConstraint)
            Some(List.empty) // subsumed without residue
          else {
            // gammConstraint is not empty
            Some(that.excludeCons(gammaConstraint))
            // if (length < that.length)
            //   Some(that.excludeCons(gammaConstraint))
            // else
            //   None // to ensure termination
          }
 */
        Timer.subsumption.stop()
        if (res != None) {
          //println(s"*** $this subsumes $that")
          reporter.onClauseSubsumed(this, that, res.get)
        }
        return res
      }
    }
  }

  def standardizeVariables(nextUnused: Int, soFar: Subst) = ???

}

/* Provide a constructor that looks like the old clause */
object Clause {
  val idCtr = new util.Counter()

  def apply(lits: List[Lit], info: ClsInfo) = new ConsClause(lits, List.empty, info)

}

object ConsClause {

  def unapply(cl: ConsClause) = Some((cl.lits, cl.constraint, cl.info))

}
