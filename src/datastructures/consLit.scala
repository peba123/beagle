package beagle.datastructures

import beagle._
import fol._
import calculus._
import datastructures._
import Signature._

/**
  * Constraint literals. So far only ordering constraints.
  */

/** Ordering constraint atom: s is greater than t wrt.\ the term ordering in effect.
  *  Nothing but a view on predicative atoms.
  *  Notice that the terms it is applied to can not only be ordinary terms
  *  but also ordinary literals turned into terms (via .toTerm)
  */
object GTR {
  def apply(lhs: Term, rhs: Term) = {
    PredEqn(PFunTerm(Sigma.gtrOperators(lhs.sort), List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    // Bad: name "$gtr" appears here and in Signature.gtrOperators. They must be the same of course.
    // Worth fixing?
    case PredEqn(PFunTerm(Operator(FG, "$gtr", _), List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object GEQ {
  def apply(lhs: Term, rhs: Term) = {
    PredEqn(PFunTerm(Sigma.geqOperators(lhs.sort), List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(Operator(FG, "$geq", _), List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object consLit {

  // Compare two terms s and t wrt. the ordering
  // Unlike Ordering.ordering.compare, s and t could be literals turned into terms,
  // hence need to unwrap in this case
  // def compareGeneral(s: Term, t: Term) =
  //   (s, t) match {
  //     case (LitToTerm(sLit), LitToTerm(tLit)) => {
  //       // In LitToTerm(sLit), sLit is the literal that was turned into the term s
  //       // println(s"$sLit compare $tLit = " + (sLit compare tLit))
  //       sLit compare tLit // k and l are literals
  //     }
  //     case (s, t) => s compare t // usual case
  //   }

  // def gtrCons(s: Term, t: Term) = compareGeneral(s, t) == Ordering.Greater
  // def geqCons(s: Term, t: Term) = compareGeneral(s, t) match {
  //   case Ordering.Equal | Ordering.Greater => true
  //   case _ => false
  // }

  def gtrCons(s: Term, t: Term) =
    (s, t) match {
      case (LitToTerm(sLit), LitToTerm(tLit)) => sLit gtr tLit
      case (s, t) => s gtr t // usual case
    }

  def geqCons(s: Term, t: Term) =
    (s, t) match {
      case (LitToTerm(sLit), LitToTerm(tLit)) => sLit geq tLit
      case (s, t) => s geq t // usual case
    }


  def isConsLit(l: Lit) =
    l match {
      case Lit(_, GTR(_, _)) => true
      case Lit(_, GEQ(_, _)) => true
      case _ => false
    }

  // In isValid(l) and isSat(l), l is an ordering literal or a "boolean constant"
  // Validity asks if all ground instances are true
  // Satisfiability asks if some ground instance is true
  // That is, the negation in the literal sits underneath this quantification
  def isValid(l: Lit) =
  l match {
      case Lit.TrueLit => true
      case Lit.FalseLit => false
      case Lit(true, GTR(lhs, rhs)) => gtrCons(lhs, rhs)
      case Lit(true, GEQ(lhs, rhs)) => geqCons(lhs, rhs)
      case Lit(false, GTR(lhs, rhs)) => geqCons(rhs, lhs)
      case Lit(false, GEQ(lhs, rhs)) => gtrCons(rhs, lhs)
    }

  def isSat(l: Lit) =
    l match {
      case Lit.TrueLit => true
      case Lit.FalseLit => false
      case Lit(true, GTR(lhs, rhs)) => !(geqCons(rhs, lhs))
      case Lit(true, GEQ(lhs, rhs)) => !(gtrCons(rhs, lhs))
      case Lit(false, GTR(lhs, rhs)) => !(geqCons(lhs, rhs))
      case Lit(false, GEQ(lhs, rhs)) => !(gtrCons(lhs, rhs))
    }

  def toPos(l: Lit) = 
    l match {
      case Lit.TrueLit | 
      Lit.FalseLit | 
      Lit(true, GTR(_, _)) | 
      Lit(true, GEQ(_, _)) => l
      case Lit(false, GTR(lhs, rhs)) => Lit(true, GEQ(rhs, lhs))
      case Lit(false, GEQ(lhs, rhs)) => Lit(true, GTR(rhs, lhs))
    }


  def isUnSat(l: Lit) = ! isSat(l)

  def simplify(l: Lit) = {
    if (isValid(l)) {
      // println(s"consLit.isValid($l)")
      Lit.TrueLit
    }
    else if (isUnSat(l)) {
      // println(s"consLit.isUnSat($l)")
      Lit.FalseLit
    }
    else l
   }


  def isInconsistentWith(l: Lit, ls: List[Lit]): Boolean =
    l match {
      case Lit.TrueLit => false
      case Lit.FalseLit => true
      case Lit(true, GTR(s, t)) =>
        ls exists {
          case Lit(true, GTR(t1, s1)) if t == t1 && s == s1 => true
          case Lit(true, GEQ(t1, s1)) if t == t1 && s == s1 => true
          case Lit(false, GTR(s1, t1)) if t == t1 && s == s1 => true
          case Lit(false, GEQ(s1, t1)) if t == t1 && s == s1 => true
          case _ => false
        }
      case Lit(true, GEQ(s, t)) =>
        ls exists {
          case Lit(true, GTR(t1, s1)) if t == t1 && s == s1 => true
          case Lit(false, GTR(s1, t1)) if t == t1 && s == s1 => true
          case Lit(false, GEQ(s1, t1)) if t == t1 && s == s1 => true
          case _ => false
        }
      case Lit(false, GTR(s, t)) => isInconsistentWith(Lit(true, GEQ(t, s)), ls)
      case Lit(false, GEQ(s, t)) => isInconsistentWith(Lit(true, GTR(t, s)), ls)
    }

}
