package beagle.bgtheory.LFA

import beagle._
import fol.Term
import fol.PFunTerm
import fol.Operator
import fol.DomElem
import fol.BG
import fol.Arity0
import datastructures.PredEqn
import datastructures.Eqn
import util.Rat

case class DomElemReal(r: Rat) extends DomElem[Rat] {

  val h = this
  lazy val op = new Operator(BG, r.toString, Arity0(RealSort)) {
    override def toTerm = Some(h)
  }
  val value = r
  // override def toString = r.toDouble.toString
}


object LessEqEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RealSort && rhs.sort == RealSort)
    PredEqn(PFunTerm(LessEqOpReal, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(LessEqOpReal, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object LessEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RealSort && rhs.sort == RealSort)
    PredEqn(PFunTerm(LessOpReal, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(LessOpReal, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RealSort && rhs.sort == RealSort)
    PredEqn(PFunTerm(GreaterEqOpReal, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(GreaterEqOpReal, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RealSort && rhs.sort == RealSort)
    PredEqn(PFunTerm(GreaterOpReal, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(GreaterOpReal, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object UMinus {
  def apply(s: Term) = PFunTerm(UMinusOpReal, List(s))

  def unapply(s: Term) = s match {
    case PFunTerm(UMinusOpReal, List(t)) ⇒ Some(t)
    case _                                        ⇒ None
  }
}

object Sum {
  def apply(s1: Term, s2: Term) = PFunTerm(SumOpReal, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(SumOpReal, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Difference {
  def apply(s1: Term, s2: Term) = PFunTerm(DifferenceOpReal, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(DifferenceOpReal, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Product {
  def apply(s1: Term, s2: Term) = PFunTerm(ProductOpReal, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(ProductOpReal, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Quotient {
  def apply(s1: Term, s2: Term) = PFunTerm(QuotientOpReal, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(QuotientOpReal, List(t1, t2))  ⇒ Some((t1, t2))
    case _                                      ⇒ None
  }
}
