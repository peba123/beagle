package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
//import util.RatInt
import util._
import bgtheory.LRA._

package object LFA {

  object RealSort extends BGSort("$real") { override def toString = "ℝ" }

  val SumOpReal = (new Operator(BG, "$sum", Arity2((RealSort, RealSort) -> RealSort)) { override def formatFn = util.printer.sumFF })
  val UMinusOpReal = (new Operator(BG, "$uminus", Arity1(RealSort -> RealSort)) { override def formatFn = util.printer.uminusFF })
  val GreaterOpReal = (new Operator(BG, "$greater", Arity2((RealSort, RealSort) -> OSort)) { override def formatFn = util.printer.greaterFF })
  val LessOpReal = (new Operator(BG, "$less", Arity2((RealSort, RealSort) -> OSort)) { override def formatFn = util.printer.lessFF })
  val DifferenceOpReal = (new Operator(BG, "$difference", Arity2((RealSort, RealSort) -> RealSort)) { override def formatFn = util.printer.differenceFF })
  val ProductOpReal = (new Operator(BG, "$product", Arity2((RealSort, RealSort) -> RealSort)) { override def formatFn = util.printer.productFF })
  // notice: quotient is FG
  val QuotientOpReal = (new Operator(FG, "$quotient", Arity2((RealSort, RealSort) -> RealSort)) { override def formatFn = util.printer.quotientFF })
  val LessEqOpReal = (new Operator(BG, "$lesseq", Arity2((RealSort, RealSort) -> OSort)) { override def formatFn = util.printer.lessEqFF })
  val GreaterEqOpReal = (new Operator(BG, "$greatereq", Arity2((RealSort, RealSort) -> OSort)) { override def formatFn = util.printer.greaterEqFF })

  val IsIntOpReal = new IsIntOp(RealSort)
  val IsRatOpReal = new IsRatOp(RealSort)
  val IsRealOpReal = new IsRealOp(RealSort)
  
  val ToIntOpReal = new ToIntOp(RealSort)
  val ToRatOpReal = new ToRatOp(RealSort)
  val ToRealOpReal = new ToRealOp(RealSort)

  // Notoce these operators are not yet added to LFAOperators
  val QuotientEOpReal = new QuotientEOp(RealSort)
  val QuotientTOpReal = new QuotientTOp(RealSort)
  val QuotientFOpReal = new QuotientFOp(RealSort)
  val RemainderEOpReal = new RemainderEOp(RealSort)
  val RemainderTOpReal = new RemainderTOp(RealSort)
  val RemainderFOpReal = new RemainderFOp(RealSort)

  val NLPPOpReal = new NLPPOp(RealSort)

  val LFAOperators = Set(SumOpReal, UMinusOpReal, GreaterOpReal, LessOpReal, DifferenceOpReal, 
			 ProductOpReal, QuotientOpReal, LessEqOpReal, GreaterEqOpReal,
			 IsIntOpReal, IsRatOpReal, IsRealOpReal,
			 ToIntOpReal, ToRatOpReal, ToRealOpReal,
			 NLPPOpReal)

  def addStandardOperators(s: Signature) = {
    var res = s
    res += RealSort
    LFAOperators foreach { res += _ }
    res
  }

  val ZeroReal = DomElemReal(RatInt(0))
  val OneReal = DomElemReal(RatInt(1))
  val MinusOneReal = DomElemReal(RatInt(-1))

  RealSort.someElement = ZeroReal

  def isLFA(e: Expression[_]) = e.isBG && (e.sorts subsetOf Set(RealSort)) // && (e.operators subsetOf LFAOperators)

}
