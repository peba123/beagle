package beagle.bgtheory.LFA

import beagle._
import fol._
import fol.Signature._
import datastructures._
import calculus._
import util._
import bgtheory._

/** Provides methods for quantifier elimination, consistency checking and simplification
  * of Linear Real Arithmetic formulas and clauses.*/
abstract class LFASolver extends Solver {
  val theoryName = "LFA"
  // val canUNSATCore = false // can be overridden

  // This is a huge duplication of essentially the same code as in LRASolver
  def QE(cl: ConsClause) = fourierMotzkin.QE(cl)

  val simpRulesTermSafe = simpRules.simpRulesTermSafe
  val simpRulesTermUnsafe = simpRules.simpRulesTermUnsafe
  val simpRulesLitSafe = simpRules.simpRulesLitSafe
  val simpRulesLitUnsafe = simpRules.simpRulesLitUnsafe

}
