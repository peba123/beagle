package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
import datastructures._
import util._
import bgtheory._
import LIA._

/** Generic interface to SMT-solvers */
abstract class SMTSolver {

  // Abstract members:
  val name: String // The name of the SMT solver, e.g. "Z3"
  val executable: String // The string to use as executable for "name", e.g. "z3 -smt2"
  val logic: String // The SMT logic to use, set via set-logic command, e.g. "QF_LIA". Set to empty if not required
  var canUNSATCore = false // The the solver can compute unsatisfiable cores, set by init()

  var extraArgs = ""

  /** Write output to a temporary file and return the name of that file.*/
  private def writeToTempFile(input: String): String = {
    val outfile = java.io.File.createTempFile("for-SMT-solver", ".smt2")
    outfile.deleteOnExit()
    val outname = outfile.getAbsolutePath()
    val out = new java.io.PrintStream(new java.io.FileOutputStream(outname))
    out.println(input)
    out.close()
    outname
  }

  /**
    * init(): checks if the executable runs ok and sets canUNSATCore
    */
  def init() {
    import scala.sys.process._

    val outname = writeToTempFile(
"""(set-logic QF_LIA)
(set-option :produce-unsat-cores true)
(assert (! (= 1 2) :named c1))
(check-sat)
(get-unsat-core)"""
    )

    val resStream = (executable + " " + outname).lineStream_!

    var haveResult = false
    for (line ← resStream)
      if (line contains "(c1)")
        canUNSATCore = true
      else if (line == "unsat")
        haveResult = true

    if (!haveResult)
      throw GeneralError("SMT-Solver: test call does not yield 'unsat'")

    // See if the bgsolver is CVC4
    try {
      //use an empty ProcessLogger to catch stdErr output from non-CVC4 solvers
      val resStream1 = (executable + " --version").lineStream_!(ProcessLogger(_ => ()))

      if (resStream1 exists { _ contains "CVC4" }) {
        reporter.debug("Background SMT solver seems to be CVC4\n")
        extraArgs = "--rewrite-divk"
      }
    } catch {
      case _:Throwable => () // ignore
    }
  }

  // TPTP operators that are built-in SMT LIB
  val builtinOps = Map(
    "$sum" -> "+",
    "$uminus" -> "-",
    "$difference" -> "-",
    "$quotient_e" -> "???_$quotient_e", // see TFF paper
    "$quotient_t" -> "???_$quotient_t", // see TFF paper
    "$quotient_f" -> "???_$quotient_f", // see TFF paper
    "$quotient" -> "div", // see TFF paper
    "$remainder_e" -> "???_$remainder_e", // see TFF paper
    "$remainder_t" -> "???_$remainder_t", // see TFF paper
    "$remainder_f" -> "???_$remainder_f", // see TFF paper
    "$product" -> "*",
    "$divides" -> "thisIsADummy",
    "$less" -> "<",
    "$lesseq" -> "<=",
    "$greater" -> ">",
    "$greatereq" -> ">="
  )

  def safeIdent(s: String) = "|"+s+"|"

  // Could possibly also use util.Printers, but prefer simpler direct approach here
  def clauseToSMT(cl: ConsClause) = {
    def litToSMT(l: Lit): String = {
      def eqnToSMT(e: Eqn) = {
        def termListToSMT(ts: List[Term]): String =
          (ts map { termToSMT(_) }).mkString(" ")
        def termToSMT(t: Term): String = {
          def intToSMT(i: Int) = if (i < 0) "(- %s)".format(i * -1) else i.toString
          t match {
            case Var(name, index, _) => safeIdent(name) + (if (index == 0) "" else "_" + index)
              // $divides is a special case:
            case PFunTerm(LIA.DividesOpInt, List(number, term)) => "(= 0 (mod %s %s))".format(termToSMT(term), termToSMT(number))
            case PFunTerm(op, args) => "(%s %s)".format(builtinOps.getOrElse(op.name, safeIdent(op.name)), termListToSMT(args))
            case SymConst(op) => safeIdent(op.name)
            case LRA.DomElemRat(Rat(numer, denom)) => {
              if (denom == 1) intToSMT(numer) else "(/ %s %s)".format(intToSMT(numer), intToSMT(denom))
            }
            case LFA.DomElemReal(Rat(numer, denom)) => {
              if (denom == 1) intToSMT(numer) else "(/ %s %s)".format(intToSMT(numer), intToSMT(denom))
            }
            case DomElemInt(i) => intToSMT(i)
            // many cases missing
          }
        }
        // Body of eqnToSMT
        e match {
          case PredEqn(a) ⇒ termToSMT(a)
          case Eqn(l ,r) ⇒ "(= %s %s)".format(termToSMT(l), termToSMT(r))
        }
      }
      // Body of litToSMT
      if (l.isPositive) eqnToSMT(l.eqn) else "(not %s)".format(eqnToSMT(l.eqn))
    }
    cl.lits match {
      case Nil => "false"
      case l :: Nil => litToSMT(l)
      case many => "(or %s)".format((many map { litToSMT(_) }).mkString(" "))
    }
  }

  def sortToSMT(s: Sort) =
    s.name match {
      case "$int" => "Int"
      case "$rat" => "Real"
      case "$real" => "Real"
      case name => safeIdent(name)
    }

  private def formatInput(clauses: Iterable[ConsClause], sig: Signature): String = {
    (if (logic.nonEmpty)                     s"(set-logic $logic)\n"
    else                                    "") +
    (if (canUNSATCore && !flags.noMuc.value) "(set-option :produce-unsat-cores true)\n"
    else                                    "") +
    // Inform the SMT solver about the relevant BG sorts.
    // These are all sorts except the builtin-ones $int, $rat, $real.
    // All these (TPTP) sorts are 0-ary
    (sig.bgSorts map { sort =>
      sort.name match {
        case "$int" | "$rat" | "$real" => ""
        case _                         => s"(declare-sort ${sortToSMT(sort)} 0)"
      }
    } mkString("\n")) + "\n" +
    "(define-fun |$$true| () Bool true)\n" +
    // declare all the BG operators, except the builtin-ones
    (sig.bgOperators map { op =>
      if (!(builtinOps contains op.name)) s"(declare-fun ${safeIdent(op.name)} (${(op.arity.argsSorts map { sort => sortToSMT(sort)} ).mkString(" ")}) ${sortToSMT(op.arity.resSort)})"
      else                                 ""
    } mkString("\n")) + "\n" +
    (clauses map { cl =>
      if (canUNSATCore && !flags.noMuc.value) s"(assert (! ${clauseToSMT(cl)} :named c${cl.id.toString}))"
      else                                    s"(assert ${clauseToSMT(cl)})"
    } mkString("\n")) + "\n" +
    "(check-sat)\n" +
    (if (canUNSATCore && !flags.noMuc.value)  "(get-unsat-core)"
    else                                     "")
  }

  /** Call the SMT solver on a given clause set.*/
  def check(clauses: Iterable[ConsClause]): SolverResult = {

    import scala.sys.process._
    
    val input = formatInput(clauses, Sigma)
    val outname = writeToTempFile(input)

    if (debugBG) {
      println("Calling SMT solver on the following input:")
      println(input)
      //("/bin/cat " + outname).!
    }

    // val resStream = ("z3 -T:150 -memory:1000 -smt2 " + outname).lines_!
    // util.Timer.z3.start()
    val resStream = (executable +
      (if (extraArgs == "") "" else " " + extraArgs) +
      " " + outname).lineStream_!

    val result = resStream dropWhile {
      case "sat" | "unknown" | "unsat" => false //stop here
      case line if (line contains "error") => throw InternalError(s"SMT solver: file $outname: $line")
      case _ => true
    }

    val res = result.head match {
      case "unknown" =>
        UNKNOWN
      case "unsat" => {
        // get the next line, with the unsat core if the flag says so
        if (canUNSATCore && !flags.noMuc.value) {
          val s = result(1).drop(1).dropRight(1) // get rid of "(" and ")"
                                                 // split s, turn "c123" into "123" then into 123
          val coreIdx = ((s split(" ")) map {s => s.drop(1).toInt }).toList
          // println(line)
          // println(coreIdx)
          UNSAT(Some(coreIdx))
        } else
          UNSAT(None)
      }
      case "sat" =>
        SAT
    }

    if (debugBG) {
      println(s"SMT solver says: $res")
      println()
    }

    res
  }

}

// class Z3(val logic: String) extends SMTSolver {
//   // Implement abstract members
//   val name = "Z3"
//   val execString = "/usr/local/bin/z3 -smt2 %s"
//   init()
// }

// class CVC4(val logic: String) extends SMTSolver {
//   // Implement abstract members
//   val name = "CVC4"
//   val execString = "/usr/local/bin/cvc4 --lang smt2 %s"
//   init()
// }

