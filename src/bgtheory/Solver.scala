package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
import datastructures._
import util._

/** Wraps the result of a call to `check()` so that unsat. cores can be returned */
sealed abstract class SolverResult

object SolverResult {
  /** Lift a Boolean to a SolverResult */
  def apply(isConsistent: Boolean): SolverResult =
    if (isConsistent) SAT else UNSAT(None)
}

case object SAT extends SolverResult
case class UNSAT(core: Option[List[Int]]) extends SolverResult
case object UNKNOWN extends SolverResult

/** Now that solver extends `Simplification` it has state, namely
 * which bgsimp level is currently set.*/
abstract class Solver extends Simplification {
  /** Used to refer to the solver from command line.*/
  val name: String

  /** The name of the theory handled by this solver, e.g. LIA */
  val theoryName: String 

  // /** Whether the solver natively supports unsatisfiable cores */
  // protected val canUNSATCore: Boolean 

  /** Quantifier elimination for clauses.
    * Assume `unabst` has been applied to cl.
    * @param cl A clause for which `hasSolverSignature` holds true.*/
  def QE(cl: ConsClause): List[ConsClause]

  /**
   *  Quantifier elimination
   *  @param cl A clause (for which hasSolverSignature does not neccessarily hold true).
   *  Eliminates all extraneous BG variables.
   *  We also assume unabst has been applied to cl.
   *  Used for optimizations only, hence is allowed to be implemented as the identity
   */

  /*
  def QEGeneral(cl: ConsClause): List[ConsClause]
   // Not used any more. If there is a pressing need could define it in terms of QE on formulas like this:
  def QEGeneral(cl: ConsClause): List[ConsClause] = {
    val extraVars = cl.BGLits.bgVars -- cl.nonBGLits.bgVars
    if (extraVars.isEmpty)
      List(cl)
    else {
      val f = QE(Exists(extraVars.toList, (cl.BGLits map { _.compl.toLiteral }).toAnd))
      val res = 
        for (conj ← cnfconversion.toDNF(f))
           yield Clause(cl.nonBGLits ::: (conj map { _.toLit.compl }), cl.idxRelevant, cl.age)
      // println("QEGeneral: given: " + cl)
      // res foreach { cl => println("QEGeneral: result: " + cl) }
      res
    }
  }
   */

  /** This tests whether the given clause is a member of the solver's
    * signature. Used to implement `isSolverClause`*/
  protected def hasSolverLiterals(cl: ConsClause): Boolean

  /** Converts into clauses of the sub-signature which the solver accepts.
    * Used to implement `asSolverClause`.
    * @param cl Must be a ground clause.*/
  protected def toSolverLiterals(cl: ConsClause): ConsClause

  /** True if `check` only accepts unit clauses as input.
    * In that case Splitting will apply to BG clauses. */
  val needsUnitClauses: Boolean

  /** Test if a given set of clauses is consistent.
   * Applies only to clauses for which isSolverClause holds true. 
   * In particular these clauses are ground, they are obtained by asSolverClauses*/
  def check(cls: Iterable[ConsClause]): SolverResult

  /** Find a minimal unsatisfiable subset of clauses if isConsistent (i.e. Solver.check())
    * does not return one.
    * @return a subset of clause ids such that this set of clauses is unsat
    * but every proper subset is sat.
    * If `cls` is satisfiable then the result is empty.*/
  def minUnsatCore(cls: Iterable[ConsClause]): List[Int] = {
    //TODO- could have timed cooper calls to avoid the situation where a few
    //clauses cause unsatisfiability, but the remainder takes a long time to
    //prove sat.
    if (check(cls) == SAT)
      List()
    else {
      util.Timer.muc.start()
      var i = 0
      //the working clause set
      //sorting by descending order of split level guarantees that the longest possible backtrack is found
      def safeMax(s: Set[Int]) = if (s.isEmpty) 0 else s.max
      var cs = cls.toList.sortWith((i1, i2) => safeMax(i1.idxRelevant) > safeMax(i2.idxRelevant))
      //removing a clause in checked results in a SAT set
      //because (cs_1 - checked) was sat and cs_n is a subset of cs_1, this means (cs_n - checked) is a subset of (cs_1 - checked)
      //therefore it is sat.
      var checked = Set.empty[ConsClause]
      while(i < cs.size) {
        if (checked(cs(i))) {
          //removing this will result in sat, but do not need to add to checked again
          i+=1
	} else if (check(cs.removeNth(i)) == UNSAT(None)) {
	  cs = cs.removeNth(i)
	  i = 0
	} else {
          //this resulted in a SAT set, but was not in checked already
          checked += cs(i)
          i+=1
        }
      }
      //if you get to here, no more removals possible

      util.Timer.muc.stop()
      //return the ids
      val res = cs.map(_.id).toList
      reporter.debug("*** min unsat. core of:")
      reporter.debug(cls map { _.id })
      reporter.debug("is "+res)
      // val uc = cls.filter(c => res.indexOf(c.id) >= 0)
      // println(s"***SANITY CHECK: ${!isConsistent(uc)}")

      res
    }
  }

  /** Whether `cl` can be sent to the BG reasoner.
    * Clauses must be ground BG and unit if required. The main check is
    * implemented in hasSolverLiterals. */
  def isSolverClause(cl: ConsClause) = {
    cl.isBG &&
    cl.isGround &&
      (!needsUnitClauses || cl.isUnitClause) &&
      hasSolverLiterals(cl)
  }

  /**
   * Given a background clause cl, compute a set of ground clauses
   * that is equivalent to cl over the background domain.
   * Moreover all literals are solver literals.
   * @return the set of ground clauses partitioned into a set that can directly be passed to the
   * BG solver and a set that cannot (because the solver needs unit clauses)
   */
  def asSolverClauses(cl: ConsClause): (List[ConsClause], List[ConsClause]) = {
    assume(cl.isBG, "asSolverClauses applied to non-background clause")

    if (isSolverClause(cl))
      (List(cl.withoutCons), List.empty)
    else {
      val hcl = cl.unabstrAggressive.withoutCons 
      // Eliminate the variables, if there are any
      // simplifyCheap is needed to eliminate FalseLit
      val groundCls = (if (hcl.isGround) List(hcl) else QE(hcl)) flatMap { _.simplifyCheap }

      // Convert the ground clauses into clauses over the proper sub-signature
      // The above QE step, as well as toSolverLiterals may result in a tautology,
      // in particular a clause containing the *FG* literal Lit.TrueLit,
      // thus turning a BG clause into a FG! We must avoid that.
      val withSolverLiterals = groundCls map {
        cl ⇒ (if (hasSolverLiterals(cl)) cl else toSolverLiterals(cl))
      } filterNot { _.isTautology }

      if (needsUnitClauses)
        (withSolverLiterals partition { _.length <= 1 })
      else
        (withSolverLiterals, List.empty)
    }
  }

}

/** A solver that can be used during preprocessing
  * It can only do evaluation of ground terms, including conversions like to_int, to_rat etc.
  * This way, clauses over multiple theories may collapse into clauses over one theory.
 */
object PreprocessingSolver extends Solver {
  val name = "PreProcSolver"
  val theoryName = "LRFIA"
  // val canUNSATCore = false
  // def hasSolverSignature(cl: ConsClause) = false
  def QE(cl: ConsClause) = List(cl)
  // def QE(f: Formula, params: Iterable[SymConst]) = f
  def hasSolverLiterals(cl: ConsClause) = true
  def toSolverLiterals(cl: ConsClause) = cl
  def check(cls: Iterable[ConsClause]) = UNKNOWN

  val needsUnitClauses = false

  val simpRulesTermSafe = 
    LRA.simpRules.simpRulesTermSafe :::
    LFA.simpRules.simpRulesTermSafe :::
    LIA.simpRules.simpRulesTermSafe 
  val simpRulesTermUnsafe = List.empty
  val simpRulesLitSafe = 
    LRA.simpRules.simpRulesLitSafe :::
    LFA.simpRules.simpRulesLitSafe :::
    LIA.simpRules.simpRulesLitSafe 
  val simpRulesLitUnsafe = List.empty
}

object EmptySolver extends Solver {
  val name = "empty"
  val theoryName = "empty"
  // val canUNSATCore = false
  // def hasSolverSignature(cl: ConsClause) = false
  def QE(cl: ConsClause) = List(cl)
  // def QE(f: Formula, params: Iterable[SymConst]) = f
  def hasSolverLiterals(cl: ConsClause) = true
  def toSolverLiterals(cl: ConsClause) = cl
  def check(cls: Iterable[ConsClause]) = UNKNOWN
  val needsUnitClauses = false

  val simpRulesTermSafe = List.empty
  val simpRulesTermUnsafe = List.empty
  val simpRulesLitSafe = List.empty
  val simpRulesLitUnsafe = List.empty
}

