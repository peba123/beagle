package beagle.bgtheory.LIA

import beagle._
import fol._
// import fol.Signature._
import datastructures._
import util._
import bgtheory._

object simpRules extends Simplification { 

  val simpRulesTermSafe: List[SimpRule[Term]] = List(
    new SimpRule[Term]("evalSum1", {
      case Sum(i1: DomElemInt, i2: DomElemInt) ⇒ DomElemInt(i1.value + i2.value)
      case _                                   ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalProduct", {
      case Product(i1: DomElemInt, i2: DomElemInt) ⇒ DomElemInt(i1.value * i2.value)
      case _                                       ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalDifference1", {
      case Difference(i1: DomElemInt, i2: DomElemInt) ⇒ DomElemInt(i1.value - i2.value)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalUMinus", {
      case UMinus(i1: DomElemInt) ⇒ DomElemInt(-i1.value)
      case _                      ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalToX", {
      case PFunTerm(ToIntOpInt, List(t)) if t.sort == LIA.IntSort ⇒ t
      case PFunTerm(ToRatOpInt, List(d: DomElemInt)) ⇒  LRA.DomElemRat(RatInt(d.value))
      case PFunTerm(ToRealOpInt, List(d: DomElemInt)) ⇒  LFA.DomElemReal(RatInt(d.value))
      case _                      ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalquotient", {
      case PFunTerm(QuotientEOpInt, List(i1: DomElemInt, i2: DomElemInt)) if i2.value != 0 ⇒ {
        // Notice that Scala division is *not* Euclidean division: -7 / 3 = -2 r -1 
        // Euclidean division gives -7 / 3 = -3 r 2 
        val (h,r) = (i1.value / i2.value, i1.value % i2.value)
        if (r<0) 
          if (i2.value < 0) 
            DomElemInt(h+1) // remainder is r-i2.value
          else
            DomElemInt(h-1) // remainder is r+i2.value
        else
            DomElemInt(h)
      }
      case _                      ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalremainder", {
      case PFunTerm(RemainderEOpInt, List(i1: DomElemInt, i2: DomElemInt)) if i2.value != 0 ⇒ {
        // Notice that Scala division is *not* Euclidean division: -7 / 3 = -2 r -1 
        // Euclidean division gives -7 / 3 = -3 r 2 
        val (h,r) = (i1.value / i2.value, i1.value % i2.value)
        if (r<0) 
          if (i2.value < 0) 
            DomElemInt(r-i2.value)
          else
            DomElemInt(r+i2.value)
        else
            DomElemInt(r)
      }
      case _                      ⇒ throw NotApplicable
    })
  )

  val simpRulesTermUnsafe: List[SimpRule[Term]] = List(

    new SimpRule[Term]("orientSum", {
      case Sum(t: Term, i: DomElemInt) if !t.isDomElem ⇒ Sum(i, t)
      case _                           ⇒ throw NotApplicable
    }),
    new SimpRule[Term]("orientDifference", {
      case Difference(t: Term, i: DomElemInt) if !t.isDomElem ⇒ Sum(DomElemInt(-i.value), t)
      case _                                  ⇒ throw NotApplicable
    }),
    new SimpRule[Term]("orientProduct", {
      case Product(t: Term, i: DomElemInt) if !t.isDomElem⇒ Product(i, t)
      case _                               ⇒ throw NotApplicable
    }),

    // todo: more rules
    new SimpRule[Term]("evalSum2", {
      case Sum(i1: DomElemInt, Sum(i2: DomElemInt, t)) ⇒ Sum(DomElemInt(i1.value + i2.value), t)
      case Sum(i1: DomElemInt, Difference(i2: DomElemInt, t)) ⇒ Difference(DomElemInt(i1.value + i2.value), t)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("assoc", {
      case Sum(Sum(s: Term, t: Term), u: Term) ⇒ Sum(s, Sum(t, u))
      case Sum(Difference(s: Term, t: Term), u: Term) ⇒ Sum(s, Difference(u, t))
      case Difference(Sum(s: Term, t: Term), u: Term) ⇒ Sum(s, Difference(t, u))
      case Difference(Difference(s: Term, t: Term), u: Term) ⇒ Difference(s, Sum(t, u))
      case Sum(s: Term, Sum(i: DomElemInt, t)) if !s.isDomElem ⇒ Sum(i, Sum(s, t))
      case Sum(s: Term, Difference(i: DomElemInt, t)) if !s.isDomElem ⇒ Sum(i, Difference(s, t))
      case Difference(s: Term, Sum(i: DomElemInt, t)) if !s.isDomElem ⇒ Sum(UMinus(i), Difference(s, t))
      case Difference(s: Term, Difference(i: DomElemInt, t)) if !s.isDomElem ⇒ Sum(UMinus(i), Sum(s, t))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalDifference2", {
      case Difference(i1: DomElemInt, Difference(i2: DomElemInt, t)) ⇒ Sum(DomElemInt(i1.value - i2.value), t)
      case Difference(i1: DomElemInt, Sum(i2: DomElemInt, t)) ⇒ Difference(DomElemInt(i1.value - i2.value), t)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalUMinus", {
      case UMinus(UMinus(t)) ⇒ t
      case _                        ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalNeutral", {
      case Difference(t: Term, ZeroInt)    ⇒ t
      case Sum(ZeroInt, t: Term)    ⇒ t
      case Sum(t: Term, ZeroInt)    ⇒ t
      case Product(OneInt, t: Term) ⇒ t
      case Product(t: Term, OneInt) ⇒ t
      case _                        ⇒ throw NotApplicable
    }))

  // todo: simplification rules for literals that play well together with canonicalisation of
  // terms. 

  // Rules to simplify literals
  val simpRulesLitSafe: List[SimpRule[Lit]] = List(
    new SimpRule[Lit]("evalLess", {
      case Lit(true, LessEqn(d1: DomElemInt, d2: DomElemInt)) ⇒
        if (d1.value < d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, LessEqn(d1: DomElemInt, d2: DomElemInt)) ⇒
        if (!(d1.value < d2.value)) Lit.TrueLit else Lit.FalseLit
      case Lit(sign, LessEqn(t1, t2)) if (t1 == t2) =>
	if(sign) Lit.FalseLit else Lit.TrueLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalLessEq", {
      case Lit(true, LessEqEqn(d1: DomElemInt, d2: DomElemInt)) ⇒
        if (d1.value <= d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, LessEqEqn(d1: DomElemInt, d2: DomElemInt)) ⇒
        if (!(d1.value <= d2.value)) Lit.TrueLit else Lit.FalseLit
      //removes tautologies like (X <= X)
      case Lit(sign, LessEqEqn(t1, t2)) if (t1==t2) =>
	if(sign) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalEqual", {
      case Lit(true, Eqn(d1: DomElemInt, d2: DomElemInt)) ⇒
        if (d1.value == d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, Eqn(d1: DomElemInt, d2: DomElemInt)) ⇒
        if (d1.value != d2.value) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalDivides", {
      case Lit(true, DividesEqn(d1: DomElemInt, d2: DomElemInt)) if d1.value != 0 ⇒
        if ((d2.value % d1.value) == 0) Lit.TrueLit else Lit.FalseLit
      case Lit(false, DividesEqn(d1: DomElemInt, d2: DomElemInt)) if d1.value != 0 ⇒
        if ((d2.value % d1.value) != 0) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    // ints can always be coerced into ints, rats and reals
    // Here are the rules where the arguments are integer-sorted
    new SimpRule[Lit]("evalIsX", {
      case Lit(t, PredEqn(PFunTerm(op, List(s)))) if 
        s.sort == LIA.IntSort && (op == IsIntOpInt || op == IsRatOpInt || op == IsRealOpInt) =>
	if (t) Lit.TrueLit else Lit.FalseLit
      case _ => throw NotApplicable
    })
  )

  val simpRulesLitUnsafe: List[SimpRule[Lit]] = List(

    // This rule gets rid of all >-literals,  <-literals, and positive ≤-literals, 
    // in terms of negative ≤-literals.
    // This is effective in particular for the GEG-problems.
    new SimpRule[Lit]("NormalizeInequality", {
      case Lit(isPos, GreaterEqn(d1, d2)) ⇒ Lit(isPos, LessEqn(d2, d1))
      case Lit(isPos, GreaterEqEqn(d1, d2)) ⇒ Lit(isPos, LessEqEqn(d2, d1))
        // NOt good for DAT043=1:
        // This doesnt hurt the GEG problems, but some SWW problems become unsolved
       // case Lit(false, LessEqn(d1, d2)) ⇒ Lit(true, LessEqEqn(d2, d1))
       // case Lit(true, LessEqn(d1, d2)) ⇒ Lit(false, LessEqn(d2, Sum(OneInt, d1)))
    // This one is bad for some SWW problems, e.g. SWW038=1
      // case Lit(false, LessEqn(d1, d2)) ⇒ Lit(false, LessEqEqn(Sum(d1, OneInt), d2))
        // This rule is important for the GEG problems, and can solve DAT043=1
        // GOOD:
      case Lit(true, LessEqEqn(d1, d2)) ⇒ Lit(false, LessEqEqn(Sum(d2, OneInt), d1))
      // case Lit(true, LessEqEqn(d1, d2)) ⇒ Lit(false, LessEqn(d2, d1))
      // case Lit(false, LessEqEqn(d1, d2)) ⇒ Lit(true, LessEqn(d2, d1))
        // Bad again for the GEG problems:
      // case Lit(isPos, LessEqEqn(d1, d2)) ⇒ Lit(!isPos, LessEqn(d2, d1))
      case _ ⇒ throw NotApplicable
    }),

    // Eliminate everything in favor of negative <-literals
/*    new SimpRule[Lit]("NormalizeInequality", {
      case Lit(isPos, GreaterEqn(d1, d2)) ⇒ Lit(isPos, LessEqn(d2, d1))
      case Lit(isPos, GreaterEqEqn(d1, d2)) ⇒ Lit(isPos, LessEqEqn(d2, d1))
      case Lit(isPos, LessEqEqn(d1, d2)) ⇒ Lit(isPos, LessEqn(d1, Sum(d2, OneInt)))
        // Bad for GEG:
      // case Lit(true, LessEqn(d1, d2)) ⇒ Lit(false, LessEqn(d2, Sum(d1, OneInt)))
      case _ ⇒ throw NotApplicable
    }),
 */

    // Incomplete
    new SimpRule[Lit]("sumDiffEqual1", {
      case Lit(isPos, Eqn(Sum(d1: DomElemInt, t), d2: DomElemInt)) ⇒ Lit(isPos, Eqn(t, DomElemInt(d2.value - d1.value)))
      case Lit(isPos, Eqn(Difference(d1: DomElemInt, t), d2: DomElemInt)) ⇒ Lit(isPos, Eqn(t, DomElemInt(d1.value - d2.value)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumDiffEqual2", {
      case Lit(isPos, Eqn(Sum(d1: DomElemInt, t1), Sum(d2: DomElemInt, t2))) ⇒ Lit(isPos, Eqn(t1, Sum(DomElemInt(d2.value - d1.value), t2)))
      case Lit(isPos, Eqn(Difference(d1: DomElemInt, t1), Difference(d2: DomElemInt, t2))) ⇒ Lit(isPos, Eqn(t1, Sum(DomElemInt(d1.value - d2.value), t2)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorEqual1", {
      case Lit(isPos, Eqn(Product(d1: DomElemInt, t), d2: DomElemInt)) if (d1.value != 0 && d2.value % d1.value == 0) ⇒ Lit(isPos, Eqn(t, DomElemInt(d2.value / d1.value)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorEqual2", {
      case Lit(isPos, Eqn(Product(d1: DomElemInt, t1), Product(d2: DomElemInt, t2))) if (d1.value != 0 && d2.value % d1.value == 0) ⇒ Lit(isPos, Eqn(t1, Product(DomElemInt(d2.value / d1.value), t2)))
      case Lit(isPos, Eqn(Product(d1: DomElemInt, t1), Product(d2: DomElemInt, t2))) if (d2.value != 0 && d1.value % d2.value == 0) ⇒ Lit(isPos, Eqn(Product(DomElemInt(d1.value / d2.value), t1), t2))
      case _ ⇒ throw NotApplicable
    }),

    // Incomplete list
    new SimpRule[Lit]("factorLessEq", {
      // Need xor (d1.value < 0) to flip the orientation of the inequality if we divide by something negative
      case Lit(isPos, LessEqEqn(Product(d1: DomElemInt, t), d2: DomElemInt)) if (d1.value != 0 && d2.value % d1.value == 0) ⇒ Lit(isPos, LessEqEqn(t, DomElemInt(d2.value / d1.value)).swapIfNeg(d1.value.signum))
      case Lit(isPos, LessEqEqn(d2: DomElemInt, Product(d1: DomElemInt, t))) if (d1.value != 0 && d2.value % d1.value == 0) ⇒ Lit(isPos, LessEqEqn(DomElemInt(d2.value / d1.value), t).swapIfNeg(d1.value.signum))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorLess", {
      case Lit(isPos, LessEqn(Product(d1: DomElemInt, t), d2: DomElemInt)) if (d1.value != 0 && d2.value % d1.value == 0) ⇒ Lit(isPos, LessEqn(t, DomElemInt(d2.value / d1.value)).swapIfNeg(d1.value.signum))
      case Lit(isPos, LessEqn(d2: DomElemInt, Product(d1: DomElemInt, t))) if (d1.value != 0 && d2.value % d1.value == 0) ⇒ Lit(isPos, LessEqn(DomElemInt(d2.value / d1.value), t).swapIfNeg(d1.value.signum))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumLessEq", {
      case Lit(isPos, LessEqEqn(Sum(d1: DomElemInt, t), d2: DomElemInt)) ⇒ Lit(isPos, LessEqEqn(t, DomElemInt(d2.value - d1.value)))
      case Lit(isPos, LessEqEqn(d2: DomElemInt, Sum(d1: DomElemInt, t))) ⇒ Lit(isPos, LessEqEqn(DomElemInt(d2.value - d1.value), t))
      case Lit(isPos, LessEqEqn(Sum(d1: DomElemInt, t), Sum(d2: DomElemInt, s))) ⇒ Lit(isPos, LessEqEqn(t, Sum(DomElemInt(d2.value - d1.value), s)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumLess", {
      case Lit(isPos, LessEqn(Sum(d1: DomElemInt, t), d2: DomElemInt)) ⇒ Lit(isPos, LessEqn(t, DomElemInt(d2.value - d1.value)))
      case Lit(isPos, LessEqn(d2: DomElemInt, Sum(d1: DomElemInt, t))) ⇒ Lit(isPos, LessEqn(DomElemInt(d2.value - d1.value), t))
      case _ ⇒ throw NotApplicable
    }), // todo: similar rules for difference

    new SimpRule[Lit]("cancel", {
      case Lit(isPos, Eqn(Sum(t, s1), Sum(t1, s2))) if t == t1 ⇒ Lit(isPos, Eqn(s1, s2))
      case Lit(isPos, Eqn(Sum(s1, t), Sum(s2, t1))) if t == t1 ⇒ Lit(isPos, Eqn(s1, s2))
      case Lit(isPos, Eqn(Sum(s1, t), t1)) if t == t1 ⇒ Lit(isPos, Eqn(s1, ZeroInt))
      case Lit(isPos, Eqn(Sum(t, s1), t1)) if t == t1 ⇒ Lit(isPos, Eqn(s1, ZeroInt))
      case Lit(isPos, Eqn(Difference(t, s1), Difference(t1, s2))) if t == t1 ⇒ Lit(isPos, Eqn(s1, s2))
      case Lit(isPos, Eqn(Difference(s1, t), Difference(s2, t1))) if t == t1 ⇒ Lit(isPos, Eqn(s1, s2))
      case Lit(isPos, Eqn(Difference(t, s1), t1)) if t == t1 ⇒ Lit(isPos, Eqn(s1, ZeroInt))
      case _ ⇒ throw NotApplicable
    }),
/*  //solves exactly 1 new problem!
    new SimpRule[Lit]("sillyMultCancel", {
      case Lit(isPos, Eqn(Product(x, y), z)) if (x==z) => Lit(isPos, Eqn(y, DomElemInt(1)))
      case Lit(isPos, Eqn(Product(x, y), z)) if (y==z) => Lit(isPos, Eqn(x, DomElemInt(1)))
      case Lit(isPos, Eqn(z, Product(x,y))) if (z == x) ⇒ Lit(isPos, Eqn(y, DomElemInt(1)))
      case Lit(isPos, Eqn(z, Product(x,y))) if (z == y) ⇒ Lit(isPos, Eqn(x, DomElemInt(1)))
      case Lit(isPos, Eqn(Product(x, y), Product(w,z))) if x == w ⇒ Lit(isPos, Eqn(y, z))
      case Lit(isPos, Eqn(Product(x, y), Product(w,z))) if x == z ⇒ Lit(isPos, Eqn(y, w))
      case Lit(isPos, Eqn(Product(x, y), Product(w,z))) if y == w ⇒ Lit(isPos, Eqn(x, z))
      case Lit(isPos, Eqn(Product(x, y), Product(w,z))) if y == z ⇒ Lit(isPos, Eqn(x, w))
      case _ ⇒ throw NotApplicable
    }),
*/
    new SimpRule[Lit]("specialHackl", {
      case Lit(isPos, Eqn(Sum(Product(c1, s1), t1), Sum(Product(c2, t2), s2) )) if c1 == c2 && s1 == s2 && t1 == t2 ⇒ Lit(isPos, Eqn(s1, t1))
      case _ ⇒ throw NotApplicable
    })

    )
}



