package beagle.bgtheory.LIA.iQE

import beagle._
//import fol._
//import util._
import datastructures.ConsClause
//import bgtheory._
import bgtheory.LIA.LIASolver

object Solver extends LIASolver {
  val name = "iQE"
  // Buggy!
  // val canUNSATCore = true
  // val canUNSATCore = false
  def hasSolverLiterals(cl: ConsClause) = true
  def toSolverLiterals(cl: ConsClause) = cl
  def check(cls: Iterable[ConsClause]) = {
    // for (cl <- cls) {
    //   println("cl:            " + cl)
    //   println("cl.isEmpty     " + cl.isEmpty)
    //   println("cl.asQEClause: " + cl.asQEClause)
    //   println("cl.asQEClause: " + cl.asQEClause.get)
    // }
    // val cooperRes = toSolverResult(cooper.isConsistent(cls))
    // println("before isConsistent")
    val iQERes = iQE.isConsistent(cls map { _.asQEClause.get })
    // println("iQERes = " + iQERes)

    // if (iQERes != cooperRes) {
    //   println("Internal error: diverging results in BG solvers:")
    //   println("\nCooper says " + cooperRes)
    //   println("iQE says    " + iQERes)
    //   println("Clause set:")
    //   cls foreach {
    //     println(_)
    //   }
    //   sys.exit()
    // }

    // println()
    // cooperRes
    iQERes
  }
  val needsUnitClauses = false
}

