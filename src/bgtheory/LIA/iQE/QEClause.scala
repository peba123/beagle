package beagle.bgtheory.LIA.iQE

import beagle._
import fol._
import util._
import bgtheory._
import LIA._

case class QEClause(lits: List[QEAtom]) {

  lazy val length = lits.length
  val isEmpty = lits.isEmpty
  val isUnit = !isEmpty && lits.tail.isEmpty
  def apply(i: Int) = lits(i)
  def subsumes(cl: QEClause) =
    // just unit subsumption for now
    (isUnit && (cl.lits exists { lits(0) subsumes _ }))

  // Unique identification of this
  // val id = QEClause.idCtr.next()

  lazy val vars =
    lits.foldLeft(Set.empty[VarOrSymConst]) {  _ + _.variable }
  // The maximal among all variables
  lazy val maxVar = {
    assume(!isEmpty)
    var soFar = lits.head.variable
    for (el <- lits.tail)
      if (el.variable > soFar)
        soFar = el.variable
    soFar
  }

  def betterThan(that: QEClause) = {
    val res = 
      (this.lits, that.lits) match {
        case (List(False(thisIdRel, _)), List(False(thatIdRel, _))) => thisIdRel.size < thatIdRel.size
        case (List(_: False), _) => true // A False clause is better than any other non-False clause
        case (_, List(_: False)) => false 
        case (List(eq: EQ), _ :: _ :: _) if eq.coeff == 1 => true // EQ unit clause better than non-unit clause
        case (_ :: _ :: _, List(eq: EQ)) if eq.coeff == 1 => false
        case (List(eq: EQ), l :: _) if eq.coeff == 1 && !l.isInstanceOf[EQ] => true // EQ clause better than any non-EQ unit clause
        case (l :: _, List(eq: EQ)) if eq.coeff == 1 && !l.isInstanceOf[EQ] => false
        case (_, _) => (that.maxVar > this.maxVar)
      }
    // println(s"$this betterThan $that = $res")
    res
  }
  override def toString = lits.toMyString("□", "", " ∨ ", "")

}

object QEClause {
  def apply(lits: QEAtom*) = new QEClause(lits.toList)
  // def apply(l1: QEAtom) = new QEClause(List(l1))
  // def apply(l1: QEAtom, l2: QEAtom) = new QEClause(List(l1, l2))

//  val idCtr = new util.Counter()

}

