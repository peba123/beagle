package beagle.bgtheory.LIA.iQE

import beagle._
import fol._
import datastructures._
import util._
import bgtheory.LIA._


abstract class QEAtom {
  def toLit: Lit
  val pivot: Monomial
  val relNodes: Set[Int] // The relevant nodes to derive this (for backtracking)
  val relClauses: Set[Int] // The relevant causes to derive this (for unsatisfiable cores)

  def added(newRelNodes: Set[Int]): QEAtom
  def subsumes(that: QEAtom): Boolean

  // derived
  def variable = pivot.x
  def coeff = pivot.c

  override def toString = toLit.toString //+ " " + relClauses.mkString("{", ", ", "}")
}


case class LB(s: Polynomial, pivot: Monomial, relNodes: Set[Int], relClauses: Set[Int]) extends QEAtom {
  def toLit = LessEqn(s.toTerm, pivot.toTerm).toLit
  def added(newRelNodes: Set[Int]) = LB(s, pivot, relNodes ++ newRelNodes, relClauses)
  def subsumes(that: QEAtom) = {
    val Monomial(a, x) = pivot
    that match {
      case LB(s1, Monomial(a1, x1), _, _) if x == x1 =>
        // s < a * x subsumes s1 < a1 * x1 iff
        // a1 * s < a * a1 * x subsumes a * s1 < a * a1 * x1 iff
        // a * s1 =< a1 * s (=< for reflexive subsumption relation)
        (s * a1 + 1) gtr (s1 * a)
      case _ => false
    }
  }

  /**
    * { 3 < x, 4x < 19, 3x < 17 }
    *  resolve to
    * (3+1)*4 < 19, (3+1)*3 < 17
    */

  /**
    *  resolve s < x and bx < t to b(s+1) < t
    */
  def resolve(ub: UB) = {
    val Monomial(a, x) = pivot
    val UB(Monomial(b, x1), t, relNodesUb, relClausesUb) = ub
    assume(x == x1)
    assume(a == 1)
    // From s < x and bx < t derive b(s + 1) < t, i.e. 0 < t - b(s + 1)
    QEAtom.zeroLTPolyToQEAtom(t - (s + 1) * b, relNodes ++ relNodesUb, relClauses ++ relClausesUb)
  }
}


case class UB(pivot: Monomial, t: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) extends QEAtom {
  def toLit = LessEqn(pivot.toTerm, t.toTerm).toLit
  def added(newRelNodes: Set[Int]) = UB(pivot, t, relNodes ++ newRelNodes, relClauses)
  def subsumes(that: QEAtom) = {
    val Monomial(b, x) = pivot
    that match {
      case UB(Monomial(b1, x1), t1, _, _) if x == x1 =>
          // b * x < t subsumes b1 * x < t1 iff
          // b * b1 * x < b1 * t subsumes b * b1 * x < b * t1 iff
          // b1 * t =< b * t1
          (t1 * b + 1) gtr (t * b1)
      case _ => false
    }
  }
}

case class DIV(k: Int, pivot: Monomial, rest: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) extends QEAtom {
  assume(k > 0)
  assume(pivot.c > 0)
  def toLit = DividesEqn(DomElemInt(k), (rest + pivot).toTerm).toLit
  def added(newRelNodes: Set[Int]) = DIV(k, pivot, rest, relNodes ++ newRelNodes, relClauses)
  def subsumes(that: QEAtom) = {
    val Monomial(a, x) = pivot
    that match {
      case DIV(k1, Monomial(a1, x1), rest1, _, _) if x == x1 =>
        k == k1 && a == a1 && rest == rest1 // could be improved
      case _ => false
    }
  }
}

case class DIVNot(k: Int, pivot: Monomial, rest: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) extends QEAtom {
  assume(k > 0)
  assume(pivot.c > 0)
  def toLit = DividesEqn(DomElemInt(k), (rest + pivot).toTerm).toNegLit
  def added(newRelNodes: Set[Int]) = DIVNot(k, pivot, rest, relNodes ++ newRelNodes, relClauses)
  def subsumes(that: QEAtom) = {
    val Monomial(a, x) = pivot
    that match {
      case DIVNot(k1, Monomial(a1, x1), rest1, _, _) if x == x1 =>
        k == k1 && a == a1 && rest == rest1 // could be improved
      case _ => false
    }
  }
}

object TrivialDIV {
  // Make a trivial DIV 1|x
  def apply(x: VarOrSymConst) =
    new DIV(1, Monomial(1, x), Polynomial(List.empty, 0, List.empty), Set.empty, Set.empty)

  def unapply(a: QEAtom): Option[VarOrSymConst] =
    a match {
      case DIV(1, Monomial(1, x), Polynomial(Nil, 0, Nil), relNodes, relClauses) if relNodes.isEmpty && relClauses.isEmpty => Some(x)
      case _ => None
    }
}


case class EQ(pivot: Monomial, s: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) extends QEAtom {
  def toLit = Eqn(pivot.toTerm, s.toTerm).toLit
  def added(newRelNodes: Set[Int]) = EQ(pivot, s, relNodes ++ newRelNodes, relClauses)
  def subsumes(that: QEAtom) = {
    val Monomial(a, x) = pivot
    that match {
      case EQ(Monomial(a1, x1), s1, _, _) if x == x1 =>
        a == a1 && s == s1
      // could also subsume inequations
      case _ => false
    }
  }
}

case class NE(pivot: Monomial, s: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) extends QEAtom {
  def toLit = Eqn(pivot.toTerm, s.toTerm).toNegLit
  def added(newRelNodes: Set[Int]) = NE(pivot, s, relNodes ++ newRelNodes, relClauses)
  def toLB = LB(s, pivot, relNodes, relClauses)
  def subsumes(that: QEAtom) = {
    val Monomial(a, x) = pivot
    that match {
      case NE(Monomial(a1, x1), s1, _, _) if x == x1 =>
        a == a1 && s == s1
      case _ => false
    }
  }
}

case class True(relNodes: Set[Int], relClauses: Set[Int]) extends QEAtom {
  def toLit = Lit.TrueLit
  val pivot = Monomial(0, SymConst(Operator(BG, "", Arity0(IntSort))))
  def added(newRelNodes: Set[Int]) = True(relNodes ++ newRelNodes, relClauses)
  def subsumes(that: QEAtom) = that.isInstanceOf[True]
}

case class False(relNodes: Set[Int], relClauses: Set[Int]) extends QEAtom {
  def toLit = Lit.FalseLit
  val pivot = Monomial(0, SymConst(Operator(BG, "", Arity0(IntSort))))
  def added(newRelNodes: Set[Int]) = False(relNodes ++ newRelNodes, relClauses)
  def subsumes(that: QEAtom) = true
}

object QEAtom {

  /** zeroEQPolyToQEAtom: take a polynomial p as 0=p and turn into a QEAtom
    */
  def zeroEQPolyToQEAtom(p: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) = {

    // Check for trivial cases first
    p match {
      case Polynomial(Nil, k, Nil) =>
        if (k == 0) True(relNodes, relClauses) else False(relNodes, relClauses)
      case p => {
        val (pivot, other) = p.factorize.separatePivot
        if (pivot.c < 0)
          EQ(pivot * -1, other, relNodes, relClauses)
        else
          EQ(pivot, other * -1, relNodes, relClauses)
      }
    }
  }

  /** zeroNEPolyToQEAtom: take a polynomial p as 0/=p and turn into a QEAtom
    */
  def zeroNEPolyToQEAtom(p: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) = {

    // Check for trivial cases first
    p match {
      case Polynomial(Nil, k, Nil) =>
        if (k == 0) False(relNodes, relClauses) else True(relNodes, relClauses)
      case p => {
        val (pivot, other) = p.factorize.separatePivot
        // println(" xx " + (p,pivot, other))
        if (pivot.c < 0)
          NE(pivot * -1, other, relNodes, relClauses)
        else
          NE(pivot, other * -1, relNodes, relClauses)
      }
    }
  }


  /** zeroLTPolyToQEAtom: take a polynomial p as 0<p and turn into a QEAtom
    */
  def zeroLTPolyToQEAtom(p: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) = {

    // convert a polynomial with a separated pivot into the proper UB or LB atom
    def h(pivotAndOther: (Monomial, Polynomial)) = {
      val (pivot, other) = pivotAndOther
      if (pivot.c < 0)
        UB(pivot * -1, other, relNodes, relClauses)
      else
        LB(other * -1, pivot, relNodes, relClauses)
    }

    // Check for trivial cases first
    p match {
      case Polynomial(Nil, k, Nil) =>
        if (k > 0) True(relNodes, relClauses) else False(relNodes, relClauses)
      // Try to factorize as much as possible
      case p @ Polynomial(_, 0, Nil) => h(p.factorize.separatePivot)
      case p @ Polynomial(_, 1, Nil) => h(((p - 1).factorize + 1).separatePivot)
      case p @ Polynomial(_, -1, Nil) => h(((p - 1).factorize + 1).separatePivot)
      case p @ Polynomial(_, k, Nil) => {
        // try both:
        val h1 = p.factorize
        val h2 = ((p - 1).factorize + 1).factorize // todo: import final factorization into main branch
        var best = p
        if (math.abs(h1.k) < math.abs(best.k)) best = h1
        if (math.abs(h2.k) < math.abs(best.k)) best = h2
        h(best.separatePivot)
      }
    }
  }

  /** nDivPolyToQEAtom: take an integer n and polynomial p as n|p and turn into a QEAtom
    */
  def nDivPolyToQEAtom(n: Int, p: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) = {
    assume(p.ims.isEmpty)
    // Take remainders
    val msRem = p.ms flatMap {
      case Monomial(c, x) =>
        if (c % n == 0) List.empty else List(Monomial(c % n, x))
    }
    val pRem = Polynomial(msRem, p.k % n, Nil)
    // println(s"(n = $n, p = $p, pRem = $pRem")
    pRem match {
      case Polynomial(Nil, k, Nil) =>
        // Constant case
        if ((k % n) == 0) True(relNodes, relClauses) else False(relNodes, relClauses)
      case p => {
        // Undecided case. Make sure n is positive and as small as possible
        val g = gcd(math.abs(n), p.gcdCoeffs) // p.gcdCoeffs is positive
        // Normalize n and p by dividing by g
        val (normn, normp) = if (n < 0) (n / -g, p / -g) else (n / g, p / g)
        if (normn == 1) True(relNodes, relClauses) else {
          val (pivot, other) = normp.separatePivot
          // make coefficient of pivot non-negative
          if (pivot.c < 0) DIV(normn, pivot * -1, other * -1, relNodes, relClauses) else DIV(normn, pivot, other, relNodes, relClauses)
        }
      }
    }
  }

    /** nDivPolyToQEAtom: take an integer n and polynomial p as n|p and turn into a QEAtom
    */
  def nNotDivPolyToQEAtom(n: Int, p: Polynomial, relNodes: Set[Int], relClauses: Set[Int]) = {

    p match {
      case Polynomial(Nil, k, Nil) =>
        // Constant case
        if ((k % n) == 0) False(relNodes, relClauses) else True(relNodes, relClauses)
      case p => {
        // Undecided case. Make sure n is positive and as small as possible
        val g = gcd(math.abs(n), p.gcdCoeffs) // p.gcdCoeffs is positive
        // Normalize n and p by dividing by g
        val (normn, normp) = if (n < 0) (n / -g, p / -g) else (n / g, p / g)
        if (normn == 1) False(relNodes, relClauses) else {
          val (pivot, other) = normp.separatePivot
          // make coefficient of pivot non-negative
          if (pivot.c < 0) DIVNot(normn, pivot * -1, other * -1, relNodes, relClauses) else DIVNot(normn, pivot, other, relNodes, relClauses)
        }
      }
    }
  }

  /** Make a QEAtom from a BG literal 
    * clauseId is the id of the clause the literal comes from
    */

  def apply(l: Lit, clauseId: Int) = {
    assume(l.isBG, "QEAtom: applied to non-BG literal " + l)
    l match {
      case Lit.TrueLit => True(Set.empty, Set(clauseId))
      case Lit.FalseLit => False(Set.empty, Set(clauseId))
      // s < t ≡ 0 < t-s
      case Lit(true, LessEqn(s, t)) ⇒ zeroLTPolyToQEAtom(t.asPolynomial - s.asPolynomial, Set.empty, Set(clauseId))
      // s ≤ t ≡ 0 ≤ t-s ≡ 0 < (t+1)-s
      case Lit(true, LessEqEqn(s, t)) ⇒ zeroLTPolyToQEAtom((t.asPolynomial + 1) - s.asPolynomial, Set.empty, Set(clauseId))
      // s > t ≡ t < s ≡ 0 < s-t
      case Lit(true, GreaterEqn(s, t)) ⇒ zeroLTPolyToQEAtom(s.asPolynomial - t.asPolynomial, Set.empty, Set(clauseId))
      // s ≥ t ≡ t ≤ s ≡ 0 ≤ s-t ≡ 0 < (s+1)-t
      case Lit(true, GreaterEqEqn(s, t)) ⇒ zeroLTPolyToQEAtom((s.asPolynomial + 1) - t.asPolynomial, Set.empty, Set(clauseId))
        // s|t we need this case because asSolverClauses might have introduced (positive) divisibility constraints
        // by quantifier elimination
      case Lit(true, DividesEqn(s: DomElemInt, t)) ⇒ nDivPolyToQEAtom(s.value, t.asPolynomial, Set.empty, Set(clauseId))
      // s = t ≡ 0 = t-s
      case Lit(true, Eqn(s, t)) ⇒ zeroEQPolyToQEAtom(t.asPolynomial - s.asPolynomial, Set.empty, Set(clauseId))

      // ¬(s < t) ≡ s ≥ t ≡ 0 < (s+1)-t
      case Lit(false, LessEqn(s, t)) ⇒ zeroLTPolyToQEAtom((s.asPolynomial + 1) - t.asPolynomial, Set.empty, Set(clauseId))
      // ¬(s ≤ t) ≡ s > t ≡ 0 < s-t
      case Lit(false, LessEqEqn(s, t)) ⇒ zeroLTPolyToQEAtom(s.asPolynomial - t.asPolynomial, Set.empty, Set(clauseId))
        // ¬(s > t) ≡ s ≤ t ≡ 0 < (t+1)-s
      case Lit(false, GreaterEqn(s, t)) ⇒ zeroLTPolyToQEAtom((t.asPolynomial + 1) - s.asPolynomial, Set.empty, Set(clauseId))
        // ¬(s ≥ t) ≡ s < t ≡ 0 < t-s
      case Lit(false, GreaterEqEqn(s, t)) ⇒ zeroLTPolyToQEAtom(t.asPolynomial - s.asPolynomial, Set.empty, Set(clauseId))
      // s ≠ t ≡ 0 ≠ t-s
      case Lit(false, DividesEqn(s: DomElemInt, t)) ⇒ nNotDivPolyToQEAtom(s.value, t.asPolynomial, Set.empty, Set(clauseId))
      case Lit(false, Eqn(s, t)) ⇒ zeroNEPolyToQEAtom(t.asPolynomial - s.asPolynomial, Set.empty, Set(clauseId))
      case _ => throw InternalError("QEAtom: don't know how to convert " + l + " to a QEAtom")
    }
  }

}
