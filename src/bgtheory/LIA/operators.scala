package beagle.bgtheory.LIA

import beagle._
import fol.Term
import fol.PFunTerm
import fol.Operator
import fol.DomElem
import fol.BG
import fol.Arity0
import datastructures.PredEqn
import datastructures.Eqn

case class DomElemInt(i: Int) extends DomElem[Int] {
  val h = this
  lazy val op = new Operator(BG, i.toString, Arity0Int) {
    override def toTerm = Some(h)
  }
  val value = i
  // doesn't work, cf ARI621=1.p
  // override val weightForKBO = value.toString.length
}

object LessEqEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == IntSort && rhs.sort == IntSort)
    PredEqn(PFunTerm(LessEqOpInt, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(LessEqOpInt, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object LessEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == IntSort && rhs.sort == IntSort)
    PredEqn(PFunTerm(LessOpInt, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(LessOpInt, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == IntSort && rhs.sort == IntSort)
    PredEqn(PFunTerm(GreaterEqOpInt, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(GreaterEqOpInt, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == IntSort && rhs.sort == IntSort)
    PredEqn(PFunTerm(GreaterOpInt, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(GreaterOpInt, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object DividesEqn {
  def apply(s: Term, t: Term) = PredEqn(PFunTerm(DividesOpInt, List(s, t)))      
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(DividesOpInt, List(s, t))) => Some((s, t))
    case _ => None
  }
}

object UMinus {
  def apply(s: Term) = PFunTerm(UMinusOpInt, List(s))

  def unapply(s: Term) = s match {
    case PFunTerm(UMinusOpInt, List(t)) ⇒ Some(t)
    case _                                        ⇒ None
  }
}

object Sum {
  def apply(s1: Term, s2: Term) = PFunTerm(SumOpInt, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(SumOpInt, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Difference {
  def apply(s1: Term, s2: Term) = PFunTerm(DifferenceOpInt, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(DifferenceOpInt, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Product {
  def apply(s1: Term, s2: Term) = PFunTerm(ProductOpInt, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(ProductOpInt, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}
