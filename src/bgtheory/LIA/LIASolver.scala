package beagle.bgtheory.LIA

import beagle._
import fol._
import fol.Signature._
import datastructures._
import calculus._
import util._
import bgtheory._

/**
 * Linear Integer Arithmetic
 * Provides methods for quantifier elimination, consistency checking and simplification.
 */

/** Things that are common to each LIA solver */
abstract class LIASolver extends Solver {

  val theoryName = "LIA"

  override def subsumes(cl1: ConsClause, cl2: ConsClause): Boolean = {
    // Rather special case.
    // Useful when you have an axiom, e.g. length(L) >= 0, which prevents the derivation of ~(length(X) < 0), ~(length(X) < -1), ~(length(X) < -2), ...
    // as seen with some SWW examples
    if (cl1.constraint.nonEmpty) return false // to be on the safe side
    (cl1.lits, cl2.lits) match {
      case (Lit(true, LessEqn(d1: DomElemInt, t1)) :: Nil, Lit(true, LessEqn(d2: DomElemInt, t2)) :: _) if (d2.value <= d1.value && t1 >>~ t2) => true
      case (Lit(true, LessEqn(d1: DomElemInt, t1)) :: Nil, Lit(false, LessEqn(t2, d2: DomElemInt)) :: _) if (d2.value < d1.value && t1 >>~ t2) => true
      case (Lit(false, LessEqn(t1, d1: DomElemInt)) :: Nil, Lit(true, LessEqn(d2: DomElemInt, t2)) :: _) if (d2.value < d1.value && t1 >>~ t2) => true
      case (Lit(false, LessEqn(t1, d1: DomElemInt)) :: Nil, Lit(false, LessEqn(t2, d2: DomElemInt)) :: _) if (d2.value <= d1.value && t1 >>~ t2) => true
      case (Lit(false, LessEqn(t1, d1: DomElemInt)) :: Nil, Lit(false, Eqn(t2, d2: DomElemInt)) :: _) if (d2.value < d1.value && t1 >>~ t2) => true
      case (Lit(false, LessEqn(d1: DomElemInt, t1)) :: Nil, Lit(false, Eqn(t2, d2: DomElemInt)) :: _) if (d1.value < d2.value && t1 >>~ t2) => true
        // Could do more cases
      case _ => false
    }
  }

  /** The default implementation for LIA QE is cooper */
  def QE(cl: ConsClause) = {
    assume(cl.isBG)
    if (cl.vars.isEmpty)
      List(cl)
    else {
      val f = cooper.Solver.QE(cl.toNegFormula, forced = false)
      for (conj ← cnfconversion.toDNF(f))
        yield 
          cl.modified(lits = conj map { _.toLit.compl }, 
                      inference = ByQE(cl, theoryName), 
                      deltaConjecture = cl.info.deltaConjecture + 1)
    }
  }

  // def QE(f: Formula, params: Iterable[SymConst] = Iterable.empty) = cooper.QE(f, forced = false, params)

  val simpRulesTermSafe = simpRules.simpRulesTermSafe
  val simpRulesTermUnsafe = simpRules.simpRulesTermUnsafe
  val simpRulesLitSafe = simpRules.simpRulesLitSafe
  val simpRulesLitUnsafe = simpRules.simpRulesLitUnsafe
}



