package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
import datastructures.Lit
import util._

package object LIA {

  // var signatureLIA = signatureEmpty

  object IntSort extends BGSort("$int") {
    override def toString = "ℤ"
  }

  // signatureLIA += IntSort
  val Arity0Int = Arity0(IntSort)

  val SumOpInt = (new Operator(BG, "$sum", Arity2((IntSort, IntSort) -> IntSort)) { override def formatFn = util.printer.sumFF })
  val UMinusOpInt = (new Operator(BG, "$uminus", Arity1(IntSort -> IntSort)) { override def formatFn = util.printer.uminusFF })
  val GreaterOpInt = (new Operator(BG, "$greater", Arity2((IntSort, IntSort) -> OSort)) { override def formatFn = util.printer.greaterFF })
  // used for divisibility constraints in Cooper:
  val DividesOpInt = (new Operator(BG, "$divides", Arity2((IntSort, IntSort) -> OSort)) { override def formatFn = util.printer.dividesFF })
  val LessOpInt = (new Operator(BG, "$less", Arity2((IntSort, IntSort) -> OSort)) { override def formatFn = util.printer.lessFF })
  val DifferenceOpInt = (new Operator(BG, "$difference", Arity2((IntSort, IntSort) -> IntSort)) { override def formatFn = util.printer.differenceFF })
  val ProductOpInt = (new Operator(BG, "$product", Arity2((IntSort, IntSort) -> IntSort)) { override def formatFn = util.printer.productFF })
  val LessEqOpInt = (new Operator(BG, "$lesseq", Arity2((IntSort, IntSort) -> OSort)) { override def formatFn = util.printer.lessEqFF })
  val GreaterEqOpInt = (new Operator(BG, "$greatereq", Arity2((IntSort, IntSort) -> OSort)) { override def formatFn = util.printer.greaterEqFF })

  val IsIntOpInt = new IsIntOp(IntSort)
  val IsRatOpInt = new IsRatOp(IntSort)
  val IsRealOpInt = new IsRealOp(IntSort)
    
  val ToIntOpInt = new ToIntOp(IntSort)
  val ToRatOpInt = new ToRatOp(IntSort)
  val ToRealOpInt = new ToRealOp(IntSort)

  // Notice only QuotientEOpInt and RemainderEOpInt have been added to LFAOperators
  val QuotientEOpInt = new QuotientEOp(IntSort)
  val QuotientTOpInt = new QuotientTOp(IntSort)
  val QuotientFOpInt = new QuotientFOp(IntSort)

  val RemainderEOpInt = new RemainderEOp(IntSort)
  val RemainderTOpInt = new RemainderTOp(IntSort)
  val RemainderFOpInt = new RemainderFOp(IntSort)


  val NLPPOpInt = new NLPPOp(IntSort)

  val LIAOperators = Set(SumOpInt, UMinusOpInt, GreaterOpInt, DividesOpInt, LessOpInt, 
			 DifferenceOpInt, ProductOpInt, LessEqOpInt, GreaterEqOpInt, 
			 IsIntOpInt, IsRatOpInt, IsRealOpInt,
			 ToIntOpInt, ToRatOpInt, ToRealOpInt, NLPPOpInt, QuotientEOpInt, RemainderEOpInt)
  
  def addStandardOperators(s: Signature) = {
    var res = s
    res += IntSort
    LIAOperators foreach { res += _ } 
    res
  }

  // signatureEmpty += equalityOp(BG, IntSort)
  // signatureInt += (new Operator(BG, "=", Arity2((AnyBGSort, AnyBGSort) -> OSort)) { override def formatFn = infix(" = ") })

  // More equality operators will be added to the current signature during parsing

  // Make an equality operator for a given sort
  // def equalityOp(kind: Kind, sort: Sort) = (new Operator(kind, "=_"+sort.name, Arity2((sort, sort) -> OSort)) { override def formatFn = infix(" ≈_"+sort+" ") }) 

  val ZeroInt = DomElemInt(0)
  val OneInt = DomElemInt(1)
  val MinusOneInt = DomElemInt(-1)

  IntSort.someElement = DomElemInt(0)

  def isLIA(e: Expression[_]) = e.isBG && (e.sorts subsetOf Set(IntSort)) // && (e.operators subsetOf LIAOperators)

  /* currently unused
  def normalize(l: Lit): Lit = l match {
    case Lit(true, GreaterEqn(t1, t2))    ⇒ Lit(true, LessEqn(t2, t1))
    case Lit(true, GreaterEqEqn(t1, t2))  ⇒ Lit(true, LessEqEqn(t2, t1))
    case Lit(false, GreaterEqn(t1, t2))   ⇒ Lit(true, LessEqEqn(t1, t2))
    case Lit(false, GreaterEqEqn(t1, t2)) ⇒ Lit(true, LessEqn(t1, t2))
    case _                                ⇒ l
  }
*/

}
