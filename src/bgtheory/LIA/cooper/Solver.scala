package beagle.bgtheory.LIA.cooper

import beagle._
import util._
import fol._
import bgtheory.LIA._
import cooperIncr.CooperSoln
import datastructures.ConsClause
import bgtheory.SolverResult

object Solver extends LIASolver {

  val name = "cooper"
  // val canUNSATCore = false
  val needsUnitClauses = false

  def hasSolverLiterals(cl: ConsClause) = true

  def toSolverLiterals(cl: ConsClause) = cl

  def check(cls: Iterable[ConsClause]): SolverResult =
    try {
      if (cls forall { _.isGround }) 
	SolverResult(isConsistent(cls))
      else 
	SolverResult(QE((cls map { _.toFormula }).toAnd, false, cls flatMap { _.symConsts }) != FalseAtom)
    } catch {
      // Ideally we would just be able to cleverly cast to a Long or BigInt to avoid
      // this issue while still using cooper.
      case IntOverflowException => throw InternalError("Integer overflow in Cooper")
    }

  var isConsistentCnt = 0

  /** Applies to lists of *ground* clauses. */
  def isConsistent(cls: Iterable[ConsClause]): Boolean = {

    reporter.debugBG(s"cooper.isConsistent (ground clauses) called on $cls")
    isConsistentCnt += 1

    Timer.cooperCalls.start()

    val f1 = cls.map(cl => toPolyAtomFormula(cl.toFormula)).toAnd
    val f2 = cooper.eliminateMany(f1, f1.symConsts)

    Timer.cooperCalls.stop()

    // We need to evaluate f2 to get the conclusive result
    return f2.simplifyCheap != FalseAtom
  }

  /** This is the entry point to the Cooper algorithm.
    * Removes from f all *explicit* quantifiers and the also the given parameters.
    * Parameter forced, if true, enforces always quantifier elimination,
    * even if f is quantifier-free and contains no parameters.
    * The result in theis case will always be TrueAtom or FalseAtom,
    * so that QE acts as a decision procedure. */
  def QE(f: Formula, forced: Boolean, params: Iterable[SymConst] = List.empty): Formula = {

    /** Cooper's algorithm requires a certain normal, achieved this way: */
    def normalize(f: Formula) = {
      import cnfconversion.rules._

      val elimUniv: FormulaRewriteRules = {
        case Forall(x, g) ⇒ Neg(Exists(x, Neg(g)))
      }
      f.reduceInnermost(elimUniv).
        reduceInnermost(elimIffNot).
        reduceInnermost(elimIff).
        reduceInnermost(elimImpl).
        reduceInnermost(pushdownNegNoQuant). // May not introduce universal quantifiers
        simplifyCheap
    }

    reporter.debugBG(s"cooper.QE called on $f")

    if (!forced && f.isQuantifierFree && params.isEmpty) 
      f
    else {
      Timer.cooperCalls.start()

      val h = Timer.normForCooper.measure { normalize(f) }
      val res = cooper.cooper(h, params)

      reporter.debugBG("cooper: final QE result "+res)
      Timer.cooperCalls.stop()

      res
    }
  }

}

/** A version of the above that stores the bindings for the outermost quantifiers in
  * any successful quantifier elimination (ie one that does not result in False).
  * The last solution is used to evaluate the next call to check, isConsistent or QE.
  * Note that the solution is mutable, it can change with each call to one of the above methods. */
class CachingSolver(initSoln: Option[CooperSoln]) extends LIASolver {

  var isConsistentCnt = 0

  //Stores the previous bindings to the outer existential on the last cooper call
  //If None, then the last call produced False.
  //However None and Some(Map.empty) produce equivalent results.
  //This does not take into account backtracking, so stored solutions can sometimes be non-sensical.
  private[cooper] var prevSoln: Option[CooperSoln] = None

  def getPrevSoln = prevSoln
  def invalidateSoln = prevSoln = None

  val name = "cooper-cached"
  // val canUNSATCore = false
  val needsUnitClauses = false

  def hasSolverLiterals(cl: ConsClause) = true

  def toSolverLiterals(cl: ConsClause) = cl

  def check(cls: Iterable[ConsClause]) =
    try {
      if (cls forall  { _.isGround }) 
	SolverResult(isConsistent(cls))
      else 
	SolverResult(QE((cls map { _.toFormula }).toAnd, false, cls flatMap { _.symConsts }) != FalseAtom)
    } catch {
      case IntOverflowException => throw InternalError("Integer overflow in Cooper")
    }

  /** Applies to lists of *ground* clauses. */
  def isConsistent(cls: Iterable[ConsClause]): Boolean = {

    reporter.debugBG(s"cooper.isConsistent (ground clauses) called on $cls")
    isConsistentCnt += 1

    Timer.cooperCalls.start()

    val f1 = cls.map(cl => toPolyAtomFormula(cl.toFormula)).toAnd
    val f2 = try {
      reporter.debugBG("cooper: elimIncr called from isConsistent()")

      cooperIncr.elimIncr(f1, f1.symConsts, prevSoln) match {
	case None => {
	  prevSoln = None

          //None should => FalseAtom, this assertion checks it
          //disable for efficiency
	  // assert({
	  //   reporter.debugBG("cooper: sanity checking FALSE result from INCR")
          //   eliminateMany(f1, f1.symConsts).simplifyCheap == FalseAtom
          // }, "Cooper and Incremental gave different solutions!")

	  FalseAtom
	}
	case Some((f, ps)) => {
	  reporter.debugBG(s"cooper: elim of ${f1.symConsts} via INCR gave $f")
	  prevSoln = Some(ps)
	  f
	}
      }
    } catch {
      case e: cooperIncr.CannotContinueException => {
	reporter.debugBG("cooper: *** Call to INCR FAILED, reason\n"+e.s)
	//stats.nrAttemptsFailed += 1
	cooper.eliminateMany(f1, f1.symConsts)
      }
    }

    Timer.cooperCalls.stop()
    
    return f2.simplifyCheap != FalseAtom
  }

  def QE(f: Formula, forced: Boolean, params: Iterable[SymConst] = List.empty): Formula = {

    /** Cooper's algorithm requires a certain normal, achieved this way: */
    def normalize(f: Formula) = {
      import cnfconversion.rules._

      val elimUniv: FormulaRewriteRules = {
        case Forall(x, g) ⇒ Neg(Exists(x, Neg(g)))
      }
      f.reduceInnermost(elimUniv).
        reduceInnermost(elimIffNot).
        reduceInnermost(elimIff).
        reduceInnermost(elimImpl).
        reduceInnermost(pushdownNegNoQuant). // May not introduce universal quantifiers
        simplifyCheap
    }

    /** Eliminate all explicit quantifiers, innermost first */
    def eliminateAll(f: Formula): Formula =
      if (f.isQuantifierFree)
        f
      else f match {
        // Cannot have an atom, because atoms are quantifier-free
        case UnOpForm(op, g)       ⇒ UnOpForm(op, eliminateAll(g))
        case BinOpForm(op, f1, f2) ⇒ BinOpForm(op, eliminateAll(f1), eliminateAll(f2))
        case Exists(xs, g) => cooper.eliminateMany(toPolyAtomFormula(eliminateAll(g)), xs)
      }

    reporter.debugBG(s"cooper.QE called on $f")

    if (!forced && f.isQuantifierFree && params.isEmpty) 
      f
    else {
      Timer.cooperCalls.start()

      val h = Timer.normForCooper.measure { normalize(f) }
      val res = {
        // First eliminate all explicit quantifiers, then the given parameters
        val r2 = toPolyAtomFormula(eliminateAll(f))

        val r3 = try {
	  reporter.debugBG("cooper: elimIncr called from cooper()")
	  cooperIncr.elimIncr(r2, params, prevSoln) match {
	    case None => {
	      prevSoln = None
	      FalseAtom
	    }
            case Some((f, ps)) => {
	      reporter.debugBG(s"cooper: elim of $params via INCR gave $f")
	      prevSoln = Some(ps)
	      f
	    }
	  }
	} catch {
	  case e: cooperIncr.CannotContinueException => {
	    reporter.debugBG("cooper: *** Call to INCR FAILED, reason\n"+e.s)
	    //stats.nrAttemptsFailed += 1
	    cooper.eliminateMany(r2, params)
	  }
	}

        stats.nrQECalls += 1
        r3.simplifyCheap
      }

      reporter.debugBG("cooper: final QE result "+res)
      Timer.cooperCalls.stop()

      res
    }
  }
}
