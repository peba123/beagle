package beagle.bgtheory.LIA

import beagle._
import fol._
import util.InternalError

/** Includes generic transformation functions for formulas containing PolyAtoms. */
package object cooper {

  /** Assume f is a quantifer-free BG formula and contains only the connectives And, Or and Neg. 
    * The result is an equivalent formula in nnf over the connectives And, Or and Neg (the latter in a limited way)
    * All (dis)equations and inequalities will be arranged suitable for Cooper using Polynomials
    * Furthermore, elimTrivial has been applied. */
  def toPolyAtomFormula(f: Formula): Formula = {

    import Polynomial._ // for "toPolynomial"

    def h(f: Formula): Formula = f match {
      case TrueAtom        ⇒ TrueAtom
      case FalseAtom       ⇒ FalseAtom
      case Less(s, t)      ⇒ ZeroLTPoly(toPolynomial(t, false) - toPolynomial(s, false))
      case LessEq(s, t)    ⇒ h(Less(s, Sum(t, OneInt))) // s < t+1
      case GreaterEq(s, t) ⇒ h(Less(t, Sum(s, OneInt)))
      case Greater(s, t)   ⇒ h(Less(t, s))
      case Divides(s, t) ⇒ s match {
        case s: DomElemInt ⇒ DividesPolyAtom(s.value, toPolynomial(t, false))
        case _             ⇒ throw InternalError("divides atom with non-integer argument: " + f)
      }
      // s = t => 0 = t - s
      case Equation(s, t)                  ⇒ ZeroEQPoly(toPolynomial(t, false) - toPolynomial(s, false))
      case a @ PolyAtom(_, _, _)           ⇒ a // assume polyatoms have been brought into canonical form before
      case And(f1, f2)                     ⇒ And(h(f1), h(f2))
      case Or(f1, f2)                      ⇒ Or(h(f1), h(f2))
        // case Iff(f1, f2) => h(And(Implies(f1, f2), Implies(f2, f1)))
        // case IffNot(f1, f2) => h(Neg(Iff(f1, f2)))
        // case Implies(f1, f2) => h(Or(Neg(f1), f2))
        // case Implied(f1, f2) => h(Implies(f2, f1))

      case Neg(TrueAtom)                   ⇒ FalseAtom
      case Neg(FalseAtom)                  ⇒ TrueAtom
      case Neg(Less(s, t))                 ⇒ h(GreaterEq(s, t))
      case Neg(LessEq(s, t))               ⇒ h(Greater(s, t))
      case Neg(Greater(s, t))              ⇒ h(LessEq(s, t))
      case Neg(GreaterEq(s, t))            ⇒ h(Less(s, t))
      case Neg(Divides(s, t))              ⇒ Neg(h(Divides(s, t)))
      case Neg(Equation(s, t))             ⇒ ZeroNEPoly(toPolynomial(t, false) - toPolynomial(s, false))
      case Neg(ZeroEQPoly(p))              ⇒ ZeroNEPoly(p)
      case Neg(ZeroNEPoly(p))              ⇒ ZeroEQPoly(p)
      case Neg(ZeroLTPoly(p))              ⇒ ZeroLTPoly((p * -1) + 1)
      // Divisibility predicates will occur only positively.
      // But at the time his called, Divides atoms should not
      // occur anyway in f
      case nd @ Neg(DividesPolyAtom(n, p)) ⇒ nd
      case Neg(And(f1, f2))                ⇒ h(Or(Neg(f1), Neg(f2)))
      case Neg(Or(f1, f2))                 ⇒ h(And(Neg(f1), Neg(f2)))
        // case Neg(Iff(f1, f2)) => h(IffNot(f1, f2))
        // case Neg(IffNot(f1, f2)) => h(Iff(f1, f2))
        // case Neg(Implies(f1, f2)) => h(And(f1, Neg(f2)))
        // case Neg(Implied(f1, f2)) => h(Neg(Implies(f2, f1)))

      case Neg(Neg(f))                     ⇒ h(f)
    }

    h(f).reduceInnermost(cnfconversion.rules.elimTrivial)
  }

  /** Replace all occurences of the variable x in f with the polynomial p.
    * @return f[p]. */
  def replace(f: Formula, x: VarOrSymConst, p: Polynomial): Formula = f match {
    case UnOpForm(op, g)       ⇒ UnOpForm(op, replace(g, x, p))
    case BinOpForm(op, f1, f2) ⇒ BinOpForm(op, replace(f1, x, p), replace(f2, x, p))
    // All these cases explictly because after replacement could get TrueAtom or FalseAtom
    // case a @ PolyAtom(_, _, _) ⇒ a.asInstanceOf[PolyAtom].replace(x, p)
    // More concise, but does it work?
    case a: PolyAtom           ⇒ a.replace(x, p)
    case _                     ⇒ f
  }

  //TODO- the other form can benefit from a generic formula 'replaceAtom':
  //actually might only apply for 'replace' derp!
  def replaceAtom(f: Formula, replaceF: (Atom) => Atom): Formula = f match {
    case a: Atom => replaceF(a)
    case UnOpForm(op,g) => UnOpForm(op,replaceAtom(g,replaceF))
    case BinOpForm(op, g1, g2) => BinOpForm(op, replaceAtom(g1,replaceF),replaceAtom(g2,replaceF))
    case _ => throw InternalError("replaceAtom assumes quantifier free formulas")
  }

  /** Rearrange the polynomials in f so that each starts with the x-monomial,
    * if it has one. */
  def rearrange(f: Formula, x: VarOrSymConst): Formula = replaceAtom(f, {
    case a: PolyAtom => a.rearrange(x)
    case a           => a
  })

}
