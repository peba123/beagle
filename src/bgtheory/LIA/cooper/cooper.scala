package beagle.bgtheory.LIA.cooper

import beagle._
import fol._
import util._
import datastructures.Clause
import bgtheory.LIA._


/**
 * Functions implementing Cooper quantifier elimination for LIA.
 *  Peter 27/1/2015: removed deferred instantiation because of unclear status
 */

object cooper {

  /**
   * Turn f into an equivalent formula such that all polynomials contain x only with a unit coefficient.
   * Assumes that f has been rearranged for x
   *
   * given 0 < cx + p and assume c|lcm, and let d = lcm/c
   * return 0 < x + dp
   *
   * For divisibility constraints transform
   * n | cx + p
   * into (normalized version of)
   * dn | x + dp
   *
   * For equational constraints transform
   *  0 = cx + p
   * into
   *  0 =  x + dp
   * Because d is positive iff k is, dp will always have the right sign
   * (and x has a positive unit coefficient)
   *
   * Disequations: similarly
   */
  def unitCoeffs(f: Formula, x: VarOrSymConst) = {    

    /**
     * @return the lcm of all coefficients of x in f
     * Getting the coefficients then applying lcm in list form
     * is much faster for large formulas
     */
    def coeffsLcm(f: Formula): Int = {
      //extract a list of polyAtom coeffs of x
      def getCoeffs(g: Formula, x: VarOrSymConst): List[Int] = {
    	g match {
          case UnOpForm(_, g) ⇒ getCoeffs(g,x)
          case BinOpForm(_, f1, f2) ⇒ (getCoeffs(f1,x):::getCoeffs(f2,x))
          case PolyAtom(_, _, Polynomial(Monomial(c, y) :: _, k, Nil)) if (y == x) ⇒ List(c.abs)
          case _ ⇒ List(1)
    	}
      }

      lcm(getCoeffs(f,x))
      //todo- what if you get int overflow here
      //- could use BigInt, or could represent lcm as products
    }

    val xcoeffslcm = coeffsLcm(f)

    /** Give all occurences of x in f a unit coefficient */
    def h(f: Formula): Formula =
      f match {
        case UnOpForm(op, g) ⇒
          UnOpForm(op, h(g))
        case BinOpForm(op, f1, f2) ⇒
          BinOpForm(op, h(f1), h(f2))
        // The case for divides is a bit special because the result requires canonical form,
        // which is obtained by calling Divides; also could simplify to True or False
        case DividesPolyAtom(n, Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
          val d = xcoeffslcm / c
          // gives the new monomial a unit coefficient
          DividesPolyAtom(d * n, Monomial(1, x) +: (Polynomial(ms, k, Nil) * d)) // d*n could be negative, but not a problem
        }
        case ZeroLTPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
          val d = xcoeffslcm / c
          if (d < 0)
            // Must multiply by -d because need a positive number to multiply an equality  
            ZeroLTPoly(Monomial(-1, x) +: (Polynomial(ms, k, Nil) * -d))
          else
            ZeroLTPoly(Monomial(1, x) +: (Polynomial(ms, k, Nil) * d))
        }
        // EQ or NE case - no problem with multiplying by a possibly negative d
        case ZeroEQPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
          val d = xcoeffslcm / c
          ZeroEQPoly(Monomial(1, x) +: (Polynomial(ms, k, Nil) * d))
        }
        case ZeroNEPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
          val d = xcoeffslcm / c
          ZeroNEPoly(Monomial(1, x) +: (Polynomial(ms, k, Nil) * d))
        }
        case _ ⇒ f
      }

    if (xcoeffslcm == 1)
      h(f) // no need for the divisibility constraint
    else
      And(DividesPolyAtom(xcoeffslcm, Polynomial(Monomial(1, x) :: Nil, 0, Nil)), h(f))

  }

  /**
   * The minus infinity formula obtained from f
   */
  def infty(f: Formula, x: VarOrSymConst): Formula =
    f match {
      case UnOpForm(op, g) ⇒
        UnOpForm(op, infty(g, x))
      case BinOpForm(op, f1, f2) ⇒
        BinOpForm(op, infty(f1, x), infty(f2, x))
      case ZeroLTPoly(Polynomial(Monomial(c, y) :: _, _, Nil)) if (y == x) ⇒ {
        assume(c == 1 || c == -1, "monomial with unit coefficient expected")
        c match {
          case 1  ⇒ FalseAtom
          case -1 ⇒ TrueAtom
        }
      }
      case ZeroEQPoly(Polynomial(Monomial(1, y) :: _, _, Nil)) ⇒
        if (y == x) FalseAtom else f
      // xxx - check this
      case ZeroNEPoly(Polynomial(Monomial(1, y) :: _, _, Nil)) ⇒
        if (y == x) TrueAtom else f
      // Divides case
      case _ ⇒ f
    }

  /**
   * Dual to the minus infinity transform.
   * Only difference is that the ZeroLT cases are reversed.
   * Divisibility atoms unchanged so # instances the same.
   */
  def posInfty(f: Formula, x: VarOrSymConst): Formula =
    f match {
      case UnOpForm(op, g) => UnOpForm(op,posInfty(g,x))
      case BinOpForm(op,f1,f2) => BinOpForm(op, posInfty(f1,x), posInfty(f2,x))
      
      case ZeroLTPoly(Polynomial(Monomial(c, y) :: _, _, _)) if (y == x) ⇒ {
        assume(c == 1 || c == -1, "monomial with unit coefficient expected")
        c match {
          case 1  ⇒ TrueAtom
          case -1 ⇒ FalseAtom
        }
      }
      case ZeroEQPoly(Polynomial(Monomial(1, y) :: _, _, _)) ⇒
        if (y == x) FalseAtom else f
      case ZeroNEPoly(Polynomial(Monomial(1, y) :: _, _, _)) ⇒
        if (y == x) TrueAtom else f
      case _ => f
    }

  /** @return The LCM of the constants of the divisibility atoms in f that contain x*/
  def divisibilityConstsLcm(f: Formula, x: VarOrSymConst): Int = 
    f.atoms.foldLeft(1)((acc,a) => a match {
      case DividesPolyAtom(n, Polynomial(Monomial(_, y) :: _, _, _)) ⇒ if (y == x) lcm(n,acc) else acc // We know n is positive
      case _ ⇒ acc
    })

  /**
   * Assume that f has been rearranged for x.
   * @return The set of lower bound polynomials of x in f.
   */
  def lowerBounds(f: Formula, x: VarOrSymConst): Set[Polynomial] =
    f.atoms.foldLeft(Set.empty[Polynomial])((acc, a) => (a match {
      case ZeroLTPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
        assume(c == 1 || c == -1, println("lowerBounds: monomial with non-unary factor encountered"))
        c match {
          case 1  ⇒ Set(Polynomial(ms, k, Nil) * -1)
          case -1 ⇒ Set.empty[Polynomial]
        }
      }
      case ZeroEQPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
        assume(c == 1, println("lowerBounds: monomial with non-unary factor encountered"))
        Set((Polynomial(ms, k, Nil) + 1) * -1)
      }
      case ZeroNEPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
        assume(c == 1, println("lowerBounds: monomial with non-unary factor encountered"))
        Set(Polynomial(ms, k, Nil) * -1)
      }
      case _ ⇒ Set.empty[Polynomial]
    }) union acc)

  /**
   * Dual 'A'-set to the lower bounds set above. Used with positive infinity transform.
   * Also assumes that f has been rearranged for x.
   */
  def upperBounds(f: Formula, x: VarOrSymConst): Set[Polynomial] =
    f.atoms.foldLeft(Set.empty[Polynomial])((acc, a) => (a match {
      case ZeroLTPoly(Polynomial(Monomial(c, y) :: ms, k, _)) if (y == x) ⇒ {
        assume(c == 1 || c == -1, println("upperBounds: monomial with non-unary factor encountered"))
        c match {
          case 1  ⇒ Set.empty[Polynomial]
          case -1 ⇒ Set(Polynomial(ms, k, List.empty))
        }
      }
      case ZeroEQPoly(Polynomial(Monomial(c, y) :: ms, k, _)) if (y == x) ⇒ {
        assume(c == 1, println("upperBounds: monomial with non-unary factor encountered"))
        Set((Polynomial(ms, k, List.empty) * -1)+1)
      }
      case ZeroNEPoly(Polynomial(Monomial(c, y) :: ms, k, _)) if (y == x) ⇒ {
        assume(c == 1, println("upperBounds: monomial with non-unary factor encountered"))
        Set(Polynomial(ms, k, List.empty) * -1)
      }
      case _ ⇒ Set.empty[Polynomial]
    }) union acc)

  /**
   * Eliminate the variable x from f because it is either unbounded below or unbounded above.
   * @return None if this does not succeed, i.e. if we cannot determine if f has a solution
   * iff f has arbitrary large/small solutions wrt x.
   */
  def elimUnbound(f: Formula, x: VarOrSymConst): Option[Formula] = {
    reporter.debugBG("elimCheap: try eliminating " + x + " because of unbound solutions")
    var sign = 0 // 0 stands for "not yet determined"

    def h(f: Formula): Option[Formula] =
      f match {
	//first two are the only recursive cases
        case UnOpForm(op, g) ⇒ // this can only be a negated divisibility constraint
	  h(g) map { UnOpForm(op, _) } //ie return Some(f) if h(g) is defined
        case BinOpForm(op, f1, f2) ⇒ {
          /*val f1Res = h(f1)
          val f2Res = h(f2)
          BinOpForm(op, f1Res, f2Res)
	  */
	  h(f1) flatMap { r1 => h(f2) map { BinOpForm(op,r1,_)} }
        }
        case DividesPolyAtom(_, Polynomial(Monomial(_, y) :: _, _, Nil)) ⇒
          if (y == x) // todo: can we do something better?
	    None
          else Some(f)
        case p @ ZeroLTPoly(Polynomial(Monomial(c, y) :: _, _, Nil)) ⇒
          if (y == x)
            (c.signum, sign) match {
              case (csign, 0) ⇒ {
                // sign was undetermined, fix it now and remove a
                sign = csign
                Some(TrueAtom)
              }
              case (-1, -1) | (1, 1) ⇒
                // sign of x in this atom is consistent with previous ones
                Some(TrueAtom)
              case (-1, 1) | (1, -1) ⇒
                // conflict in signs, cannot eliminate
		None
            }
          else Some(f)
        case ZeroEQPoly(Polynomial(Monomial(_, y) :: _, _, Nil)) ⇒
          if (y == x) None else Some(f)
        case ZeroNEPoly(Polynomial(Monomial(_, y) :: _, _, Nil)) ⇒
        // never a problem 
          Some(if (y == x) TrueAtom else f)
        case _ ⇒ Some(f) //throw InternalError("unboundElim unexpected case: " + f)
      }

    h(f)
  }

  /**
   * Eliminate x from f because it occurs in some equation in f
   * Works best on conjunctions of atoms.
   * Expects that PolyAtoms of f have been rearranged for x.
   * No simplification is done here, so successful results will include at
   * least one T atom as a conjunct.
   * @return None if x *cannot* be eliminated, or Some(f2) where f2 is
   * f with x eliminated.
   */
  def elimEquation(f: Formula, x: VarOrSymConst): Option[Formula] = {
    reporter.debugBG("elimCheap: try eliminating " + x + " as part of an equation")
    for (h ← f.toListAnd)
      h match {
        case ZeroEQPoly(Polynomial(Monomial(1, y) :: r, k, Nil)) if (y == x) ⇒
          return Some(replace(f, x, Polynomial(r, k, Nil) * -1))
        case ZeroEQPoly(Polynomial(Monomial(-1, y) :: r, k, Nil)) if (y == x) ⇒
          return Some(replace(f, x, Polynomial(r, k, Nil)))
        case _ ⇒ () // continue
      }
    // nothing found
    return None
  }

  def elimSubsumption(f: Formula, x: VarOrSymConst): Option[Formula] = {
    reporter.debugBG("elimCheap: try eliminating some occurrences of " + x + " by subsumption")
    var res = List.empty[Formula]
    var bestPos: Option[Formula] = None
    var bestNeg: Option[Formula] = None
    var success = false
    for (h ← f.toListAnd)
      h match {
        case p @ ZeroLTPoly(Polynomial(List(Monomial(1, y)), k, Nil)) if (y == x) ⇒ {
          bestPos match {
            case None => bestPos = Some(p)
            case Some(ZeroLTPoly(Polynomial(_, kBest, _))) =>
              if (k < kBest) {
                // p is better and subsume current best
                bestPos = Some(p)
                success = true
              }
              else {
                // old best is at least as good as p and subsumes p
                success = true
              }
          }
        }
        case p @ ZeroLTPoly(Polynomial(List(Monomial(-1, y)), k, Nil)) if (y == x) ⇒ {
          bestNeg match {
            case None => bestNeg = Some(p)
            case Some(ZeroLTPoly(Polynomial(_, kBest, _))) =>
              if (k < kBest) {
                // p is better and subsume current best
                bestNeg = Some(p)
                success = true
              }
              else {
                // old best is at least as good as p and subsumes p
                success = true
              }
          }
        }
        case _ ⇒ res ::= h // irrelevant - continue
      }

    (bestNeg, bestPos) match {
      // test for inconsistencies
      case (Some(ZeroLTPoly(Polynomial(_, kNeg, _))),
          Some(ZeroLTPoly(Polynomial(_, kPos, _)))) if (!(kNeg + kPos > 1)) => Some(FalseAtom)
      case (_, _) => 
        if (success) {
          // don't forget to add best
          if (bestPos != None) res ::= bestPos.get
          if (bestNeg != None) res ::= bestNeg.get
          Some(res.reverse.toAnd)
        }
        else
          None
    }
  }

  /**
   * Eliminate a given variable x from f in a cheap way.
   * If f contains x, then rearrange f for x and then
   * try `elimEquation`; `elimSubsumption` and `elimUnbound`
   * in order to simplify.
    * Result is an AND list (i.e. convert back to a formula using .toAnd).
   */
  def elimCheap(f: Formula, xs: Iterable[VarOrSymConst]): List[Formula] = {
    var conjRes = f.toListAnd

    for (x <- xs) {

      // partition conjRes into two parts, one that contains x and one that doesn't
      val (conjX, conjNoX) = conjRes partition { _ contains x }

      if (conjX.nonEmpty) {
        //var conjXRes = elimCheap(conjXtoAnd, x).simplifyCheap
        val g = rearrange(conjX.toAnd.simplifyCheap, x)
        reporter.debugBG("elimCheap: try to eliminate " + x + " from " + g)

        val res = elimEquation(g, x) orElse {
          // Must try elimination by Unbound first, because subsumption does not remove *all* occurrences of x
	  elimUnbound(g, x)
        } orElse {
	  elimSubsumption(g, x)
        } map { h =>
	  reporter.debugBG("elimCheap: success, obtained " + h)
	  h.simplifyCheap
        }

        // Commented this line for the time being because of incorrect result
        // for Philipp's problem quadraticInEq12.p
        // conjXRes = elimShadows(conjXRes.toListAnd, x).toAnd.simplifyCheap

        //multiply out with conjNoX
        res match {
          case None => () //conjRes does not change
          case Some(TrueAtom) => conjRes = conjNoX
          case Some(FalseAtom) => return List(FalseAtom)
          case Some(conjXRes) =>
            // must be a conjunction
            conjRes = conjXRes.toListAnd ::: conjNoX
        }
      }
    } //end for loop

    // conjRes is simplifiedCheap but may contain False
    if (conjRes contains FalseAtom)
      List(FalseAtom)
    else
      conjRes filterNot { _ == TrueAtom }

  }

  /** One element of a conjunction is false */
  case object FALSECONJUNCTION extends Exception

  /** Given a conjunction of divisibility literals puts those literals into triangular form.
    * A set of divisibility literals is in triangular form wrt a variable order `xs = x_1 < ... < x_n`
    * if for each `x_i` at most one literal contains `x_i` as the least free var wrt `xs`.
    * 
    * Assumes that elements of xs are distinct.
    * 
    * Note that trivial divisibility literals are filtered from the result, but false atoms
    * can be derived (e.g. from 2 | 1 + x, 2 | 2 + x) in which case FALSECONJUNCTION is thrown.
    */
  def triangulateDiv(divLits: List[DividesPolyAtom], xs: List[VarOrSymConst]): List[DividesPolyAtom] = {

    reporter.debugBG(s"triangulateDiv: Variable order=$xs")

    /** Assumes x occurs in both d1, d2. Replace lits according to theorem in Cooper.
      * LHS result has x RHS doesnt.*/
    def replace(d1: DividesPolyAtom, d2: DividesPolyAtom, x: VarOrSymConst): (Atom, Atom) = {
      //let d1 = m | ax + b and d2 = n | cx + d
      // where b,d are polynomials that don't contain x
      val (DividesPolyAtom(m, ax), DividesPolyAtom(n, bx)) = (d1, d2)
      val Some((Monomial(a, _), b)) = ax.rearrange(x).uncons
      val Some((Monomial(c, _), d)) = bx.rearrange(x).uncons
      //let gcd = p*(a*n) + q*(c*m)
      //note that signs are corrected inside egcd()
      val (gcd, p, q) = egcd((a*n), (c*m))
      //then {d1, d2} are equivalent to mn | bnp + dqm + (gcd)x, gcd | bc - da
      (DividesPolyAtom(m * n, (b * (n * p)) + (d * (q * m)) + Monomial(gcd, x)), DividesPolyAtom(gcd, (b * c) - (d * a)))
    }

    //cheap test
    if (divLits.size < 2 || xs.isEmpty)
      return divLits.toList

    //choose from s two divisibility lits containing x
    //sth there is no y < x (wrt xs) that occurs in at least 2 divisibility constraints of s

    //invariant: acc is in triangular form
    var acc = List.empty[DividesPolyAtom]
    var rem = divLits

    for { x <- xs } {
      //where x=xs(0)
      //in tri form iff |withX| <= 1
      //else combine all into accX
      //have accX with x and (noX :: rest) without x
      //next step 
      val (withX, rest) = rem.partition(_.contains(x))
      val open = withX.toIterator

      if (open.isEmpty) //skip x
        ()
      else {
        //apply thm until triangular form is reached
        var accX: Atom = open.next            //processed atom with x
        var noX = List.empty[DividesPolyAtom] //processed atoms that don't contain x

        while (open.nonEmpty) {
          val next = open.next
          //only do this if there is no variable < x in next
          replace(accX.asInstanceOf[DividesPolyAtom], next, x) match {
            case (_, FalseAtom) | (FalseAtom, _) =>
              throw FALSECONJUNCTION
            case (TrueAtom, r2: DividesPolyAtom) =>
              noX ::= r2
              //need to replace accX with next open elt. if it exists
              if (open.hasNext) {
                accX = open.next
              } else {
                //result is just noX
                accX = TrueAtom
              }
            case (r1: DividesPolyAtom, TrueAtom) =>
              accX = r1
            case (r1: DividesPolyAtom, r2: DividesPolyAtom) =>
              accX = r1
              noX ::= r2
          }
        }
        //after while loop nothing in noX has x
        //original set is equal to (accX :: noX) ++ rest
        if (accX != TrueAtom)
          acc ::= accX.asInstanceOf[DividesPolyAtom]

        rem = (noX ++ rest)
        // assert(rest.forall(!_.contains(x)), "no div lits with x should be in rest")
        // assert(noX.forall(!_.contains(x)), "no div lits with x should go into noX")

      }
    }
    //after for loop rem is empty and acc is equiv to input only in tri. form
    //assert(rem.isEmpty) //no it can contain div lits that do not have any vars in xs
    return acc ++ rem

  }

  /** Assume system S is already triangular so there is at least one div lit for `xs.head` which has no vars.
    * @param maxValues maps vars to the highest value they need instantiating to, found using `elimDefer`.
    * @param f is the formula to instantiate minus div. literals in `s`.
    * @return `f` instantiated wrt the values given in maxValues and modulo div lits in `s`.
    */
  def solveDiv(s: List[DividesPolyAtom], f: Formula, maxValues: Map[VarOrSymConst, Int]): Formula = {

    reporter.debugBG(s"solveDiv: constraints\n$s\n -- for formula:\n$f\n -- bounds:\n$maxValues")

    //if s empty generate a disjunct of values from the values of js
    if (s.isEmpty) {
      val toInstantiate = maxValues.filterKeys(f contains _)
      return (toInstantiate.foldLeft(f :: Nil) { case (acc, (j, d)) =>
        acc flatMap { f =>
          for { l <- 1 to d } yield
            replace(f, j, Polynomial.Const(l))
        }
      }).toOr.simplifyCheap
    }

    if (maxValues.keys.filter(f.contains(_)).isEmpty) {
      reporter.debugBG("solveDiv: no vars require instantiation")
      return f
    }

    // let s.head be m | ax + b, where b is ground
    s.find(_.p.varOrSymConsts.size == 1) match {
      case None => {
        //if no such literal exists, assume it was already solved and instantiate over [1,D]
        //e.g. m | mx + m becomes T and is removed from s by triangulation

        //heuristic- in that case select an x with smallest D to get fewer instances
        //val (x, d) = maxValues.toList.sortWith(_._2 < _._2).head
        val sel = s.find(_.p.varOrSymConsts.exists(maxValues.isDefinedAt(_))).get
        val x = sel.p.varOrSymConsts.find(maxValues.isDefinedAt(_)).get //above is buggy, select arbitrary x instead

        reporter.debugBG( "solveDiv: could not find single var literal\n"+
                         s"        : using literal $sel\n"+
                         s"solveDiv: replacing $x across range [1, ${maxValues(x)}]")

        //expand f into a disjunction of its instances
        //overall result is (res) & (s') where s' is the result of replacing in s
        //s is a conjunction of divisibility lits
        //so if some div lit is falsified return false
        //if some instance is true, return T & (s')
        //if all instances are false return false
        val res = 
        (for { l <- 1 to maxValues(x) } yield {
          //instantiate x with l
          val lp = Polynomial.Const(l)
          //it is important to not remove sel from this as it might not be satisfied yet
          val newDivs1 = s.map(_.replace(x, lp))

          if (newDivs1.exists(_ == FalseAtom))
            return FalseAtom

          //remove true atoms from conjunction
          //this allows newDivs to have element type DividesPolyAtom rather than Atom
          val newDivs: List[DividesPolyAtom] = newDivs1 collect { case pa: DividesPolyAtom => pa }

          val newF = replace(f, x, lp).simplifyCheap

          if (newF == TrueAtom)
            And(newF, newDivs.toAnd)
          else if (newF == FalseAtom)
            newF //do not return, it will be filtered later
          else
            solveDiv(newDivs, newF, maxValues)
        }).toOr.simplifyCheap //could do something better here-- tail call optimise

        reporter.debugBG(s"solveDiv: instantiated formula is $res")
        res
      }
      case Some(sel @ DividesPolyAtom(m, Polynomial(List(Monomial(a, x)), b, Nil))) => {
        val sRemaining: List[DividesPolyAtom] = s.filterNot(_ == sel)

        reporter.debugBG(s"solveDiv: working on $sel")

        // apply theorem to m | ax + b:
        // let d = p*a + q*m, where d is GCD(a,m)
        val (d, p, q) = egcd(a, m)

        if ((b % d) != 0) {
          reporter.debugBG(s"solveDiv: no solution, as gcd($a, $m)=$d does not divide $b")
          return FalseAtom
        } else {
          //instantiate rest of s with each solution to x in the range [1, xMax] and solve
          val xMax = maxValues(x)

          //solutions have the form:
          //x = -(p * b / d) + l * (m / d)
          def xFunc(l: Int) = -(p * b / d) + l * (m / d)
          
          //find l sth x is in range [1, xMax]
          val minL = Math.ceil((1 + (p * b / d)) * (d.toDouble / m)).toInt
          val maxL = Math.ceil(xMax * (d.toDouble/m) + (p * b / d) * (d.toDouble/m)).toInt
          reporter.debugBG(s"solutions for $x have the form ${-(p * b / d)} + l * ${(m / d)}, where l is in [$minL, $maxL]")

          if (minL > maxL) {
            reporter.debugBG("-> No solutions!")
            return FalseAtom
          }

          if (sRemaining.isEmpty) {
            //disjunction of instances of f
            val f2 = (for { l <- minL to maxL } yield {
              replace(f, x, Polynomial.Const(xFunc(l)))
            }).toOr.simplifyCheap

            //instantiate any remaining vars in f
            val toInstantiate = (f.vars.filter(maxValues.isDefinedAt(_)))
            val res = (for {
              y <- toInstantiate
              j <- 1 to maxValues(y)
            } yield {
              replace(f2, y, Polynomial.Const(j))
            }).toOr.simplifyCheap

            reporter.debugBG(s"solveDiv: produced instance $res")
            return res
          } else {
            //call recursively on all instances x -> l
            (for { l <- minL to maxL } yield {
              val lp = Polynomial.Const(xFunc(l))
              reporter.debugBG(s"replacing $x with solution $lp")
              //remove true atoms since these will be evaluated later
              val newDivs: List[DividesPolyAtom] =
                sRemaining.map(_.replace(x, lp)) collect { case pa: DividesPolyAtom => pa }
              reporter.debugBG(s"updated constraints are $newDivs")

              val newF = replace(f, x, lp).simplifyCheap

              if (newF == FalseAtom)
                newF
              else if (newF == TrueAtom)
                And(newF, newDivs.toAnd)
              else
                solveDiv(newDivs, newF, maxValues)
            }).toOr.simplifyCheap //could do something better here-- tail call optimise
          }
        }
      }
    }
  }

  /** The result of a deferred elimination.
    * The variable is replaced with new variable `index` which must be instantiated
    * over the range [1, max]. */
  case class EliminationResult(formula: Formula, index: Var, max: Int)

  def eliminateDefer(g: Formula, x: VarOrSymConst): EliminationResult = {

    reporter.debugBG("cooper: eliminate " + x + " from " + g)

    val h = unitCoeffs(rearrange(g, x), x)
    reporter.debugBG("cooper: elimination formula (has unit coefficients): " + h)

    val B = lowerBounds(h, x)
    reporter.debugBG("cooper: lower bounds: " + B)

    val uB = upperBounds(h,x)
    reporter.debugBG("cooper: upper bounds: " + uB)

    //test which is 'simpler'
    val usePosTransform = 
      (uB.size < B.size)

    if (usePosTransform) reporter.debugBG("cooper: using positive transform!")

    val bounds = if (usePosTransform) uB else B
    val inftyF =
      (if (usePosTransform) posInfty(h, x) else infty(h, x)).simplifyCheap
    reporter.debugBG("cooper: infty formula: " + inftyF)

    val jVar = AbstVar("inc_"+x, IntSort)
    val jMon = Monomial(1, jVar)

    if (inftyF == TrueAtom) 
      return EliminationResult(TrueAtom, jVar, 1)

    val resInfty = {
      if (!inftyF.contains(x))
        inftyF
      else
        replace(inftyF, x, Polynomial(List(jMon), 0, Nil)).simplifyCheap
    }
    reporter.debugBG(s"${if(usePosTransform) '+' else '-'}infinity formula with $x replaced:\n$resInfty")

    val resBounds =
      for { b ← bounds } yield {
        val replaceTerm = if (usePosTransform) b - jMon else b + jMon
        val hMax = replace(h, x, replaceTerm).simplifyCheap
        hMax
      }

    reporter.debugBG(s"Elimination formula instances: $resBounds")

    // Result:
    val resF = (resInfty :: resBounds.toList).toOr.simplifyCheap
    val D = divisibilityConstsLcm(h, x)
    EliminationResult(resF, jVar, D)
  }
 
  /**
   * Eliminate the variable x from g, return an equivalent formula.
   * Can either instantiate or defer if the number of instances will be large.
   */
  def eliminate(g: Formula, x: VarOrSymConst): Formula = {

    reporter.debugBG("cooper: eliminate " + x + " from " + g)

    val f = rearrange(g, x)
    reporter.debugBG("cooper: rearranged: " + f)
    
    val h = unitCoeffs(f, x) // The elimination formula with x unit coefficients everywhere
    reporter.debugBG("cooper: elimination formula (has unit coefficients): " + h)

    val hInfty = infty(h, x).simplifyCheap // The minus infinity version of h
    reporter.debugBG("cooper: -infinity formula: " + hInfty)

    //new pos infty transform
    val hPosInf = posInfty(h, x).simplifyCheap
    reporter.debugBG("cooper: +infinity formula: " + hPosInf)

    if(hInfty==TrueAtom || hPosInf==TrueAtom) 
      return TrueAtom

    val B = lowerBounds(h, x)
    reporter.debugBG("cooper: lower bounds: " + B)

    val uB = upperBounds(h,x)
    reporter.debugBG("cooper: upper bounds: " + uB)

    //test which is 'simpler'
    //****** TO DISABLE POS TRANSFORM, JUST SET THIS TO FALSE
    val usePosTransform = 
      (uB.size < B.size)

    if (usePosTransform) reporter.debugBG("cooper: using positive transform!")

    val D = divisibilityConstsLcm(h, x)
    reporter.debugBG("cooper: lcm of divisibility constraints: " + D)
    
    val bounds = if (usePosTransform) uB else B
    val inftyF = if (usePosTransform) hPosInf else hInfty

    case object TRUEDISJUNCTION extends Exception
    try {
      //***TODO- could try both +infty and -infty looking for TRUEDISJUNCTION
      //is it better to do this or to just choose one?

      reporter.debugBG(s"Instances of ${if(usePosTransform) '+' else '-'}infinity formula:")
      val resInfty = {
	if (!inftyF.contains(x)) reporter.debugBG(List(inftyF))
        else
	  for (j ← 1 to D;
	    hInftyj = replace(inftyF, x, Polynomial(List.empty, j, List.empty)).simplifyCheap;
	    if (hInftyj != FalseAtom)) yield {
	    if (hInftyj == TrueAtom) throw TRUEDISJUNCTION
	    reporter.debugBG(hInftyj)
            hInftyj
	  }
      }

      reporter.debugBG("Instances of elimination formula:")
      val resBounds = {
        val replaceSign: (Int) => Int = if (usePosTransform) (x) => -x else (x) => x
        for {
          b ← bounds
          j ← 1 to D
        } yield {
          val hMax = replace(h, x, b + replaceSign(j)).simplifyCheap
          reporter.debugBG(hMax)
          if (hMax == TrueAtom) throw TRUEDISJUNCTION
          hMax
        }
      }
      // Result:
      (resInfty ++ resBounds).toOr

    } catch {
      case TRUEDISJUNCTION ⇒ return TrueAtom
    }
  }

  /**
   * Given a conjunction, use the equivalence c < x & x < d == c < d + 1
   * to reduce.
   * Assume every literal of conj contains x.
   */
  // Peter 21/1/2015: buggy version

  def elimShadows(conj: List[Formula], x: VarOrSymConst): List[Formula] = {
    //elimCheap usually gets it first
    if(conj.isEmpty || conj.tail.isEmpty) return conj

    //note that s < x => 0 < x - s
    //and x < t => 0 < t - x
    //i.e. check the sign of x to find the orientation
 
    //what about <= ? ok because this is included in the s/t

    //note that we haven't called unitCoeffs yet!
    //could call unitCoeffs here, or just deal with literals which have c=1 already
    // reporter.debugBG(s"elimShadows: eliminate $x from $conj")

    val (ineqs,rem) = conj.partition(_.isInstanceOf[ZeroLTPoly])
    val ineqsR = ineqs.map(_.asInstanceOf[PolyAtom].p.rearrange(x))
    val (lb,ub) = ineqsR.partition( {
      case Polynomial(Monomial(c,_)::_,_,_) => c > 0 //it is a lower bound
    })

    if (lb.isEmpty || ub.isEmpty) return conj

    reporter.debugBG(s"elimShadows: lb polys are $lb")
    reporter.debugBG(s"elimShadows: ub polys are $ub")

    //store the equivalent atoms in here
    var equivalent = List[Atom]()

    /**
     * Given lower bound (b < ax) and upper bound (cx < d)
     * then then real shadow is cb < ad + 1
     * By transitivity (b < ax) & (cx < d) => cb < ad + 1
      
      Nope - this should be (b < ax) & (cx < d) => cb < ad - 1
      With the obvious fix still buggy, though

     * but not the other way, e.g. 1 < 3x & 5x < 2 == False but 5.1 < 3.2 + 1 = 5 < 7
     * It is equivalent if a or c is 1 AND there are no other terms, but not
     * otherwise.
     * Hence we store the equivalent shadows just in case we can replace later.
     */
    def makeRealShadow(lb: List[Polynomial], ub: List[Polynomial]): List[Atom] =
      for(l <- lb;
	  //l = (b < ax)
	  Monomial(a,_) :: b = l.ms;
	  u <- ub;
	  //u = (cx < d)
	  Monomial(c,_) :: d = u.ms) yield {
	    //cb < ad + 1 <=> 0 < ad - cb + 1; notice that c is already negated in its monomial
	    val ad = Polynomial(d,u.k,u.ims) * a
	    val cb = Polynomial(b,l.k,l.ims) * c
	    val res = ZeroLTPoly(ad + cb + 1)
	    if(a.abs == 1 || c.abs == 1) equivalent ::=res
	    res
	  }

    val rs = makeRealShadow(lb,ub)

    if (rs.toAnd.simplifyCheap == FalseAtom) {
      reporter.debugBG("elimShadows: real shadow was unsat so formula is false!")
      return List(FalseAtom)
    } else if(rem.isEmpty) {
      //attempt to simplify by removing atoms with coeff 1 for x
      //filter for c==1
      //join equivalent and return
      //this only works if all inequalities can be removed otherwise it is unsound.
      //TODO: possibly != can be transformed into (<) | (>) and this also simplified
      val toReplace = ineqsR.filterNot(_.ms.head.c.abs == 1)
      if (toReplace.length != ineqsR.length) return conj
      else {
	reporter.debugBG(s"elimShadows: equivalent atoms are $equivalent")
	val res = toReplace.map(ZeroLTPoly(_)) ++ equivalent
	reporter.debugBG(s"elimShadows: after replacement of equiv atoms we have $res")
	return res
      }
    } else return conj
  }

  /** Experimental variant of eliminateMany that uses deferred instantiation and divisibility
    * constraint solving to limit the number of instances. */
  def eliminateManyX(g: Formula, xs: Iterable[VarOrSymConst], instContext: Map[VarOrSymConst, Int] = Map()): Formula = {
    reporter.debugBG(s"cooper.eliminateManyX($g, $xs, $instContext)")
    var resOr = List.empty[Formula]
    var localInstContext = instContext

    for (conj <- g.toListOr) {
      var conjRes = elimCheap(conj, xs)

      //at this point all cheap eliminations have been performed on conjRes
      //which is a list of conjuncts

      //using a while loop is safer than a for loop when mutating a variable
      //outside the loop body!
      val vars = xs.toList.tails
      var foundNeg = false
      while (vars.hasNext && !foundNeg) {
        vars.next match {
          case Nil => ()
          case (x :: remXs) =>
            val (conjX, conjNoX) = conjRes partition { _.contains(x) }
            if (conjX.nonEmpty) {
              //eliminate expects a conjunction
              val EliminationResult(conjXRes, j, r) = eliminateDefer(conjX.toAnd, x)
              localInstContext += (j -> r)
              reporter.debugBG(s"cooper: elimination result = $conjXRes")
              reporter.debugBG(s"cooper: where $j is in [1,$r]")
              //update conjRes with result and recursive call
              conjXRes match {
                case TrueAtom =>
                  conjRes = conjNoX
                case FalseAtom =>
                  foundNeg = true
                  conjRes = List(FalseAtom)
                case conjXRes @ Or(_, _) => //(a v b v c) & conjNoX ==> (a & conj) v (b & conj) v ...
                  val mult = (conjXRes.toListOr map { And(_, conjNoX.toAnd) }).toOr
                  reporter.debugBG("cooper: multplying solution to preserve structure...")
                  conjRes = eliminateManyX(mult, remXs, localInstContext).toListAnd
                case conjXRes @ And(_, _) => // must be a conjunction
                  conjRes = (conjXRes.toListAnd ::: conjNoX)
                case cr =>
                  conjRes = cr :: conjNoX //or simplification can produce a literal
              }
            }
        }
        reporter.debugBG(s"cooper: after that loop have conjRes=$conjRes")
      }

      //all variables eliminated from conjRes
      val h = conjRes.toAnd
      reporter.debugBG(s"cooper: all variables eliminated from $conj giving $h")
      if (h == TrueAtom) return h

      resOr ::= h
    } //end of outer for loop over disjuncts

    if (resOr.exists(_ == TrueAtom)) {
      reporter.debugBG(s"cooper: found T before instantiation")
      return TrueAtom
    }

    //replacing triangulated lits does not affect problem
    val js = localInstContext.keys.toList
    val r2 = (for { conj <- resOr } yield {
      val (divLits, rest) = conj.toListAnd.partition(_.isInstanceOf[DividesPolyAtom])
      try {
        val triLits = triangulateDiv(divLits.map(_.asInstanceOf[DividesPolyAtom]), js)
        // (triLits ++ rest).toAnd
        val resInner = solveDiv(triLits, rest.toAnd, localInstContext)

        reporter.debugBG(s"cooper: after div solution get $resInner")

        if (resInner == TrueAtom) return resInner

        resInner
      } catch {
        case FALSECONJUNCTION => FalseAtom
      }
    }).toOr.simplifyCheap

    val res = r2 /*(localInstContext.foldLeft(r2 :: Nil) { case (acc, (j, d)) =>
      acc flatMap { f =>
        for { l <- 1 to d } yield 
          replace(f, j, Polynomial.Const(l))
      }
    }).toOr.simplifyCheap*/

    reporter.debugBG("Elimination of " + xs + " from " + g + " gives " + res)
    res
  }

  /**
   * Assumes that g is a quantifier-free formula with normalized atoms (i.e. PolyAtoms), not already simplified.
   * leaves a result that is supposed to be simplified by the caller
   * (Rationale: simplification is best done on the context, i.e. the formula that contains g)
   * Multiplies out disjunctions resulting from instantiation.
   */
  def eliminateMany(g: Formula, xs: Iterable[VarOrSymConst]): Formula = {
    var resOr = List.empty[Formula]
    // println("\n=============================")
    // println("Elimination of " + xs + " from")
    // printer.pp(g)
    // println()
      for (conj <- g.toListOr) {
        // first we apply eliminate cheap to conj
        var conjRes = elimCheap(conj, xs)

        for (x ← xs) {
          // partition conjRes into two parts, one that contains x and one that doesn't
          val (conjX, conjNoX) = conjRes partition { _ contains x }
          if (conjX.nonEmpty) {
            // need to go the expensive way and eliminate x
	    val conjXRes = eliminate(conjX.toAnd, x)
            // Simplify cheap and look at the result. If it is a disjunction, multiply out with conjNoX
            conjXRes.simplifyCheap match {
              case TrueAtom => conjRes = conjNoX
              case FalseAtom => conjRes = List(FalseAtom)
              case conjXRes @ Or(_, _) =>
                conjRes = eliminateMany((conjXRes.toListOr map { el => (el :: conjNoX).toAnd }).toOr, xs).simplifyCheap.toListAnd
              case conjXRes =>
                // must be a conjunction
                conjRes = conjXRes.toListAnd ::: conjNoX
            }
          }
        }
        // After all variables eliminated from conjRes
        val h = conjRes.toAnd
        if (h == TrueAtom) return TrueAtom
        resOr ::= conjRes.toAnd
      }

      var res = resOr.toOr
      reporter.debugBG("Elimination of " + xs + " from " + g + " gives " + res)
      res
  }

 /**
  * Eliminate all explicit existential quantifiers from f and also the given parameters params.
  * Assume f has been normalized for quantifier elimination,
  * i.e. no universal quantifiers, and the boolean connectives
  * are Or, And, and Neg. 
  */
  def cooper(f: Formula, params: Iterable[SymConst]) = {

    /** Eliminate all explicit quantifiers, innermost first */
    def eliminateAll(f: Formula): Formula =
      if (f.isQuantifierFree)
        f
      else f match {
        // Cannot have an atom, because atoms are quantifier-free
        case UnOpForm(op, g)       ⇒ UnOpForm(op, eliminateAll(g))
        case BinOpForm(op, f1, f2) ⇒ BinOpForm(op, eliminateAll(f1), eliminateAll(f2))
        case Exists(xs, g) =>
          if (util.flags.cooperSolveDiv.value) eliminateManyX(toPolyAtomFormula(eliminateAll(g)), xs)
          else eliminateMany(toPolyAtomFormula(eliminateAll(g)), xs)
      }

    // First eliminate all explicit quantifiers, then the given parameters
    val r1 = eliminateAll(f)
    val r2 = toPolyAtomFormula(r1)

    val r3 =
      if (util.flags.cooperSolveDiv.value) eliminateManyX(r2, params)
      else eliminateMany(r2, params)

    stats.nrQECalls += 1
    r3.simplifyCheap
  }

}
