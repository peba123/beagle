package beagle.bgtheory.LIA.cooper

/** Implements a special version of Cooper QE that stores solutions for a single 
  * quantifier level.
  * This is distinct from constraint satisfaction because the variables have infinite domains
  * and distinct from linear programming because the formula contains disjunctions and divisibility
  * constraints */
object cooperIncr {

  //stats counters:
  //stats.nrIncrSubsume
  //stats.nrIncrCalls
  //stats.nrSolutionsFailed
  //stats.nrSolutionsReused
  //stats.nrSolutionsExtended

  import beagle._
  import fol._
  import util._
  import cooper._
  import bgtheory.LIA.{Monomial, Polynomial, IntSort}

  type CooperSoln = Map[VarOrSymConst, Polynomial]
  case class CannotContinueException(s: String) extends Message

  private def report(s: String) = reporter.debugBG("incr: "+s)

  /** A symbolic representation of a possible solution to a disjunct. */
  private sealed trait Soln {

    /** The free variables in the solution. */
    val vars: Set[VarOrSymConst]

    /** If the solution can be evaluated, get its final value. */
    def get: Int

    /** Replace a symbolic constant inside this solution. */
    def replace(x: VarOrSymConst, p: Polynomial): Soln

    /** Whether the solution can be evaluated. */
    def closed: Boolean = vars.isEmpty
  }

  /** An exact solution of the form x=t, although t may contain variables. */
  private case class SingleSoln(t: Polynomial, norm: Int) extends Soln {

    lazy val vars = t.varOrSymConsts

    def get = 
      if (!this.closed)
	throw CannotContinueException(s"All variables in $this should have been evaluated")
      else 
        t.k / norm

    def replace(x: VarOrSymConst, p: Polynomial) =
      SingleSoln(t.rearrange(x).replace(x,p), norm)
  }

  /** A solution to a disjunct of the negative infinity formula involves finding a lower bound. */
  private case class LBSoln(x: VarOrSymConst, j: Int, lcm: Int, norm: Int, ltBounds: Set[Polynomial], 
		    neBounds: Set[Polynomial]) extends Soln {

    // def closed =
    //   ltBounds.forall(_.ms.isEmpty) && neBounds.forall(_.ms.isEmpty)

    lazy val vars = (ltBounds ++ neBounds).flatMap(_.varOrSymConsts)

    def get = {
      // soln = -k.lcm + j, and
      // for all bi in ltBounds soln < bi, and 
      // for all ni in neBounds soln != ni
      assume(ltBounds.forall(_.getCoeff(x)==0), s"$x should not occur in a bound for $this")
      // if there are monomials remaining, then assume they can be set to zero
      //TODO- is this safe?
      val bounds = ltBounds.map(_.k) ++ (neBounds map { b => 
	if (b.getCoeff(x) == 1 && b.k == 0) 
	  throw CannotContinueException(s"Cannot handle $this") 
	else if (b.getCoeff(x) != 0) { 
	  (b.k.toDouble/(1 - b.getCoeff(x))).floor.toInt - 1 
	} else 
          b.k
      })

      if (bounds.isEmpty) 
        j / norm
      else {
	val lb = bounds.min - 1
	//lb - j / lcm
	val k = ((lb - j).toDouble / lcm).floor.toInt
	(k * lcm + j) / norm
      }
    }

    def replace(y: VarOrSymConst, p: Polynomial) = {
      LBSoln(x, j, lcm, norm,
	     ltBounds.map(_.rearrange(y).replace(y,p)), 
	     neBounds.map(_.rearrange(y).replace(y,p))
	   )
    }
  }

  /** Use elimShadows to test for unsatisfiability */
  private def isUnsat(g: Formula, xs: Iterable[VarOrSymConst]): Boolean = {
    // g = conj v ...
    val orRes = for (conj <- g.toListOr) yield {
      var conjRes: List[Formula] = conj.toListAnd
      for (x ← xs;
	   if conj.contains(x)) {
        val (conjX, conjNoX) = conjRes partition { _ contains x }
        //conj = conjX & conjNoX
	elimShadows(conjX, x).toAnd.simplifyCheap match {
          case TrueAtom => conjRes = conjNoX
          case FalseAtom => conjRes = List(FalseAtom)
          case _ => ()
        }
      }
      conjRes
    }
    val r1 = orRes.flatten.distinct
    if(r1.length < g.toListOr.length) 
      report(s"$g was shortened to $r1 by elimShadows!")
    r1 == List(FalseAtom)
  }

  /** Attempt to simplify f using subsume */
  private def simpSubsume(f: Formula, x: VarOrSymConst): Formula =
    elimSubsumption(rearrange(f, x), x) map { g =>
      report(s"Subsume reduced formula to $g")
      stats.nrIncrSubsume += 1
      g
    } getOrElse { rearrange(f, x) }

  /** Eliminate variables from g by a backtracking search over disjuncts of the B-formula in the Cooper
    * algorithm transform.
    * If possible, returns an assignment to `xs` that produces an equivalent QF-formula.
    * If this call is nested in other QE calls then the binding becomes meaningless- i.e. it is only useful if
    * this is the last QE step.
    * 
    * If a disjunct of the B-formula is satisfied, then  this corresponds to an exact solution, 
    * and this is returned as `Some(f, solution)` where `f` is `g` with the substitution applied.
    * If no disjunct from a B-formula is satisfied under any binding, then choose a disjunct from the inf-
    * formula that is true (or at least not false).
    * This is not immediately a solution but usually one can find a lower bound of all < and != atoms in the
    * formula to find a solution.
    * Currently this returns None only when all of the disjuncts produced by Cooper are false.
    * If it finds a solution it cannot solve (i.e. the bounds have unknown variables in them) it throws an
    * InternalError.
    */
  def elimIncr(gIn: Formula, xs: Iterable[VarOrSymConst], oldSoln: Option[CooperSoln] = None): Option[(Formula, CooperSoln)] = {
    //TODO- use elimCheap as a pre-test
    //TODO- expression simplify?

    stats.nrIncrCalls += 1
    report(s"called on $gIn\n --extending $oldSoln")

    // A single elimination step in cooper's algorithm, eliminating x from f.
    // Recursively expands just the B-formula disjuncts searching for a literal that is
    // True wrt `binds`.
    // If none is found then explore the Inf-formulas for a solution also.
    def h(f: Formula, x: VarOrSymConst, xs: Iterable[VarOrSymConst], 
	  binds: Map[VarOrSymConst, Soln]): Option[Map[VarOrSymConst, Soln]] = {

      report(s"eliminate $x from $f")
      if (!f.contains(x)) {
        report(s"$x not found, binding to 0")
        if (xs.isEmpty)
          return Some(binds + (x -> SingleSoln(Polynomial.Zero, 1)))
        else
          return h(f, xs.head, xs.tail, binds + (x -> SingleSoln(Polynomial.Zero, 1)))
      }

      //this normalises the formula by taking the gcd of all of the x coeffs and 
      //writing (d | x) & normed_form ...
      //therefore the *actual* solution value needs to undo this normalisation factor
      //it must be stored along with the solution
      val f2 = simpSubsume(f, x)
      val working = unitCoeffs(f2, x)
      
      val xNorm =
        util.lcm(f2.atoms collect {
          case pa: PolyAtom if (pa.contains(x)) => pa.p.getCoeff(x)
        })

      val infF: Formula = infty(working, x).simplifyCheap
      report(s"inf formula for $x is $infF")
      
      val lcm = divisibilityConstsLcm(working, x)

      //we will replace with b+j where b is a bound polynomial
      val j = AbstVar("j_" + x, IntSort)
      val jMonomial = Monomial(1, j)

      //the disjuncts over the bounds (B-set)
      val minfs: Iterable[(Polynomial, Formula)] = {
        //complicate sort function that attempts to choose bound with low # of vars
        //reducing branch ratio or low coeff if vars are the same.
        //throws an error if you just use # vars to compare.
	val bounds = lowerBounds(working, x).toList
          .sortWith((b1, b2) =>
          (b1.varOrSymConsts.size < b2.varOrSymConsts.size) ||
            ( (b1.varOrSymConsts.size == b2.varOrSymConsts.size) &&
            ( if (b1.ms.nonEmpty)
              (b1.ms.head.c <= b2.ms.head.c)
            else
              b1.k <= b2.k )))

	report(s"B-Set for $x is ${bounds.mkString("{",",","}")}")
	bounds map { b => (b + jMonomial, replace(working, x, b + jMonomial).simplifyCheap) }
      }
      
      for { (b, disj) <- minfs
	jval <- { report(s"select b=$b"); (1 to lcm) }
	if (disj != FalseAtom) } {

	val newF = replace(rearrange(disj, j), j, Polynomial.Const(jval)).simplifyCheap
	report(s"j=$jval yields $newF")

	if (newF == TrueAtom) {
	  //ie. found a satisfied disjunct
	  val newB = b.replace(j, Polynomial.Const(jval))
	  val resBinds = binds.mapValues(_.replace(x, newB)) + (x -> SingleSoln(newB, xNorm))
	  
	  report(s"satisfying binding found:\n$resBinds")
	  
	  return Some(resBinds)
	} else if(newF != FalseAtom && !xs.isEmpty){
	  //does not immediately falsify so store as a binding and recurse
	  //store as a binding (overwrites previous ones!)
	  val newB = b.replace(j, Polynomial.Const(jval))
	  report(s"adding binding $x -> $newB")

	  val resBinds = binds.mapValues(_.replace(x, newB)) + (x -> SingleSoln(newB, xNorm))

	  //recursive case
	  val res = h(newF, xs.head, xs.tail, resBinds)
	  //return if this actually finds a solution
	  if (res.nonEmpty) return res
	  else report(s"backtrack to $x")
	}
	//otherwise xs is Empty and we continue to check inf-formula
      }

      //could test infF:
      for (jval <- 1 to lcm) {
	//in the infinity formula we replace x with j not b+j
	val fj = replace(infF, x, Polynomial.Const(jval)).simplifyCheap

	if (fj == TrueAtom) {
	  //return and test
	  val lb = (working.atoms collect { 
	    case ZeroLTPoly(p) if (p.getCoeff(x) == -1) => p.copy(ms = p.ms.filterNot(_.x == x)) 
	  }).toSet
	  val ne = (working.atoms collect { 
	    case ZeroNEPoly(p) if(p.getCoeff(x) != 0) => 
	      p.copy(ms = p.ms.filterNot(_.x == x)) * (-1 * p.getCoeff(x)) 
	  }).toSet
	  //these are set to false
	  val ub = (working.atoms collect { 
	    case ZeroLTPoly(p) if(p.getCoeff(x) == 1) => p.copy(ms = p.ms.filterNot(_.x == x)) 
	  }).toSet
	  val eq = (working.atoms collect { 
	    case ZeroEQPoly(p) if(p.getCoeff(x) != 0) => p.copy(ms = p.ms.filterNot(_.x == x)) 
	  }).toSet

	  report(
	    s"Possible solution in inf-formula, less than:\n   $lb\n --not equal to:\n  $ne"
	  )
	  report(s"must falsify UBs: $ub and EQ: $eq")

	  //TODO- this is possibly not a closed solution 
	  //i.e. we might not be able to solve this bound
	  return Some(binds + (x -> LBSoln(x, jval, lcm, xNorm, lb, ne)))
	}
	else if (fj != FalseAtom && !xs.isEmpty) {
	  //recurse
	  report("Adding an inf-formula constraint")
	  //these are set to true
	  val lb = (working.atoms collect { 
	    case ZeroLTPoly(p) if(p.getCoeff(x) == -1) => p.copy(ms = p.ms.filterNot(_.x==x)) 
	  }).toSet
	  val ne = (working.atoms collect { 
	    case ZeroNEPoly(p) if(p.getCoeff(x) != 0) => 
	      p.copy(ms = p.ms.filterNot(_.x==x)) * (-1 * p.getCoeff(x)) 
	  }).toSet
	  //these are set to false
	  val ub = (working.atoms collect { 
	    case ZeroLTPoly(p) if(p.getCoeff(x) == 1) => p.copy(ms = p.ms.filterNot(_.x==x)) 
	  }).toSet
	  val eq = (working.atoms collect { 
	    case ZeroEQPoly(p) if(p.getCoeff(x) != 0) => p.copy(ms = p.ms.filterNot(_.x==x)) 
	  }).toSet

	  report(s"must be less than:\n   $lb\n --and not equal to:\n  $ne")
	  report(s"must falsify UBs: $ub and EQ: $eq")

	  val res = h(fj, xs.head, xs.tail, binds + (x -> LBSoln(x, jval, lcm, xNorm, lb, ne)))
	  if(!res.isEmpty) return res
	}
      }

      report(s"No satisfying bindings for $x under $binds")

      return None
    }

    def elimCheapFilter(g: Formula, xs: Iterable[VarOrSymConst]): Formula = {
      (for (conj <- g.toListOr) yield {
        elimCheap(conj, xs).toAnd
      }).toOr.simplifyCheap
    }

    /** Apply an existing solution to a formula, similar to applySubst
      * except using polynomials. */
    def applySoln(f: Formula, soln: CooperSoln): Formula = soln.foldLeft(f) { 
	case (acc, (x, v)) => replace(rearrange(acc, x), x, v)
      } simplifyCheap

    /* begin elimIncr */
    val g = gIn.simplifyCheap

    if (xs.isEmpty) {
      report("There were no parameters to eliminate")
      if (g != FalseAtom) {
        if (oldSoln.nonEmpty)
	  return (oldSoln map { s => (applySoln(g, s), s) })
        else
          return Some((g, Map.empty[VarOrSymConst, Polynomial]))
      } else
        return None
    }

    //either use an old solution or try a new one
    val res: Option[Map[VarOrSymConst, Soln]] = oldSoln flatMap { soln =>
      
      val evalG = applySoln(g, soln)
      report(s"wrt existing solution, formula is $evalG")

      //variables which remain to be eliminated after application of old solution
      val remainingXs = xs.filterNot(soln.isDefinedAt(_))
      report(s"$remainingXs remain for elimination")

      if (evalG == FalseAtom) {
	stats.nrSolutionsFailed += 1
	report("Extension of existing solution FAILED!\nRetrying...")
	None //steps thru to orElse block
      } else if(remainingXs.isEmpty) {
	stats.nrSolutionsReused += 1
	report("Existing solution OK!")
	Some(soln.mapValues(SingleSoln(_, 1)))
      } else {
	report("Extending solution")
        if (elimCheapFilter(evalG, remainingXs) == FalseAtom)
          None
        else
	  h(evalG, remainingXs.head, remainingXs.tail, soln.mapValues(SingleSoln(_, 1)))
	//if None we still need to check with empty map...
      }
    } orElse {       // Try to find a new solution if none of the above has worked...
      if (elimCheapFilter(g, xs) == FalseAtom)
        None
      else
        h(g, xs.head, xs.tail, Map())
    }

    report(s"Unevaluated solution $res")

    res map { r =>
      //guess unconstrained variables
      //for any open RHS variables which are not on the LHS
      val unconstrained: Set[VarOrSymConst] =
        r.values.flatMap(_.vars).toSet -- (r.keys.toSet)

      //guess =0 and update
      val s0: CooperSoln = unconstrained.map((_ -> Polynomial.Zero)).toMap
      report(s"These values are guessed $s0")

      val rWithGuesses: Map[VarOrSymConst,Soln] = s0.foldLeft(r) {
	case (acc, (x, v)) => acc.mapValues(_.replace(x, v))
      }

      //closed solns and non-closed solns
      var (closed, open) = rWithGuesses.partition(_._2.closed)
      //accumulate all closed solutions as polynomials
      var closedAcc = closed.mapValues(v => Polynomial.Const(v.get))

      while (open.nonEmpty) {
	val open2 = closedAcc.foldLeft(open) { 
	  case (acc, (x, v)) => acc.mapValues(_.replace(x, v)) 
	}

	val (newClosed, remOpen) = open2.partition(_._2.closed)

	if (newClosed.isEmpty) {
	  report(s"$remOpen could not be resolved")
	  throw CannotContinueException(s"$remOpen could not be resolved")
	} else {
	  closedAcc = closedAcc ++ (newClosed.mapValues(v => Polynomial.Const(v.get)))
	  open = remOpen
	}
      }

      //open is empty and closedAcc has all solutions to rWithGuesses
      val res2: CooperSoln = closedAcc ++ s0
      
      if (oldSoln != None) stats.nrSolutionsExtended += 1
      report("SOLUTION: \n"+res2.map(p => p._1+" = "+p._2).mkString("\n"))

      val resultSoln = applySoln(g, res2)

      if (resultSoln == FalseAtom) 
	throw CannotContinueException(s"$res2 was not actually a solution to $g")
      (resultSoln, res2)
    }
  }

}
