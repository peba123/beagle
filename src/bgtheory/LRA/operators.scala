package beagle.bgtheory.LRA

import beagle._
import fol.Term
import fol.PFunTerm
import fol.Operator
import fol.DomElem
import fol.BG
import fol.Arity0
import datastructures.PredEqn
import datastructures.Eqn
import util.Rat

case class DomElemRat(r: Rat) extends DomElem[Rat] {
  val h = this
  lazy val op = new Operator(BG, r.toString, Arity0(RatSort)) {
    override def toTerm = Some(h)
  }
  val value = r
}

object LessEqEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RatSort && rhs.sort == RatSort)
    PredEqn(PFunTerm(LessEqOpRat, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(LessEqOpRat, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object LessEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RatSort && rhs.sort == RatSort)
    PredEqn(PFunTerm(LessOpRat, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(LessOpRat, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RatSort && rhs.sort == RatSort)
    PredEqn(PFunTerm(GreaterEqOpRat, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(GreaterEqOpRat, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object GreaterEqn {
  def apply(lhs: Term, rhs: Term) = { 
    require(lhs.sort == RatSort && rhs.sort == RatSort)
    PredEqn(PFunTerm(GreaterOpRat, List(lhs, rhs)))
  }
  def unapply(e: Eqn) = e match {
    case PredEqn(PFunTerm(GreaterOpRat, List(lhs, rhs))) => Some((lhs, rhs))
    case _ => None
  }
}

object UMinus {
  def apply(s: Term) = PFunTerm(UMinusOpRat, List(s))

  def unapply(s: Term) = s match {
    case PFunTerm(UMinusOpRat, List(t)) ⇒ Some(t)
    case _                                        ⇒ None
  }
}

object Sum {
  def apply(s1: Term, s2: Term) = PFunTerm(SumOpRat, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(SumOpRat, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Difference {
  def apply(s1: Term, s2: Term) = PFunTerm(DifferenceOpRat, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(DifferenceOpRat, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Product {
  def apply(s1: Term, s2: Term) = PFunTerm(ProductOpRat, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(ProductOpRat, List(t1, t2)) ⇒ Some((t1, t2))
    case _ ⇒ None
  }
}

object Quotient {
  def apply(s1: Term, s2: Term) = PFunTerm(QuotientOpRat, List(s1, s2))

  def unapply(s: Term) = s match {
    case PFunTerm(QuotientOpRat, List(t1, t2))  ⇒ Some((t1, t2))
    case _                                      ⇒ None
  }
}
