package beagle.bgtheory.LRA

import beagle._
import fol._
// import fol.Signature._
import datastructures._
import util._
import bgtheory._

object simpRules extends Simplification { 

  val simpRulesTermSafe: List[SimpRule[Term]] = List(
    //combine all SimpRules here for better performance
    new SimpRule[Term]("evalGround", {
      case Sum(i1: DomElemRat, i2: DomElemRat)        => DomElemRat(i1.value + i2.value)
      case Product(i1: DomElemRat, i2: DomElemRat)    => DomElemRat(i1.value * i2.value)
      case Difference(i1: DomElemRat, i2: DomElemRat) => DomElemRat(i1.value - i2.value)
      case s @ Quotient(i1: DomElemRat, i2: DomElemRat)   => 
        if (i1 == ZeroRat) ZeroRat
        else if (i2 == ZeroRat) {
          println("** WARNING: div by zero in $quotient: "+s)
          ZeroRat
        } else DomElemRat(i1.value / i2.value)
      case UMinus(i1: DomElemRat)                     => DomElemRat(i1.value * -1)
      case t                                          => throw NotApplicable
    }),

    new SimpRule[Term]("evalToX", {
      case PFunTerm(op: ToIntOp, List(i: DomElemRat)) ⇒ 
	LIA.DomElemInt( math.floor(i.value.toDouble).toInt )
      case PFunTerm(op: ToRatOp, List(s)) if s.sort == LRA.RatSort ⇒ s
      case PFunTerm(op: ToRealOp, List(i: DomElemRat)) ⇒ LFA.DomElemReal(i.value)
      case _                      ⇒ throw NotApplicable
    }),

    //used to recover from moving NL product to the foreground, if possible.
    new SimpRule[Term]("replaceNLPP", {
      case NLPPOp(l: DomElemRat, r) => LRA.Product(l, r)
      case NLPPOp(l, r: DomElemRat) => LRA.Product(l, r)
      case _                        => throw NotApplicable
    })
  )

  val simpRulesTermUnsafe: List[SimpRule[Term]] = List(

    new SimpRule[Term]("orient", {
      case Sum(t: Term, i: DomElemRat) if !t.isDomElem ⇒ Sum(i, t)
      case Difference(t: Term, i: DomElemRat) if !t.isDomElem ⇒ Sum(DomElemRat(i.value.uminus), t)
      case Product(t: Term, i: DomElemRat) if !t.isDomElem ⇒ Product(i, t)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalSum2", {
      case Sum(i1: DomElemRat, Sum(i2: DomElemRat, t)) ⇒ Sum(DomElemRat(i1.value + i2.value), t)
      case Sum(i1: DomElemRat, Difference(i2: DomElemRat, t)) ⇒ Difference(DomElemRat(i1.value + i2.value), t)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalUMinus", {
      case UMinus(UMinus(t)) ⇒ t
      case _                        ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("assoc", {
      case Sum(Sum(s: Term, t: Term), u: Term) ⇒ Sum(s, Sum(t, u))
      case Sum(Difference(s: Term, t: Term), u: Term) ⇒ Sum(s, Difference(u, t))
      case Difference(Sum(s: Term, t: Term), u: Term) ⇒ Sum(s, Difference(t, u))
      case Difference(Difference(s: Term, t: Term), u: Term) ⇒ Difference(s, Sum(t, u))
      case Sum(s: Term, Sum(i: DomElemRat, t)) if !s.isDomElem ⇒ Sum(i, Sum(s, t))
      case Sum(s: Term, Difference(i: DomElemRat, t)) if !s.isDomElem ⇒ Sum(i, Difference(s, t))
      case Difference(s: Term, Sum(i: DomElemRat, t)) if !s.isDomElem ⇒ Sum(UMinus(i), Difference(s, t))
      case Difference(s: Term, Difference(i: DomElemRat, t)) if !s.isDomElem ⇒ Sum(UMinus(i), Sum(s, t))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalDifference2", {
      case Difference(i1: DomElemRat, Difference(i2: DomElemRat, t)) ⇒ Sum(DomElemRat(i1.value - i2.value), t)
      case Difference(i1: DomElemRat, Sum(i2: DomElemRat, t)) ⇒ Difference(DomElemRat(i1.value - i2.value), t)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalNeutral", {
      case Difference(t: Term, s: Term) if (s == ZeroRat) ⇒ t
      case Difference(t: Term, s: Term) if (t == ZeroRat) ⇒ s
      case Sum(s: Term, t: Term) if (s == ZeroRat) ⇒ t
      case Sum(s: Term, t: Term) if (t == ZeroRat) ⇒ s
      case Product(s: Term, t: Term) if (s == OneRat) ⇒ t
      case Product(s: Term, t: Term) if (t == OneRat) ⇒ s
      case Quotient(t: Term, s: Term) if (s == OneRat) ⇒ t
      case Quotient(t: Term, s: Term) if (s == MinusOneRat) ⇒ UMinus(t)
      case Quotient(t: Term, s: Term) if (t == ZeroRat) ⇒ ZeroRat
      case _                            ⇒ throw NotApplicable
    }))

  // Rules to simplify literals
  val simpRulesLitSafe: List[SimpRule[Lit]] = List(

    // This rule gets rid of all >-literals
    new SimpRule[Lit]("NormalizeInequality", {
      case Lit(isPos, GreaterEqn(d1, d2)) ⇒ Lit(isPos, LessEqn(d2, d1))
      case Lit(isPos, GreaterEqEqn(d1, d2)) ⇒ Lit(isPos, LessEqEqn(d2, d1))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalLess", {
      case Lit(true, LessEqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (d1.value < d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, LessEqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (!(d1.value < d2.value)) Lit.TrueLit else Lit.FalseLit
      case Lit(isPos, LessEqn(t1, t2)) if (t1 == t2) =>
	if (isPos) Lit.FalseLit else Lit.TrueLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalLessEq", {
      case Lit(true, LessEqEqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (d1.value <= d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, LessEqEqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (!(d1.value <= d2.value)) Lit.TrueLit else Lit.FalseLit
      case Lit(sign, LessEqEqn(t1, t2)) if (t1 == t2) =>
	if (sign) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalEqual", {
      case Lit(true, Eqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (d1.value == d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, Eqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (d1.value != d2.value) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalIsX", {
      case Lit(t, PredEqn(PFunTerm(op: IsIntOp, List(d: DomElemRat)))) ⇒
	    if (d.value.isRatInt == t) Lit.TrueLit else Lit.FalseLit
      case Lit(t, PredEqn(PFunTerm(op: IsRatOp, List(s)))) if s.sort == RatSort ⇒
	    if (t) Lit.TrueLit else Lit.FalseLit
      case Lit(t, PredEqn(PFunTerm(op: IsRealOp, List(s)))) if s.sort == RatSort ⇒
	    if (t) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    })
  )

  val simpRulesLitUnsafe: List[SimpRule[Lit]] = List(

    // Incomplete
    new SimpRule[Lit]("sumDiffEqual1", {
      case Lit(isPos, Eqn(Sum(d1: DomElemRat, t), d2: DomElemRat)) ⇒ Lit(isPos, Eqn(t, DomElemRat(d2.value - d1.value)))
      case Lit(isPos, Eqn(Difference(d1: DomElemRat, t), d2: DomElemRat)) ⇒ Lit(isPos, Eqn(t, DomElemRat(d1.value - d2.value)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumDiffEqual2", {
      case Lit(isPos, Eqn(Sum(d1: DomElemRat, t1), Sum(d2: DomElemRat, t2))) ⇒ Lit(isPos, Eqn(t1, Sum(DomElemRat(d2.value - d1.value), t2)))
      case Lit(isPos, Eqn(Difference(d1: DomElemRat, t1), Difference(d2: DomElemRat, t2))) ⇒ Lit(isPos, Eqn(t1, Sum(DomElemRat(d1.value - d2.value), t2)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorEqual1", {
      case Lit(isPos, Eqn(Product(d1: DomElemRat, t), d2: DomElemRat)) if (d1.value != 0) ⇒ Lit(isPos, Eqn(t, DomElemRat(d2.value / d1.value)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorEqual2", {
      case Lit(isPos, Eqn(Product(d1: DomElemRat, t1), Product(d2: DomElemRat, t2))) if (d1.value != 0) ⇒ Lit(isPos, Eqn(t1, Product(DomElemRat(d2.value / d1.value), t2)))
      case Lit(isPos, Eqn(Product(d1: DomElemRat, t1), Product(d2: DomElemRat, t2))) if (d2.value != 0) ⇒ Lit(isPos, Eqn(Product(DomElemRat(d1.value / d2.value), t1), t2))
      case _ ⇒ throw NotApplicable
    }),

    // Incomplete
    new SimpRule[Lit]("factorLessEq", {
      case Lit(isPos, LessEqEqn(Product(d1: DomElemRat, t), d2: DomElemRat)) if (d1.value != 0) ⇒ Lit(isPos, LessEqEqn(t, DomElemRat(d2.value / d1.value)).swapIfNeg(d1.value.signum))
      case Lit(isPos, LessEqEqn(d2: DomElemRat, Product(d1: DomElemRat, t))) if (d1.value != 0) ⇒ Lit(isPos, LessEqEqn(DomElemRat(d2.value / d1.value), t).swapIfNeg(d1.value.signum))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorLess", {
      case Lit(isPos, LessEqn(Product(d1: DomElemRat, t), d2: DomElemRat)) if (d1.value != 0) ⇒ Lit(isPos, LessEqn(t, DomElemRat(d2.value / d1.value)).swapIfNeg(d1.value.signum))
      case Lit(isPos, LessEqn(d2: DomElemRat, Product(d1: DomElemRat, t))) if (d1.value != 0) ⇒ Lit(isPos, LessEqn(DomElemRat(d2.value / d1.value), t).swapIfNeg(d1.value.signum))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumLessEq", {
      case Lit(isPos, LessEqEqn(Sum(d1: DomElemRat, t), d2: DomElemRat)) ⇒ Lit(isPos, LessEqEqn(t, DomElemRat(d2.value - d1.value)))
      case Lit(isPos, LessEqEqn(d2: DomElemRat, Sum(d1: DomElemRat, t))) ⇒ Lit(isPos, LessEqEqn(DomElemRat(d2.value - d1.value), t))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumLess", {
      case Lit(isPos, LessEqn(Sum(d1: DomElemRat, t), d2: DomElemRat)) ⇒ Lit(isPos, LessEqn(t, DomElemRat(d2.value - d1.value)))
      case Lit(isPos, LessEqn(d2: DomElemRat, Sum(d1: DomElemRat, t))) ⇒ Lit(isPos, LessEqn(DomElemRat(d2.value - d1.value), t))
      case _ ⇒ throw NotApplicable
    }) // todo: similar rules for difference
    )

}
