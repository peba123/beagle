package beagle.bgtheory.LRA

import beagle._
import fol.VarOrSymConst
import datastructures._
// import calculus._
import util._
import bgtheory._

//Java libraries for Simplex
import org.apache.commons.math3.optim.linear._
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType.MAXIMIZE
import collection.JavaConverters._

/** Linear Rational Arithmetic
 * Provides methods for quantifier elimination, consistency checking and simplification.*/
abstract class LRASolver extends Solver {
  val theoryName = "LRA"
  // val canUNSATCore = false // can be overridden
  // def hasSolverSignature(cl: Clause) = cl.isLRA
  def QE(cl: ConsClause) = fourierMotzkin.QE(cl)

  val simpRulesTermSafe = simpRules.simpRulesTermSafe
  val simpRulesTermUnsafe = simpRules.simpRulesTermUnsafe
  val simpRulesLitSafe = simpRules.simpRulesLitSafe
  val simpRulesLitUnsafe = simpRules.simpRulesLitUnsafe

}

/** Uses Simplex Solver from Apache Commons to implement check() for ground LRA-clauses. */
object SimplexSolver extends LRASolver {
  import Polynomial._

  val name = "simplex"
  val needsUnitClauses = true

  private val allowedOps = Set(SumOpRat, UMinusOpRat, GreaterOpRat, LessOpRat, DifferenceOpRat, 
			 ProductOpRat, LessEqOpRat, GreaterEqOpRat)

  def hasSolverLiterals(cl: ConsClause): Boolean = {
    //check operators to account for the mixed theory case
    //non-linear products are filtered since they are replaced with FG-kinded nlpp operators
    //quotient and other misc operators should have been filtered too.
    //signature should be that of linear terms, no constants
    cl.operators.forall(allowedOps contains _)
  }

  /** simply return the clause for now */
  def toSolverLiterals(cl: ConsClause): ConsClause = cl
  //TODO- could normalise quotient by multiplication if present, but no guarantees of linearity after

  /** Case where there are only universal variables and no LRA constants in the clauses
    * Check by negating each clause and searching for a counter-example.
    * The simplex solver accepts conjunctions of linear constraints which may have operators =, >=, <=
    * Literals s < t are translated to d <= t - s for 0 < d and set the objective function to maximise d.
    * If d <= 0 in the solution, then assume no solution exists.
    * This case is possibly not used, due to assumptions in asSolverClauses (i.e. that the argument be ground)*/
  private def checkVarsOnly(cls: Iterable[ConsClause]): SolverResult = {

    //shadow variable d=delta
    val delta = fol.AbstVar("delta", RatSort)

    for { c <- cls } {
      reporter.debugBG(s"--testing $c")

      //give a canonical order to vars
      //note that delta is always the 0th elt.
      val cVars = delta :: c.vars.toList
      reporter.debugBG("vars: " + cVars.mkString(", "))

      //trivial objective function
      val objFun = new LinearObjectiveFunction(1.0 +: Array.fill(cVars.length-1)(0.0), 0)

      //must negate literals then eliminate disequations
      //but this means that multiple clauses can be created:
      //c & (a != b) ==> c & (a < b | b < a)
      //must be split into conjuncts again...
      
      val (neqLits, rem) = c.lits map {
        _.compl
      } partition {
        case Lit(false, PredEqn(_)) => false
        case Lit(false, Eqn(_, _)) => true
        case _ => false
      }

      //rem translated to constraint form
      val remC = rem map { litToConstraint(_, delta, cVars.tail) }

      val neqC = neqLits.foldLeft(List.empty[List[LinearConstraint]]) { (acc, l) =>
        val Lit(_, Eqn(s, t)) = l
        val c1 = litToConstraint(Lit(true, LessEqn(s, t)), delta, cVars.tail)
        val c2 = litToConstraint(Lit(true, LessEqn(t, s)), delta, cVars.tail)

        if (acc.isEmpty) List(List(c1), List(c2))
        else (acc map { c1 :: _ }) ++ (acc map { c2 :: _ })
      }

      //multiply out over remainder lits
      val resCs =
        if (neqC.isEmpty) List(remC)
        else neqC map { _ ++ remC }

      for { constraints <- resCs } {

        try {
          reporter.debugBG("Simplex called on:\n"+
            constraints.map(showConstr(_)).mkString("\n"))

          val solution = new SimplexSolver().optimize(objFun, new LinearConstraintSet(constraints.asJava), MAXIMIZE)

          reporter.debugBG("Simplex-- solution found:\n" +
            cVars.mkString(" ") + "\n" +
            solution.getPoint().mkString(" "))

          val deltaSoln = solution.getPoint()(0)
          if (deltaSoln <= 0) //no solution
            ()
          else {
            reporter.debugBG("Simplex reports INVALID")
            return SolverResult(false)
          }
        } catch {
          case e: NoFeasibleSolutionException => //failed, continue with other disjuncts
            reporter.debugBG("Simplex-- no feasible solution")
          case e: UnboundedSolutionException =>
            reporter.debugBG("Simplex-- unbounded solution")
            reporter.debugBG("Simplex reports INVALID")
            return SolverResult(false) //has a solution, but cannot be optimized
        }
      }
    }
    //after the outer for loop, all disjuncts checked
    reporter.debugBG("Simplex-- all disjuncts checked, counter-example not found")
    reporter.debugBG("Simplex reports VALID")
    return SolverResult(true)
  }

  /** Assumes no universal variables and only unit clauses in cls.
    * Eliminates implicit existential quantification (of LRA constants) via Simplex. */
  private def checkConstsOnly(cls: Iterable[ConsClause]): SolverResult = {
    reporter.debugBG("eliminate constants only")

    //shadow variable used to support strict inequalities
    val delta = fol.BGConst("delta", RatSort)

    val lits = cls map { c =>
      assert(c.isUnitClause, "Simplex only works for unit clauses")
      c.lits.head
    } toList

    //give a canonical order to vars
    val cVars = delta :: lits.symConsts.filter(_.isBG).toList
    reporter.debugBG("vars: " + cVars.mkString(", "))

    //trivial objective function
    //delta will be maximized to ensure d > 0 if at all possible
    val objFun = new LinearObjectiveFunction(1.0 +: Array.fill(cVars.length-1)(0.0), 0)

    //must eliminate disequations (a != b) <==> (a < b | b < a)
    //but this means splitting into conjuncts again...
    
    val (neqLits, rem) = lits partition {
      case Lit(false, PredEqn(_)) => false
      case Lit(false, Eqn(_, _)) => true
      case _ => false
    }

    //expand negated equalities and translate to constraints
    //this step also multiplies the new clauses together
    val neqC = neqLits.foldLeft(List.empty[List[LinearConstraint]]) { (acc, l) =>
      val Lit(_, Eqn(s, t)) = l
      val c1 = litToConstraint(Lit(true, LessEqn(s, t)), delta, cVars.tail)
      val c2 = litToConstraint(Lit(true, LessEqn(t, s)), delta, cVars.tail)

      if (acc.isEmpty) List(List(c1), List(c2))
      else (acc map { c1 :: _ }) ++ (acc map { c2 :: _ })
    }

    //rem translated to constraint form
    val remC = rem map { litToConstraint(_, delta, cVars.tail) }

    //multiply out over remaining lits
    //resCs will be in DNF
    val resCs =
      if (neqC.isEmpty) List(remC)
      else neqC map { _ ++ remC }

    //solutions show that the formula is valid, not counter-satisfiable.
    for { constraints <- resCs } {

      try {
        reporter.debugBG("Simplex called on:\n"+
          constraints.map(showConstr(_)).mkString("\n"))

        val solution = new SimplexSolver().optimize(objFun, new LinearConstraintSet(constraints.asJava), MAXIMIZE)

        reporter.debugBG("Simplex-- solution found:\n" +
          cVars.mkString(" ") + "\n" +
          solution.getPoint().mkString(" "))

        val deltaSoln = solution.getPoint()(0)
        if (deltaSoln <= 0) //no solution
          () //continue with other disjuncts
        else { //valid solution has delta > 0
          reporter.debugBG("Simplex reports VALID")
          return SolverResult(true)
        }
      } catch {
        case e: NoFeasibleSolutionException => //failed, continue with other disjuncts
          reporter.debugBG("Simplex-- no feasible solution")
        case e: UnboundedSolutionException =>
          reporter.debugBG("Simplex-- unbounded solution")
          reporter.debugBG("Simplex reports VALID")
          return SolverResult(true) //has a solution, but cannot be optimized
      }
    }

    reporter.debugBG("Simplex-- all disjuncts checked, solution not found")
    reporter.debugBG("Simplex reports INVALID")
    return SolverResult(false)
  }

  /** Convert a Lit to a constraint for Simplex.
    * Constraints use relations EQ, LEQ and GEQ, so no negated equations are accepted.
    * Instead these must be expanded to (s < t v t < s).
    * In order to support strict inequalities, this uses a shadow variable delta and translates
    * s < t to d <= t - s, tacitly assuming that 0 < d will be checked later.*/
  private def litToConstraint(l: Lit, delta: VarOrSymConst, vars: Iterable[VarOrSymConst]): LinearConstraint = {
    //produce the coefficient array wrt vars
    def coeffArray(p: Polynomial): Array[Double] =
      ((delta::vars.toList) map { y =>
        p.ms.find(_.x == y).map(_.c.toDouble).getOrElse(0.0)
      }).toArray

    l match {
      case Lit(true, LessEqn(s, t))    => {
        //s < t becomes 0 < t - s then 0 < delta <= t - s then -k <= -delta + t - s
        val s1 = toPolynomial(t) - toPolynomial(s) + Monomial(Rat(-1,1), delta)
        new LinearConstraint(coeffArray(s1), Relationship.GEQ, -s1.k.toDouble)
      }
      case Lit(true, LessEqEqn(s, t))  => {
        // s <= t becomes s - t <= -k
        val s1 = toPolynomial(s) - toPolynomial(t)
        new LinearConstraint(coeffArray(s1), Relationship.LEQ, -s1.k.toDouble)
      }
      case Lit(true, Eqn(s, t)) => {
        //s = t becomes s - t = -k
        val s1 = toPolynomial(s) - toPolynomial(t)
        new LinearConstraint(coeffArray(s1), Relationship.EQ, -s1.k.toDouble)
      }
      case Lit(false, LessEqn(s, t))   => {
        // !(s < t) becomes (s >= t) becomes s - t >= -k
        val s1 = toPolynomial(s) - toPolynomial(t)
        new LinearConstraint(coeffArray(s1), Relationship.GEQ, -s1.k.toDouble)
      }
      case Lit(false, LessEqEqn(s, t)) => {
        //!(s <= t) becomes s > t then s - t >= d > 0 then s - t - delta >= -k
        val s1 = toPolynomial(s) - toPolynomial(t) + Monomial(Rat(-1,1), delta)
        new LinearConstraint(coeffArray(s1), Relationship.GEQ, -s1.k.toDouble)
      }
    }
  }

  private def showConstr(lc: LinearConstraint): String = {
    lc.getCoefficients().toArray().mkString(" ") + " " +
    lc.getRelationship() + " " +
    lc.getValue()
  }

  /** Expects unit clauses of linear terms with no constant symbols.
    * Calls simplex solver on negated clause literals (after multiplying out negated equations).
    * Solutions to the linear programming problem (1 or more per clause, depending on literals)
    * are counter-examples to the clause. If a solution is found for any clause then the set is unsat.
    * Otherwise it is sat. */
  def check(cs: Iterable[ConsClause]): SolverResult = {
    reporter.debugBG(s"Simplex.check on $cs")

    val cls = cs.toList

    if (cls.isEmpty) SolverResult(true) //strange input cf NUM901=1
    else if (cls.vars.nonEmpty && !cls.symConsts.exists(_.isBG))
      checkVarsOnly(cls)
    else if (cls.vars.isEmpty && cls.symConsts.nonEmpty)
      checkConstsOnly(cls)
    else {
      throw new InternalError("Unexpected quantifier structure in Simplex.check(): "+cs)
      UNKNOWN
    }

  }

}
