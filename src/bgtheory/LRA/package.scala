package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
import datastructures._
import calculus._
import util.RatInt

package object LRA {

  object RatSort extends BGSort("$rat") { override def toString = "ℚ" }

  val SumOpRat = (new Operator(BG, "$sum", Arity2((RatSort, RatSort) -> RatSort)) { override def formatFn = util.printer.sumFF })
  val UMinusOpRat = (new Operator(BG, "$uminus", Arity1(RatSort -> RatSort)) { override def formatFn = util.printer.uminusFF })
  val GreaterOpRat = (new Operator(BG, "$greater", Arity2((RatSort, RatSort) -> OSort)) { override def formatFn = util.printer.greaterFF })
  // used for divisibility constraints in Cooper:
  val DividesOpRat = (new Operator(BG, "$divides", Arity2((RatSort, RatSort) -> OSort)) { override def formatFn = util.printer.dividesFF })
  val LessOpRat = (new Operator(BG, "$less", Arity2((RatSort, RatSort) -> OSort)) { override def formatFn = util.printer.lessFF })
  val DifferenceOpRat = (new Operator(BG, "$difference", Arity2((RatSort, RatSort) -> RatSort)) { override def formatFn = util.printer.differenceFF })
  val ProductOpRat = (new Operator(BG, "$product", Arity2((RatSort, RatSort) -> RatSort)) { override def formatFn = util.printer.productFF })
  // notice: quotient is FG
  val QuotientOpRat = (new Operator(FG, "$quotient", Arity2((RatSort, RatSort) -> RatSort)) { override def formatFn = util.printer.quotientFF })
  val LessEqOpRat = (new Operator(BG, "$lesseq", Arity2((RatSort, RatSort) -> OSort)) { override def formatFn = util.printer.lessEqFF })
  val GreaterEqOpRat = (new Operator(BG, "$greatereq", Arity2((RatSort, RatSort) -> OSort)) { override def formatFn = util.printer.greaterEqFF })

  //x-theory tests
  val IsIntOpRat = new IsIntOp(RatSort)
  val IsRatOpRat = new IsRatOp(RatSort)
  val IsRealOpRat = new IsRealOp(RatSort)
  
  val ToIntOpRat = new ToIntOp(RatSort)
  val ToRatOpRat = new ToRatOp(RatSort)
  val ToRealOpRat = new ToRealOp(RatSort)

  // Notoce these operators are not yet added to LRAOperators
  val QuotientEOpRat = new QuotientEOp(RatSort)
  val QuotientTOpRat = new QuotientTOp(RatSort)
  val QuotientFOpRat = new QuotientFOp(RatSort)
  val RemainderEOpRat = new RemainderEOp(RatSort)
  val RemainderTOpRat = new RemainderTOp(RatSort)
  val RemainderFOpRat = new RemainderFOp(RatSort)

  val NLPPOpRat = new NLPPOp(RatSort)

  val LRAOperators = Set(SumOpRat, UMinusOpRat, GreaterOpRat, LessOpRat, DifferenceOpRat, 
			 ProductOpRat, QuotientOpRat, LessEqOpRat, GreaterEqOpRat, 
			 IsIntOpRat, IsRatOpRat, IsRealOpRat,
			 ToIntOpRat, ToRatOpRat, ToRealOpRat,
			 NLPPOpRat)
  

  def addStandardOperators(s: Signature) = {
    var res = s
    res += RatSort
    LRAOperators foreach { res += _ }
    res
  }

  val ZeroRat = DomElemRat(RatInt(0))
  val OneRat = DomElemRat(RatInt(1))
  val MinusOneRat = DomElemRat(RatInt(-1))

  RatSort.someElement = ZeroRat

  def isLRA(e: Expression[_]) =
    e.isBG && (e.sorts subsetOf Set(RatSort)) // && (e.operators subsetOf LRAOperators)

  // For simplicity treat reals as rationals. The "platonic" reals are different to the "platonic" rationals, of course,
  // but the TPTP reals are IEEE floating point numbers. So, treating them as "unlimited precision" rationals is not more incorrect
  // than treating them as platonic reals.

  def toRat(cl: ConsClause) = {

    def toRat(l: Lit) = {

      def toRat(t: Term): Term = {
        import bgtheory.LFA._
        import bgtheory.LIA._

        def sortToRat(s: Sort) = if (s == RealSort) RatSort else s

        t match {
          case AbstVar(name, index, s) ⇒ if (s == RealSort) AbstVar(name, index, RatSort) else t
          case GenVar(name, index, s)  ⇒ if (s == RealSort) GenVar(name, index, RatSort) else t
          case DomElemReal(r)          ⇒ DomElemRat(r)
          case DomElemInt(_)           ⇒ t
          case DomElemRat(_)           ⇒ t
          case NonDomElemFunTerm(Operator(kind, name, Arity(argsSorts, resSort)), args) ⇒
            NonDomElemFunTerm(Operator(kind, name, Arity((argsSorts map { sortToRat(_) }), sortToRat(resSort))), args map { toRat(_) })
          case TT => TT
        }
      }
      Lit(l.isPositive, Eqn(toRat(l.eqn.lhs), toRat(l.eqn.rhs)))
    }

    //Clause(cl.lits map { toRat(_) }, cl.idxRelevant, cl.age, BySimple(cl), cl.deltaConjecture)
    cl.mapLits({ toRat(_) }).setInfo(cl.info.copy(inference = BySimple(cl)))
  }
}
