package beagle

import fol._
import datastructures.ConsClause
import util._

package object bgtheory {

  /** Thrown when there is a conversion error from Formula to e.g. PolyAtom/Polynomial*/
  case class ConversionError(s: String) extends Message

  /**
   * All the solvers we have.
   */
  private val solvers: List[Solver] = List(
    PreprocessingSolver,
    // disabled, as unsound:
    // LIA.iQE.Solver,
    LIA.cooper.Solver,
    LRA.SimplexSolver,
    EmptySolver)

  /**
   * The solver we are using, set in main.scala depending on the -format flag
    *  Can be one of the built-in solvers or the path to an executable SMT-sovler
   */
  var solver: Solver = null // EmptySolver

  def setSolver(n: String) {
    (solvers find { _.name == n }) match {
      case Some(s) => {
	solver = s
	s.init()
      }
      case None => throw CmdlineError("no such solver: " + n)
    }
  }


  def setSMTSolver(path: String, bgSort: Sort) {

      val smtsolver = new SMTSolver {
        val name = "SMT"
        val executable = path
        val logic = bgSort match {
          case LIA.IntSort => "QF_LIA"
          case LFA.RealSort => "QF_LRA"
          case LRA.RatSort => "QF_LRA"
          case s => throw InternalError(s"unexpected sort: $s")
        }
        init()
      }

    solver = bgSort match {
      case LIA.IntSort => {
        new LIA.LIASolver {
          val name = "LIA-SMT-Solver"
          def hasSolverLiterals(cl: ConsClause) = true
          def toSolverLiterals(cl: ConsClause) = cl
          def check(cls: Iterable[ConsClause]) = smtsolver.check(cls)
          val needsUnitClauses = false
          // override val canUNSATCore = smtsolver.canUNSATCore
        }
      }
      case LFA.RealSort => {
        new LFA.LFASolver {
          val name = "LFA-SMT-Solver"
          def hasSolverLiterals(cl: ConsClause) = true
          def toSolverLiterals(cl: ConsClause) = cl
          def check(cls: Iterable[ConsClause]) = smtsolver.check(cls)
          val needsUnitClauses = false
          // override val canUNSATCore = smtsolver.canUNSATCore
        }
      }
      case LRA.RatSort => {
        new LRA.LRASolver {
          val name = "LRA-SMT-Solver"
          def hasSolverLiterals(cl: ConsClause) = true
          def toSolverLiterals(cl: ConsClause) = cl
          def check(cls: Iterable[ConsClause]) = smtsolver.check(cls)
          val needsUnitClauses = false
          // override val canUNSATCore = smtsolver.canUNSATCore
        }
      }
      case s => throw InternalError(s"unexpected sort: $s")
    }

    solver.init()
  }


  /** Thrown by some solvers if they encounter a nonlinear term */
  case class NonLinearTermFail(t: Term) extends Exception

  /** Thrown by some solvers if they encounter a term of a sort they cannot handle */
  case class IllSortedTermFail(t: Term) extends Exception

}

// Some simple operator generator classes
// All operators have to be FG. They are only dealt with by simplification, over fully ground instantiated terms,
// hence they are declared as "partially implemented"
class ToIntOp(sort: Sort) extends Operator(FG, "$to_int", Arity1(sort -> bgtheory.LIA.IntSort)) { override val partial = true }
class ToRatOp(sort: Sort) extends Operator(FG, "$to_rat", Arity1(sort -> bgtheory.LRA.RatSort)) { override val partial = true }
class ToRealOp(sort: Sort) extends Operator(FG, "$to_real", Arity1(sort -> bgtheory.LFA.RealSort)) { override val partial = true }

class IsIntOp(sort: Sort) extends Operator(FG, "$is_int", Arity1(sort -> Signature.OSort)) { override val partial = true }
class IsRatOp(sort: Sort) extends Operator(FG, "$is_rat", Arity1(sort -> Signature.OSort)) { override val partial = true }
class IsRealOp(sort: Sort) extends Operator(FG, "$is_real", Arity1(sort -> Signature.OSort)) { override val partial = true }

class QuotientEOp(sort: Sort) extends Operator(FG, "$quotient_e", Arity2((sort, sort) -> bgtheory.LIA.IntSort)) { override val partial = true }
class QuotientTOp(sort: Sort) extends Operator(FG, "$quotient_t", Arity2((sort, sort) -> bgtheory.LIA.IntSort)) { override val partial = true }
class QuotientFOp(sort: Sort) extends Operator(FG, "$quotient_f", Arity2((sort, sort) -> bgtheory.LIA.IntSort)) { override val partial = true }

class RemainderEOp(sort: Sort) extends Operator(FG, "$remainder_e", Arity2((sort, sort) -> bgtheory.LIA.IntSort)) { override val partial = true }
class RemainderTOp(sort: Sort) extends Operator(FG, "$remainder_t", Arity2((sort, sort) -> bgtheory.LIA.IntSort)) { override val partial = true }
class RemainderFOp(sort: Sort) extends Operator(FG, "$remainder_f", Arity2((sort, sort) -> bgtheory.LIA.IntSort)) { override val partial = true }


/**
 * This is a FG operator used to replace $product in case a non-linear term is found.
 * It will be eliminated with simplification rules if the term ever becomes linear again.
 * TODO- perhaps give this a nicer print function?
 */
class NLPPOp(sort: Sort) extends Operator(FG, "#nlpp", Arity2((sort,sort) -> sort))
{ override val partial = true
  override def formatFn = util.printer.nlppFF 
}
object NLPPOp {
  def unapply(t: Term) = t match {
    case PFunTerm(op: NLPPOp, List(l, r)) => Some(l, r)
    case _ => None
  }
}
