package beagle.me

import beagle.fol._
import beagle.datastructures._
import beagle.util._
// import pterm._
import TPOT._
import beagle.util._

import collection.mutable.LinkedHashSet
import collection.immutable.BitSet

case class Remainder(lits: List[TPOT], age: Int, idxUniversalSides: BitSet, idxNonUniversalSides: BitSet, isFull: Boolean) {
  // extends Ordered[Remainder]

  lazy val length = lits.length
  lazy val weight = lits.foldLeft(0) { _ + _.length }
  lazy val idxRelevant = idxUniversalSides ++ idxNonUniversalSides
  val isEmpty = lits.isEmpty
  val nonEmpty = lits.nonEmpty

  override def toString = lits.mkString("{", ", ", "}")+ (if (idxNonUniversalSides.isEmpty) "ᵁ" else "")  + " " + idxRelevant.mkString("{", ", ", "}")
}

case class Propagator(lits: List[TPOT], negAssertOnly: Boolean) {

  val substArray = new SubstArray(lits)

  // substArray.add(PosVUnit(makeVar("x")))

  def addToStack(u: Unit) = substArray.add(u)
  def nextRemainder() = {
    if (substArray.next()) {
      // Some((substArray.currentSmgu, substArray.currentMates))
      Some(substArray.currentMates)
    }
    else None
  }

  class SavedState() {
    val savedState = new substArray.SavedState
    def restore() { savedState.restore() }
  }

  override def toString = lits.mkString(" | ") + (if (negAssertOnly) " (negAssertOnly)" else "")
}


case class Unit(lit: TPOT, age: Int, isUniversal: Boolean, idxRelevant: BitSet, isRight: Boolean) {

  lazy val vars = lit.vars
  lazy val weight = lit.length

  // todo: Can set isUniversal = true if vars.isEmpty?
  @inline def applySubst(σ: TPOTSubst) = Unit(σ(lit), age, isUniversal, idxRelevant, isRight)

  @inline def isVariant(that: Unit) = lit isVariant that.lit

  def fresh() = TPOTSubst.mkRenaming(vars)(this)

  lazy val compl = Unit(lit.compl, age, isUniversal, idxRelevant, isRight)

  // neg is like complement, but does Skolemization if isValid (later)
  lazy val neg = {
    if (isUniversal) {
      require(lit.isGround) // for now. Todo: SKolemization
      Unit(lit.compl, age, isUniversal = true, idxRelevant, isRight)
    } else 
      Unit(lit.compl, age, isUniversal = false, idxRelevant, isRight)
    }

/*
  def isNegVUnit = this match { case NegVUnit(_) => true; case _ => false }
  def isPosVUnit = this match { case PosVUnit(_) => true; case _ => false }
  @inline def isPseudoUnit = isNegVUnit || isPosVUnit
 */

  // A bit faster:
  @inline def isPseudoUnit = lit.elems match {
    case Signature.litOp :: _ :: x :: Nil if x.isInstanceOf[Var] => true
    case _ => false
  }
  @inline def isPosVUnit = lit.elems match {
    case Signature.litOp :: TTOp :: x :: Nil if x.isInstanceOf[Var] => true
    case _ => false
  }
  @inline def isNegVUnit = lit.elems match {
    case Signature.litOp :: FFOp :: x :: Nil if x.isInstanceOf[Var] => true
    case _ => false
  }

  override def toString = {
    this match {
      case NegVUnit(x) => "¬" + x
      case PosVUnit(x) => x.toString
      case _ =>
        val l1 = if (isUniversal) lit + "ᵁ" else lit
        val l2 = if (isRight) l1 else "[" + l1 + "]"
        l2 + " " + idxRelevant.mkString("{", ", ", "}")
    }
  }
}

object NegVUnit {
  def apply(x: Var) = Unit(negVLit(x), 0, isUniversal = false, BitSet.empty, isRight = true)
  def unapply(u: Unit) = u match {
    case Unit(negVLit(x), 0, false, BitSet.empty, true) => Some(x)
    case _ => None
  }
}

object PosVUnit {
  def apply(x: Var) = Unit(posVLit(x), 0, isUniversal = false, BitSet.empty, isRight = true)
  def unapply(u: Unit) = u match {
    case Unit(posVLit(x), 0, false, BitSet.empty, true) => Some(x)
    case _ => None
  }

}


