package beagle.me

// import scalaxy.loops._
// import scala.language.postfixOps // Optional.
import beagle.fol._
import beagle.util._
import beagle.datastructures._
import collection.mutable.ArrayBuffer
import collection.mutable.HashMap
import collection.mutable.HashSet

/**
 * Discrimination tree indexing for TPOTs.
 */

trait DT[T] { //extends Iterable[TPOT] 
  // T is the type of the information stored with the TPOTs

  val name: String

  // Consider only entries whose age is at most (≤) a global value currentAge.
  // CurrentAge is also used when new entries are added.
  // Use getCurrentAge/setCurrentAge to modify currentAge, because of side effects
  private var currentAge = -1

  class Node(val key: Key, val pred: Node) {
    val succs = ArrayBuffer.empty[Node]

    var info = Option.empty[T] // (Inner nodes can carry info)
    var age = -2 // The age of info. Only relevant if info.nonEmpty

    var subTreeMaxAge = -2 // The maximum age mong all successors; -2 if no succs
    // def setSuccsMaxAge { subTreeMaxAge = succs.foldLeft(-1) { _ max _.subTreeMaxAge } }

    def isLeaf = succs.isEmpty
    def isRoot = pred == null

    // For each successor succ of this, collect the first k terms underneath succ, return
    // the finale
    def subtermNodes(k: Int): List[Node] = {
      assume(k > 0)
      var res = List.empty[Node]

      def subTermNodesInner(n: Node, k: Int) {
          for (succ <- n.succs) {
            val hk = succ.key match {
              case _: Var => k - 1
              case op: Operator => k - 1 + op.arity.nrArgs
            }
            if (hk == 0) res ::= succ else subTermNodesInner(succ, hk)
          }
      }
      subTermNodesInner(this, k)
      res
    }

    def toTermUpto(anc: Node) = {
      var res = List.empty[Key] // TPOTElems()
      var n = this
      while (n != anc) {
        res ::= n.key
        n = n.pred
      }
      TPOT(res)
    }

    def klone(clonePred: Node): Node = {
      val res = new Node(key, clonePred)
      res.succs ++= succs map { _.klone(res) }
      res.info = info
      res
    }

    override def toString =
      s"Node($key, $pred) { info = $info }"
  }

  var root = new Node(Signature.trueOp, null) // Signature.trueOp is a dummy
  // "var" because of save/restore
  // The maximum of all nodes in the tree
  // def maxAgeInTree = root.age max root.subTreeMaxAge

  // A path is a possibly empty sequence of pairs (n₀, i₀), (n₁, i₁), ..., (nₖ, iₖ) such that nₖ is the root and n₀ is a stop node
  // The value iⱼ says that in node nⱼ the iⱼ-th successor has been taken in the path. i₀ is always irrelevant
  type PathEntry = (Node, Int)
  type Path = List[PathEntry]
  @inline def nodeOf(pe: (Node, Int)) = pe._1
  @inline def succOf(pe: (Node, Int)) = pe._2

  def getCurrentAge() = currentAge

  def setCurrentAge(newAge: Int) {

    // invalidate all nodes in n and below whose age is strictly greater than currentAge
    def invalidateOlderThanCurrentAge(n: Node) {
        // first invalidate the subtrees below n
        // this involves recomputing n.subTreeMaxAge
        for (i <- 0 until n.succs.length) {
          val succ = n.succs(i)
          if (succ.subTreeMaxAge > currentAge) {
            invalidateOlderThanCurrentAge(succ)
            // Maintain correct value for n.subTreeMaxAge
            n.subTreeMaxAge = n.subTreeMaxAge max succ.age
          }
        }
        // invalidate n itself if required
        if (n.info.nonEmpty && n.age > currentAge) {
          n.info = None
          n.age = -2
        }
    }
    if (newAge > currentAge)
      // as we are about to increass currentAge, we need to invalidate all entries with a higher age
      // than current age in order not to make these old (obsolete) entries avalable again
      invalidateOlderThanCurrentAge(root)
    currentAge = newAge
  }

  // Enumerating all paths in this
  // Sets current to the first substitution going depth-first left-right
  /*
  var currentPath: Path = List.empty
  var exhausted = false // whether all substitutions have been enumerated
   */

  // Finds the first path, starting from a given one.
  // Result is the *longest* path going depth-first left-right
  final def findFirst(p: Path): Path = {
    val ((start, _) :: more) = p
    //  very special case first
    if (start.isRoot && start.info.isEmpty && start.succs.isEmpty)
      // That is, start points at a root-only tree and which is empty
      List.empty
    else {
      var (n, res) = (start, more)
      while (!n.isLeaf) {
        res ::= (n -> 0)
        n = n.succs(0)
      }
      res ::= (n -> -1) // res is a path to a leaf node
      // if n's info age is too high, n is no good, and we need to find next from leaf
      if (n.age <= currentAge) res else findNext(res)
    }
  }

  // Finds next stop node succeeding a given current node. 
  // This means stopping at a prefix, or an ancestor node that has unexplored siblings
  // whatever comes first
  final def findNext(p: Path): Path = {
    var res = p.tail
    while (res.nonEmpty) {
      val ((n, i)) = res.head
      if (n.info.nonEmpty && n.age <= currentAge)
        return res
      else if (i < n.succs.length - 1)
        return findFirst((n.succs(i + 1), 0) :: (n, i + 1) :: res.tail)
      else res = res.tail
    }
    // res is empty, i.e. no next path found
    List.empty
  }

  def sizeDT(n: Node): Int = {
    val h = if (n.info.nonEmpty && n.age <= currentAge) 1 else 0
    h + n.succs.foldLeft(0) { _ + sizeDT(_) }
  }


  // def longestPrefix(n: Node, t: TPOT): (Node, TPOT) = longestPrefix(n, t, TPOTSubst.empty)

  def longestPrefixPath(n: Node, t: TPOT): (Path, TPOT) = {
    def h(n: Node, t: List[Key], acc: Path): (Path, TPOT) = {
        t match {
          case Nil => ((n, -1) :: acc, TPOT(t))
          case key :: keys => {
            for (i <- 0 until n.succs.length)
              if (n.succs(i).key == key)
                return h(n.succs(i), keys, ((n, i)) :: acc)
            (acc, TPOT(t))
          }
        }
      }
    h(n, t.elems, List.empty)
  }

  // Finds the longest path p starting from n such that p is a prefix of tχ (in preorder)
  // Returns the node and the postfix where the search has to stop

  // doesn't work:
  /*  def longestPrefix(n: Node, t: TPOT): (Node, TPOT) = {
    val (pe, tRest) = longestPrefixPath(n, t)
    ((nodeOf(pe.head), tRest))
    }
 */
  def longestPrefix(n: Node, t: TPOT): (Node, TPOT) = {
    @annotation.tailrec
    def h(n: Node, t: List[Key]): (Node, TPOT) = {
        t match {
          case Nil => (n, TPOT(t))
          case key :: keys =>
            (n.succs find { _.key == key }) match {
              case None => (n, TPOT(t)) // for signature with fixed arities, n cannot be a leaf
              case Some(succ) => h(succ, keys)
            }
        }
      }
    h(n, t.elems)
  }


  @inline def getSucc(pe: PathEntry) = {
    val ((n, i)) = pe
    n.succs(i)
  }

  def added(t: TPOT, info: T) {
      var (n, rem) = longestPrefix(root, t)
      // Append rem to n
      for (el <- rem.elems) {
        n.succs += new Node(el, n)
        n = n.succs.last
      }
      // println(s"added($t, $info)")
      // n is pointing to a leaf or inner node
      n.info = Some(info)
      n.age = currentAge
      // propagate subTreeMaxAge upwards
      while (!n.isRoot && n.pred.subTreeMaxAge < currentAge) {
        n.pred.subTreeMaxAge = currentAge
        n = n.pred
      }
  }

  // todo: test this; do we need to respect age?
  def removed(t: TPOT) {

    @annotation.tailrec def adjustSubTreeMaxAge(n: Node) {
      // The correct value for n.subTreeMaxAge
      val newSubTreeMaxAge = n.succs.foldLeft(-2) { _ max _.subTreeMaxAge }
      if (newSubTreeMaxAge < n.subTreeMaxAge) {
        // n needs adjustment, and propagate upwards
        n.subTreeMaxAge = newSubTreeMaxAge
        if (!n.isRoot) adjustSubTreeMaxAge(n.pred)
      }
    }

    var (p, rem) = longestPrefixPath(root, t)
    if (rem.isEmpty && p.nonEmpty && nodeOf(p.head).info.nonEmpty) {
      // otherwise this does not contain t
      var (n, _) :: rest = p // rest cannot be empty because then n would be root and an info node, impossible
      if (!n.isLeaf) {
        // Removal is simple:
        n.info = None
        n.age = -2
        adjustSubTreeMaxAge(n.pred)
      } else {
        do {
          var ((m, i)) = rest.head
          // invariant: m.succs(i) is a leaf
          m.succs.remove(i)
          adjustSubTreeMaxAge(m) // could be optimized?
          rest = rest.tail
        } while (rest.nonEmpty &&
          getSucc(rest.head).info.isEmpty &&
          getSucc(rest.head).isLeaf)
      }
    }
  }

  case class UNIFIABLE(σ: TPOTSubst, info: T) extends Exception

  def mgus(t: TPOT, successCheck: (T, TPOTSubst) => Boolean = (_, _) => true, stopAtFirst: Boolean = false) = {

    // t is TPOT that needs to be unified in all possibly ways with the subtree at n,
    // each unifier extending σres, the current result.
    def mgusInner(σres: TPOTSubst, n: Node, t: TPOT): List[(TPOTSubst, T)] = {
      val (m, rem) = longestPrefix(n, t)
      if (rem.isEmpty) {
        // println(m.info)
        if (m.info.nonEmpty && m.age <= currentAge && successCheck(m.info.get, σres)) {
          if (stopAtFirst) {
            mguTimerDT.stop
            throw UNIFIABLE(σres, m.info.get)
          } else List((σres, m.info.get))
        } else List.empty
      } else if (rem.head.isInstanceOf[Operator]) {
        var res = List.empty[(TPOTSubst, T)]
          // only need to look at variable successors, otherwise longest prefix would have advanced
          for (succ <- m.succs; if succ.key.isInstanceOf[Var]) {
            val x = succ.key.asInstanceOf[Var]
            try {
              val δ = σres.get(x) match {
                case None => σres.clone().extended(x -> rem.first)
                case Some(s) => σres.clone().extendedToMGU(s, rem.first)
              }
              res :::= mgusInner(δ, succ, rem.rest)
            } catch {
              case UNIFYFAIL => () // continue loop
            }
          }
        res
      } else {
        // rem.head is an instance of Var
        val y = rem.head.asInstanceOf[Var]
        σres.get(y) match {
          case None =>
            // Bind y to the first subterms in succs 
            val msubs = m.subtermNodes(1)
            var res = List.empty[(TPOTSubst, T)]
            for (msub <- msubs) {
              val s = msub.toTermUpto(m)
              // have variable y now to be paired with s
              try {
                val δ = σres.clone().extended(y -> s)
                res :::= mgusInner(δ, msub, rem.rest)
              } catch {
                case UNIFYFAIL => ()
              }
            }
            res
          case Some(u) =>
            // replace y by u in top position of rem and continue
            mgusInner(σres, n, TPOT(u.elems ::: rem.elems.tail))
        }
      }
    }
    mguTimerDT.start
    val res = mgusInner(TPOTSubst.empty, root, t)
    mguTimerDT.stop
    res
  }

  def mguFind(t: TPOT, successCheck: (T, TPOTSubst) => Boolean = (_, _) => true): Option[(TPOTSubst, T)] = try {
    mgus(t, successCheck, stopAtFirst = true)
    None
  } catch {
    case UNIFIABLE(σ, info) => Some((σ, info))
  }

  // Whether some term in the index subsumes t
  def getSubsumer(t: TPOT, successCheck: (T, TPOTSubst) => Boolean = (_, _) => true): Option[(TPOTSubst, T)] = {

    // val χ = TPOTSubst.empty
    def subsumesInner(γ: TPOTSubst, n: Node, t: TPOT): Option[(TPOTSubst, T)] = {

      val (m, rem) = longestPrefix(n, t)
      // show()
      // println(tPre)
      if (rem.isEmpty) {
        if (m.info.nonEmpty && m.age <= currentAge && successCheck(m.info.get, γ))
          Some((γ, m.info.get))
        else None
      } else {
        // Only chance to get subsumption is to find a successor node of m that is a variable
        // to match with remNext
        // println(s"yyy rem = $rem, remNext = $remNext, remRest = $remRest")
          for (succ <- m.succs; if succ.key.isInstanceOf[Var]) {
            val x = succ.key.asInstanceOf[Var]
            γ.extendToMatcher(TPOT(List(x)), rem.first) match {
              case None => () // continue search
              case Some(δ) =>
                val h = subsumesInner(δ, succ, rem.rest)
                if (h != None) return h
            }
          }
        // No subsumption with any successor
        None
      }
    }
    subsTimerDT.start
    val res = subsumesInner(TPOTSubst.empty, root, t)
    subsTimerDT.stop
    res
  }

  @inline def subsumes(t: TPOT, successCheck: (T, TPOTSubst) => Boolean = (_, _) => true): Boolean =
    getSubsumer(t, successCheck) != None

  // todo: remove code duplication with getSubsumer above
  def getVariant(t: TPOT, successCheck: (T, TPOTSubst) => Boolean = (_, _) => true): Option[(TPOTSubst, T)] = {

    def containsVariantInner(γ: TPOTSubst, n: Node, t: TPOT): Option[(TPOTSubst, T)] = {
      val (m, rem) = longestPrefix(n, t)
      if (rem.isEmpty) {
        if (m.info.nonEmpty && m.age <= currentAge && successCheck(m.info.get, γ))
          Some((γ, m.info.get))
        else None
      } else {
        // Only chance to get subsumption is to find a successor node of m that is a variable
        // to match with remNext
        for (succ <- m.succs) succ.key match {
          case x:Var =>
            γ.extendToRenaming(TPOT(List(x)), rem.first) match {
              case None => () // continue search
              case Some(δ) =>
                val h = containsVariantInner(δ, succ, rem.rest)
                if (h != None) return h
            }
          case _:Operator => ()
        }
        None
      }
    }
    variantTimerDT.start
    val res = containsVariantInner(TPOTSubst.empty, root, t)
    variantTimerDT.stop
    res
  }

  @inline def containsVariant(t: TPOT, successCheck: (T, TPOTSubst) => Boolean = (_, _) => true): Boolean =
    getVariant(t, successCheck) != None
}

class TPOTSet(val name: String) extends collection.mutable.AbstractSet[TPOT] with collection.mutable.Set[TPOT] with DT[Int] {

  class SavedState {
    val sRoot = root.klone(null)
    def restore() {
      root = sRoot
    }
  }

  def contains(t: TPOT) = {
    val (n, rem) = longestPrefix(root, t)
    n.info.nonEmpty && n.age <= getCurrentAge() && rem.isEmpty
  }

  override def size: Int = sizeDT(root)

  def +=(t: TPOT) = { this.added(t, 0); this }
  def -=(t: TPOT) = { this.removed(t); this }

  def iterator = new Iterator[TPOT] {
    var exhausted = false
    var nextPath: Path = List.empty
    var haveUncollectedPath = false

    def hasNext = {
      if (exhausted) false
      else if (haveUncollectedPath) true
      else {
        if (nextPath.isEmpty)
          nextPath = findFirst(List((root, -1)))
        else
          nextPath = findNext(nextPath)
        exhausted = (nextPath.isEmpty)
        haveUncollectedPath = !exhausted
        // showDT()
        // println(s"hasNext = $haveUncollectedPath")
        haveUncollectedPath
      }
    }

    def next() = {
      // println("xx1")
      if (!haveUncollectedPath) hasNext // try to get a path
      // println("xx2")
      if (haveUncollectedPath) {
        haveUncollectedPath = false
        // println("xx3" + nextPath.head)
        nodeOf(nextPath.head).toTermUpto(root)
      } else TPOT(List.empty)
    }
  }

  def show() {
    println("Terms in index " + name)
    println("—————————————–—————————————–")
    this foreach { println(_) }
    println("—————————————–—————————————–")
    // for some reason this does not work:
    // for (t <- this) { t:TPOT => println(t) }
  }
}

object TPOTSet {
  def empty(name: String = "") = new TPOTSet(name)
}

class TPOTMap[T](val name: String) extends collection.mutable.AbstractMap[TPOT, T] with collection.mutable.Map[TPOT, T] with DT[T] {

  def +=(kv: (TPOT, T)) = { this.added(kv._1, kv._2); this }
  def -=(key: TPOT) = { this.removed(key); this }

  def get(key: TPOT): Option[T] = {
    val (n, rem) = longestPrefix(root, key)
    if (n.info.nonEmpty && n.age <= getCurrentAge() && rem.isEmpty)
      n.info
    else None
  }

  // def klone() = {
  //   val r = root.klone(null)
  //   new TPOTMap[T](name) {
  //     override val root = r
  //   }
  // }

  class SavedState {
    val sRoot = root.klone(null)
    def restore() {
      root = sRoot
    }
  }

  // defined in MapLike
  // def apply(key: TPOT) = longestPrefix(root, key)._1.getInfo

  override def size: Int = sizeDT(root)

  def iterator = new Iterator[(TPOT, T)] {
    var exhausted = false
    var nextPath: Path = List.empty
    var haveUncollectedPath = false

    def hasNext = {
      if (exhausted) false
      else if (haveUncollectedPath) true
      else {
        if (nextPath.isEmpty)
          nextPath = findFirst(List((root, -1)))
        else
          nextPath = findNext(nextPath)
        exhausted = (nextPath.isEmpty)
        haveUncollectedPath = !exhausted
        haveUncollectedPath
      }
    }

    def next() = {
      if (!haveUncollectedPath) hasNext // try to get a path
      if (haveUncollectedPath) {
        haveUncollectedPath = false
        val n = nodeOf(nextPath.head)
        (n.toTermUpto(root), n.info.get)
      } else throw GeneralError("TPOTMap:: next() on empty iterator")
    }
  }

  def show() {
    println("Terms in index " + name)
    println("—————————————–—————————————–")
    this foreach { kv => println(kv._1 + " → " + kv._2) }
    println("—————————————–—————————————–")
    // for some reason this does not work:
    // for (t <- this) { t:TPOT => println(t) }
  }

}

object TPOTMap {
  def empty[T](name: String = "") = new TPOTMap[T](name)
}
