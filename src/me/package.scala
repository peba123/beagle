package beagle

import beagle.fol._
import beagle.util._
import beagle.datastructures._
// import collection.mutable.{ HashSet => Set } 
import collection.mutable.PriorityQueue
import collection.mutable.Queue
import collection.mutable.HashSet
import collection.mutable.ListBuffer
import collection.immutable.BitSet

package object me {

  type Key = VarOrOp
  // type TPOTElems = List[Key]

  type Binding = (Var, TPOT)
  @inline final def key(bind: (Var, TPOT)) = bind._1
  @inline final def value(bind: (Var, TPOT)) = bind._2

  object FAIL extends Exception // derivations FAIL
  object UNIFYFAIL extends Exception

  val debug = beagle.util.flags.debug.value == "inf"
  def printlnd(arg: Any) { if (debug) println(arg) }
  def printlnd() { if (debug) println() }

  val totalTimer = Timer("total")
  val subsTimerDT = Timer("DT subsumption")
  val variantTimerDT = Timer("DT variant")
  val mguTimerDT = Timer("DT mgu")
  val nextTimerDT = Timer("DT next()")
  val combineTimerST = Timer("ST combine()")
  val addTimerST = Timer("ST add()")
  val nextTimerST = Timer("ST next()")
  val mguTimer = Timer("mgu()")
  val nextTimer = Timer("SA next()")
  val addTimer = Timer("SA add()")
  val updateTimer = Timer("SA update()")
  val findTimer = Timer("SA find()")
  val combineTimer = Timer("combine()")
  val combineMaybeTimer = Timer("combineMaybe()")
  val combineMemoizingTimer = Timer("combineMemoizing()")
  val otherTimer = Timer("other")
  val timers = List(totalTimer, subsTimerDT,
    variantTimerDT,
    mguTimerDT,
    nextTimerDT,
    // addTimerST,
    //nextTimerST,
    // combineTimerST,
    nextTimer, findTimer, updateTimer, mguTimer, combineTimer, combineMaybeTimer, combineMemoizingTimer, otherTimer)

  object depthOrdering extends scala.math.Ordering[Unit] {
     def compare(a: Unit, b: Unit) = b.vars.size - a.vars.size // prefer lighter units
  }

  var forwardCheckCnt = 0
  var smguCnt = 0
  var mguCntYes = 0
  var mguCntNo = 0
  var subsCnt = 0
  var memCallsYes = 0
  var memCallsNo = 0
  var combineMaybeCntYes = 0
  var combineMaybeCntNo = 0
  var combineCntYes = 0
  var combineCntNo = 0
  var onUpdateBlockedCnt = 0
  var nextCntST = 0
  var nextCntDT = 0

  var infPropCnt = 0
  var infRemainderCnt = 0
  var infBackjumpCnt = 0
  var infAssertCnt = 0
  var infSplitCnt = 0
  var otherCnt1 = 0L
  var otherCnt2 = 0L

  def stats() {
    println()
    println("Inferences")
    println("——————————")
//    println(s"# propagation         : $infPropCnt")
    println(s"# remainder           : $infRemainderCnt")
    println(s"# assert              : $infAssertCnt")
    println(s"# split               : $infSplitCnt")
    println(s"# close               : $infBackjumpCnt")
    println()
    println("Operations")
    println("——————————")
    println(s"# smgu                : $smguCnt")
    println(s"# forward checking    : $forwardCheckCnt")
    println(s"# mgu yes             : $mguCntYes")
    println(s"# mgu no              : $mguCntNo")
    println(s"# DT subsumption      : $subsCnt")
    println(s"# DT next()           : $nextCntDT")
    // println(s"# ST next()           : $nextCntST")
    println(s"# memoization yes     : $memCallsYes")
    println(s"# memoization no      : $memCallsNo")
    println(s"# combine yes         : $combineCntYes")
    println(s"# combine no          : $combineCntNo")
    println(s"# combineMaybe yes    : $combineMaybeCntYes")
    println(s"# combineMaybe no     : $combineMaybeCntNo")
    println(s"# update blocked      : $onUpdateBlockedCnt")
    println(s"# other1              : $otherCnt1")
    println(s"# other2              : $otherCnt2")
    println()
    println("Timing (in seconds)")
    println("——————————————————–")
    timers foreach { t => {
      t.stop() //if not stopped already
      println("%-20s : %.2f".format(t.name, t.total))
    }}
  }


  var statsShown = false

  def makeVar(s: String) = GenVar(s, 0, Signature.ISort)

  abstract class PseudoLit

  object negVLit extends PseudoLit {
    def apply(x: Var) = TPOT(List(Signature.litOp, FFOp, x))
    def unapply(t: TPOT) = t.elems match {
      case List(Signature.litOp, FFOp, x) if x.isInstanceOf[Var] => Some(x)
      case _ => None
    }
  }

  object posVLit extends PseudoLit {
    def apply(x: Var) = TPOT(List(Signature.litOp, TTOp, x))
    def unapply(t: TPOT) = t.elems match {
      case List(Signature.litOp, TTOp, x) if x.isInstanceOf[Var] => Some(x)
      case _ => None
    }
  }


  // val negVLit = PFunTerm(Signature.litOp, List(FF, GenVar("v", 0, Signature.ISort))).toTPOT

  private def shutdownHook = {
    if (flags.modelEvolution.value && !statsShown) {
      totalTimer.stop()
      Saturation.stats()
      stats()
      println()
    }
  }
  sys.addShutdownHook(shutdownHook)

  def test(input: List[ConsClause]) {

    if (beagle.util.verbose) println("Saturating ...")
    val satisfiable = Saturation.run(input)
    if (beagle.util.verbose) {
      Saturation.stats()
      stats()
      println()
    }
    if (satisfiable) {
      if (beagle.util.verbose || beagle.util.flags.proof.value) {
        Saturation.oldUniversal.show()
        Saturation.oldNonUniversal.show()
        println()
      }
      println("Status: Saturation")
    } else println("Status: Refutation")
    statsShown = true
  }

  implicit class TPOTList(ts: List[TPOT]) {
    def vars = ts.foldLeft(Set.empty[Var]) { _ ++ _.vars  }
  }

}
