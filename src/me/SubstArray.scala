// todo: when working with low-high bounds one shoule really use an index comprised of only these

package beagle.me

import beagle.fol._
import beagle.util._
import beagle.datastructures._

// todo: unit-resolution kind of inferences. A universal side literal might (complementary) subsume a current base literal instance.
// In this case just consider the base literal instance resolved away.

// todo: enable forward-checking again

/*
 * A substitution array is a datastructure consisting of, essentially, a
 * clause with stacks of complementary-unifiable literals on top of each 
 * clause literal. 
 * It supports reasonably efficient computation of (all) simultaneous most general unifiers
 * using ,e.g., n or n-1, literals, where n is the length of the clause.
 */

class SubstArray(body: List[TPOT]) {
  assume(body.nonEmpty)
  // Assume skipSets doesn't contain duplicates, although this does not hurt either
  import collection.mutable.ArrayBuffer
  import collection.mutable.WeakHashMap
  // import collection.mutable.LongMap
  // import collection.immutable.HashSet
  import collection.immutable.BitSet

  // Options
  val forwardChecking = true
  val memoizing = true

  val debug = false // beagle.util.flags.debug.value == "inf"

  // todo: use sortHeadForDecProp
  val bases = body // TPOT.sortForVars(body, Set.empty) //  basesIn sortBy { _.vars.size }
  // if (bases.length > 1) println(bases.reverse)
  // println(bases.mkString(" ∨ "))

  val width = bases.length
  val infty = width // for forward checking, infty means "not incompatible"

  // val extraRelevantVars = extraRelevantVarsIn.toSet

  class Node(val u: Unit, val σ: TPOTSubst) {
    // fwCheck: value is i if this node has been disabled ("forward checked")
    // because of incompatibility of σ with smgus(i)
    var fwCheck = infty
    var isUniversalSubsumer = false
  }

  class Col(val i: Int, val baseCompl: TPOT, relevantVars: Set[Var]) {
    var low = 0
    var high = 0

    val nodes = ArrayBuffer.empty[Node]
    @inline def height = nodes.length

    def save() = (low, high, nodes map { _.fwCheck })
    def restore(savedState: (Int, Int, ArrayBuffer[Int])) {
        val (slow, shigh, sfwCheck) = savedState
        low = slow
        high = shigh
        for (j <- 0 until sfwCheck.length) nodes(j).fwCheck = sfwCheck(j)
        nodes.trimEnd(nodes.length - sfwCheck.length)
    }

    def add(u: Unit) =
      (u.lit mguMaybe baseCompl) &&
    ((u.lit mgu baseCompl) match {
      case None => false
      case Some(σ) =>
        nodes += new Node(u, σ)
        true
    })

    def show() {
      println("column %d, base = %s, j_%d = %d, low = %d, high = %d, height = %d:".format(i, baseCompl.compl, i, js(i), low, high, height))
      for (j <- 0 until height) {
        println("  u_%d = %s, σ_%d = %s".format(j, nodes(j).u, j, nodes(j).σ))
      }
    }
  }

  // relevantVars(i) is the set of variables occuring in bases(i+1) ∪ bases(i+2) ∪ ... ∪ bases(width-1) ∪ extraRelevantVars
  // in the substitution trees at position i Need to consider only substitutions with these domains
  // todo: do we need to take variables of constraints into account?
  val relevantVars = new Array[Set[Var]](width)
    for (i <- 0 until width)
      relevantVars(i) = Set.empty ++ bases.drop(i + 1).vars

  val cols = (for ((b, i) <- (bases zip (0 until width))) yield {
    new Col(i, b.compl, relevantVars(i))
  }).toArray

  // for skipping:
  // optimize {
  // for ((b, i) <- (bases zip (0 until width)))  {
  //   cols(i).add(Unit(b.compl, isUniversal = false, BitSet.empty, isRight = true))
  //   }
  // }

  // def isFeasible = cols forall { i => i.height > i.low }

  // Status information, all stack-like, valid from 0 until i, for a current i
  val smgus = new Array[TPOTSubst](width)
  val js = new Array[Int](width) // the j_i's 
  var haveLeaf = false // Indicates whether a leaf was reached, i.e. an smgu was computed
  var leafIsAssert = false

  // var currentSmgu: TPOTSubst = null // The smgu computed by next()
  var currentMates: List[Unit] = null // The units paired with the bases

  // "Abbreviations"
  @inline def node(i: Int, j: Int) = cols(i).nodes(j)
  @inline def fwCheck(i: Int, j: Int) = node(i, j).fwCheck
  @inline def σ(i: Int, j: Int) = node(i, j).σ
  @inline def high(i: Int) = cols(i).high
  @inline def low(i: Int) = cols(i).low
  @inline def height(i: Int) = cols(i).height

  def showFwCheckMatrix(iStart: Int) {
    println()
    // println("smgu(%d) = %s".format(iStart, smgus(iStart)))
    show()
    for (i <- iStart + 1 until width) {
      val li = low(i); val hi = high(i)
      print(s"fwCheck($i), low($i) = $li, high(i) = $hi: ")
      for (j <- low(i) until high(i))
        print("%3d".format(fwCheck(i, j)))
      println()
    }
  }

  val mem = new WeakHashMap[List[Int], Option[TPOTSubst]]()

  // too slow:
  // @inline final def memKey(i: Int, j: Int) = js.toList.dropRight(width - i) :+ j
  def memKey(i: Int, j: Int) = {
    var res = List.empty[Int]
    for (k <- 0 until i) res ::= js(k)
    res ::= j
    res
  }

  def combineMemoizingOpt(i: Int, j: Int) = {
    assume(i > 0)
    combineMemoizingTimer.start()
    val res =
      // if (node(i, j).u.isPseudoUnit)
      //   // always successfull
      //   Some(smgus(i-1).clone() += σ(i, j).bindings.head)
      // else
        if (memoizing) {
        val key = memKey(i, j)
        mem.get(key) match {
          case None =>
            memCallsNo += 1
            val δOpt = if (σ(i, j) combineMaybe smgus(i - 1))
              σ(i, j) combineOpt smgus(i - 1)  else None
            mem += (key -> δOpt)
            δOpt
          case Some(σOpt) => memCallsYes += 1; σOpt
        }
      } else {
        // No memoizing
        if (σ(i, j) combineMaybe smgus(i - 1)) σ(i, j) combineOpt smgus(i - 1) else None
      }
    combineMemoizingTimer.stop()
    // println("σ(%d, %d) = %s, smgus(%d) = %s, res = %s".format(i, j, σ(i, j), i-1, smgus(i-1), res))
    res
  }

  class SavedState() {
    val (sjs, shaveLeaf, scols, scurrentMates, sleafIsAssert) = (js.clone(), haveLeaf, cols map { _.save() }, currentMates, leafIsAssert)
    def restore() {
        // first restore js and cols
        for (i <- 0 until width) {
          js(i) = sjs(i)
          cols(i).restore(scols(i))
        }
        haveLeaf = shaveLeaf
        leafIsAssert = sleafIsAssert
        // finally need to recompute smgus, currentSmgu and currentMates
        if (haveLeaf) {
          var prevSmgus = TPOTSubst.empty
          for (i <- 0 until width) {
            smgus(i) = σ(i, js(i)) combine prevSmgus
            prevSmgus = smgus(i)
          }
          // currentSmgu = smgus(width - 1)
          currentMates = scurrentMates
        }
        mem.clear()
    }
  }

  // Update the fwCheck Matrix starting from column i
  // Assumes that js(0)...js(i) have been set
  def updateFwCheckMatrix(i: Int): Boolean = {
    assume(i >= 0)

    // val oldSidesUniversal = allSidesUniversal(i-2)

    updateTimer.start()
    // val lower = math.max(i, 1) // 0 is trivial
    val upper = if (forwardChecking) width else math.min(width, i + 1)
    // val relDom = smgus(i).dom.toSet
    lazy val smgusIMinus1 = if (i == 0) TPOTSubst.empty else smgus(i - 1)
    for (i1 <- i until upper) {
        if (width-1 == i1 && i1 == i+1 && allSidesUniversal(i)) {
          updateTimer.stop()
          return true
        } else {

      var deadend = true // whether *all* (relevant) nodes in column i1 are blocked

      // if (low(i1) == 0) {
      //   // heuristics: use ST index
      //   val jsI1Combinable = ((substs(i1) combine smgus(i)) map { _._2 }).toSet
      //   for (j1 <- low(i1) until high(i1);
      //     if fwCheck(i1, j1) >= i) { // not blocked yet, need to update
      //     if (jsI1Combinable contains j1) {
      //       node(i1, j1).fwCheck = infty
      //       deadend = false
      //     } else
      //       node(i1, j1).fwCheck = i
      //   }
      // } else {
      for (
        j1 <- (if (i1 == i) js(i1) else low(i1)) until high(i1);
        if fwCheck(i1, j1) >= i
      ) { // not blocked yet, need to update
        if (σ(i1, j1) combineMaybe smgusIMinus1) {
          node(i1, j1).fwCheck = infty
          deadend = false
        } else
          node(i1, j1).fwCheck = i
      }
      // }
      if (deadend) {
        // showFwCheckMatrix(iStart)
        // printlnd("\ndeadend: i1 = %d, i = %d, j_%d = %d".format(i1, i, i, j))
        // if (realHighs(i1) > realLows(i1) && i1 > i+1) forwardCheckCnt += 1 // otherwise trivial
        if (i1 > i + 1) forwardCheckCnt += 1 // otherwise trivial
        updateTimer.stop()
        return false
      }
      // } else
      //   // no intersection of variables
      //   for (j1 <- low(i1) until high(i1);
      //     if fwCheck(i1, j1) >= i)
      //     node(i1, j1).fwCheck = infty
    }
      // println("updating OK")
      }
    updateTimer.stop()
    true
    // }
  }

  @inline def allSidesUniversal(i: Int) = {
    (0 to i) forall { k => node(k, js(k)).u.isUniversal }
  }

  // todo: currentmates: directly return remainders instead of mates


  def findInternal(iStart: Int, jStart: Int): Boolean = {
    // assume iStart >= 0
    // assume that smgus(0),...,smgus(iStart-1) has been set

    def setCurrentMates(i: Int) {
        val σ = smgus(i-1)
        currentMates = List.empty
        for (k <- 0 until i) currentMates ::= σ(node(k, js(k)).u)
    }

    // i >= 0
    var i = iStart
    js(i) = jStart
    var deltai = 0
    var stop = false
    while (!stop) {
      // assume i < width
      // assume js(i) has been set
      if (i == width-1 && allSidesUniversal(i-1)) {
        // Assert unifier:
        // Consider last column literal paired with -v or +v, i.e. do a (universal)  assert only.
        // todo: Forward checking?
        // todo: update?
        if (low(i) == 0) {
          // Implicitly resolve with -v / +v :
          setCurrentMates(i)
          smgus(i) = smgus(i-1)
          currentMates ::= Unit(smgus(i)(cols(i).baseCompl), 0, isUniversal = false, BitSet.empty, isRight = true)
          // currentMates = currentMates.reverse
          leafIsAssert = true
          // println(leafIsAssert + " " + currentMates)
          stop = true // return true
        } else
          // low(i) > 0 means that the other stacks combinations, with the same js(0)...js(i-1), was seen before
          // Hence no need to return it again
          // Other way to see this: For assert unifiers need to unify with -v or +v only, but this is impossible because
          // -v / +v is at cols(i, 0) however low(i) > 0, so we can't hit -v / +v
          deltai = -1
      } else { // if (updateFwCheckMatrix(i))
        // FW check ok, ordinary unifier
        val h = high(i)
        var j = js(i)
        var ok = false
        val hadPseudoUnit = (0 until i) exists { k => node(k, js(k)).u.isPseudoUnit }
        val hadPosVUnit = (0 until i) exists { k => node(k, js(k)).u.isPosVUnit }
        while (!ok && j < h) {
          if (i == 0) {
            // Special case, avoids trivially successfull but time consuming combine calls below
            js(i) = j
            smgus(i) = σ(i, j)
            ok = true
          } else if (node(i, j).u.isPosVUnit && hadPseudoUnit) j += 1
          else if (node(i, j).u.isNegVUnit && hadPosVUnit) j += 1
          else { // if (true || fwCheck(i, j) == infty)
            // val k = js(0)
            // println(s"js(0) = $k, i = $i, j = $j")
            // otherCnt1 += 1
            // We don't need this check because update for i has been called before,
            // which does a combineMaybe check
            // if (σ(i, j) combineMaybe smgusIMinus1)
            combineMemoizingOpt(i, j) match {
              case None => j += 1
              case Some(δ) =>
                js(i) = j
                smgus(i) = δ
                ok = true
            }
          } 
        }
        if (ok) deltai = +1 else deltai = -1
      }

      if (!stop) {
        if (deltai == +1) {
          i += 1
          if (i == width) {
            setCurrentMates(i)
            // currentMates = currentMates.reverse
            leafIsAssert = false
            // println(leafIsAssert + " " + currentMates)
            stop = true // return true
          } else js(i) = low(i) // i < width
        } else {
          // assume(deltai == -1)
          i -= 1
          if (i < iStart) stop = true else js(i) += 1
        }
      }
    }
    !(i < iStart)
  }

  // Recursive version: *much* slower, as expect 10 millions recursive calls per minute

  /*
  def findInternal(i: Int, jStart: Int): Boolean = optimize {
    // i >= 0
    if (i == width) {
      // println("find OK, smgu = " + smgus(width-1))
      // println("     js = " + js.toList)
      true
    } else {
      // println(s"called find($i, $jStart)")
      val smgusIMinus1 =
        if (i == 0) TPOTSubst.empty else smgus(i - 1)
      for (
        j <- (jStart until high(i));
        if fwCheck(i, j) == infty
      ) {
        if (σ(i, j) combineMaybe smgusIMinus1)
          σ(i, j) combineOpt smgusIMinus1 match {
            case None => ()
            case Some(δ) =>
              js(i) = j
              smgus(i) = δ
              if (update(i) && findInternal(i + 1, 0)) return true
          }
      }
      false
    }
  }
 */

  def find(i: Int, jStart: Int) = findTimer.measure(findInternal(i, jStart))

  def nextFromRestart(): Boolean =  {
    // Last ressort: see if there are unworked literals on some stack
    // Assume that all ways of getting smgus have been exhausted, hence:
    for (i <- (0 until width)) cols(i).low = 0
    (0 until width) find { i => height(i) > high(i) } match {
      case None => false // no unfinished stack - give up
      case Some(i) =>
        // i could be 0
        // println(s"nextFromRestart(), i = $i, height($i) = %d, high($i) = %d".format(height(i), high(i)))
        // show()
        // Reset column counter js(i) to pick up from the new elements
        cols(i).low = high(i)
        cols(i).high = height(i)
        js(i) = low(i)
        // Need to save smgusSoFar(cursor) for later restoration and init a new smgusSoFar
        // smgusSoFar(cursor).reset()
        findFromRoot()
    }
  }

  // finds next path, if possible, starting from column cursor, js(cursor) and the next smgusSoFar(cursor-1)
  def findFromRoot(): Boolean =  {
    // println("nextFromCursor(): cursor = %d, j_%d = %d, low = %d, high = %d".format(cursor, cursor, js(cursor), low(cursor), high(cursor)))
    // show()
    if (find(0, low(0)))
      true
    else
      nextFromRestart()
    /*    for (j <- low(0) until high(0)) {
      js(0) = j
      smgus(0) = σ(0, j)
      if (update(0) && find(1, 0)) {
        // println("firstFromRoot() success")
        return true
      }
    }
    // No success, try with (another) postponed column, if any
    // But first reset all columns (at most one will be affected)
    nextFromRestart()
 */
  }

  def nextFromLeaf(): Boolean =  {
    // println("nextFromLeaf()")
    // show()
    val start = if (leafIsAssert) width-2 else width-1
    for (i <- start to 0 by -1)
      if (find(i, js(i) + 1)) {
        // println("nextFromLeaf() success")
        return true
      }
    nextFromRestart()
  }

  def next(): Boolean =  {
    nextTimer.start()
    // if (!isFeasible) { nextTimer.stop(); false }
    // else {
      if (haveLeaf) haveLeaf = nextFromLeaf() else haveLeaf = nextFromRestart()
      if (haveLeaf) {
        smguCnt += 1
        // println("nextSmgu() = " + smgus(width-1))
        // currentSmgu = smgus(width - 1)
        // currentMates set by find:
        // currentMates = List.empty
        // for (i <- 0 until width)
        //   currentMates ::= smgus(width-1)(node(i, js(i)).u)
      }
      nextTimer.stop()
      haveLeaf
   // }
  }

  // Add l as a fresh variant to all columns; returns true iff successful in at least one case
  def add(u: Unit) = {
    addTimer.start()
    var res = false
    var ufresh = u.fresh() // the fresh variant we try to put on top of some stack
    for (i <- 0 until width)
      if (cols(i).add(ufresh)) {
        res = true // found one stack to add to, which is enough
        ufresh = u.fresh()
      } // need a new fresh variant
    addTimer.stop()
    res
  }

  def show() {
    for (i <- 0 until width) {
      cols(i).show()
      println()
    }
  }

/*  def showIfSmgu() {
    return
    if (currentSmgu != null)
      println("Index: %s smgu: %s".format(js.reverse.mkString("[", ", ", "]"), currentSmgu))
  }
 */

  def stats() {
    println("Base: %s".format(bases.mkString(" ∨ ")))
    // println("# nodes                : %d".format(cols.foldLeft(0) { _ + _.nodes.length }))
    // var nrPathsTotal = 1L
    // for (i <- 0 until width) nrPathsTotal *= high(i)
    // println("# paths                : %d".format(nrPathsTotal))
    println(s"# smgus               : $smguCnt")
    println(s"# forward checking    : $forwardCheckCnt")
  }
}
