package beagle.me

import beagle._
import util._
import fol._
import collection.mutable.PriorityQueue

/*
abstract class ClauseSet {
  type Collection <: Iterable[ConsClause]

  // Abstract members
  val clauses: Collection // All clauses in the clause set, use mutable collections
  def addToClauses(cl: ConsClause): Unit // Destructively add a clause to clauses
  def klone(): ClauseSet // Shallow copy  
  
  val maxWeight = if (flags.weightBound.value == 0) 20000 else flags.weightBound.value

  // Derived notions
  def add(cl: ConsClause) {
    // Check if clause is too heavy or got a variant already
    // if (cl.weight > maxWeight || (clauses exists { (_ ~ cl) })) return
    // if (cl.weight > maxWeight || (clauses exists { (_.lits == cl.lits) })) return
    if (cl.weight > maxWeight) return
    addToClauses(cl)
  }

  def add(cls: Iterable[ConsClause]) {
    cls foreach { add(_) }
  }

  def size = clauses.size
  def isEmpty = clauses.isEmpty

  // lazy val unitClauses = clauses filter { _.isUnitClause }
  // Can't use lazy val, because clauses is a mutable data structure
  def unitClauses: Iterable[ConsClause]

  def show() {
    clauses foreach { println(_) }
  }

}
 */

class RemSet {

  object WeightOrdering extends scala.math.Ordering[Remainder] {

    def compare(a: Remainder, b: Remainder) = {
      if (false) 0
      else if (a.length < b.length) 1
      else if (b.length < a.length) -1
      else if (a.idxNonUniversalSides.size < b.idxNonUniversalSides.size) 1
      else if (b.idxNonUniversalSides.size < a.idxNonUniversalSides.size) -1
//      else if (a.age < b.age) 1 // prefer older clauses
//      else if (a.age > b.age) -1
      else if (a.weight < b.weight) 1
      else if (b.weight < a.weight) -1
      else if (a.age > b.age) 1 // prefer newer clauses
      else if (a.age < b.age) -1
      else 0
    }
  }

  val remainders = PriorityQueue.empty[Remainder](WeightOrdering)

  def size = remainders.size
  def clear() { remainders.clear() }

  def isEmpty = remainders.isEmpty
  def nonEmpty = remainders.nonEmpty

  def +=(rem: Remainder) = {
    remainders += rem
    this
  }

  def ++=(rems: Iterable[Remainder]) = {
    remainders ++= rems
    this
  }

  override def clone() = {
    val h = remainders
    val res = new RemSet {
      override val remainders = h.clone()
    }
    res
  }

  def removeLightest() = remainders.dequeue()

  def removeOldest() = { 
  // Select an oldest clause, and among these a lightest one
    require(!remainders.isEmpty, "removeOldest: remainder set is empty")
    
    var best = remainders.dequeue() 
    var kept = List.empty[Remainder] // All clauses but the removed one  
    
    // We remove the clauses one by one, inspecting if we've got a better one,
    // thereby remembering those to be kept
    while (!remainders.isEmpty) {
      val next = remainders.dequeue()
      if (next.age < best.age ||
        (next.age == best.age && next.weight < best.weight)) {
        // found a better best: save the current best and make the new best the current best
        kept ::= best
        best = next
      } else 
        // best preserves, next is top be kept
        kept ::= next
    }
    // clauses is empty at this point, still need to push kept into clauses
    remainders ++= kept
    best
  }

  def removeNewest() = { 
  // Select an oldest clause, and among these a lightest one
    require(!remainders.isEmpty, "removeOldest: remainder set is empty")
    
    var best = remainders.dequeue() 
    var kept = List.empty[Remainder] // All clauses but the removed one  
    
    // We remove the clauses one by one, inspecting if we've got a better one,
    // thereby remembering those to be kept
    while (!remainders.isEmpty) {
      val next = remainders.dequeue()
      if (next.age > best.age ||
        (next.age == best.age && next.weight > best.weight)) {
        // found a better best: save the current best and make the new best the current best
        kept ::= best
        best = next
      } else 
        // best preserves, next is top be kept
        kept ::= next
    }
    // clauses is empty at this point, still need to push kept into clauses
    remainders ++= kept
    best
  }


}


