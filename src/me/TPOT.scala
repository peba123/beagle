package beagle.me

import beagle.fol._
import beagle.datastructures._
import collection.mutable.LinkedHashMap
import collection.mutable.HashSet
import collection.mutable.HashMap
import collection.mutable.ListBuffer
// import beagle.fol.Ordering._

/**
  *  Pre-order traversal of terms
  */

case class TPOT(elems: List[Key]) {
// extends collection.immutable.LinearSeq[Key] with collection.LinearSeqOptimized[Key, TPOT] 
  def nonEmpty = elems.nonEmpty
  def isEmpty = elems.isEmpty
  def head = elems.head
  lazy val tail = TPOT(elems.tail)
  lazy val length = elems.length

  def apply(i: Int) = elems(i)
  // def contains(k: Key) = elems contains k
  // lazy val vars = (elems filter { _.isInstanceOf[Var] }).toSet.asInstanceOf[Set[Var]]
  lazy val varsCnt = {
    var res = HashMap.empty[Var, Int]
    for (e <- elems)
      e match {
        case x: Var => res += x -> (res.getOrElse(x, 0) + 1)
        case _ => ()
      }
    res
  }
  lazy val vars = varsCnt.keySet
  lazy val isGround = vars.isEmpty

  lazy val $ = TPOTSubst.mk$Subst(vars)(this)

  // KBO
  import Ordering._
  def ≻ (that: TPOT) = TPOT.compare(this, that) == Greater
  def ≽ (that: TPOT) = this == that || (TPOT.compare(this, that) == Greater)

  // todo: can vastly improve:
  def isStrictlyMaximalIn(ts: List[TPOT]) = !(ts exists { _ ≽ this })
  def isMaximalIn(ts: List[TPOT]) = !(ts exists { _ ≻ this })

  def flatMap(f: (Key ⇒ collection.GenTraversableOnce[Key])): TPOT = TPOT(elems flatMap f)

    // The complement of a literal represented as a TPOT 
  lazy val compl = {
    val Signature.litOp :: sign :: a = elems
    TPOT(Signature.litOp :: (if (sign == TTOp) FFOp else TTOp) :: a)
  }

  lazy val atomOf = {
    val Signature.litOp :: sign :: a = elems
    TPOT(a)
  }

  lazy val isPositive = {
    val Signature.litOp :: sign :: a = elems
    sign == TTOp
  }


  // uncons: return the length of the first term and the remaining terms as a TPOT
  private def unCons = {
    var len = 0
    var rest = elems
    var nrTerms = 1 // number of terms in rest still to skip 
    while (nrTerms > 0) {
      nrTerms -= 1 // we are about to skip one element in rest
      rest.head match {
        case _: Var => len += 1
        case op : Operator => nrTerms += op.arity.nrArgs; len += 1
      }
      rest = rest.tail
    }
    (len, TPOT(rest))
  }

  lazy val (firstLen, rest) = unCons
  lazy val first = new TPOT(elems.dropRight(length - firstLen)) {
      override lazy val length = firstLen // We know this already here
  }

  // This should be at least as fast, but it is actually slower
/*
  lazy val first = {
    var h = List.empty[Key]
    var (i, rest) = (0, elems)
    while (i < firstLen) {
      h ::= rest.head
      rest = rest.tail
      i += 1
    }
    new TPOT(h.reverse) {
      override lazy val length = firstLen // We know this already here
    }
  }
 */

  def mgu(that: TPOT): Option[TPOTSubst] =
    try {
      mguTimer.start()
      // TPOTSubst.clashPreTest((this, that)) // part of extendedToMGU
      val res = Some(TPOTSubst.empty.extendedToMGU(this, that))
      mguCntYes += 1
      mguTimer.stop()
      // otherTimer.stop()
      res
    } catch { case UNIFYFAIL => mguCntNo +=1; mguTimer.stop(); None }

  // Returns true if this and that are possibly unifiable.
  // That is, if false, this and that are definitely not unifiable
  def mguMaybe(that: TPOT): Boolean =
    try {
      TPOTSubst.clashPreTest(this, that)
      true
    } catch { case UNIFYFAIL => false }


  def isVariant(that: TPOT): Boolean = {
    // otherTimer.start()
    val res = TPOTSubst.empty.extendToRenaming(this, that) != None
    // otherTimer.stop()
    res
  }

  def isInstantiationOf(that: TPOT): Boolean =
    TPOTSubst.empty.extendToMatcher(that, this) != None


  def toTerm: Term =
    head match {
      case v: Var => v
      case op: Operator =>
        var h = tail
        var argsRev = List.empty[Term]
        for (i <- 0 until op.arity.nrArgs) {
          argsRev ::= h.first.toTerm
          h = h.rest
        }
        NonDomElemFunTerm(op, argsRev.reverse)
    }

  override def toString = toTerm.toString


}


object TPOT {


// Sort the head literals, prefer head literals with smaller variable sets and so that less guessing will result
  def sortForVars(lits: List[TPOT], initVars: Set[Var]) = {
    // assume(lits.nonEmpty)
    // the variables in the body together with the variables in the result so far:
    var varsCovered = initVars // body.foldLeft(Set.empty[Var]) { _ ++ _.vars }
    var res = List.empty[TPOT] // result (so far)
    var open = lits // the head literals still to be sorted
    while (open.nonEmpty) {
      var (best :: rest) = open // pick frst open literal as a candidate for best
      open = List.empty
      // go through rest and see if we can find a better literal 
      while (rest.nonEmpty) {
        val next = rest.head
        val nextExcess = next.vars diff varsCovered
        val bestExcess = best.vars diff varsCovered 
        if ((next.vars subsetOf best.vars) || nextExcess.size < bestExcess.size) {
          // prefer next over best
          open ::= best // current best to be considered in next round again
          best = next
        } else
          open ::= next // next to be considered in next round
        rest = rest.tail
      }
      res ::= best // got the next best literal
      varsCovered ++= best.vars
    }
    res.reverse
  }


  /**
   * Weight :
   * w(x) = varWeight, for all variables x
   * w(c) >= varWeight, for all constants c
   * if w(f)=0 for some unary function symbol f then f >=_prec g for all function symbols g
   * where >=_prec is a precedence on the function symbols
   *
   * Definition of KBO:
   *
   * s >_KBO t iff
   * (1) #(x,s) >= #(x,t) for all variables x and w(s) > w(t), or
   * (2) #(x,s) >= #(x,t) for all variables x, w(s) = w(t), and
   *    (a) t = x, s = f^n(x) for some n >= 1, or
   *    (b) s = f(s1,...,sm), t = g(t1,...,tn) and f>g, or
   *    (c) s = f(s1,...,sm), t = f(t1,...,tm) and
   *          (s1,...,sm) (>_KBO)_lex (t1,...,tm)
   *
   * Notice that case (2a) is relevant only if unary function symbols have a weight 0
   */

  import Ordering._


  // Straight from the definition
          // todo: improve by linearly scanning over s and t as long as possible
  def compare(s: TPOT, t: TPOT): OrderingResult =
    // Check variable condition, which is common to both cases
    if (t.vars forall { x ⇒ s.varsCnt.getOrElse(x, 0) >= t.varsCnt(x) }) {
      val (sw, tw) = (s.length, t.length) // for simplicity
      if (sw > tw)
        Greater 
      else if (sw == tw) {
        (s.head, t.head) match {
          case (f: Operator, g: Operator) if f == g ⇒ compare_lex(s.tail, t.tail)
          case (f: Operator, g: Operator) if Ordering.gtr_prec(f, g) ⇒ Greater
          case (_, _) ⇒ Unknown
        }
      } else
        // sw < tw 
        Unknown
    } else
      // variable condition not met
      Unknown

  def compare_lex(ss: TPOT, ts: TPOT): OrderingResult = {
    // strip off equal prefixes
    var (restSs, restTs) = (ss, ts)
    while (restSs.nonEmpty && (restSs.head == restTs.head)) {
      restSs = restSs.tail
      restTs = restTs.tail
    }
    if (restSs.isEmpty)
      Equal
    else 
      compare(restSs.first, restTs.first)
  }

  val FalseLit = Lit.FalseLit.toTPOT

}
