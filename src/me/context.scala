package beagle.me

// import scalaxy.loops._
// import scala.language.postfixOps // Optional.
import beagle.fol._
import beagle.util._
import beagle.datastructures._

import collection.mutable.LongMap
import collection.mutable.ListBuffer
import collection.mutable.ArrayBuffer
import collection.mutable.TreeSet


// Context elements
class CEL(val atom: TPOT) {
  var locRed: Boolean = false // whether this is locally redundant
  val units = TreeSet.empty[Int] // the UIDs of the unit clauses this appears in the body of
  override def toString = s"CEL($atom) { locRed = $locRed }"
}

// The context (ctxt) is a sequence of CELs, indexed  by the UIDs of the CEL's atoms

object ctxt {

  val elems = new LongMap[CEL]
  def apply(id: Long) = elems(id)


  @inline def contains(id: Long) = elems contains id
  @inline def get(id: Long) = elems.get(id)


}
