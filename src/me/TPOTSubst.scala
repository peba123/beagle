package beagle.me

import beagle.fol._
import beagle.util._
import beagle.datastructures._
import collection.mutable.LinkedHashMap
import collection.mutable.HashMap
import collection.mutable.ListMap
import collection.mutable.HashSet
import collection.mutable.ListBuffer

class TPOTSubst extends Iterable[Binding] {

  // Chose one:
  // var bindings = List.empty[Binding]
  // val bindings = HashMap.empty[Var, TPOT]
  val bindings = ListMap.empty[Var, TPOT]
  val dom = HashSet.empty[Var] // The domain of this
  val vcod = HashSet.empty[Var] // The variables in the codomain of this

  // Mixin iterable
  def iterator = bindings.iterator

  override def size = dom.size

  @inline final def get(x: Var) = bindings.get(x)

  @inline final def getOrElse(x: Var, default: => TPOT) =
    get(x) match {
      case None => default
      case Some(t) => t
    }

  def += (bind: Binding) = {
    // bindings = bind :: bindings
    bindings += bind
    dom += key(bind)
    vcod ++=  value(bind).vars
    this
  }

  def ++= (binds: Iterable[Binding]) = {
    if (binds.nonEmpty) {
      bindings ++= binds
        for ((x, t) <- binds) {
          dom += x
          vcod ++= t.vars
        }
    }
    this
  }

  def -= (x: Var) = {
    dom -= x
    bindings -= x
    // Need to reconstruct vcod from scratch
    vcod.clear()
    for ((_, t) <- bindings) {
      vcod ++= t.vars
    }
    this
  }

  // Apply this to a TPOT
   def apply(t: TPOT): TPOT = {
     if (isEmpty || t.isGround) t
     else {
       val res = ListBuffer.empty[Key]
         for (k <- t.elems) {
           k match {
             case x: Var => get(x) match {
               case None => res += x
               // case None => res ::= x
               case Some(s) => res ++= s.elems
               // case Some(s) => res = s.elems ::: res
             }
             case u: Operator => res += u
             // case u: Operator => res ::= u
           }
         }
       val h = TPOT(res.toList)
       // val h = TPOT(res.reverse)
       // val t1 = t.elems
       // val h1 = h.elems
       // println(s"$this($t1) = $h1")
       h
     }
   }

  def apply(ts: List[TPOT]): List[TPOT] = ts map { this(_) }
  def apply(u: Unit): Unit = u.applySubst(this)

  def restrictTo(vars: Set[Var]) = {
    // More elegant:...
    // optimize { TPOTSubst.empty ++= bindings filterKeys { vars contains _ } }
    // but this is faster
      var res = List.empty[Binding]
      for ((x, t) <- bindings) {
        if (vars contains x)
          res ::= ((x -> t))
      }
      TPOTSubst.empty ++= res
    }


  // Whether a binding x -> t clashes with a binding in this
  @inline final def clashPreTest(bind: (Var, TPOT)): Boolean = {
    val (x, t) = bind
    t.head.isInstanceOf[Operator] && (
      get(x) match {
        case Some(s) => s.head.isInstanceOf[Operator] && (t.head != s.head)
        case _ => false
      }
    )
  }

  override def clone(): TPOTSubst = new TPOTSubst() ++= this

  // def isUncombinableCheap(that: TPOTSubst): Boolean = 
  //   try { this foreach { that.clashPreTest(_) }; false } catch { case UNIFYFAIL => true }

  // x is bound to s, and a subterm of s is replaced by t.
  // Check that the binding composed this way will not be cyclic
  def occurCheck(s: TPOT, t:TPOT, x: Var) {
    if ((s.tail.nonEmpty || t.tail.nonEmpty) && (t.vars contains x)) throw UNIFYFAIL
  }

  def applyWithOccurCheck(t: TPOT, x: Var): TPOT = {
    if (isEmpty || t.isGround) t
    else {
      val res = ListBuffer.empty[Key]
      // var res = List.empty[Key]
        for (k <- t.elems) {
          k match {
            case y: Var => get(y) match {
              case None => res += y
              // case None => res ::= y
              case Some(s) => occurCheck(t, s, x)
                res ++= s.elems
                // res = s.elems ::: res
            }
            case u: Operator => res += u
            // case u: Operator => res ::= u
          }
        }
      TPOT(res.toList)
      // TPOT(res.reverse)
    }
  }



  // Extend the idempotent substitution σ by a new idempotent binding x → t.
  // Assume x ∉ dom(σ) 
  // Throws UNIFYFAIL in case of occur check
  def extended(bind: (Var, TPOT)): TPOTSubst = {
    // otherTimer.start()
    val (x, t) = bind
    // first apply this to t, with occur check
    val tthis = applyWithOccurCheck(t, x)
    if (x != tthis.head) {
      // That is, the binding to be added to this is not trivial
         if (vcod contains x) 
           // Apply x -> tthis to all bindings in this
           for ((y, s) <- this; if (s.vars contains x)) {
             occurCheck(s, tthis, y)
             val u = TPOTSubst(x -> tthis)(s)
             if (u != y) this += (y -> u)
           }
      this += (x -> tthis)
    }
    // otherTimer.stop()
    this
  }

  // Compute a matcher from s to t under a pre-existing substitution this,
  // Optionally returns the matcher
  def extendToMatcher(s: TPOT, t: TPOT): Option[TPOTSubst] = {
    var (hs, ht) = (s, t)
    var stop = false
    val γRes = this.clone()
    do {
      while (hs.nonEmpty && hs.head == ht.head) { hs = hs.tail; ht = ht.tail }
      if (hs.isEmpty)
        stop = true
      else
        hs.head match {
          case x: Var =>
          // only chance to get a matcher
            γRes.get(x) match {
              case None => γRes += (x -> ht.first) // no problem, get a new binding
              case Some(h) =>
                if (h != ht.first) stop = true // Clash
                else { // continue search
                  hs = hs.tail
                  ht = ht.rest
                }
            }
          case op: Operator => stop = true // Clash
        }
    } while (! stop)
    if (hs.isEmpty) Some(γRes) else None
  }

  // def isInjectiveOn(dom: Iterable[Var]) = ???


  // Compute a renaming matcher from s to t under a pre-existing substitution this,
  // Optionally returns the matcher
  def extendToRenaming(s: TPOT, t: TPOT): Option[TPOTSubst] = {
    var (hs, ht) = (s, t)
    var stop = false
    val γRes = this.clone()
    do {
      while (hs.nonEmpty && hs.head == ht.head) { hs = hs.tail; ht = ht.tail }
      if (hs.isEmpty)
        stop = true
      else
        hs.head match {
          case x: Var =>
          // only chance to get a matcher
            γRes.get(x) match {
              case None =>
                // try to extend γRes with (x -> ht.first)
                if (ht.head.isInstanceOf[Var] && // Renaming: must map x to a variable
                  ( !(γRes.vcod contains ht.head.asInstanceOf[Var]))) // injectivity
                  γRes += (x -> ht.first) // no problem, get a new binding
                else stop = true // cannot make a renaming
              case Some(h) =>
                if (h != ht.first) stop = true // Clash
                else { // continue search
                  hs = hs.tail
                  ht = ht.rest
                }
            }
          case op: Operator => stop = true // Clash
        }
    } while (! stop)
    if (hs.isEmpty) Some(γRes) else None
  }


  // desctructively extends this to a mgu for the given arguments, if possible
  def extendedToMGU(s: TPOT, t: TPOT): TPOTSubst =  {

    // import collection.mutable.ListBuffer
    TPOTSubst.clashPreTest(s, t)
    var open = List((s, t))
    while (open.nonEmpty) {
      var (hs, ht) = open.head
      open = open.tail
      while (hs.nonEmpty) {
        if (hs.head == ht.head) {
          // just read over and forget 
          hs = hs.tail; ht = ht.tail
        } else {
          // arrived at disagreement pair
          // Quick pre-test for function symbol clash, avoids subsequent splitting
          if (hs.head.isInstanceOf[Operator] && ht.head.isInstanceOf[Operator])
            throw UNIFYFAIL
          // split both hs and ht into first and rest and look into first components
          // sNext and tNext are a disagreement pair
          // Expand first components by this, if a variable
          val sNext = if (hs.head.isInstanceOf[Var]) this(hs.first) else hs.first
          val tNext = if (ht.head.isInstanceOf[Var]) this(ht.first) else ht.first
            (sNext.head, tNext.head) match {
            case (x: Var, y: Var) if x == y => ()
            case (x: Var, _: Var) => this.extended(x -> tNext) // never an occur check problem
            case (x: Var, _: Operator) =>
              TPOTSubst.clashPreTest(hs.rest, ht.rest, (x -> tNext))
              TPOTSubst.clashPreTest(open, (x -> tNext))
              if (tNext.vars contains x) throw UNIFYFAIL // occur check
              this.extended(x -> tNext)
            case (_: Operator, y: Var) =>
              TPOTSubst.clashPreTest(hs.rest, ht.rest, (y -> sNext))
              TPOTSubst.clashPreTest(open, (y -> sNext))
              if (sNext.vars contains y) throw UNIFYFAIL // occur check
              this.extended(y -> sNext)
            case (sOp: Operator, tOp: Operator) =>
              if (sOp != tOp) throw UNIFYFAIL
              TPOTSubst.clashPreTest(sNext, tNext)
              open ::= ((sNext, tNext))
          }
          hs = hs.rest; ht = ht.rest
        }
      }
    }
    this
  }
/*
  def extendedToMGUTimed(ssAndTs: Iterable[(TPOT, TPOT)]): TPOTSubst = {
    mguTimer.start()
    try  { val res = extendedToMGU(ssAndTs); mguCntYes += 1; mguTimer.stop(); res }
    catch { case UNIFYFAIL => mguCntNo += 1; mguTimer.stop(); throw UNIFYFAIL }
  }
 */
  def combineMaybe(that: TPOTSubst): Boolean =  {
    combineMaybeTimer.start()
    // otherTimer.start()
    val (δ, σ) = if (this.size < that.size) (this, that) else (that, this)
    // otherTimer.stop()
    val clash = δ exists { σ.clashPreTest(_) }
    if (clash) combineMaybeCntNo +=1 else combineMaybeCntYes += 1
    combineMaybeTimer.stop()
    !clash
  }

  def combine(that: TPOTSubst): TPOTSubst = {
    combineTimer.start()
    // δ is the subst whose bindings are stepwisely added to σ
    // Assume that a combineMaybe call has been done earlier
    val (δ, σ) = if (this.size < that.size) (this, that) else (that, this)
    if (δ.isEmpty) {
      combineTimer.stop(); σ
    } else {
      val σRes = σ.clone()
      // sometimes the former, sometimes the latter version is better
      // σRes.extendedToMGUTimed(σ map { case (x, t) => (TPOT(List(x)), t) })
        for ((x, t) <- δ)
          // σRes.extendedToMGUTimed(List((TPOT)List(x)), t)))
          if (σRes.dom contains x)
            σRes.extendedToMGU(TPOT(List(x)), t)
          else 
            σRes.extended(x -> t)
      combineTimer.stop()
      // if (σRes exists { case (_, t) => t.head.isInstanceOf[Var] })
      // println(s"$this combine $that = $σRes")
      σRes
    }
  }

  def combineOpt(that: TPOTSubst): Option[TPOTSubst] =
    try { val res = this combine that; combineCntYes += 1; Some(res) } catch {
      case UNIFYFAIL =>
        combineCntNo += 1
        combineTimer.stop()
        None }

  override def toString = (this map { case (v,t) => v + " → " + t }).mkString("[", ", ", "]")

}

object TPOTSubst {

  def mkRenaming(vars: Iterable[Var]): TPOTSubst =
    TPOTSubst.empty ++= vars map { x => x -> x.freshVar().toTPOT }

  def mk$Subst(vars: Iterable[Var]): TPOTSubst =
    TPOTSubst.empty ++= vars map { _ -> $.toTPOT }

  // def fresh(t: TPOT) = {
  //   mkRenaming(t.vars)(t)
  // }

  def apply(bind: (Var, TPOT)): TPOTSubst =  new TPOTSubst() += bind

  def clashPreTest(s: TPOT, t:TPOT, bind: (Var, TPOT)) {
    val (x, u) = bind
    if (u.head.isInstanceOf[Operator]) {
      var (sRest, tRest) = (s, t)
      while (sRest.nonEmpty) {
        (sRest.head, tRest.head) match {
          case (y:Var, op:Operator) if (y == x && op != u.head) => throw UNIFYFAIL
          case (op:Operator, y:Var) if (y == x && op != u.head) => throw UNIFYFAIL
          case (_, _) => ()
        }
        sRest = sRest.rest
        tRest = tRest.rest
      } 
    }
  }

  // whether a binding x -> t clashes with some pair (sᵢ,tᵢ) = (x,s) or (sᵢ,tᵢ) = (s,x) in lists of terms s₁...sₙ t₁...tₙ
  def clashPreTest(ssAndTs: Iterable[(TPOT, TPOT)], bind: (Var, TPOT)) {
    val (x, t) = bind
    if (t.head.isInstanceOf[Operator]) {
      ssAndTs foreach { st => clashPreTest(st._1, st._2, bind) }
    }
  }

  def clashPreTest(s: TPOT, t: TPOT) {
    // val s1 = s.elems
    // val t1 = t.elems
    // println(s"s = $s1")
    // println(s"t = $t1")

    var (sRest, tRest) = (s, t)
      while (sRest.nonEmpty) {
        (sRest.head, tRest.head) match {
          case (op1:Operator, op2:Operator) if (op1 != op2) => throw UNIFYFAIL
          case (_, _) => ()
        }
        sRest = sRest.rest
        tRest = tRest.rest
      } 
  }

  def mkDomSubsts(xs: List[Var]): List[TPOTSubst] = {
  // assume that xs is a list of finite domain variables
      xs match {
        case Nil ⇒ List(TPOTSubst.empty)
        case x :: rest ⇒
          // Build the cross product
          for (
            gamma ← mkDomSubsts(rest);
            t ← Sigma.domain(x.sort)
          ) yield gamma.clone() += (x -> t.toTPOT)
      }
    }

  def empty = new TPOTSubst()

}

