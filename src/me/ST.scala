
package beagle.me

// import scalaxy.loops._
// import scala.language.postfixOps // Optional.
import beagle.fol._
import beagle.util._
import beagle.datastructures._
// import pterm._


// todo: add parameter for preferred order of variables in tree

/**
  * Substitution trees for a specified domain of variables.
  * Bindings for extraneous variables are silently removed.
  */

class ST[T](domain: Option[Set[Var]], val name: String = "") {
  import collection.mutable.ArrayBuffer
  import collection.mutable.HashMap
  // import collection.immutable.HashSet

  class Node(val bind: Binding, val pred: Node) {
    val v = bind._1
    val t = bind._2

    val succs = ArrayBuffer.empty[Node]
    val succsCodom = HashMap.empty[TPOT, Int]

    // each element in info is used as column indices in a SubstArray, i.e. it is pointing back to the substitutions
    // this node represents. If empty no such substitution. 
    var info = Option.empty[T]

    @inline def hasInfo = info.nonEmpty  // whether this node ends at a substitution. Could be an inner node
    var subst: TPOTSubst = null // The substitution stored at this node, if hasInfo

    @inline def isLeaf = succs.isEmpty
    @inline def isRoot = pred == null

    // From this, an inner node or leaf, construct the substitution it represents by going up to the root
    lazy val toTPOTSubst = {
      // assume(info != -1)
      val res = TPOTSubst.empty
      var n = this
        while (!n.isRoot) {
          res += n.bind
          n = n.pred
        }
      // val h = TPOTSubst.empty ++= res.tail // tail for stripping off dummy binding in root node
      res
    }
    override def toString = s"Node($bind, $info)"

  }

  // whether a binding is relevant for this ST
  @inline def relevant(bind: Binding) = domain == None || (domain.get contains key(bind))

  val root = new Node(makeVar("_") -> TPOT(List(makeVar("_"))), null) // dummy

  // A path is a possibly empty sequence of pairs (n₀, i₀), (n₁, i₁), ..., (nₖ, iₖ) such that nₖ is the root and n₀ is a hasInfo node
  // The value iⱼ says that in node nⱼ the iⱼ-th successor has been taken in the path.
  // i₀ has a differen meaning: it means that the i₀-th info element is next to be chosen when iterating through the substitutions in this
  type PathEntry = (Node, Int)
  type Path = List[PathEntry]
  @inline def nodeOf(pe: (Node, Int)) = pe._1
  @inline def succOf(pe: (Node, Int)) = pe._2

  // Enumerating all paths in this
  // Sets current to the first substitution going depth-first left-right
  var currentPath: Path = List.empty
  var exhausted = false // whether all substitutions have been enumerated

  // Finds the first path, starting from a given one.
  // Result is the *longest* path going depth-first left-right
  private final def findFirst(p: Path): Path = {
    val ((start, _) :: more) = p
    //  very special case first
    if (start.isRoot && !start.hasInfo && start.isLeaf)
      // this is the only case of a leaf node that is not a hasInfo node
      List.empty
    else {
      var (n, res) = (start, more)
      while (!n.isLeaf) {
        // tacitly assume that a leaf is a hasInfo node
        res ::= (n -> 0)
        n = n.succs(0)
      }
      res ::= (n -> 0) // leaf
      res
    }
  }

  // Finds next hasInfo node succeeding a given current node. 
  private final def findNext(p: Path): Path = {
    // println(s"ST $name: findNext()")
    var res = p.tail
    while (res.nonEmpty) {
      val ((n, i)) = res.head
      if (n.hasInfo)
        return (n, 0) :: res.tail
      else if (i < n.succs.size-1)
        return findFirst((n.succs(i+1), 0) :: (n, i+1) :: res.tail)
      else res = res.tail
    }
    // res is empty, i.e. no next path found
    List.empty
  }


  def reset() {
    // println(s"ST index $name: reset()")
    // println(s"ST $name: reset()")
    exhausted = false
    currentPath == List.empty
  }

  def next(): Option[(TPOTSubst, T)] = {
    // println(s"ST $name: next()")
    nextCntST += 1
    nextTimerST.start()
    if (!exhausted) {
      if (currentPath.isEmpty)
        currentPath = findFirst(List((root, -1)))
      else
        currentPath = findNext(currentPath)
      exhausted = (currentPath.isEmpty)
    }
    val res =
      // if (exhausted) None else Some(nodeOf(currentPath.head).toTPOTSubst)
      if (exhausted) None else
        Some(nodeOf(currentPath.head).subst,
          nodeOf(currentPath.head).info.get)
    nextTimerST.stop()
    // println(s"ST $name: next() = " + res)
    // show()
    res
  }


  def size(n: Node): Int =
    if (n.isLeaf) 1 else {
      val h = if (n.hasInfo) 1 else 0
      h + n.succs.foldLeft(0) { _ + size(_) }
    }

  def size: Int = size(root)

  // Finds the longest path p starting from n such that σ is equal with
  // Returns the node and δ, a copy of σ with all the equal substitutions removed
  def longestPrefix(n: Node, σ: TPOTSubst): (Node, TPOTSubst) = {

    @annotation.tailrec
    def h(n: Node, δ: TPOTSubst): (Node, TPOTSubst) =
      if (n.isLeaf) (n, δ)
      else {
        val x = n.succs(0).v // same variable for all sister nodes
        δ.get(x) match {
          case None => (n, δ)
          case Some(s) =>
            // That is, δ has the binding x -> s
            // Find s in the successor nodes
            n.succsCodom.get(s) match {
              case None => (n, δ) // not found among bindings for x
              case Some(i) => 
                // remove x -> s from δ and continue with succ
                δ -= x
                h(n.succs(i), δ)
            }
            // n.succs find { _.t == s } match {
            //   case None => (n, δ) // not found among bindings for x
            //   case Some(succ) =>
            //     // remove x -> s from δ and continue with succ
            //     δ -= x
            //     h(succ, δ)
            // }
        }
      }
    h(n, σ.clone())
  }

  def added(σ: TPOTSubst, info: T) {
    addTimerST.start()
    // println("ST have before add:")
    // show()

    // println(s"add($σ, $info), δ = $δ, n = $n")

    def extendHere(n: Node, δ: TPOTSubst) {
        var res = n
        for (bind <- δ; if relevant(bind)) {
          val k = new Node(bind, res)
          res.succs += k
          res.succsCodom += (value(bind) -> (res.succs.length-1))
          res = res.succs.last
        }
        res.info = Some(info)
        res.subst = σ
    }

    def h(n: Node, δ: TPOTSubst) { 
      if (n.succs.isEmpty)
        extendHere(n, δ)
      else {
        val x = n.succs(0).v // same variable for all sister nodes
        δ.get(x) match {
            case None =>
            // add δ underneath all successors
            n.succs foreach { h(_, δ) }
          case Some(s) =>
            // That is, δ has the binding x -> s
            // Try to find s in the successor nodes
            n.succsCodom.get(s) match { 
              case Some(i) =>
                // yes, have x -> t in succs. Remove x -> s from δ and continue with succ
                h(n.succs(i), δ.clone() -= x)
              case None =>
                // no, append to succs a new branch for x->s and continue
                val k = new Node((x -> s), n)
                n.succs += k
                n.succsCodom += (s -> (n.succs.length-1))
                h(n.succs.last, δ.clone() -= x)
            }
/*
            n.succs find { _.t == s } match {
              case Some(succ) =>
                // yes, have x -> t in succs. Remove x -> s from δ and continue with succ
                h(succ, δ.clone() -= x)
              case None =>
                // no, append to succs a new branch for x->s and continue
                val k = new Node((x -> s), n)
                n.succs += k
                h(n.succs.last, δ.clone() -= x)
            }
 */
        }
      }
    }
    h(root, σ)
    // val res = if (n.getInfo == -1) { n.setInfo(info); true } else false // already there
    // println("ST after add:")
    // show()
    addTimerST.stop()
  }

  // set membership in the index
  def contains(σ: TPOTSubst) = {
    // show()
    val (n, rem) = longestPrefix(root, σ)
    val res = n.hasInfo && rem.isEmpty
    // println(s"contains($σ) = $res\n")
    res
  }

  def hasInfoNodes(n: Node): List[Node] = {
    if (n.isLeaf)
      // always a hasInfo node unless it is the root
      if (n.hasInfo) List(n) else List.empty
    else {
      var res = List.empty[Node]
      if (n.hasInfo) res ::= n
      for (m <- n.succs) res :::= hasInfoNodes(m)
      res
    }
  }

  // Combine all substitutions in this with σ; return the combined substitutions paired with the info
  // of the substitution in this
  def combine(σ: TPOTSubst) = {
    combineTimerST.start()

    // Extend σres with all bindings in σ, return the singleton list, or the empty list if not possible
    def extend(σres: TPOTSubst, σ: TPOTSubst, info: T) =
      if (σ.isEmpty)
        // shortcut avoids cloning
        List((σres, info))
      else try {
        val hres = σres.clone()
        for (bind <- σ) hres.extended(bind)
        List((hres, info))
      } catch {
        case UNIFYFAIL => List.empty // can be for occur check
      }

    // σres is the result substitution so far
    // σ is the substitution that still needs to be combined with all substitutions below n
    // Invariant: dom(σres) and dom(σ) is disjoint
    def combineInner(n: Node, σres: TPOTSubst, σ: TPOTSubst): List[(TPOTSubst, T)] = {
    if (n.isRoot && !n.hasInfo && n.isLeaf)
      // this is the only case of a leaf node that is not a hasInfo node
      List.empty
    else if (n.isLeaf)
        extend(σres, σ, n.info.get)
      else {
        // n is an inner node
        // if n is an info node we get the extension by σ as above
        var res = if (n.hasInfo) extend(σres, σ, n.info.get) else List.empty
        // consider the variable x bound in the next layer of n
        val x = n.succs(0).v // same variable for all sister nodes
        σ.get(x) match {
          case None =>
            // no bindings for x in σ
            // try all bindings in for x in succs, continuing with same σ
            for (succ <- n.succs) try {
              res :::= combineInner(succ, σres.clone().extended(succ.bind), σ)
            } catch {
              case UNIFYFAIL => ()
            }
          case Some(s) =>
            // That is, σ has the binding x -> s
            for (succ <- n.succs) try {
              res :::= combineInner(succ, σres.clone().extended(succ.bind).extendedToMGU(succ.t, s), σ.clone() -= x)
            } catch {
              case UNIFYFAIL => ()
            }
        }
        res
      }
    }
    // println("combine, have:")
    // show()
    val res = combineInner(root, TPOTSubst.empty, σ)
    // val rst = root.subTreeInfo
    // println(s"$size terms, combine($σ, $relevantInfo) = $res, root.subTreeInfo = $rst")
    // println("size = %5d, low = %5d, high = %5d".format(size, low, high))
    // println(res.length + " substitutions")
    combineTimerST.stop()
    res
  }



  def show() {
    println("Bindings in index " + name)
    println("——————————–—————–")
    // toBindings(root) map { _.tail } foreach { binds => println(binds) }
    hasInfoNodes(root) foreach { n =>
      println(n.toTPOTSubst + ":" + n.info)
    }

    // toBindings(root) foreach { binds =>
    //   println((binds map { case (vt,i) => vt._1  + "→" + vt._2 + ":" + i  }).mkString("[", ", ", "]")) }
  }

}
