package beagle.me

// import scalaxy.loops._
// import scala.language.postfixOps // Optional.
import beagle.fol._
import beagle.util._
import beagle.datastructures._

import collection.mutable.ListBuffer
import collection.mutable.ArrayBuffer
import collection.mutable.PriorityQueue
import collection.mutable.HashSet
import collection.immutable.BitSet

// todo: use mutable BitSet ?
// todo: bring back head subsumption wrt universal olds

object Saturation {

  val debug = false

  var props = List.empty[Propagator]
  // var negProps = List.empty[Propagator]

  // Keep old units apart for better efficiency of subsumption checking
  val oldUniversal = new TPOTMap[Unit]("old universal")
  val oldNonUniversal = new TPOTMap[Unit]("old non universal")
  // val opt = new TPOTMap[Unit]("old universal")

  val neuRemainders = new RemSet // these do require split and possibly backjump
  // val neuRemainders = ListBuffer.empty[Remainder] // these do require split and possibly backjump

  /**
   * Save current state information.
   * Notice this does not include "old" clauses, which are handled by age
   */

  class SavedState {
    val sProps = props map { prop => new prop.SavedState() }
    // val sNegProps = negProps map { prop => new prop.SavedState() }

    val sOldUniversal = new oldUniversal.SavedState()
    val sOldNonUniversal = new oldNonUniversal.SavedState()
    // val sOpt = new opt.SavedState()

    val sNeuRemainders = neuRemainders.clone()

    def restore() {
      sProps foreach { _.restore() }
      // sNegProps foreach { _.restore() }

      sOldUniversal.restore()
      sOldNonUniversal.restore()
      // sOpt.restore()

      neuRemainders.clear(); neuRemainders ++= sNeuRemainders.remainders
    }
  }

  class DecisionLevelInfo(val rightUnitFn: BitSet ⇒ Unit, val savedState: SavedState)
  var decisionLevels = ArrayBuffer.empty[DecisionLevelInfo]

  def setup(input: List[ConsClause]) {
    input foreach {
      case ConsClause(List(), _, _) => throw FAIL

      case ConsClause(List(lit @ Lit(true, _)), _, _) =>
        neuRemainders += new Remainder(List(lit.toTPOT), 0, BitSet.empty, BitSet.empty, isFull = true)

      case ConsClause(List(lit @ Lit(false, _)), _, _) =>
        neuRemainders += new Remainder(List(lit.toTPOT), 0, BitSet.empty, BitSet.empty, isFull = true)

      case ConsClause(litsIn, _, _) =>
        val lits = litsIn map { _.toTPOT }
        // val (pos, neg) = lits partition { _.isPositive }
        // val negSortedForProp = TPOT.sortForVars(neg, Set.empty)
        // val posSortedForProp = TPOT.sortForVars(pos, negSortedForProp.vars)
        // val prop = new Propagator(negSortedForProp ::: posSortedForProp, negAssertOnly = false)
        val prop = new Propagator(TPOT.sortForVars(lits, Set.empty), negAssertOnly = false)
        // prop.addToStack(PosVUnit(makeVar("x")))
        if (flags.negAssert.value) prop.addToStack(PosVUnit(makeVar("x")))
        props ::= prop

        // if (negUnitPropagation && (lits exists { !_.isPositive })) { 
        //   val posSortedForNegProp = TPOT.sortForVars(pos, Set.empty)
        //   val negSortedForNegProp = TPOT.sortForVars(neg, posSortedForNegProp.vars)
        //   val negProp= new Propagator(posSortedForNegProp ::: negSortedForNegProp, negAssertOnly = true)
        //   negProp.addToStack(PosVUnit(makeVar("x")))
        //   props ::= negProp
        // }
    }

/*    if (debug) {
      println("Propagators")
      props foreach { println(_) }
      println()
      println("\nneuRemainders")
      println("————–——————————–")
      neuRemainders foreach { rem => println(rem) }
      println()
    }
 */
  }

/*  def oldConflict(u: Unit) = {
    // We check the universal ones first to get better bakjumping behaviour (avoid non-universal old units)
    val Unit(lit, _, isUniversal, _, _) = u
    if (isUniversal)
      oldUniversal.mguFind(lit.compl) orElse
        oldNonUniversal.mguFind(lit.compl, (uOld, σ) => σ(uOld) isVariant uOld) // todo: natively implement
    else
      oldUniversal.getSubsumer(lit.compl) orElse
        oldNonUniversal.getVariant(lit.compl)

  }
  @inline def oldConflicts(u: Unit) = oldConflict(u).nonEmpty
 */
  def oldConflictUniversal(lit: TPOT) = {
    oldUniversal.mguFind(lit.compl) orElse
    oldNonUniversal.mguFind(lit.compl, (uOld, σ) => σ(uOld) isVariant uOld) // todo: natively implement
  }

  def oldConflictNonUniversal(lit: TPOT) = {
    oldUniversal.getSubsumer(lit.compl) orElse
    oldNonUniversal.getVariant(lit.compl)
  }
  @inline def oldConflictsNonUniversal(lit: TPOT) = oldConflictNonUniversal(lit).nonEmpty

  def oldSubsumes(lit: TPOT, successCheck: (Unit, TPOTSubst) => Boolean = (_, _) => true) =
    (oldNonUniversal.getVariant(lit, successCheck) orElse
      oldUniversal.getSubsumer(lit, successCheck)).nonEmpty

  /**
   * Make a remainder from a given list of decLits, supposed to be clause literals
   * That is, the sign will flip
   * The remainder is built modulo variants and modulo being in conflict with old
   * Also return the idxRelevant of those old units that conflict a decLit
   */
  def makeRemainder(decLits: List[TPOT], checkAssert: Boolean) = {
    var (remainder, idxUniversalSides, idxNonUniversalSides, maxAgeNonRemainderSides) = (List.empty[TPOT], BitSet.empty, BitSet.empty, 0)
    for (lit <- decLits) {
      oldConflictNonUniversal(lit) match {
        case None => remainder ::= lit
        case Some((_, uOld)) =>
          if (uOld.isUniversal)
            idxUniversalSides ++= uOld.idxRelevant
          else
            idxNonUniversalSides ++= uOld.idxRelevant
          maxAgeNonRemainderSides = math.max(maxAgeNonRemainderSides, uOld.age)
      }
    }
    var res = (remainder, idxUniversalSides, idxNonUniversalSides, maxAgeNonRemainderSides)
    // remainder could be a universal assert and only now be in conflict:
    if (checkAssert && remainder.nonEmpty && remainder.tail.isEmpty && idxNonUniversalSides.isEmpty && !remainder.head.isGround) {
      oldConflictUniversal(remainder.head) match {
        case None => ()
        case Some((_, uOld)) => res = (List.empty, uOld.idxRelevant ++ idxUniversalSides, idxNonUniversalSides, maxAgeNonRemainderSides)
      }
    }
    res
  }

  /**
   * Collect the complement of all non-universal sides, instantiated by σ.
   * This gives a list of clause literals
   */
  def collectDecLits(sides: List[Unit]) = {
      var (decLits, idxUniversalSides, maxAgeUniversalSides) = (List.empty[TPOT], BitSet.empty, 0)
      for (side <- sides)
        if (side.isUniversal) {
          idxUniversalSides ++= side.idxRelevant
          maxAgeUniversalSides = math.max(maxAgeUniversalSides, side.age)
        } else
          decLits ::= side.lit.compl
      (decLits, idxUniversalSides, maxAgeUniversalSides)
  }

  /*
  def negAssert(u: Unit) {
    if (debug) println("negAssert " + u)
  }
   */

  def propagate(): Boolean = {
      var someRes = false
      for (prop <- props) {
        prop.nextRemainder() match {
          case None => ()
          case Some(sides) =>
            someRes = true
            val (sidesDecLits, idxUniversalSides, maxAgeUniversalSides) = collectDecLits(sides)
            val (remainderLits, idxUniversal, idxNonUniversal, maxAgeNonRemainderSides) = makeRemainder(sidesDecLits, checkAssert = true)

            if (remainderLits.isEmpty) {
              if (debug) {
                println("propagate: CLOSE(" + (idxUniversalSides ++ idxUniversal ++ idxNonUniversal).mkString("{", ", ", "}") + ")")
                println("           propagator: " + prop)
                println("           sides: " + sides)
              }
              throw CLOSE(idxUniversalSides ++ idxUniversal ++ idxNonUniversal)
            }

            // todo: resolve 

            if (remainderLits exists { oldSubsumes(_) }) {
              if (debug) println("subsumed remainder (propagate) ")
              subsCnt += 1
            } else {
              // val remainder = Remainder(List(remainderLits.head), math.max(maxAgeUniversalSides, maxAgeNonRemainderSides)+1, idxUniversalSides ++ idxUniversal, idxNonUniversal, isFull = remainderLits.tail.isEmpty)
              val remainder = Remainder(remainderLits, math.max(maxAgeUniversalSides, maxAgeNonRemainderSides)+1, idxUniversalSides ++ idxUniversal, idxNonUniversal, isFull = true)
              if (debug) {
                println("propagate:          prop = " + prop)
                println("propagate:         sides = " + sides)
                println("propagate:   sideDecLits = " + sidesDecLits)
                println("propagate: remainderLits = " + remainderLits)
                // println("remainder: " + remainder)
                // println("     prop: " + prop)
                // println("    sides: " + sides.mkString(", "))
              }
              // println(remainder.length + " remainder " + remainder) //  + ", sides " + (sides map { σ(_) } ))
              neuRemainders += remainder
              infRemainderCnt += 1
            }
        }
      }
      someRes
  }

  case class CLOSE(idxRelevant: BitSet) extends Exception

  private def saturate() {

    object CONTINUE extends Exception

    var nrRounds = 0

    if (!debug && beagle.util.verbose)
      println("░ %10s %14s %14s %14s ░".format("#decisions", "oldUniv", "oldNonUniv", "neuRemainders"))

    var oldNrDec = decisionLevels.length

    var splitRightUnit: Option[Unit] = Some(NegVUnit(makeVar("v")))

    while (splitRightUnit.nonEmpty || neuRemainders.nonEmpty) try {

      if (!debug && beagle.util.verbose && (nrRounds % 1000 == 0 || (oldNrDec != decisionLevels.length && nrRounds % 1 == 0)))
        println("░ %10d %14s %14d %14s ░".format(decisionLevels.length, oldUniversal.size, oldNonUniversal.size, neuRemainders.size))

      nrRounds += 1
      oldNrDec = decisionLevels.length

      var sel: Unit = null

      if (splitRightUnit.nonEmpty) {

        sel = splitRightUnit.get
        if (debug) println("split right " + sel)
        splitRightUnit = None

      } else if (neuRemainders.nonEmpty) {

        // val remainder =
        //   if (nrRounds % 17 == 0)
        //     neuRemainders.removeOldest()
        //   else 
        //     neuRemainders.removeLightest()
        val remainder = neuRemainders.removeLightest()
        val Remainder(remainderLits, remainderAge, idxUniversalSides, idxNonUniversalSides, isFull) = remainder

/*        assume(remainderLits.nonEmpty && remainderLits.tail.isEmpty)
        val remainderLit = remainderLits.head
        if (oldSubsumes(remainderLit)) {
          if (debug) println("subsumed remainder " + remainder)
          subsCnt += 1
          throw CONTINUE
        }
        val isUniversal = (isFull && idxNonUniversalSides.isEmpty) || remainderLit.isGround
        (if (isUniversal) oldConflictUniversal(remainderLit) else oldConflictNonUniversal(remainderLit)) match {
          case None =>
            // Split or assert
            if (isFull) {
              sel = Unit(remainderLit, remainderAge, isUniversal = isUniversal, idxUniversalSides ++ idxNonUniversalSides, isRight = true)
              if (debug) println("assert " + sel + " from remainder " + remainder)
              infAssertCnt += 1
            } else {
              // proper split
              // selLit cannot be in conflict, even if ground (in that case, if it were in conflict it would have been detected)
              // todo: universal non-ground splitting
              sel = Unit(remainderLit, remainderAge, isUniversal = remainderLit.isGround, BitSet(decisionLevels.length), isRight = false)
              val rightUnitFn = { idxRelevant: BitSet ⇒ Unit(remainderLit.compl, remainderAge, isUniversal = remainderLit.compl.isGround, idxRelevant, isRight = true) }
              decisionLevels += new DecisionLevelInfo(rightUnitFn, new SavedState)
              if (debug) println("split " + sel)
              infSplitCnt += 1
            }
          case Some((_, uOld)) => throw CLOSE(uOld.idxRelevant ++ idxUniversalSides ++ idxNonUniversalSides)
        }
      }
 */
        if (remainderLits exists { oldSubsumes(_) }) {
          if (debug) println("subsumed remainder " + remainder)
          subsCnt += 1
          throw CONTINUE
        }
        // Find a remainder literal that is not in conflict with old, if possible
        // Also collect the relevant ancestors of the remainder literals as we go, but do not join them
        // already now, as this is costly and potentially not needed
        val (nonConflictLits, idxUniversalConflictLits, idxNonUniversalConflictLits, maxAgeNonRemainderLits) = makeRemainder(remainderLits, checkAssert = idxNonUniversalSides.isEmpty)
        lazy val idxRelevant = remainder.idxRelevant ++ idxUniversalConflictLits ++ idxNonUniversalConflictLits
        val newAge = math.max(remainderAge, maxAgeNonRemainderLits+1)
        if (nonConflictLits.isEmpty) {
          if (debug) println("CLOSE(" + idxRelevant.mkString("{", ", ", "}") + ")")
          throw CLOSE(idxRelevant)
        }

        val isAssert = nonConflictLits.tail.isEmpty && idxNonUniversalSides.isEmpty && idxNonUniversalConflictLits.isEmpty
        if (isAssert) {
          val selLit = nonConflictLits.head
          sel = Unit(selLit, newAge, isUniversal = true, idxRelevant, isRight = true)
          if (debug) println("assert " + sel + " from remainder " + remainder)
          infAssertCnt += 1
        } else if (nonConflictLits.size < remainderLits.size)
          // Ignore this remainder as at least one remainder lit is in conflict, which will be detected also when using the conflicting literal
          // as a side unit
          throw CONTINUE
        else {
          // proper split
          val selLit = nonConflictLits.head
          // selLit cannot be in conflict, even if ground (in that case, if it were in conflict it would be removed by makeRemainder)
          val isUniversal = selLit.isGround
          sel = Unit(selLit, newAge, isUniversal = isUniversal, BitSet(decisionLevels.length), isRight = false)
          val rightUnitFn = { idxRelevant: BitSet ⇒ Unit(selLit.compl, newAge, isUniversal = selLit.compl.isGround, idxRelevant, isRight = true) }
          decisionLevels += new DecisionLevelInfo(rightUnitFn, new SavedState)
          if (debug) println("split " + sel)
          infSplitCnt += 1
        }
      }
      if (debug) println("[" + decisionLevels.length + "] " + sel)
      (if (sel.isUniversal) oldUniversal else oldNonUniversal) += sel.lit -> sel

      for (prop <- props) {
        // if (prop.negAssertOnly && sel.isNegVUnit)
        //   () // println("skip " + sel)
        // else
          prop.addToStack(sel)
      }

      var exhausted = false
      do {
          exhausted = !propagate()
      } while (! exhausted)

      
    } catch {
      case CONTINUE => ()
      case CLOSE(idxRelevant) =>
        if (idxRelevant.isEmpty) {
          infBackjumpCnt += 1
          throw FAIL
        } else {
          val backtrackLevel = idxRelevant.max
          val backtrackLevelInfo = decisionLevels(backtrackLevel)
          backtrackLevelInfo.savedState.restore()
          decisionLevels.reduceToSize(backtrackLevel) // shorten stack
          val rightUnit = backtrackLevelInfo.rightUnitFn(idxRelevant - backtrackLevel)
          infBackjumpCnt += 1
          // if (debug) {
          //   println("close " + idxRelevant.mkString("{", ", ", "}"))
          // }
          assume(splitRightUnit.isEmpty)
          splitRightUnit = Some(rightUnit)
        }
    }
  }

  def run(input: List[ConsClause]) = {

    var satisfiable = true
    try {
      setup(input)
      totalTimer.start()
      saturate()
    } catch {
      case FAIL => satisfiable = false
    }
    totalTimer.stop()
    satisfiable
  }

  def stats() {
    println()
    println("Sizes")
    println("—————")
    println("|oldUniversal|        : " + oldUniversal.size)
    println("|oldNonUniversal|     : " + oldNonUniversal.size)
    // println("|neuPosAsserts|       : " + neuPosAsserts.size)
    // println("|neuNegAsserts|       : " + neuNegAsserts.size)
    println("|neuRemainders|       : " + neuRemainders.size)
  }

}

