
package beagle.util

case class Timer(name: String) {

  /** Accumulated time in nanosecs */
  var acc:Long = 0 

  /** Whether the timer is currently running */
  var started = false

  /** Time of the last start */
  private var startTime:Long = 0

  @inline final def measure[T](call: => T): T = {
    start()
    val res = call
    stop()
    res
  }

  @inline final def start() {
    // Need to stop first, to use sinceStated in a meaningful way
    if (started)
      stop()
    startTime = System.nanoTime()
    started = true
  }

  /** Stop the timer, saving accumulated time. */
  @inline final def stop() {
    if (started) {
      acc += System.nanoTime() - startTime
      started = false
    }
  }

  /** Total time (in seconds) this timer has run. */
  def total = acc / 1e9

  /** @return if the timer is running, gives the time of the current run in seconds. */
  def sinceStarted() =     
    if (started) 
      (System.nanoTime() - startTime) / 1e9
    else
      0.0
}

object Timer {

  val total = Timer("Total")

  /** Used in State.derive */
  val mainLoop = Timer("Main loop")
  val preProc = Timer("Preprocessing")
  val parsing = Timer("Parsing")
  val reduction = Timer("Reduction")
  val inferencing = Timer("Inferencing")
  val subsumption = Timer("Subsumption")
  val demod = Timer("Demodulation")
  val simplificationBG = Timer("BG Simplification")
  val cnfConversion = Timer("CNF conversion")
  val x = Timer("X")

  /** Time spent during weak abstraction of clauses */
  val abs = Timer("Abstraction")

  /** Time spent in minUnsatCore, if enabled */
  val muc = Timer("MUC search")

  //dbg timers
  val normForCooper = Timer("Normalising for Cooper")
  val cooperCalls = Timer("Cooper")

  //dbg idx
  val idx_insert = Timer("Index Insertion")
  val idx_delete = Timer("Index Deletion")
  val idx_match = Timer("Index Matching")

  val bgTaut = Timer("BG Taut test")

  /** Timers for which to print statistics */
  val timers = List(preProc, parsing, cnfConversion, mainLoop, inferencing, reduction, demod, simplificationBG, subsumption, 
  abs, muc,
  cooperCalls, total,
  idx_insert, idx_delete, idx_match,
  bgTaut)

  def stats() {
    println("Timing (in seconds)")
    println("----------------------")
    timers foreach { t => {
      t.stop() //if not stopped already
      println("%-20s : %.2f".format(t.name, t.total))
    }}
  }

  /** @return Current time in seconds to ns accuracy*/
  def now() = System.nanoTime() / 1e9

}

