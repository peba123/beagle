package beagle.util

import beagle._
import datastructures._
import fol._

abstract class Printer {
  val name: String

  //Common symbols
  val negSym = "¬"
  val orSym = "∨"
  val andSym = "∧"
  val impSym = "⇒"
  val convImpSym = "⇐"
  val iffSym = "⇔"
  val iffNotSym = "⇎"
  val emptyClauseSym = "□"
  val equalSym = "≈"
  val equalOrderedSym = "→"
  val forallSym = "∀"
  val existSym = "∃"
  val falseSym = "⊥"
  val trueSym = "⊤"

  //Common format functions
  val sumFF = Operator.infix(" + ")
  val uminusFF = Operator.prefix("-")
  val greaterFF = Operator.infix(" > ")
  val dividesFF = Operator.infix("|")
  val lessFF = Operator.infix(" < ")
  val differenceFF = Operator.infix(" - ")
  val productFF = Operator.infix("·")
  val nlppFF = Operator.infix("·")
  val quotientFF = Operator.infix("/")
  val lessEqFF = Operator.infix(" ≤ ")
  val greaterEqFF = Operator.infix(" ≥ ")
  val gtrFF = Operator.infix(" ≻ ") // Term ordering
  val gtrEqFF = Operator.infix(" ≽ ") 
  
  def sortToString(s: Sort): String
  def opToString(op: Operator): String
  def arityToString(a: Arity): String
  def clauseToString(c: ConsClause): String
  def litToString(l: Lit): String
  def eqnToString(e: Eqn): String
  //def atomToString(a: Atom): String
  def domElemToString[T](d: DomElem[T]): String
  def quantifierToString(formula: QuantForm): String
  def varToString(v: Var): String
  def signatureToString(s: Signature): String
  def letFormulaToString(f: LetFormula): String
  def letTermToString(t: Let): String
  def pp(f: Formula): Unit // pretty printer

  // Used by pretty printer
  def nlspaces(n: Int) {
    println()
    for (i <- 1 to n) print(' ')
  }

}


object DefaultPrinter extends Printer {

  val name = "default"

  def sortToString(s: Sort): String = s.name 
  
  def opToString(op: Operator): String = 
    if (beagle.util.flags.longNames.value) op.name else op.shortName
  
  def arityToString(a: Arity): String = 
  	(if (a.argsSorts.isEmpty) "" 
  	  else (a.argsSorts.toMyString("", " × ", "") + " → ")) + a.resSort
  
  def clauseToString(c: ConsClause): String = {
    (if (flags.raw.value) {
      def annotation(i: Int) =
	(if (c.iMaximal contains i) "*" else "") + (if (c.iEligible contains i) "+" else "")

      // Not very elegant
      (if (c.isEmpty) 
        emptyClauseSym
      else {
	var res = c.lits(0).toString + annotation(0)
	for (i ← 1 until c.length)
	  res = res + " "+orSym+" " + c.lits(i).toString + annotation(i)
	res
      }) // + (if (idxRelevant.isEmpty) "" else ":" + idxRelevant.toList.toMyString("{", ", ", "}"))
    } else {
      if (c.unabstrCautious.isEmpty) 
        emptyClauseSym
      else
        c.unabstrCautious.lits.mkString(" "+orSym+" ")
    }) + (if (c.constraint.nonEmpty) " || " + c.constraint.mkString(", ") else "")
    // + ":d=" + c.deltaConjecture + ":w=" + c.weight + ":a=" + c.age
       // Cannot do this: infinite recursion
       // + (if (c.isBG && !c.isSolverClause)
       //           " (as BG solver clauses: " + c.asSolverClauses.get.toMyString("{", ", ", "}") + ")"
       //         else "" )

  }
	
  def litToString(l: Lit): String =
    (l.eqn match {
      case PredEqn(equ) ⇒ (if (l.isPositive) "" else negSym) + l.eqn.toString // Works also for PseudoVar and Assert  
      case _ ⇒ (if (l.isPositive) l.eqn.toString else negSym+"(" + l.eqn.toString + ")")
    }) // + (if (l.canonical != l) "{" + l.canonical + "}" else "")
  
  def eqnToString(e: Eqn): String = {
    (e match {
      case PredEqn(a) ⇒ a.toString// + "{" + a.canonical + "}"
      case _ ⇒ {
        val eqsym = if (e.isOrdered) equalOrderedSym else equalSym
        if (e.sort == Signature.ISort)
          e.lhs.toString + eqsym + e.rhs.toString
        else
          e.lhs.toString + " " + eqsym + "_" + e.sort + " " + e.rhs.toString  // + ":" + weight
      }
    }) // + e.termIndex 
  }
  /*
  def atomToString(a: Atom): String =
    // Generic equality "=" is not an operator in our signatures, hence check explicity
    this match {
      case Equation(s, t) ⇒ "(" + s.toString + " = " + t.toString + ")"
      case _ ⇒ Sigma.findFormatFn(pred, Arity(args.sortsOf, Signature.OSort)) match {
        case Some(fn) ⇒ fn(args)
        case None     ⇒ pred + args.toMyString("", "(", ",", ")") // Fallback 
      }
    }
  */
  
  def domElemToString[T](d: DomElem[T]): String = d.value.toString
  
  // def toStringNoSort(v: Var) = v.name + (if (v.index > 0) "_" + v.index else "") + (if (v.isAbstVar) "ᵃ" else "")
  def varToStringWithSort(v: Var) =
    varToString(v) + (if (v.sort == Signature.ISort) "" else ":" + v.sort.name)

  def quantifierToString(formula: QuantForm): String =
    "(" + formula.q + " " + (formula.xs map { varToStringWithSort(_) }).toMyString("", " ", "") + " " + formula.f + ")"
  
  def letFormulaToString(letf: LetFormula): String = 
    "(let " + (letf.bindings map {
      case (v, t) => v + equalSym + t
    }).toMyString("", " ", "") + " in " + letf.body + ")"

  def letTermToString(let: Let): String = 
    "(let " + (let.bindings map {
      case (v, t) => v + equalSym + t
    }).toMyString("", " ", "") + " in " + let.body + ")"


  def varToString(v: Var): String = v.name + (if (v.index > 0) "_" + v.index else "") + (if (v.isAbstVar) "ᵃ" else "")
  
  def signatureToString(s: Signature): String = {
    "Background sorts: " + s.bgSorts.toList.mkString("{", ", ", "}") + "\n" +
    "Foreground sorts: " + s.fgSorts.toList.mkString("{", ", ", "}") + "\n" +
    "Background operators: " + "\n" +
    s.bgOperators.map(op ⇒ "    " + op.name + ": " + op.arity).mkString("\n") + "\n" +
    "Foreground operators: " + "\n" +
    s.fgOperators.map(op ⇒ "    " + op.name + ": " + op.arity).mkString("\n") + "\n" +
    "Precedence among foreground operators: " + 
      ( s.fgOperators.toList.sortWith(s.gtrPrecSimple(_, _)) map { _.name }).mkString(" > ") + "\n" +
    "Templates for Foreground sorts\n" +
    s.fgSorts.filterNot(so => so == Signature.OSort | so == Signature.TSort ).map(so => "    " + so + ": " + s.templates(so).mkString(", ")).mkString("\n")+
    "\nSorts with finite domains:\n"+
    (s.domain map { case ((s: Sort, elems: List[Term])) => ("    " + s + ": " + elems.mkString(", ")) }).mkString("\n")
  }


  def pp(f: Formula, indent: Int) {
    f match {
      case a @ Atom(_, _) => print(a)
      case UnOpForm(op, f) => {
        print(op)
        pp(f, indent+1)
      }
      case BinOpForm(op, f1, f2) =>
        op match {
          case And | Or => {
            val fs = f.toList(op)
            print("( ");
            for (fss <- fs.tails; if !fss.isEmpty) {
              pp(fss.head, indent+2)
              // More formulas?
              if (!fss.tail.isEmpty) {
                nlspaces(indent)
                print(op.toString + " ")
              }
            }
            print(")")
          }
          case _ => {
            print("( "); pp(f1, indent+2)
            nlspaces(indent)
            print(op.toString + " ")
            pp(f2, indent+2); print(")")
          }
        }
      case QuantForm(q, xs, f) => {
        print(q + " " + (xs map { varToStringWithSort(_) }).toMyString("", " ", ""))
        nlspaces(indent+2)
        pp(f, indent+2)
      }
      case LetFormula(bindings, body) => {
        print("(let " + (bindings map {
          case (v, t) => v + " = " + t
        }).toMyString("", " ", "") + " in ")
        nlspaces(indent+2)
        pp(body, indent+2)
        print(")")
      }
      case ITEForm(cond, thenForm, elseForm) => {
        print("$ite_f(" + cond + ",")
        nlspaces(indent+2); pp(thenForm, indent+2); print(",")
        nlspaces(indent+2); pp(elseForm, indent+2); print(")")
      }
    }

  }

  def pp(f: Formula) { pp(f, 0) }
}

object TFFPrinter extends Printer {

  val name = "tff"

  override val negSym = "~"
  override val orSym = "|"
  override val andSym = "&"
  override val impSym = "=>"
  override val convImpSym = "<="
  override val iffSym = "<=>"
  //val iffNotSym = "⇎"
  //val emptyClauseSym = "□"
  override val equalSym = "="
  override val equalOrderedSym = "="
  override val forallSym = "!"
  override val existSym = "?"
  override val falseSym = "$false"
  override val trueSym = "$true"
    
  override val sumFF = Operator.functional("$sum")
  override val uminusFF = Operator.functional("$uminus")
  override val greaterFF = Operator.functional("$greater")
  override val dividesFF = Operator.functional("$divides")
  override val lessFF = Operator.functional("$less")
  override val differenceFF = Operator.functional("$difference")
  override val productFF = Operator.functional("$product")
  override val nlppFF = Operator.functional("$product")
  override val quotientFF = Operator.functional("$quotient")
  override val lessEqFF = Operator.functional("$lesseq")
  override val greaterEqFF = Operator.functional("$greatereq")
  override val gtrFF = Operator.functional("$gtr")

 //Assume we never print predefined types like $i, $o, $tType etc.
  //Perhaps we should check for this?
  def sortToString(s: Sort): String = "tff("+s.name+", type, "+s.name+": $tType "+")."
  
  def opToString(op: Operator): String = {
    val name =
      if (util.printer.name == "tff" && op.name(0) == '#')
        "'" + op.name + "'" else op.name
    "tff("+name+", type, "+name+": "+ op.arity.toString+")."
  }
  def arityToString(a: Arity): String = 
    (if (a.nrArgs > 1) a.argsSorts.map(_.name).mkString("(", " * ", ")")+" > "
    else if (a.nrArgs==1) a.argsSorts.head.name+" > "
    else "")+a.resSort.name
  

  /** Doesn't print a constraint at all, if present */
  def clauseToString(cl: ConsClause) = {
    val clUn = cl.unabstrCautious
    if (clUn.isEmpty)
      falseSym
    else if (clUn.vars.isEmpty)
      clUn.lits.mkString("("," | ", ")")
    else
      "(!" + clUn.vars.map(varToStringWithSort(_)).mkString("[",", ","]: ") + (clUn.lits map { litToString(_) }).mkString("(", " | ",")")+")"
  }

/*
  def clauseToString(c: Clause): String =
    "tff("+
  // "c_"+"a"+c.age+"_w"+c.weight+
  c.id+
  ", plain, "+
		  (if (c.unabstrCautious.isEmpty) falseSym 
	      else if (c.unabstrCautious.vars.isEmpty)
	        c.unabstrCautious.lits.mkString("("," "+orSym+" ",")")
	      else
	        "("+forallSym+c.unabstrCautious.vars.map(x => x+": "+x.sort.name).mkString("[",", ","]: ")+
	        	c.unabstrCautious.lits.mkString("(", " "+orSym+" ",")")+")"
	        //c.unabstr.toFormula.toString)+
	      )+")."
 */
  
  def litToString(l: Lit): String =
  	l.eqn match {
      case PredEqn(equ) ⇒ (if (l.isPositive) "" else negSym) + eqnToString(l.eqn)
      case _ ⇒ (if (l.isPositive) l.eqn.toString else l.eqn.lhs.toString+"!="+l.eqn.rhs.toString)
    }
  
  def eqnToString(e: Eqn): String = {
    e match {
      case PredEqn(a) ⇒ a.toString
      case _ ⇒ e.lhs.toString + equalSym + e.rhs.toString
    }
  }
  
  //def atomToString(a: Atom): String
//  def toStringNoSort(v: Var) = v.name + (if (v.index > 0) "_" + v.index else "") + (if (v.isAbstVar) "a" else "")
  def varToStringWithSort(v: Var) = varToString(v) + (if (v.sort == Signature.ISort) "" else ":" + v.sort.name)


  def quantifierToString(formula: QuantForm): String = 
    "(" + formula.q + (formula.xs map { varToStringWithSort(_) }).mkString("[",", ","]")+ ": " + formula.f + ")"
    
  def letFormulaToString(letf: LetFormula): String = 
    "($let_vf(" + 
      (letf.bindings map { case (v,t) => v + "=" + t }).toMyString("[", ", ", "]") + ", " + letf.body + "))"

  def letTermToString(let: Let): String = 
    "($let_vt(" + 
      (let.bindings map { case (v,t) => v + "=" + t }).toMyString("[", ", ", "]") + ", " + let.body + "))"

  def varToString(v: Var): String = v.name.capitalize + (if (v.index > 0) "_" + v.index else "") + (if (v.isInstanceOf[AbstVar]) "a" else "")
  
  def domElemToString[T](d: DomElem[T]): String = {
    val s = d.value.toString
    if(s.charAt(0)=='-') "$uminus("+s.substring(1)+")"
    else s
  }
  
  val builtinSorts = List("$i","$o","$tType")
  // val builtinFGOps = List("$true","$false")
  val builtinBGOps = List("$sum","$uminus","$difference","$greater","$greatereq",
		  					"$less","$lesseq","$quotient","$product","$divides",
		  					"$is_int","$is_rat")
  
  def signatureToString(s: Signature): String = {
    //Operator precedence
    "%$ " + s.fgOperators.toList.filterNot(op => op.name(0) == '$')
    		                    .sortWith(s.gtrPrecSimple(_, _))
                                    .map(_.name)
                                    .distinct
    		                    .mkString(" > ")+"\n"+
    //ignore built in sorts: $i, $o, $tType
    "\n%Foreground sorts:\n"+
    s.fgSorts.filterNot(sort => builtinSorts.contains(sort.name)).mkString("\n")+"\n"+
    //ignore built in operators eg. $greater, $false
    "\n%Background operators:\n"+
    s.bgOperators.filterNot(op => builtinBGOps.contains(op.name)).mkString("\n")+"\n"+
    "\n%Foreground operators:\n"+
    // Don't want to see nlpp operators here, as they are printed using $product
    s.fgOperators.filterNot(op => op.name(0) == '$' || op.name == "#nlpp" ).mkString("\n")/*+
    "\n%Sorts with finite domains:\n"+
    (s.domain map { case ((s: Sort, elems: List[Term])) => ("%   " + s + ": " + elems.mkString(", ")) }).mkString("\n")*/
  }

  // The code below is a bit repetitive with the code for the default printer above,
  // but it is probably not worth factoring out the commonalities.
  def pp(f: Formula, indent: Int) {
    f match {
      case a @ Atom(_, _) => print(a)
      case UnOpForm(op, f) => {
        print(op)
        pp(f, indent+1)
      }
      case BinOpForm(op, f1, f2) =>
        op match {
          case And | Or => {
            val fs = f.toList(op)
            print("( ");
            for (fss <- fs.tails; if !fss.isEmpty) {
              pp(fss.head, indent+2)
              // More formulas?
              if (!fss.tail.isEmpty) {
                nlspaces(indent - (op.toString.length - 1))
                print(op.toString + " ")
              }
            }
            print(")")
          }
          case _ => {
            print("( "); pp(f1, indent+2)
            nlspaces(indent - (op.toString.length - 1))
            print(op.toString + " ")
            pp(f2, indent+2); print(")")
          }
        }
      case QuantForm(q, xs, f) => {
        print("( " + q + (xs map { varToStringWithSort(_) }).mkString("[",", ","]") + ": ")
        nlspaces(indent+2)
        pp(f, indent+2)
        print(")")
      }
      case LetFormula(bindings, body) => {
        print("($let_vf(" +
          (bindings map { case (v,t) => v + " = " + t }).toMyString("[", ", ", "]") + ", ")
        nlspaces(indent+2)
        pp(body, indent+2)
        print("))")
      }
    }
  }

  def pp(f: Formula) { 
    print("tff(formula, axiom, (")
    nlspaces(2)
    pp(f, 2) 
    print(")).")
  }

}
