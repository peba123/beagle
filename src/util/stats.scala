package beagle.util


object stats {
  
  case class SimpCtr(name: String) {
    private var tries = 0
    private var successes = 0
    
    def tried { tries+=1 }
    def succeed { successes+=1 }
    
    private val paddedName = name+"              ".drop(name.length)
    override def toString = paddedName+": "+successes
    // Printing of tries for now disabled. Not so informative?
    // override def toString = paddedName+": "+successes+"/"+tries
  }

  var nrInfRef = 0
  var nrInfPara = 0
  var nrInfChain = 0
  var nrInfFact = 0
  var nrInfDefine = 0
  var nrPartInst = 0
  // var nrInfDefineForDerivations = 0
  var nrInfSplit = 0
  var nrInfClose= 0

  var nrIntraInst = 0
  var nrInterInst = 0

  val simpDemod = SimpCtr("#Demod")
  val simpSubsume = SimpCtr("#Subsume")
  val simpTaut = SimpCtr("#Tautology")
  val simpNegUnit = SimpCtr("#SimpNegUnit")
  val simpOrphanDel = SimpCtr("#OrphanDel")
  // val theorySimp = SimpCtr("#BGTheorySimp")
  val simpBackRed = SimpCtr("#BackRed")

  //var nrSimpSubsume = 0
  //var nrSimpDemod = 0
  // var nrSimpTaut = 0
  // var nrSimpNegUnit = 0
  // var nrTheorySimp = 0
  var nrBGConsistencyChecks = 0
  var nrQECalls = 0
  var nrOrderedLits = 0
  // var nrOrphanDel = 0
  // var nrBackRed = 0
  var nrRestarts = 0 // Number of restarts after Define has been applied as a last resort, see main.scala
  var nrStrategies = 1 // Number of strategies tried. Only greater than 1 if auto mode.
  var nrAttemptedPara = 0
  var nrAvoidedPara = 0

  //cooper incremental
  var nrIncrSubsume = 0
  var nrIncrCalls = 0
  var nrSolutionsFailed = 0
  var nrSolutionsReused = 0
  var nrSolutionsExtended = 0
  
  var indTotalFound   = 0
  var indSupFalsePos  = 0
  var indSimpFalsePos = 0
  var indRetCalls     = 0
  
  var idxInsertions = 0
  var idxDeletions = 0
  var idxBoolReductions = 0
  var idxNegUnitSimp = 0
  var idxDemod = 0
  var idxUnitSubsume = 0
  var idxNonUnit = 0

  var timeSpentIndexing  = 0.0
  var timeSpentLookingUp = 0.0
  var timeSpentSuperpositioning = 0.0
  var timeSpentNegunits = 0.0
  var timeSpentDemodulating = 0.0
  var timeSpentSubsuming = 0.0
  
  def show() {
    
    println("Inference rules")
    println("----------------------")
    println("#Ref     : " + nrInfRef)
    println("#Sup     : " + nrInfPara)
    println("#Fact    : " + nrInfFact)
    println("#Define  : " + nrInfDefine)
    println("#Split   : " + nrInfSplit)
    println("#Chain   : " + nrInfChain)
    println("#Close   : " + nrInfClose)
    if (flags.useInst.value) {
    println("#Intra-inst: " + nrIntraInst)
    println("#Inter-inst: " + nrInterInst)
    }
    println()
    println("Ordering : " + flags.termOrdering.value)
    println()

    // println("Simplification rules (#succeeded/#tried)")
    println("Simplification rules")
    println("----------------------")
    println(simpSubsume)
    println(simpDemod)
    println(simpTaut)
    println(simpNegUnit)
    //println("#OrphanDel    : " + nrOrphanDel)
    // Not effective, can as well not do it
    // println(simpOrphanDel)
    // println(theorySimp)
    println(simpBackRed)
    println()
    if (flags.indexing.value) {
    println("Unit Indexing")
    println("---------------------------")
    println("#Index insertions       : " + idxInsertions)
    println("#Index deletions        : " + idxDeletions)
    println("#Index Bool. reductions : " + idxBoolReductions)
    println("#Index Neg. unit simp   : " + idxNegUnitSimp)
    println("#Index Demodulations    : " + idxDemod)
    println("#Index Unit subsumptions: " + idxUnitSubsume)
    println("#Indexed non-units      : " + idxNonUnit)
    println()
    }
    // println("Other")
    // println("---------------------------")
    println("#Partial instantiations: " + nrPartInst)
    println("#Strategies tried      : " + nrStrategies)
    // println("#Restarts with Define  : " + nrRestarts)
    // println("#QE                    : " + nrQECalls)
    // println("#BG consistency checks : " + nrBGConsistencyChecks)
    // println("#Attempted para        : " + nrAttemptedPara)
    // println("#Avoided para          : " + nrAvoidedPara)
    //println("#Ordered Literals : " + nrOrderedLits)
    println()
    if (flags.cooperCache.value) {
      println("Cooper Cached Solutions")
      println("---------------------------")
      println("#Calls to elimIncr  : " + nrIncrCalls)
      println("#Solutions extended : " + nrSolutionsExtended)
      println("#Solutions failed   : " + nrSolutionsFailed)
      println("#Solutions reused   : " + nrSolutionsReused)
      println()
    }
    if (warningCnt > 0)
      println("#Warnings         : " + warningCnt + "\n")
    Timer.stats()
  }
  
  
  
}
