package beagle.util

import beagle._

/**
 * Each Flag fits the following class, which makes it easy to write
 * code for parsing and printing usage information once and for all.
 */
abstract class Flag[T] {
  val option: String
  val default: T
  var value: T
  def setValue(v: String): Unit
  def toString1: String
  def toString2: String
  // def valueInt = value.toInt // use with caution
  // def valueBoolean = value != "off" // use with caution
}

// Currently there are three kinds of flags, for boolean, multiple choice and Int values

case class BoolFlag(option: String,
                    default: Boolean,
                    doc: String) extends Flag[Boolean] {
  var value = default
  def setValue(v: String) { value = v.toBoolean }

  def toString1 = option + " "
  def toString2 = doc // + ", default: " + default

}

case class ChoiceFlag(option: String,
                      values: List[String],
                      doc: String) extends Flag[String] {
  val default = values.head
  var value = default
  var valueInt = 0
  def setValue(v: String) {
    if (values contains v) {
      value = v
      valueInt = values.indexOf(v)
    } else
      throw new CmdlineError("Bad value for flag " + option + ": " + v)
  }
  def toString1 = option + " " + values.toMyString("[", "|", "]")
  def toString2 = doc + ", default: " + default
  def valueBoolean = value != "off" // use with caution
}

case class StringFlag(option: String, kind: String,
                      doc: String) extends Flag[String] {
  val default = ""
  var value = default
  def setValue(v: String) {
      value = v
  }
  def toString1 = option + " " + kind
  def toString2 = doc
}

case class IntFlag(option: String,
                   doc: String,
                   default: Int) extends Flag[Int] {
  var value = default
  def setValue(v: String) {
    value = v.toInt
  }

  def toString1 = option + " " + "Integer"
  def toString2 = doc + ", default: " + default
}

// Whether negative literals in clauses are selected.
// If yes, the selected negative literal is the sole literal in the clause
// that is subject to an inference, SupNeg or Ref (Ref does not apply to
// non-equational literals, but SupNeg does).
// If no, every literal in the clause is subject to a SupNeg or SupPos inference
// from an equational literal (SupPos inferences from non-equational literals
// would result in tautologies, and SupNeg inferences from non-equational literals
// are supplanted by CUIs using these (positive) context literals instead.)
// Instead of Ref the context literal X=X is used. 
// The benefit is that the CUIs are obtained by simultaneous mgus *only after
// all SupNeg inferences have been carried out*. Thus, permutations of 
// Sup-Neg and Ref inferences are avoided. With Selection, however, chances
// are better to derive positive clauses with isolated literals.

/**
 * Flags are simply indicator values which can be checked/updated at any time.
 * Some flags may depend on other flags being set, this makes it harder to add
 * effects to flags, e.g. setting bgsimp requires bgsolver to be set.
 */
object flags {

  val weightBound = IntFlag("-wb",
    "Weight bound: maximum weight of clauses considered for derivations. 0 means no limit",
    0)
    
  val weightAgeRatio = IntFlag("-war", 
      "Weight-Age ratio: a value of n means that n lightest or other (as per wgr) clauses are selected then one oldest clause. Use -1 to disable", 5)
  //  val calculus = ChoiceFlag("-calc", 
  // List("ME+Sup", "Sup", "ME"),
  // "The calculus to use.")

  val weightGoalRatio = IntFlag("-wgr", 
      "Weight-Goal ratio: a value of n means that n lightest or other (as per war) clauses are selected then one with a shortest derivation distance from the conjecture. Use -1 to disable", 9)

  val timeout = IntFlag("-t",
      "Timeout, in seconds. 0 means 7 days", 0)

  val useInst = BoolFlag("-inst", false,
   "Use clause instantiation when all else fails")

  // val delBGLemmas = BoolFlag("-delbgl", false,
  //   "Delete BG lemmas. All parameter-free BG clauses are considered as BG lemmas.")

  val help = BoolFlag("-help", false,
    "This text")

  val auto= BoolFlag("-auto", false,
     "Turn on automatic strategy selection. If on, beagle first tries to find a proof within the timelimit T/2,\n" +
     "  where T is the Timeout in use, cf the -t flag. If the result is inconclusive after reaching this timelimit or before,\n" +
     "  beagle spends the remaining time on finding a proof with the -genvars and -chaining flags on.\n" +
     "  This flag is effective only when -genvars is not given at the command line.")
    
  val relevance = IntFlag("-rel",
    "Relevance filtering: only use clauses whose FG operators are transitively contained in the conjecture\n" +
      "  FG operators, limiting the given number of steps for the transitive closure; -1 means no limit.\n" +
      "  If the input file contains no conjecture formula this flag is ignored",
    -1)

  val chaining = BoolFlag("-chaining", false,
    "Turn on the chaining inference rule for inequations")

  val simpneu = BoolFlag("-simpnew", false,
    "Turn on simplification of 'new' clauses by clauses in old and new clauses derived")

  val domElemWeightMultiplier = IntFlag("-dewm",
    "Domain Element Weight Multiplier: how heavy domain elements weigh in clauses for selecting lightest clauses. Applies only to 'small' dom elements whose string representation is less than 3",
    2)
    

  val bgsimp = (new IntFlag("-bgsimp", 
"""Level of aggressivity of BG simplification. Valid values are:
  0: no BG simplification at all. Only for test purposes
  1: "cautious": (meant to be) complete. Essentially, evaluation of ground BG terms, safe unabstraction
  2: "aggressive": artithmetic subterms are freely re-written, unsafe unabstraction of also FG terms
  3: "ridiculous": extends "aggressive", no unabstraction at all
""", 2) {
      import bgtheory._
      // set the simp behaviour as a side-effect
      override def setValue(v: String) {
        super.setValue(v)
	if (bgtheory.solver != null) {
          v match {
            case "0" => solver.setBGSimpLevel(OFF_BG_SIMP)
            case "1" => solver.setBGSimpLevel(CAUT_BG_SIMP)
            case "2" | "3" => solver.setBGSimpLevel(AGGR_BG_SIMP)
            case s => throw CmdlineError("bad value for flag bgsimp: " + s)
          }
	} //otherwise it will be set by init
      }
    })

  val nodemod = BoolFlag("-nodemod", false,
    "Turn off demodulation. Entails -nodefine. Useful only for debugging purposes")

  val nodefine = BoolFlag("-nodefine", false,
    "Disable the Define inference rule")

  val raw = BoolFlag("-raw", false,
    "Display clauses in abstracted form ('raw')")

  val cnfOnly = BoolFlag("-cnfonly", false,
    "Stop after CNF conversion")

  val nobred = BoolFlag("-nobred", false,
    "Turn off backward reductions")

  val noMuc = BoolFlag("-nomuc", false,
    "Do not search for a minimal unsatisfiable core for unsatisfiable sets of BG clauses")
  
  // Currently disabled
  // val stdlemmas = BoolFlag("-stdlemmas", false,
  //   "Use some standard background theory lemmas")
  
  val nopartinst = BoolFlag("-nopartinst", false,
    "Turn off partial instantiating superposition for finite sorts.")

/* removed for now, it seems the code for finite beagle is gone
  val finiteBeagle = BoolFlag("-fin", false,
    "Run the Finite Beagle algorithm. EXPERIMENTAL. Specify finite quantification 'X ∈ [m..n]' as $fin(X, m, n+1).\n"+
     "  This is lazily checked, but roughly all BSFG terms should have finite quantifiers. See also -lin")
 */

  val linStrategy = BoolFlag("-lin", false, "Linear Strategy for Finite Beagle: Focus on repairing just one variable at a time")

  val genvars = BoolFlag("-genvars", false,
    "BG sorted variables are taken as general variables, default is abstraction variables")

  // removed, not effective
  // val conjectureGenvars = BoolFlag("-cgenvars", false,
  //   "BG sorted variables in clauses stemming from the conjecture are taken as general variables, default is abstraction variables")

  val experimental = BoolFlag("-x", false,
    "Try out experimental changes")

  val iQE = BoolFlag("-iQE", false, "Try out the all new iQE solver on the input clauses, which must be BG. Just for debugging")

  val stdabst = BoolFlag("-stdabst", false,
    "Use standard abstraction instead of weak abstraction")

  val hsp = BoolFlag("-hsp", false, 
      "Use the hierarchic superposition calculus, i.e., -stdabst -bgsimp cautious -split 0" )

  val keep = BoolFlag("-keep", false,
      "Keep clauses over backtracking if possible" )
    
  val include = StringFlag("-include", "FILE", "Include FILE. Relative file names are resolved wrt $BEAGLEDIR" )

  val lemmas = StringFlag("-lemmas", "FILE", "Include FILE as a list of lemma clauses to be put into 'usable' straight away. If not set, standard lemmas are loaded from the directory $BEAGLEDIR/lemmas." )

  val liasolver = StringFlag("-liasolver", "SOLVER", "Use built-in SOLVER as the linear integer arithmetics background solver. One of 'cooper' or 'smt'. If given overrides -smtsolver flag")
  //  val noinst = BoolFlag("-noinst", false,
  //"Turn off instantiation of clauses with at least three literals for finite domains")

  val cooperCache = BoolFlag("-cooper-cache", false, 
     "EXPERIMENTAL optimization to cooper: store successful evaluations of parameters between runs. Also prints a solution to the parameters at the end in case of saturation.")

  val cooperSolveDiv = BoolFlag("-cooper-sd", false,
     "EXPERIMENTAL optimization to cooper: uses deferred instantiation and divides constraint solving to reduce instantiation.")


  val smtsolver = StringFlag("-smtsolver", "PATH", "Full path to an external SMT solver executable to be used for linear rational/real arithmetics. Implicitly sets liasolver to 'smt' unless overridden by -liasolver")

  val termOrdering = ChoiceFlag("-ord",
    List("auto", "LPO", "KBO"),
    "Ordering used to compare terms. Default is auto, which selects LPO for problems with arithmetics and KBO otherwise")

  val split = ChoiceFlag("-split",
    List("on", "off", "purebgc", "nopurebgc"),
    "Splitting: on: split whenever possible; off: never; purebgc: pure BG clauses only; nopurebgc: all but pure BG clauses")

  val listsFlag = BoolFlag("-lists", false,
    "Predefine the lists data type; only relevant for SMT-LIB input files")

  val arraysFlag = BoolFlag("-arrays", false,
    "Predefine the array sort; only relevant for SMT-LIB input files")

  val longNames = BoolFlag("-longnames", false,
    "Display identifiers with their full names, i.e., including the sort information that is part of their names; usually only relevant for SMT-LIB input files")

  val negSelection = BoolFlag("-negsel", false,
    "Selection function for clause literals. true: a negative literal is selected if there is one; false: some maximal literal is selected")

  val paramsOpSet = ChoiceFlag("-params",
    List("BG", "FG"),
    "Whether parameters are background or foreground operators")

  val quietFlag = BoolFlag("-q", false, "Quiet")

  val testFlag = BoolFlag("-test", false, "Ignore. Internal only for testing new code")

  // val nonccnfFlag = BoolFlag("-nonccnf", false, "Allow constraints in non-CNF form")

  // val backwardSimp = BoolFlag("-bs", false, "Whether to do backwards simplification (includes backward subsumption)")

  val formatFlag = ChoiceFlag("-format",
    List("auto", "smt", "tff-ari", "tff-int", "tff-rat", "tff-real", "tff","cnf"),
    """Format of input formulas. 'auto' selects 'smt' or 'tff-ari', depending on the extension of the input file name:
  '.smt' or 'smt2' selects 'smt' (SMT-LIB format v2), otherwise 'tff-ari' (TPTP TFF format). 
  'tff-ari'recognises any of 'tff-int', 'tff-rat' and 'tff-real'""")

  val cnfConversion = ChoiceFlag("-cnf",
    List("standard", "optimized"),
    "which kind of CNF conversion to use")

  val debug = ChoiceFlag("-d", List("off", "inf", "bg"), """Debug: "inf" - prints current clause sets and inferences carried out; "bg": only useful for developers""")

  val version = BoolFlag("-version", false, "Show version number and exit")

  val proof = BoolFlag("-proof", false, "Write a refutation to STDOUT following a successful derivation" ) //Proof output

  val proofFile = StringFlag("-proof-file", "", "Write a TFF formatted refutation to the specified file, including typing information")

  val nonlpp = BoolFlag("-nonlpp", false, "Do not add axioms for nonlinear multiplication" ) //Proof output
  	
  val tptpHome = StringFlag("-tptp", "", "Absolute path to TPTP home to resolve relative include statements in TPTP files")
  
  val inferSorts = BoolFlag("-infer", false, "Do sort inference on operators which have $i sort. Useful for CNF or FOF input")

  val modelEvolution = BoolFlag("-me", false,
   "Use the Model Evolution calculus. Experimental and limited in functionality. In particular no equality.")

  val negAssert = BoolFlag("-negassert", false,
   "In conjunction with -me, assert negative literals as well.")


  val printer = ChoiceFlag("-print", List("default", "tff"),
     "How to display clauses. In TFF mode clauses are output in TPTP-TFF format, but program output " +
       "information remains")

  val indexing = BoolFlag("-idx", false, 
    "Use discrimination tree indexing for unit clauses in old")

  val negConjecture = BoolFlag("-negconj", false, "Do not negate TFF conjecture clauses. Useful to prove counter-satisfiability.")

  /** List of flags which are actually included.*/
  val flags =
    // useInst, experimental currently not offered
    List(help, version, timeout, auto, split, relevance, chaining, bgsimp, simpneu, nodemod, nonlpp, keep, iQE, nodefine, nobred, genvars, // conjectureGenvars,
         stdabst, hsp, include, lemmas, nopartinst, termOrdering, negSelection, paramsOpSet, // universalClausalSplit,
         weightAgeRatio,
         weightGoalRatio,
     domElemWeightMultiplier,
         weightBound, cnfOnly, formatFlag, liasolver, smtsolver, cnfConversion, quietFlag, debug, proof, listsFlag, arraysFlag, longNames, testFlag,
      raw, printer, tptpHome, inferSorts, noMuc,
      // finiteBeagle,
      linStrategy, modelEvolution, negAssert, useInst, indexing, experimental, negConjecture,
         cooperCache, cooperSolveDiv)

  def usage() {
    println("flags:")
    for (flag ← flags) {
      println("  " + flag.toString1)
      println("  " + flag.toString2)
      println()
    }
  }

  /** @return the index past parsing the flags in args */
  def parseflags(args: Array[String]) = {
    var i = 0
    var done = false
    while (i < args.length && !done) {
      (flags find { _.option == args(i) }) match {
        case None ⇒
          if (args(i)(0) == '-') throw new CmdlineError("Unknown flag: " + args(i))
          else done = true
        case Some(flag @ BoolFlag(_, _, _)) ⇒ { flag.setValue("true"); i += 1 }
        case Some(flag)                     ⇒ { i += 1; flag.setValue(args(i)); i += 1 }
      }
    }
    i
  }
}
