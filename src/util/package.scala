
package beagle

import fol._
import datastructures._

package object util {

  // Used for handling conjectureFGOps in TPTPParser
  implicit class MyOptionSet[T](o: Option[Set[T]]) {
    def optUnion(p: Option[Set[T]]) = 
        (o, p) match {
          case (None, None) => None
          case (Some(oOps), None) => Some(oOps)
          case (None, Some(pOps)) => Some(pOps)
          case (Some(oOps), Some(pOps)) => Some(oOps union pOps)
        }
  }

  implicit class MySet[T](s: Set[T]) {
    /** set intersection emptiness test - not directly in the scala API it seems */
    def intersects(t: Set[T]) = {
      val (small, big) = if (s.size < t.size) (s, t) else (t, s)
      // loop only over smaller set, assuming that containmaint test is cheap
      small exists { big.contains(_) }
      }
  }

  // implicit class MyLitList(lits: List[Lit]) {
  //   def gtr(other: List[Lit]) = Lit.mso_gtr(lits, other)
  // }

  implicit class MyListOfSets[T](s: Iterable[Set[T]]) {

    // Compute all hitting sets of s
    def hittingSets: Set[Set[T]] = {
      s match {
        case Nil => Set(Set.empty)
        case first :: rest => {
          var res = Set.empty[Set[T]]
          rest.hittingSets foreach { restResElem =>
            first foreach { res += restResElem + _
            }
          }
          res
        }
      }
    }
  }


  implicit class MyList[T](l: List[T]) {

    def removeNth(n: Int) = {
      //equal to l.take(n-1)++l.drop(n+1)
      //@annotation.tailrec
      def hRemoveNth(l: List[T], n: Int): List[T] = {
        // could do a better non-tail recursive version
        if (n == 0)
          l.tail
        else
          l.head :: hRemoveNth(l.tail, n - 1)
      }
      hRemoveNth(l, n)
    }

    def replaceNth(n: Int, x: T) = 
      l.updated(n,x)
      /*{
      //equal to l.updated(n,x)
      def hReplaceNth(l: List[T], n: Int, x: T): List[T] = {
        // could do a better non-tail recursive version
        if (n == 0)
          x :: l.tail
        else
          l.head :: hReplaceNth(l.tail, n - 1, x)
      }
      hReplaceNth(l, n, x)
    }*/

    /** Removes the first element that satisfies the predicate p.
     * Returns the removed element and the list with the element removed, if such an element exists
      */
    def extract(p: T => Boolean): Option[(T, List[T])] = {
      var rest = l
      var soFar = List[T]()
      while (!rest.isEmpty) {
        if (p(rest.head))
          return Some((rest.head, soFar.reverse ::: rest.tail))
        soFar ::= rest.head
        rest = rest.tail
      }
      // Coming here means nothing hs been found
      return None
    }

    // use distinct instead
    // def toListSet = {
    //   var res = List.empty[T]
    //   for (x ← l)
    //     if (!(res contains x)) res ::= x
    //   res.reverse
    // }

    // Simplistic, but what can you do?
    def subsetOf(other: List[T]) = l forall { other contains _ }

    def without(other: List[T]) =
      l filterNot { el => other contains el } 

    def count(lit: T) = l count { _ == lit }

    // Multiset ordering greater or equal
    def mso_geq(gtr: (T, T) => Boolean) = (k: List[T]) =>
    k forall { y ⇒
        !(l.count(y) < k.count(y)) ||
          (l exists { x ⇒ gtr(x, y) && l.count(x) > k.count(x) })
      }

    def mso_equ(k: List[T]) =
      (l ::: k).toSet forall { x => l.count(x) == k.count(x) }

    def mso_gtr(gtr: (T, T) => Boolean) = (k: List[T]) =>
    !(l mso_equ k) && (l.mso_geq(gtr)(k))

    def toMyString(toStringFn: T ⇒ String, ifEmpty: String, lsep: String, isep: String, rsep: String): String =
      if (l.isEmpty)
        ifEmpty
      else
        lsep + l.map(toStringFn(_)).reduceLeft(_ + isep + _) + rsep

    // Convenience Functions
    def toMyString(ifEmpty: String, lsep: String, isep: String, rsep: String): String =
      toMyString(x ⇒ x.toString, ifEmpty, lsep, isep, rsep)

    def toMyString(lsep: String, isep: String, rsep: String): String =
      toMyString(lsep + rsep, lsep, isep, rsep)

  }

  // implicit def toMyList[T](s: List[T]) = new MyList(s)

  var cachingSolver = new bgtheory.LIA.cooper.CachingSolver(None)
  var printer: Printer = DefaultPrinter
  var reporter: Reporter = DefaultReporter
  
  def verbose = !flags.quietFlag.value
  def debug = flags.debug.value == "inf"
  def debugBG = flags.debug.value == "bg"
  
  /** 
   *  Hidden flag which can be set during parsing to indicate a clause set with
   *  no equality. In this case we should not perform positive superposition as it
   *  is always trivial.
   */
  var noEquality = false;

  var warningCnt = 0
  def warning(s: String) {
    warningCnt += 1
    reporter.log("Warning: " + s)
  }

  class Counter {
    var ctr = 0;
    def next() = {
      ctr += 1;
      ctr
    }
  }

  abstract class Message extends Exception {
    val s: String
  }

  case class GeneralError(s: String) extends Message

  case class SyntaxError(s: String) extends Message

  case class InternalError(s: String) extends Message

  case class CmdlineError(s: String) extends Message

  case class NotImplemented(method: String) extends Exception(method+" is not implemented")

  /**
   * Thrown when a computation produces a value larger than MaxInt
   * Ideally we could just return a Long or BigInt instead but everywhere else
   * we have used Int so would need to cast or use some clever implicits to
   * avoid changing everything.
   */
  case object IntOverflowException extends Exception

  // def gcd(a: Int, b: Int) = gcd1(a max b, a min b)
  // def gcd1(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  
  /*
   * When broken into two methods you can get a stack overflow exception
   * for large numbers or frequent calls.
   * Putting it in a single method and using the final keyword allows
   * the scala compiler to recognise the recursion and optimise.
   */
  final def gcdInner(x: BigInt, y: BigInt): BigInt = {
    val a = x.abs //Math.abs(x)
    val b = y.abs //Math.abs(y)
    val c = a max b
    val d = a min b
    
    if (d == 0) c
    else gcdInner(d, c % d)
  }

  def gcd(x: Int, y: Int): Int = gcdInner(x, y).toInt //doesnt need to check for overflow

  /**
   * The extended GCD algorithm also returns Bézout coefficients
   * That is, egcd(a, b) returns a triple (g, x, y)
   * such that ax + by = g = gcd(a, b)
   */
  final def egcdUnchecked(a: BigInt, b: BigInt): (BigInt, BigInt, BigInt) = {
    if (a == 0)
      (b, 0, 1)
    else {
      val (g, y, x) = egcdUnchecked(b % a, a)
      (g, x - (b / a) * y, y)
    }
  }

  def egcd(a: Int, b: Int): (Int, Int, Int) = {
    // assume(a >= 0)
    // assume(b >= 0)
    val (g, x1, y1) = egcdUnchecked(a.abs, b.abs)
    val x = if (a < 0) -x1 else x1
    val y = if (b < 0) -y1 else y1
    assert(a*x + b*y == g, s"EGCD: expected $g == $a*$x + $b*$y")
    (g.toInt, x.toInt, y.toInt)
  }


  /**
   * @return least positive common multiple of a and b.
   * @throws IntOverflow if LCM of a and b is larger than Int.MaxValue.
   */
  def lcm(a: BigInt, b: BigInt) = {
    if (a==0 && b==0) 0
    else {
      val res = a.abs * (b.abs / gcdInner(a, b))
      if (!res.isValidInt) throw IntOverflowException
      res.toInt
    }
  }

  /**
   * LCM for lists which checks for integer overflow.
   * Use this instead of list.reduce(lcm(_,_))
   * @throws IntOverflowException if integer overflow is detected.
   */
  def lcm(l: List[Int]): Int = {
    def h(ls: List[BigInt], acc: BigInt): BigInt = {
      if (ls.isEmpty) acc
      else {
	val a = ls.head
        if (a == 0) return 0
	//in the final product the gcd of a & x is contributed by a
	val nextls = ls.tail.map(x => x/gcdInner(a, x))
	//apply the same to the tail and store in acc
	h(nextls, acc*a)
      }
    }

    val res = h(l.map(x => BigInt(x.abs)).sortWith(_ > _), 1)
    if (!res.isValidInt) throw IntOverflowException
    res.toInt
  }

}
