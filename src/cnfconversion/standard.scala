package beagle.cnfconversion

import beagle._
import fol._
import rules._

object standard {

  /**
   * Convert a formula to CNF, i.e. a list of list of Literals
   * @param full if true then pure BG formulas are also subject to CNF conversion,
   * which may introduce Skolem functions.
   * It may often be better to have full false, so that these formulas are treated as atoms by the
   * CNF conversion and treated by QE later
   */
  def toCNF(f: Formula, full: Boolean): (List[List[Formula]], Set[Operator]) = {

    //accumulates skolem operators
    var newOps = Set.empty[Operator]

    //println("cnf convert "+f)
    //both full and non-full share these steps initially:
    //must eliminate all of <=>, =>, -- and push negations down to atoms
    //println("mark 1")
    val h1 = f.
	reduceOutermost(elimIffNot).
	reduceOutermost(elimIff).
	reduceOutermost(elimImpl).
        reduceInnerOnePass(elimTrivial)
    // By removing true/false a FG (sub-)formula may turn into a BG formula, hence preventing skolemization where it should not happen

    // h1 is a conjunction of formulas, even if a singleton.
    // Convert each element separately into CNF, which can be much more efficient.
    // In particular we save unneccessary pulling out quanifiers. A good example is SWV998=1

    //println("mark 2")
    // Do skolemisation of outer existential if possible
    // useful in case the formula has an existential quantifier over a conjunction of
    // universally quantified inner formulas which generate skolem functions
    // in this case the h.toList(And) optimisation is blocked and we get massive skolem functions
    // eg. PUZ001+2.p yields a 17 place skolem function
    val h2 = 
      h1 match {
          case g @ Exists(xs, f) if (full || !g.isBG) => {
	    // Peter 10/6/2014 - do *not* Skolemize if this is a BG formula (in the non-full case)
            // Rationale: cooper will treat it
	    val sigma = 
	      xs.foldLeft(Subst.empty)( (acc, x) => { 
		val (binding, op) = skolemization.mkSkoSubst(Nil, x)
		newOps += op
		Sigma += op
		acc + binding
	      } )
            sigma(f)
          }
          case x => x
        }

    //println("mark 3")
    //note that the effect of full is to prevent recursing into BG subformulas in
    //fastCNF and fastSkolem
    val res = 
      h2.toListAnd flatMap { g => {
        // Bug! this is not enough:
	// val g1 = fastSkolem(g.reduceOutermost(pushNegThruQuant), !full)
        // Example:
        // (assert
        //   (exists ((l Int))
        // 	  (not (or
        // 		(<= 0 l)
        // 		(let ((m (div2 l)))
        // 		  true
        // 		  )))))
        // This is unsatisfiable but if only pushNegThruQuant
        // is done then the exists-quantifier coming from the let is eliminated
        // incorrectly, because the outer "not" is ignored
	val g1 = fastSkolem(g.reduceOutermost(pushdownNeg), !full)
	  // Only universal quantifiers in front now - remove
	  // The matrix now is made from Neg, And, and Or,
	(if (full)
	  g1.reduceOutermost(pushdownNeg).
	    //included in pushdownNeg
	    reduceInnerOnePass(elimTrivial)
	 else
	   g1.reduceOutermostFG(pushdownNeg).
	     reduceInnerOnePass(elimTrivial)
	 ).toListAnd
      }}

    //println("mark 4")
    //if full, don't do fgOnly...
    (fastCNF(res.map(_.toListOr), !full), newOps)
  }

  /** Quickly convert a QF NNF formula (and-list of or-lists) to CNF.
   * Effectively implements the rules pushDownAndOr and flattenAndOr
   *
   * In the FG only case treat BG formulas as atomic, all FG literals must
   * still be multiplied as usual.
   *
   * @param fgOnly treat BG subformulas as "literals" i.e. do not multiply
   * them out
   * 
   * @todo built-in check for trivial formulas
   * @todo support the FG only case- do not expand BG formulas,
   * possibly quantified subformulas too.
   */
  @annotation.tailrec
  def fastCNF(cls: List[List[Formula]], fgOnly: Boolean = false, 
	      done: Vector[List[Formula]] = Vector.empty): List[List[Formula]] = {

    //TODO- we only need the first open clause so change this to a while loop that stops
    //when we have an open clause.
    //println(s"** $cls XXX $done")

    val (clsDone, clsOpen) = 
      if(fgOnly)
	//treat pure BG formulas as literals
	cls partition { _.forall(l => l.isLiteral || l.isBG) }
      else
	cls partition { _.forall(_.isLiteral) }

    //println(s"-- open = $clsOpen")

    if(clsOpen.isEmpty) return (done ++ clsDone).toList

    //take the first subformula of a disjunction that is not a literal
    val cl = clsOpen.head
    val (lits, binops) = 
      if(fgOnly)
	cl partition { l => l.isLiteral || l.isBG }
      else
        cl partition { _.isLiteral }

    //it must be a conjunction
    val conj: List[Formula] = binops.head.toListAnd
    //distribute its conjuncts over the remaining subformulas
    val prod = conj map { c =>  (c :: binops.tail ::: lits) flatMap { _.toListOr } } 

    fastCNF(prod ::: clsOpen.tail, fgOnly, done ++ clsDone)
    //TODO- the insertions for ::: are expensive- is there a better way?
  }

  /** Assumes that negs are pushed through quantifiers.
   * @param qList is the open universal quantifiers on this branch.
   * @return the matrix of a formula equisatisfiable with f in which all universal quantifiers
   * are removed and all existential quantifiers are replaced with skolem functions.
   * @todo add a flag to ignore BG subformulas to replace pulloutQuants for the non-full case.
   */
  def fastSkolem(f: Formula, fgOnly: Boolean = false, qList: Set[Var] = Set()): Formula = {
    
    f match {
      case _ if (f.isQuantifierFree || (fgOnly && f.isBG)) => f
      case Forall(xs, Forall(ys, g)) => 
	fastSkolem(Forall(xs ::: ys, g), fgOnly, qList)
      case Forall(xs, g) => {
	val rho = Term.mkRenaming(xs)
	val xs2 = rho(xs).asInstanceOf[List[Var]].toSet
	(for (fi <- rho(g).toListAnd) yield {
	  val relevant = xs2 intersect (fi.vars)
	  fastSkolem(fi, fgOnly, qList ++ relevant)
	}) reduceLeft (And(_,_))
      }
      case Exists(xs, Exists(ys, g)) => 
	fastSkolem(Exists(xs ::: ys, g), fgOnly, qList)
      //distribute Exists over Or
      case Exists(xs, g @ Or(_,_)) => {
        (for (fi <- g.toListOr) yield {
          val exVars = xs restrictTo fi.vars
          if (exVars.isEmpty)
            fastSkolem(fi, fgOnly, qList)
          else 
            fastSkolem(Exists(exVars, fi), fgOnly, qList)
        }) reduceLeft (Or(_,_))
      }
      // Try to distribute over And-- skolemise only if it helps
      /*case Exists(xs, g @ And(_,_)) => {
        val subFs = g.toListAnd
        val shared = xs filter { x => subFs.forall(_.vars(x)) }
        val relVars = (qList intersect g.vars).toList
        
        //build the skolemising substitution
	val rho = shared.foldLeft(Subst.empty) { (acc, y) =>
	  val (s, skoOp) = skolemization.mkSkoSubst(relVars, y)
	  Sigma += skoOp
	  acc + s
	}

        (for {
           fi <- subFs
           newFi = rho(fi) //note that all shared vars are replaced by rho
           newExistVars = xs restrictTo newFi.vars
         } yield {
           if (newExistVars.isEmpty)
             fastSkolem(newFi, fgOnly, qList)
           else
             fastSkolem(Exists(xs restrictTo newFi.vars, newFi), fgOnly, qList)
        }) reduce(And(_,_))
      }*/
      case Exists(xs, g) => {
	//skolemize wrt open universals
	//replace xs with new skolem functions of the open universals
	var subst = Subst.empty
	for(y <- xs) {
	  val (s, skoOp) = skolemization.mkSkoSubst(qList.toList, y)
	  Sigma += skoOp
	  subst += s
	}
	fastSkolem(subst(g), fgOnly, qList)
      }
      case UnOpForm(op, g) => UnOpForm(op, fastSkolem(g, fgOnly, qList))
      case BinOpForm(op, g1, g2) => 
	BinOpForm(op, fastSkolem(g1, fgOnly, qList), fastSkolem(g2, fgOnly, qList))
    }
  }

}
