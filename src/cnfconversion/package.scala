package beagle

package object cnfconversion {

  import beagle._
  import util._
  import fol._
  import datastructures._
  import bgtheory._
  import calculus._

  import cnfconversion.rules._

  /** Used to transform constraints into DNF */
  def toDNF(f: Formula): List[List[Formula]] =
    f match {
      //skip some simple cases to which reduction rules never apply
      //usually get these by passing an empty set of BG lits, hence might not
      //be needed for toCNF
      case Neg(TrueAtom) => List(List(FalseAtom))
      case Neg(FalseAtom) => List(List(TrueAtom))
      case Neg(Atom(_, _)) => List(List(f))
      case Atom(_, _) => List(List(f))
      //the usual case
      case _ =>
        standard.fastSkolem(
	  f.reduceOutermost(elimIffNot).
            reduceOutermost(elimIff).
            reduceOutermost(elimImpl)).
          // Only universal quantifiers in front now - remove
          // The matrix now is made from Neg, And, and Or,
          // convert to CNF
          reduceOutermost(pushdownNeg).
          //reduceInnermost(elimNegNeg).
          reduceInnermost(pushdownAnd).
          reduceInnermost(flattenAndOr).
          reduceInnermost(elimTrivial).
          toListOr map { _.toListAnd }
    }

  def toCNF(f: Formula, full: Boolean): List[List[Formula]] = {
    Timer.cnfConversion.start()
    // println("xxx " + f)
    val (res, newSig) = 
      if (flags.cnfConversion.value == "optimized")
	optimized.toCNF(f, full)
      else
	standard.toCNF(f, full)
    newSig foreach { Sigma += _ }
    Timer.cnfConversion.stop()
    // println("xxx res " + res)
    res
  }

  /**
   * Eliminate all occurrences of greater/greatereq in terms of less/lesseq.
   * Works for both integers and rationals.
   */

  // Not used anymore. Use simplification rules instead.
/*
  def normalizeIneq(l: Formula): Formula = l match {
      // case Neg(Less(t1, t2)) ⇒ LessEq(t2, t1)
      // case Neg(LessEq(t1, t2)) ⇒ Less(t2, t1)
      case Neg(Greater(t1, t2)) ⇒ LessEq(t1, t2)
      case Neg(GreaterEq(t1, t2)) ⇒ Less(t1, t2)
      case Greater(t1, t2) ⇒ Less(t2, t1)
      case GreaterEq(t1, t2) ⇒ LessEq(t2, t1)
      case _ ⇒ l
  }
 */
  /**
   * Convert a "clause", as a list of literals into clauses. Because of QE more than one clause may result.
   * Also use the PreprocessingSolver to simplify arithmetic literals, possibly over multiple arithmetic sorts.
   * The resulting clauses are abstracted.
   * @param canLIAQE Indicates whether LIA QE can be applied to the BG-part of the literals.
   */

  /*
  def literalListToClauses(literalList: List[Formula], canLIAQE: Boolean) = {
    // println("toClauses: " + literalList)
    val (fgs, bgs) = purification.purifyFormulas(literalList)
    // println("fgs = " + fgs + " bgs = " + bgs)
    // Negate the bgs for eliminating the (now) existentially quantified extra variables
    val bgsNeg =
      if (!canLIAQE) {
        // Don't bother to do QE
        // If !canLIAQE we assume that literalList is obtained by full clause
        // normalform transformation, i.e., all explicit quantifiers have been eliminated
        Neg(bgs.toOr)
      } else {
        val extraVars = bgs.bgVars -- fgs.bgVars
          LIA.LIASolverClauses.QE(Exists(extraVars.toList, Neg(bgs.toOr)))
      }
    // println("bgsNeg = " + bgsNeg)
    // Negate the result again and join with the fgs 
    toDNF(bgsNeg) map { 
      conj => Clause(fgs ::: (conj map { _.toLit.compl }), Set.empty, 0).unabstrAggressive.simplifyBG.unabstrAggressive 
    }
  }
   */

  // literalListToClauses now no longer uses purification. Purification may destroy sufficient completeness!

  def literalListToClauses(literalList: List[Formula], canLIAQE: Boolean, source: Formula) = {
    // println("toClauses: " + literalList)
    val (fgs, bgs) = literalList partition { _.isFG }
    // println("fgs = " + fgs + " bgs = " + bgs)
    // Eliminate extra variables in bg sub-clause
    // inference is the chain of inferences documenting the making of the final clauses
    val (bgsAfterQE, inference) =
      if (!canLIAQE || bgs.isEmpty) {
        // Don't bother to do QE
        (bgs.toOr, ByCNF(source))
      } else {
        val extraVars = bgs.bgVars -- fgs.bgVars
        // The sub-clause to eliminate variables from
        val from = Forall(extraVars.toList, bgs.toOr)
        val to = LIA.cooper.Solver.QE(from, forced = false)
        if (from == to)
          (bgs.toOr, ByCNF(source))
        else 
        (to,
          ByCNF((to :: fgs).toOr.closure(Forall) withInference
            ByEquivalence(
              literalList.toOr.closure(Forall) withInference(ByCNF(source)),
              // This is the LIA theorem implicitly used
              Iff(from, to).closure(Forall) withInference ByBGTheorem("LIA"))))
      }
    // join the new bg part with th fg literals
    val fgsLits = fgs map { _.toLit }
    val deltaConjecture =
      if (source.role == Some("negated_conjecture") // && (fgsLits exists { !_.isPositive })
      ) 0 else 1000
    toCNF(bgsAfterQE, full = true) map { disj =>
      new ConsClause(fgsLits ::: (disj map { _.toLit }),
        Nil,
        // ClsInfo(Set.empty, 0, ByCNF(source, lemmas), deltaConjecture))
        ClsInfo(Set.empty, 0, inference, deltaConjecture))
        .unabstrAggressive
        .simplifyBG
        .unabstrAggressive
    }
  }



  /**
   * Convert each formula to clausal form, which possibly extends the signature
   */
  def formulasToClauses(fs: Seq[Formula], forceGenvars: Boolean, haveLIA: Boolean): List[ConsClause] = {
    // Lemma annotated clauses have ordinary variables, that's the whole difference
    (for (
      hf ← fs;
      f = hf.elimNonLinearMult.iteExpanded.letExpanded;
      // canLIAQE = false; 
      // canLIAQE = LIA.isLIA(f); //f.isLIA;
      literalList ← toCNF(f, full = !haveLIA);
      cl ← literalListToClauses(literalList, canLIAQE = haveLIA, hf)
    ) yield {
      if (forceGenvars || hf.annotation == Some(LemmaTerm))
        cl.freshGenVars()
      else
        cl
        }).toList
      .reverse // To preserve the order in which clauses are listed
  }

}

