package beagle.cnfconversion

import beagle._
import fol._
import Signature._
import util._
import rules._

/*
 * Naive Skolemization
 */

object skolemization {

  /** Assume all quantifiers to be eliminated have been pulled out.
   * @param full if false then do not skolemize background formulas. 
   * Rationale is that QE takes care of quantifiers for BG formulas.
   * @return the skolemised formula and the signautre updated to include
   * the new skolem operators.
   */
  @annotation.tailrec
  def skolemize(g: Formula, sig: Signature): (Formula, Signature) = {
    g match {
      // Simple cases to collapse quantifiers
      case Forall(xs, Forall(ys, f)) ⇒ skolemize(Forall(xs ::: ys, f), sig)
      case Exists(xs, Exists(ys, f)) ⇒ skolemize(Exists(xs ::: ys, f), sig)

      case Forall(xs, Exists(ys, f)) ⇒ {
	//collect skolem substitutions in sigma, and new skolem ops in newSig
	val (sigma, newSig) =
          ys.foldLeft((Subst.empty, sig))({ case((subst, sig2), y) => {
	    val (s, op) = mkSkoSubst(xs, y)
	    (subst + s, sig2 + op)
	  }})

	skolemize(Forall(xs, sigma(f)), newSig)
      }
      // Case of one single row of exists-quantifier, without forall in front.
      case Exists(ys, f) ⇒ {
	//collect skolem substitutions in sigma, and new skolem ops in newSig
	val (sigma, newSig) = 
          ys.foldLeft((Subst.empty, sig))({ case((subst, sig2), y) => {
	    val (s,op) = mkSkoSubst(List.empty, y)
	    (subst + s, sig2 + op)
	  }})
        skolemize(sigma(f), newSig)
      }
      // skolemize(Forall(List.empty, Exists(ys, f)))
      case _ ⇒ (g, sig) // only universal quantifiers, if any 
    }
  }

  /** Make a Skolemizing substitution for the variable `y` in context
   * of leading universally quantified variables `xs`.
   * Depends on flags.paramsOpSet.
   * @return The existentially quatified variable which is replaced (i.e. `y`),
   * the skolem term to replace it and the new skolem operator introduced.
   */
  def mkSkoSubst(xs: List[Var], y: Var): ((Var, FunTerm), Operator) = {
    val skoFun = "#skF_" + skfCtr.next() // #skf
    val skoTermArity = Arity((xs.sortsOf), y.sort)
    // Figure out if we can make a background constant
    val skoTermKind =
      if ((y.sort.kind == BG) && xs.isEmpty && flags.paramsOpSet.value == "BG")
        BG
      else
        FG
    //val skoOp = Operator(skoTermKind, skoFun, skoTermArity)
    val skoOp = new SkolemOp(skoTermKind, skoFun, skoTermArity)
    //Sigma += skoOp

    ((y -> NonDomElemFunTerm(skoOp, xs)), skoOp) 
    // Technically, this is not a Subst but never mind
  }

  private val skfCtr = new Counter

}
