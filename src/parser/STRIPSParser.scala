package parser


import scala.util.matching.Regex
import scala.util.parsing.combinator._
import java.io.FileReader

import beagle._
import fol._
import util._
import Signature._
import bgtheory.LIA.cooper._

/**
 * Parses STRIPS format files. Does not currently do any transformation/encoding of problem.
 * So initial state is just a conjunction of predicates and possible actions are universally
 * closed implications. Everything has $i sort.
 * STRIPS files have .pddl suffix and usually require both a problem file and a domain file 
 * (defines predicates and actions); multiple problem files can have the same domain.
 */
class STRIPSParser(domains: Map[String,List[Operator]]) extends JavaTokenParsers with PackratParsers {
  
  //COMMENT:= ;;.* (note comments can appear in formulas!)
  protected override val whiteSpace = """(\s|;;.*)+""".r
  
  //used for retrieving operators used in action/init/goal formulas
  var preds = List[Operator]()
  
  var objectsList = List[Const]()
  
  val worldIndex = AbstVar("i",bgtheory.LIA.IntSort)
  val iSucc = bgtheory.LIA.Sum(worldIndex,bgtheory.LIA.DomElemInt(1))
  
  lazy val define = dom_define | prob_define
  
  lazy val dom_define = ("(" ~> "define" ~> "(" ~> "domain" ~> name <~ ")") ~ 
  							("("~>":predicates"~>rep1(pred_def)<~")") ~ 
  							( rep1(action) <~ ")") ^^ { 
  								case name ~ pred ~ action => (name,pred,action) 
  							}
  							
  lazy val prob_define = ("(" ~> "define" ~> "(" ~> "problem" ~> name <~ ")") ~ 
  							("("~>":domain"~> name <~ ")" ^^ { case dom =>
  							  if(domains.isDefinedAt(dom)) {preds=domains(dom); dom}
  							  else throw new Exception("Domain "+dom+" has not been loaded")
  							}) ~ 
  							objects ~
  							init ~
  							("("~>":goal"~> formula <~ ")") <~ ")" ^^ { 
  								case name ~ dom ~ objects ~ init ~ goal => (name, dom, objects.toSet, init, goal)
    						}
  
  lazy val name = regex("[a-z0-9-]*".r)
  
  /** Updated to add an integer world parameter */
  lazy val pred_def: PackratParser[Operator] = "("~>name~rep(param)<~")" ^^ { 
    case name~params => {
      val newOp = new Operator(FG,name,Arity(params.map(_=>Signature.ISort):::List(bgtheory.LIA.IntSort),Signature.OSort))
      preds::=newOp
      newOp
    }
  }
  
  lazy val param = "?"~>regex("[a-z0-9]+".r)
  
  /** Now returns action_atom, pre_formula, effect_formula */
  lazy val action: PackratParser[Action] = 
    ("(" ~> ":action" ~> name) ~ 
		(":parameters" ~> "(" ~> rep(param_as_var) <~ ")") ~
		( action_pre.? ) ~ 
		( action_eff ) <~ ")" ^^ { 
			case name ~ params ~ pre ~ eff => {
		  		preds::=new Operator(FG,name,Arity(params.map(_=>Signature.ISort):::List(bgtheory.LIA.IntSort),Signature.OSort))
		  		Action(Atom(name,params:::List(worldIndex)),pre,eff)
			 }
		}
  
  lazy val action_pre: PackratParser[Formula] = ":precondition" ~> formula
  
  lazy val action_eff: PackratParser[Formula] = ":effect" ~> formula ^^ { Subst(worldIndex -> iSucc)(_) }
  
  lazy val objects = "("~>":objects"~>rep1("[a-z0-9]+".r)<~")" ^^ { case obj => {
	  val res = obj.map(FGConst(_,Signature.ISort))
	  objectsList = res
	  res
  	}
  }
  
  lazy val init = "("~>":init"~>rep(gnd_atom_checked)<~")" ^^ { _.reduceLeft(And(_,_)) }
  
  //formulas
  lazy val formula: PackratParser[Formula] = atomic_formula | not_formula | and_formula | or_formula
    
  lazy val not_formula: PackratParser[Formula] = "(" ~> "not" ~> formula <~ ")" ^^ { formula => Neg(formula) }
  
  lazy val and_formula: PackratParser[Formula] = "("~> "and" ~> rep1(formula) <~ ")" ^^ { case fList => fList.reduceLeft(And(_,_)) }
  
  lazy val or_formula: PackratParser[Formula] = "("~> "or" ~> rep1(formula) <~ ")" ^^ { case fList => fList.reduceLeft(Or(_,_)) }
  
  lazy val atomic_formula = "("~> name ~ repsep(param_as_var," ") <~ ")" ^^ {
    case n ~ args => preds.find(_.name==n) match {
    	case Some(pred) => Atom(n,args:::List(AbstVar("i",bgtheory.LIA.IntSort)))
    	case None => throw new SyntaxError(n+" not found in domain definitions")
      }
  }
  
  //retrieve an atom equal to the matched term
  lazy val gnd_atom_checked: PackratParser[Formula] = "(" ~> name ~ rep(regex("[a-z0-9]+".r)) <~ ")" ^^ { 
    case pred ~ args => {
	  (preds.find(_.name==pred),
	      args.map(c => objectsList find {case Const(a) => a.name==c })) match {
	    case (Some(op),argsOpt) if(argsOpt.forall(_!=None)) => Atom(op.name,argsOpt.map(_.get))
	    case (None, _) => throw new SyntaxError("Undefined predicate "+pred)
	    case (_, _) => throw new SyntaxError("Undefined constant")
	  }
  	} 
  }
  
  lazy val param_as_var: PackratParser[Var] = "?"~>regex("[a-z0-9]+".r) ^^ { x => GenVar(x) }
}

object STRIPSParser {
  def parseSTRIPSFile(domainFiles: List[String], probFile: String) {
    //TODO- how should we look up domain files?
    
    //This stores all actions indexed by domain
    var actionMap = Map[String,List[Action]]()
    
    //Parse all the domains given
    val domains = domainFiles.map(fName => {
    			      val parser = new STRIPSParser(Map())
				      val reader = new FileReader(fName)
				      val (name,ops,actions) = parser.parseAll(parser.dom_define,reader).get
				      actionMap+=(name->actions)
				      (name,ops)
				    }).toMap
    
	//Parse the problem file
    val domParser = new STRIPSParser(domains)
    val reader = new FileReader(probFile)
    val (probName, domain, objects, init, goal) = domParser.parseAll(domParser.prob_define, reader).get
    
    //now get this to a signature and clause set...
    
    //formulas: actions init goal
    //operators: preds constants -everything has sort $i for now
    //precedence?
    /*(List(init,goal):::actionMap(domain).values.toList,
     Signature,new Signature(Set[Sort](), myTPTPParser.signature.operators, myTPTPParser.signature.precs ::: precs)),
     false,
     Set[])*/
  }
  
  /**
   * Take a list of predicates and add an extra parameter to model the world that the
   * predicate holds for.
   * Some predicates can do without this?
   */
  def addWorldToPreds(preds: List[Operator]): List[Operator] = {
    for(p <- preds;
    	if(p.arity.resSort == Signature.OSort)) yield {
      new Operator(p.kind,p.name,Arity(p.arity.argsSorts:::List(bgtheory.LIA.IntSort),p.arity.resSort))
    }
  }
  
  /**
   * Build frame axioms for an action, i.e. if an action predicate holds in some
   * state then each of the predicates it does not affect hold in the next state.
   */
  def frameAxioms(action: Action, preds: List[Operator]) {
    for(p <- preds;
        if(!action.eff.operators(p))) yield { 
      val pAtom = Atom(p.name,p.arity.argsSorts.map(GenVar("X",_).freshVar))
      val pAtomSucc = pAtom.args.last match {
        case i: Var => Subst(i -> bgtheory.LIA.Sum(i,bgtheory.LIA.DomElemInt(1)))(pAtom)
      }
      Implies(And(pAtom,action.predAtom),pAtom).closure(Forall)
    }
  }
  
  /**
   * Actions cannot occur simultaneously and an action occurs at every time.
   */
  def actionsAtomic(actions: List[Action]) {
    //no combination of distinct actions happens at the same time
    val i = AbstVar("I",bgtheory.LIA.IntSort)
    for(List(a1,a2) <- actions.combinations(2)) yield {
      Neg(And(a1.freshAtomWIndex(i),a2.freshAtomWIndex(i))).closure(Forall)
    }
    
    //no two distinct versions of the same action happen at the same time
    for(a <- actions) {
      //all but i distinct implies not a1 a2
      val a1 = a.freshAtomWIndex(i)
      val a2 = a.freshAtomWIndex(i)
      a1.vars.zip(a2.vars)
    }
    
    //at every time an action happens
    //forall i & some ...s either a1 a2 a3 ...
    
  }
  
  
}

case class Action(predAtom: Atom, pre: Option[Formula], eff: Formula) {
  //val worldVar = predAtom match { case Atom(_,args: List[Var]) => args.last }
  
  def freshAtomWIndex(i: AbstVar): Formula = {
    val res = predAtom.fresh()
    res match {
      case Atom(_,args:List[Var]) => Subst(args.last -> i)(res)
    }
  }
  
  /**
   * Build action formulas from tuple.
   * Also include that an action implies its preconditions
   */
  val asFormula: Formula = 
    pre match {
      case Some(pre) =>
        And(Implies(And(pre,predAtom),eff),Implies(predAtom,pre)).closure(Forall)
      case None => 
        Implies(predAtom,eff).closure(Forall)
    }
}
