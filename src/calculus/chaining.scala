package beagle.calculus

import beagle._
import datastructures._
import fol._
import util._
import PMI._
import bgtheory._

object chaining {

  /* Note the explicit combination of constraints and application of subst in chain() */

  def chain(from: ConsClause, fromPos: Int, fromLit: Lit, into: ConsClause, intoPos: Int, intoLit: Lit) = {
    var res = List.empty[ConsClause]

    def addToRes(l: Lit, sigma: Subst) {
      util.stats.nrInfChain += 1
      /*res = Clause(sigma(l :: from.lits.removeNth(fromPos) ::: into.lits.removeNth(intoPos)),
        into.idxRelevant ++ from.idxRelevant,
        math.max(into.age, from.age) + 1, ByChaining(from, into), math.min(from.deltaConjecture, into.deltaConjecture)+1).abstr :: res*/
      val newInfo = 
        ClsInfo(into.idxRelevant ++ from.idxRelevant,
          math.max(into.age, from.age) + 1, 
          ByChaining(from, into), 
          math.min(from.info.deltaConjecture, into.info.deltaConjecture) + 1)

      res ::= new ConsClause(l :: from.lits.removeNth(fromPos) ::: into.lits.removeNth(intoPos), 
        from.constraint ++ into.constraint, newInfo).applySubst(sigma).abstr
    }

    // assume fromLit is positive
    val Lit(true, LIA.LessEqn(s, t)) = fromLit
    intoLit match {
      case Lit(false, LIA.LessEqn(u, v)) => {
        (s mgu u) match {
          case None => ()
          case Some(sigma) => addToRes(Lit(false, LIA.LessEqn(t, v)), sigma)
        }
        (t mgu v) match {
          case None => ()
          case Some(sigma) => addToRes(Lit(false, LIA.LessEqn(u, s)), sigma)
        }
      }
      case Lit(true, LIA.LessEqn(u, v)) => {
        (t mgu u) match {
          case None => ()
          case Some(sigma) => addToRes(Lit(true, LIA.LessEqn(s, v)), sigma)
        }
        (s mgu v) match {
          case None => ()
          case Some(sigma) => addToRes(Lit(true, LIA.LessEqn(u, t)), sigma)
        }
      }
    }
    res
  }

  def chainingIntoClauses(from: ConsClause, into: ListClauseSet) = {
    // Assume clauses are variable disjoint
    var res = List.empty[ConsClause]

    for (
      // eligible literals are never pure BG, hence no need to test that
      fromPos ← from.iEligible intersect from.iPosLits;
      fromLit = from(fromPos))
      fromLit match {
        case Lit(true, LIA.LessEqn(_, _)) =>
          for (intoCl <- into.clauses;
            intoPos ← intoCl.iEligible; // todo: use index
            intoLit = intoCl(intoPos))
            // todo: extend for reals and rationals
            intoLit match {
              case Lit(_, LIA.LessEqn(_, _)) =>
                res :::= chain(from, fromPos, fromLit, intoCl, intoPos, intoLit)
              case _ => ()
            }
        case _ =>
      }
    res
  }
}
