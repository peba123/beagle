package beagle.calculus

import beagle._
import datastructures.ConsClause
import fol.Formula
import util._

/** Inferences are carried by Clauses to describe their origin in a derivation. */
abstract class Inference {
  /** The name of the used inference rule */
  val name: String 

  /** The name of the involved BG theory */
  val bgTheory: String = "empty"

  /** getString: return a string representation of this inference; 
    * cl is meant to be the conlusion of the inference.
    * Notice there is no explicit conclusion field. Instead the conclusion is given implicitly, 
    * by the clause this Inference instance is attached to (in its info field), similarly
    * for formulas. It would be difficult to set such an explicit conclusion field because
    * both the clause and its info field are created at the same time.
    */
  def getString(concl: ClauseOrFormula, bgTheoryMentioned: String): String
  def getTFFString(concl: ClauseOrFormula, bgTheoryMentioned: String): String
}

abstract class ZeroPremisesInference extends Inference {

  def getString(concl: ClauseOrFormula, bgTheoryMentioned: String) =
    "%5s: %s\n       by %s%s".format(concl.idString, concl.toStringNoConst, bgTheoryMentioned, name)
  def getTFFString(concl: ClauseOrFormula, bgTheoryMentioned: String): String = 
    "tff(%s, plain, %s, inference(%s, [status(thm)%s], [])).".format(concl.idString, concl.toStringNoConst, name, 
      bgTheoryMentioned)
}

abstract class OnePremiseInference extends Inference {
  val premise: ClauseOrFormula

  def getString(concl: ClauseOrFormula, bgTheoryMentioned: String) =
    "%5s: %s\n       by %s%s(%s)".format(concl.idString, concl.toStringNoConst, bgTheoryMentioned, name, premise.idString)
  def getTFFString(concl: ClauseOrFormula, bgTheoryMentioned: String): String = 
    "tff(%s, plain, %s, inference(%s, [status(thm)%s], [%s])).".format(concl.idString, concl.toStringNoConst, name, 
      bgTheoryMentioned, premise.idString)
}

abstract class TwoPremisesInference extends Inference {
  val leftPremise: ClauseOrFormula
  val rightPremise: ClauseOrFormula

  def getString(concl: ClauseOrFormula, bgTheoryMentioned: String) =
    "%5s: %s\n       by %s%s(%s, %s)".format(concl.idString, concl.toStringNoConst, bgTheoryMentioned, name, 
      leftPremise.idString, rightPremise.idString)
  def getTFFString(concl: ClauseOrFormula, bgTheoryMentioned: String): String = 
    "tff(%s, plain, %s, inference(%s, [status(thm)%s], [%s, %s])).".format(concl.idString, concl.toStringNoConst, name, 
      bgTheoryMentioned, leftPremise.idString, rightPremise.idString)
}

abstract class MultiPremisesInference extends Inference {
  val premises: List[ClauseOrFormula]

  def getString(concl: ClauseOrFormula, bgTheoryMentioned: String) =
    "%5s: %s\n       by %s%s%s".format(concl.idString, concl.toStringNoConst, bgTheoryMentioned, name, 
      if (premises.nonEmpty) (premises map { _.idString }).mkString("(", ", ", ")") else "")

  def getTFFString(concl: ClauseOrFormula, bgTheoryMentioned: String): String = 
    "tff(%s, plain, %s, inference(%s, [status(thm)%s], %s)).".format(concl.idString, concl.toStringNoConst, name, bgTheoryMentioned,
                (premises map { _.idString }).mkString("[", ", ", "]"))
}

/* Inferences with special print functions */

case object ByInput extends Inference { 
  val name = "input"
  def getString(concl: ClauseOrFormula, bgTheoryMentioned: String): String =
    "%5s: %s\n       by input".format(concl.idString, concl.toStringNoConst)

  def getTFFString(concl: ClauseOrFormula, bgTheoryMentioned: String): String =
    concl match {
      case f: Formula =>
        "tff(%s, %s, %s, file('%s', %s)).".format(
          f.idString,
          f.role.getOrElse("axiom"),
          f, f.source.getOrElse("unknown"), f.name.getOrElse("unknown"))
      case cl: ConsClause => "tff(%s, axiom, %s).".format(cl.idString, cl.toStringNoConst)
    }
}

case class ByBGTheorem(override val bgTheory: String) extends ZeroPremisesInference { val name = "theorem" }
case class ByCNF(premise: ClauseOrFormula) extends OnePremiseInference { val name = "cnfTransformation" }
case class ByRef(premise: ClauseOrFormula) extends OnePremiseInference { val name = "reflexivity"; override val bgTheory = "equality" }
case class ByFact(premise: ClauseOrFormula) extends OnePremiseInference { val name = "factorization"; override val bgTheory = "equality" }
case class BySup(leftPremise: ClauseOrFormula, rightPremise: ClauseOrFormula) extends TwoPremisesInference { val name = "superposition"; override val bgTheory = "equality" }
case class ByRes(leftPremise: ClauseOrFormula, rightPremise: ClauseOrFormula) extends TwoPremisesInference { val name = "resolution" }
case class ByChaining(leftPremise: ClauseOrFormula, rightPremise: ClauseOrFormula) extends TwoPremisesInference { val name = "chaining"; override val bgTheory = "equality" }
case class ByDefine(premise: ClauseOrFormula) extends OnePremiseInference { val name = "define"; override val bgTheory = "equality" }
case class BySplitLeft(premise: ClauseOrFormula) extends OnePremiseInference { val name = "splitLeft" }
case class BySplitRight(leftEmptyClause: ClauseOrFormula, premise: ClauseOrFormula) extends OnePremiseInference { val name = "splitRight" }
case class ByClose(premises: List[ClauseOrFormula], override val bgTheory: String) extends MultiPremisesInference { val name = "close" }
case class ByDemod(premises: List[ClauseOrFormula]) extends MultiPremisesInference { val name = "demodulation"; override val bgTheory = "equality" }
case class ByNegUnitSimp(premises: List[ClauseOrFormula]) extends MultiPremisesInference { val name = "negUnitSimplification" }
case class ByBGSimp(premise: ClauseOrFormula, override val bgTheory: String) extends OnePremiseInference { val name = "backgroundSimplification" }
case class BySimple(premise: ClauseOrFormula) extends OnePremiseInference { val name = "simple" }
case class ByQE(premise: ClauseOrFormula, override val bgTheory: String) extends OnePremiseInference { val name = "quantifierElimination" }
case class ByEquivalence(leftPremise: ClauseOrFormula, rightPremise: ClauseOrFormula) extends TwoPremisesInference { val name = "equivalence" }


trait ClauseOrFormula {
  val id: Int
  def inference: Inference // The inference that lead to this
  // In proof we do not want to see ordering constraints
  def toStringNoConst =
    this match {
      case f: Formula => f.toString
      case cl: ConsClause => cl.modified(constraint = List.empty).toString
    }
  def idString =
    this match {
      case f: Formula => "f_" + id
      case cl: ConsClause => "c_" + id
    }
}

object Proof {

  /** Generate a proof, i.e. a sequence of inferences from input formulas and derived clauses
    *  The given clause cl contains in its inference part a tree-shaped refutation
    */
  def mkRefutation(concl: ClauseOrFormula): List[ClauseOrFormula] = {

    import scala.collection.mutable.ListBuffer
    var linRef = ListBuffer.empty[ClauseOrFormula]

    /** Linearize the refutation behind the given clause */
    def linearRefutation(concl: ClauseOrFormula) {
      // println(s"%s linearRefutation($concl)".format(concl.idString))
      if (!(linRef exists (_.idString == concl.idString))) {
        // Loop check. Must work with idString, not id, because a clause and a formula could have the same id!
        concl.inference match {
          case ByInput => linRef += concl
          // case _: ByCNF => linRef += concl
          case inf: BySplitRight => {
            linearRefutation(inf.leftEmptyClause)
            linRef += concl
          }
          case _: ZeroPremisesInference => {
            linRef += concl
          }
          case inf: OnePremiseInference => {
            linearRefutation(inf.premise)
            linRef += concl
          }
          case inf: TwoPremisesInference => {
            linearRefutation(inf.leftPremise)
            linearRefutation(inf.rightPremise)
            linRef += concl
          }
          case inf: MultiPremisesInference => {
            (inf.premises foreach { linearRefutation(_) })
            linRef += concl
          }
        }
      }
    }

    linearRefutation(concl)

    // Put "ByInput" first.
    val (byInput, others) = linRef.toList partition { _.inference == ByInput }

    val all = byInput ::: others

    // Remove duplicates
    var noDupls = List.empty[ClauseOrFormula]
    for (h <- all) {
      if (!(noDupls exists { _.idString == h.idString }))
        noDupls ::= h
    }
    noDupls.reverse // preserve order
  }

  def refToTFFString(ref: List[ClauseOrFormula]): String =
    ref map { concl =>
      val bgTheoryMentioned =
        concl.inference.bgTheory match {
          case "empty" => ""
          case str => s", theory('$str')"
        }
      concl.inference.getTFFString(concl, bgTheoryMentioned)
    } mkString("\n")

  def showRefutationTFF(ref: List[ClauseOrFormula]) =
    println(refToTFFString(ref))

  def showRefutationDefault(ref: List[ClauseOrFormula]) {
    for (concl <- ref) {
      val bgTheoryMentioned =
        concl.inference.bgTheory match {
          case "empty" | "equality" => ""
          case s => s + "-"
        }
      println(concl.inference.getString(concl, bgTheoryMentioned))
    }
  }

}

