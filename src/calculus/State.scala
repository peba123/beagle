package beagle.calculus

import beagle._
import datastructures._
import util._
import derivationrules._
import simplification._
import fol._
import collection.mutable.ArrayBuffer
import bgtheory._
import LIA.cooper.CachingSolver
import LIA.cooper.cooperIncr.CooperSoln

//case object FAIL extends Exception
case object TIMEOUT extends Exception

/**
 * @param given, the given clauses, expected to be already cheaply simplified
 * @param sos, old clauses to start with (typically lemmas)
 */
class State(given: List[ConsClause], sos: List[ConsClause]) {

  case class CLOSE(empty: ConsClause) extends Exception
  case object CONTINUESELECTED extends Exception

  /** The unprocessed clauses, those awaiting inference */
  var neu = new PQClauseSet 
  var old = new ListClauseSet 
  // var posUnits = new ListClauseSet 
  // var negUnits = new ListClauseSet
  
  // The clause sets manipulated:
  // var neu = new PQClauseSet(None) // The unprocessed clauses, those awaiting inference

  // var old = new ListClauseSet(None) // The processed clauses, those that are closed under inferences

  var decisionLevels = ArrayBuffer.empty[decisionLevelInfo]

  Term.resetDefineMap()
  
  old.add(sos) // could do interreduction

  /** The current values for the above clause sets, used for backtracking, as well as
    * the caching solver in use at that state. */
  class StateInfo(val neu: PQClauseSet, val old: ListClauseSet, val oldSoln: Option[CooperSoln])
  // class StateInfo(val neu: ListClauseSet, val old: ListClauseSet)

  /**
   * Provides the information needed to backtrack to a decision level.
   * @param rightClausesFn A function that takes relevant indices to make a right conclusion
   * for the split inference that created that decision level.
   * @param stateInfo Holds the clause sets as they were before the left split.
   */
  case class decisionLevelInfo(rightClausesFn: ((Set[Int], ConsClause) ⇒ List[ConsClause]), stateInfo: StateInfo)

  /**
   * Add a clause to neu.
   * @param throwCLOSEOnEmpty Throw CLOSE exception when detecting an empty clause in
   * the clause set after simplification.
   * @throws CLOSE if throwCLOSEOnEmpty is set and an empty clause is detected after
   * simplification.
   */
  def addToNeu(cl: ConsClause, throwCLOSEOnEmpty: Boolean = true, simplify: Boolean = true) {
    
    val cl1 = 
      if (cl.isUnitClause && cl.constraint.nonEmpty) {
        // println(s"Warning: unit clause with non-empty constraint, removing constraint: $cl")
        cl.modified(constraint = List.empty)
      } else cl

    val clSimp = 
      if (simplify) {
        // Do full reduction before adding the clause
        cl1.reduceWithStatus(old) match {
          case (clRed, true) => clRed  // reduction applied, includes cheap simplification
          case (clRed, false) => clRed flatMap { _.simplifyCheap }
        }
      } else List(cl1)

    if (throwCLOSEOnEmpty)
      (clSimp find { _.isEmpty }) match {
        case None     ⇒ ()
        case Some(cl) ⇒ {
          throw CLOSE(cl.modified(constraint = List.empty))
          // Could also reject the empty clause if ordering constraint is unsat, but why?
        }
      }

    neu.add(clSimp)
  }

  /**
   * Applying the single premise derivation rules Equality Resolution and
   * Equality Factoring on a new clause.
   * @param cl a clause which is not pure background.
   */
  def singlePremiseInferences(cl: ConsClause): Iterable[ConsClause] = {
    assume(!cl.isPureBG)
    Timer.inferencing.start()

    var infResults = List.empty[ConsClause]

    // Simplification done later
    for (newCl ← infRef(cl)) {
      reporter.onInference(cl, newCl, "Ref")
      infResults ::= newCl
    }

    for (newCl ← infFact(cl)) {
      reporter.onInference(cl, newCl, "Fact")
      infResults ::= newCl
    }

    if (util.flags.useInst.value) {
      for (newCl <- instantiation.intraclauseSym(cl)) {
        reporter.onInference(cl, newCl, "Inst")
        infResults ::= newCl
      }
    }

    Timer.inferencing.stop()
    infResults
  }

  def clausalInferences(cs: ListClauseSet, cl: ConsClause) : Iterable[ConsClause] = {
    assume(!cl.isPureBG)
    Timer.inferencing.start()
    val clFresh = cl.fresh
    val res = infSupIntoClauses(clFresh, cs) ++ infSupFromClauses(cs, clFresh) ++ infChaining(clFresh, cs) ++ 
      (if (util.flags.useInst.value) instantiation.interclauseSym(cl, cs) else Nil)
    Timer.inferencing.stop()
    res
  }

  /**
   * Main procedure.
   * Modified to return the empty clause used in closing instead of FAIL.
   * @param timeout Time allowed just for this call.
   * @throws TIMEOUT if `timeout` is exceeded.
   * @return Left(emptyClause) or Right(this) respective representing the empty
   * clause used to close or the final state if saturation is achieved.
   */
  def derive(timeout: Double): Either[ConsClause, State] = {

    Timer.mainLoop.start()

    // val war = math.max(flags.weightAgeRatio.value, 1)
    // val wgr = math.max(flags.weightGoalRatio.value, 1)
    val war = flags.weightAgeRatio.value
    val wgr = flags.weightGoalRatio.value
    val upto = List(war+1, wgr+1, 1).max
    var warOverWgr = true

    reporter.onProofStart(given)
    
    given foreach { neu.addAndInterreduce(_) }
    
    /** How many lightest clauses have been selected in a row already */
    var lightestCnt = 0

    while (!neu.isEmpty) try {
      // println("time main loop: " + Timer.mainLoop.elapsedSecs)
      // println("time left: " + timeleft)
      // timeleft = timeleft - Timer.mainLoop.elapsedSecs
      //thrown from here
      if (Timer.mainLoop.sinceStarted() > timeout) throw TIMEOUT

      //if (debug) show()
      reporter.onNewState(old, neu)
      val sel =
        if (war == 0 && lightestCnt == 0 && wgr == 0) {
          warOverWgr = !warOverWgr
          if (warOverWgr) neu.removeOldest() else neu.removeGoaliest()
        } else if (war == 0)
          neu.removeOldest()
        else if (wgr == 0)
          neu.removeGoaliest()
        else if (war > 0 && wgr > 0 && lightestCnt > 0 &&
            lightestCnt % war == 0 && lightestCnt % wgr == 0) {
          warOverWgr = !warOverWgr
          if (warOverWgr) neu.removeOldest() else neu.removeGoaliest()
        } else if (war > 0 && lightestCnt > 0 && lightestCnt % war == 0)
          neu.removeOldest()
        else if (wgr > 0 && lightestCnt > 0 && lightestCnt % wgr == 0) 
          neu.removeGoaliest()
        else 
          neu.removeLightest()

      lightestCnt += 1
      if (lightestCnt == upto) lightestCnt = 0

      // Reduce does not do cheap simplification, precisely if sel has not been touched.
      // But then sel has been cheaply simplified before it was put into neu.
      // In other words, invariant: clauses in neu are cheaply simplified, hence so is sel,
      // and reduce preserves cheap simplification
      // var selReduced = sel.reduce(old) // reduce returns zero or more clauses
      // Do a subsumption test:
      var selReduced = sel.reduce(old) filter { selCl =>
                  !(old.clauses exists { cl => (cl subsumes selCl).nonEmpty })
                  // !(clauses exists { cl => (cl.lits == intoRedCl.lits) })
                }
      // reduce returns zero or more clauses
      if (selReduced.isEmpty) { 
        reporter.onClauseDeleted(sel)
      }
      // go through the clauses in selReduced and perform inferences
      while (!selReduced.isEmpty) try {
        var selRed = selReduced.head
        selReduced = selReduced.tail

        // Save call to ~ if not neccessary because we do not report the result anyway.
        if (reporter.isDebugReporter && !(selRed ~ sel)) reporter.onSimplified(sel, selRed)
        if (selRed.isEmpty) throw CLOSE(selRed)

        if (selRed.lits contains Lit.TrueLit)
          throw CONTINUESELECTED

        reporter.selectedClause(selRed, decisionLevels.length)

        // Try Define
        for ((selRedNew, defClauses) <- infDefine(selRed)) {
          reporter.onDefine(selRed, defClauses, selRedNew)
          defClauses foreach { addToNeu(_) }
          addToNeu(selRedNew) // infDefine could be applicable to selRedNew again,
                              // hence it goes into neu
          throw CONTINUESELECTED
        }

        // Try Split
        for ((leftClauses, rightClausesFn) <- infSplit(selRed, decisionLevels.length)) {
          // println("xx split %s into %s and %s".format(selRed, leftClauses, rightClausesFn))
          reporter.onSplit(selRed, leftClauses, decisionLevels.length)
        
          // Make the decision point
          decisionLevels += decisionLevelInfo(rightClausesFn,
            new StateInfo(neu = neu.klone(), old = old.klone(), oldSoln = util.cachingSolver.getPrevSoln))
        
          leftClauses foreach { addToNeu(_) }
          throw CONTINUESELECTED
        }

        // We are adding selRed to old and close under inferences.
        // Also invoke the BG solver if appropriate

        if (flags.nobred.value)
          old.add(selRed)
        else {
          val newFromOld = old.addAndBackwardReduce(selRed)
          newFromOld foreach { cl =>
            addToNeu(cl)
            reporter.onClauseAdded(cl)
          }
        }
        reporter.onClauseAdded(selRed)

        if (selRed.isBG) {
          // In case the solver needs unit clauses need to add the solver clauses that are not unit clauses (or not empty)
          // to neu, so that these can be split on in the next round.
          // asSolverClauses provides us readily with these sets:
          val (inOld, newfromBGClauses) = selRed.asSolverClauses.get
          // newfromBGClauses is already simplified cheap

          if (!inOld.isEmpty) {
            // Might have the empty clause now, asSolverCauses might generate it
            // by simplification
            (inOld find { _.isEmpty }) match {
              case None => ()
              case Some(cl) => {
                throw {
                  CLOSE(cl)
                }
              }
            }

            // Have some new clauses the solver understands, hence check close
            val bgClauses = old.clauses filter { _.isBG } flatMap { _.asSolverClauses.get._1  }

	    stats.nrBGConsistencyChecks += 1;
            // println("XXXXX " + solver.name)
            (if (flags.cooperCache.value) util.cachingSolver.check(bgClauses)
            else solver.check(bgClauses)) match {
              case UNSAT(core) => {
	        // find the relevant BG clauses
	        val relBGClauses =
	          if (util.flags.noMuc.value || util.flags.split.value == "off")
                    // all clauses are relevant
                    bgClauses
                  else {
                    // Get the ids of the core clauses:
                    val idxCore = core match {
                      case None =>
                        //heuristic for running MUC, use the naive approach
		        solver.minUnsatCore(bgClauses)
                      case Some(coreIdx) => coreIdx
                    }
                    bgClauses filter { idxCore contains _.id }
                  }

                // The indices of the relevant BG clauses
                val idxRelBGClauses = relBGClauses.foldLeft(Set.empty[Int]) { (acc, cl) => acc ++ cl.idxRelevant }
                reporter.onBGInconsistent(selRed, old)
	        stats.nrInfClose += 1;

                throw CLOSE(
                  new ConsClause(Nil, Nil,
                    ClsInfo(idxRelBGClauses, 0, ByClose(relBGClauses.toList, solver.theoryName), (relBGClauses map { _.info.deltaConjecture }).min+1))
                )
              }
              case SAT | UNKNOWN => ()
            }
          }

          newfromBGClauses foreach { cl =>
            addToNeu(cl, simplify = false)
            reporter.onClauseAdded(cl)
          }
        }

        if (!selRed.isPureBG) {
          singlePremiseInferences(selRed) foreach { addToNeu(_) }
          clausalInferences(old, selRed) foreach { addToNeu(_) }
        }

/*      if (flags.simpneu.value && selRed.isUnitClause && selRed(0).isFlat) {
//        neu.reduce(new ImmutableListClauseSet(List(selRed)))
        val oldFlat = new ImmutableListClauseSet(old.clauses filter { cl => cl.isUnitClause && cl(0).isFlat })
        neu.reduce(oldFlat)
      }
 */

        // def useForSimpNeu(cl: ConsClause) = cl.isUnitClause && cl(0).isFlat && !cl(0).isNLPP
        def useForSimpNeu(cl: ConsClause) = cl.isUnitClause && cl.isGround

        if (flags.simpneu.value && useForSimpNeu(selRed)) {
          // Reduction of neu. However neu is potentially big, and so reduce is too expensive,
          // hence reduce only in a cheap way.
          // val oldFlat = new ImmutableListClauseSet(old.clauses filter { useForSimpNeu(_) })
          // neu.reduce(oldFlat, selRed)
          neu.reduce(selRed)
        }
        // if (flags.simpneu.value && selRed.isUnitClause && selRed(0).isFlat) {
        //   neu.reduce(old, cheap = true)
        // }
      } catch {
        case CONTINUESELECTED ⇒ ()
      }
      // No more selected clauses, continue outer loop by selectedthe next neu clause
    } catch {
      case CLOSE(emptyCl) ⇒ {
	val idxRelevant = emptyCl.idxRelevant
        // logvClause(Clause(List.empty, idxRelevant, 0))
        reporter.onClose(decisionLevels.length, emptyCl)
	if (idxRelevant.isEmpty) {
          Timer.mainLoop.stop()
	  //throw FAIL
	  return Left(emptyCl)
        } else {
          val backtrackLevel = idxRelevant.max
          val blInfo = decisionLevels(backtrackLevel)
          decisionLevels.reduceToSize(backtrackLevel) // shorten stack
          val preservedClauses = 
            if (flags.keep.value) {
              (old.clauses
               // ++ neu.clauses
              ) filter { cl =>
              (cl.idxRelevant.isEmpty // || cl.idxRelevant.max < backtrackLevel
              ) &&
                !(blInfo.stateInfo.neu.clauses exists { _ == cl }) &&
                !(blInfo.stateInfo.old.clauses exists { _ == cl })
              }
            } else List()
          old = blInfo.stateInfo.old
          neu = blInfo.stateInfo.neu
          neu.add(preservedClauses)
          //restore the caching solver with the solution as used at the decision point
          if (flags.cooperCache.value)
            util.cachingSolver = new CachingSolver(blInfo.stateInfo.oldSoln)
          //new
	  //solverBGClauses = (old.clauses filter { cl ⇒ cl.isBG && solver.canClose(cl) }).toList

          //if (verbose || debug) log("=== Backtrack to level " + backtrackLevel + " ===")
          
          val rightClauses = blInfo.rightClausesFn(idxRelevant - backtrackLevel, emptyCl) 
          //logd("Right clauses are:")
          // Do the right split
          rightClauses foreach { cl ⇒
            //logd(cl)
            addToNeu(cl, throwCLOSEOnEmpty = false)
          }
          reporter.onBacktrack(backtrackLevel, rightClauses, old, neu)
          
          //logd("\nState is now:")
          //if (debug) show()
        }
      }
    }
    println()
    Timer.mainLoop.stop()
    Right(this)
  }
}

