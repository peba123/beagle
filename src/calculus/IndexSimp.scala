package beagle.calculus

import beagle._
import datastructures._
import indexing._
import PMI.Pos
import fol._
import util._
import scala.util.control.Breaks

/** Implements various forms of demodulation using the discrimination tree index */
object IndexSimp {

  def simplifyPos(cl: ConsClause, clsIndexed: UnitClauseIndex): (ConsClause, Boolean) = {
    val (cl1, touched1) = reduceBool(cl.unabstrCautious, clsIndexed.posBoolUnits)
    val (cl2, touched2) = IndexSimp.demodulate(cl1, clsIndexed.posUnits)

    if (touched1 || touched2) (cl2, true)
    else (cl.unabstrCautious, false)
  }

  def simplifyNeg(cl: ConsClause, clsIndexed: UnitClauseIndex): (ConsClause, Boolean) = {
    val (cl1, touched1) = IndexSimp.reduceBool(cl.unabstrCautious, clsIndexed.negBoolUnits, usingNegUnits = true)
    val (cl2, touched2) = IndexSimp.negUnitSimp(cl1, clsIndexed.negUnits)
    
    //case: only negUnit indexing
    // val (cl0, touched0) = IndexSimp.negUnitSimp(cl3, negUnits)
    // val (cl00, touched00) = negUnitSimp(cl0, cls.unitClauses filter { cl => !cl(0).isPositive && cl(0).isPredLit })

    //case: only boolean reduction
    // val (cl0, touched0) = IndexSimp.reduceBool(cl3, negBool, usingNegUnits = true)
    // val (cl00, touched00) = negUnitSimp(cl0, cls.unitClauses filter { cl => !cl(0).isPositive && !cl(0).isPredLit })
    //val (cl000, touched000) = demodulate(cl00, newUnits filter { !_.isPositive })
    if (touched1 || touched2) (cl2, true)
    else (cl.unabstrCautious, false)
  }

  //given that we have t[s] and have found l=r in the index st mu(l) = s
  // we want to remove index vars from r and check that t[r] is a proper simp
  // using the definitions given below.
  private def demodFilter(s: Term, pos: Pos, k: Lit, inputCl: ConsClause, clDemod: ConsClause) = (mu: Subst, t: Term, unitCl: ClInfo) => {
    val unit = unitCl.unit
    val isOrdered = unit(0).eqn.isOrdered //correct?
    val lhs = t
    val rhs = unitCl.rhs
    val eRhsInst = mu(rhs)
    //pre-test-- skip if false
    if (!isOrdered && !(s gtr eRhsInst)) None
    else {
      val kDemod = k.replaceAt(pos, eRhsInst)
      // Various conditions to check if we're ok to proceed,
      // the nontrivial one is whether demodulation with e gives a smaller clause than cl
      if (pos.tail.nonEmpty || // paramodulating into a proper subterm of the lhs or rhs, or
          !k.isPositive || // or a negative literal, or
          kDemod.isTrivialPos || // a trivial literal; clauses with trivial positive literals will be removed anyway
          (k.eqn.isOrdered && pos.head == 1) || // demodulating the rhs of an ordered equation
                                                // Coming here means we are demodulating the top-level of the (potentially) bigger side of k.
                                                // In this case we to make sure explictly that the demodulator is smaller than the clause to be demodulated.
                                                // For example, demodulating a->b into a->c with a > b > c would violate this, as a->b > a->c.
          (clDemod.length > 1) || // Relax the just said: experimental
          (inputCl.lits exists { _ gtr Lit(true, Eqn(s, eRhsInst)) })) { // Lit(true, Eqn(s, eRhsInst)) is the instantiated demodulator

        //if we can continue, return idxRel and the age
        Option((kDemod, unit))
      } else {
        None
      }
    }
  }

  /**
   * Demodulate a clause cl exhaustively with an index over positive unit clauses `posUnits`.
   * The result needs abstraction
   */
  def demodulate(cl: ConsClause, posUnits: DiscrTreeIndex[ClInfo]): (ConsClause, Boolean) = {

    if (flags.nodemod.value) return (cl, false)

    // assume posUnits are all ordered
    var idx = Set.empty[Int] // The indices of the clauses used for demodulation.
    var touchedLit = false // whethwer some literal was touched by simplification
    var demodMaxAge = 0 

    var unitsUsed = List.empty[ConsClause]

    var clDemod = cl.unabstrCautious // The currently demodulized version of cl

    def demodulate(l: Lit): Lit = {
      var k = l // the copy of l we work on stepwisely
      var done = false
      val breakInner = new Breaks
      do { 
        //stats.simpDemod.tried

        breakInner.breakable {

          for {
            (s, pos) <- k.subTermsWithPos
            MatchResult(mu, t, info) <- posUnits.findAllMatching(s)
            (kDemod, u) <- demodFilter(s, pos, k, cl, clDemod)(mu, t, info)
          } {
            idx ++= u.idxRelevant
            unitsUsed ::= u
            stats.idxDemod += 1
	    stats.simpDemod.succeed
            k = kDemod // Update k with current result
            touchedLit = true
            demodMaxAge = math.max(demodMaxAge, u.age)
            if (k.isTrivialPos)
              return Lit.TrueLit
            else if (k.isTrivialNeg)
              return Lit.FalseLit
            else
              breakInner.break() // Continue with outer loop
          }

          simplification.nlppDemod(k) map { kDemod =>
            touchedLit = true
            if (kDemod.isTrivialPos)
              return Lit.TrueLit
            else if (kDemod.isTrivialNeg)
              return Lit.FalseLit
            else {
              k = kDemod
              breakInner.break() // Continue with outer loop
            }
          }

          // After both for loops
          // Coming here means that no rule has applied
          done = true
        } // breakInner
      } while (!done)
      k // final result
    }

    // Body of demodulation for clauses

    var someTouched = false
    do {
      touchedLit = false
      val clDemodResLits = clDemod.lits map { demodulate(_) }
      if (touchedLit) {
        someTouched = true
        clDemod = cl.modified(
          lits = clDemodResLits, 
          constraint = (cl.constraint ++ (unitsUsed flatMap { _.constraint })).distinct,
          idxRelevant = clDemod.idxRelevant ++ idx, 
          age = math.max(cl.age, demodMaxAge) + 0, 
          inference = ByDemod(unitsUsed :+ cl), 
          deltaConjecture = cl.info.deltaConjecture + 1).unabstrCautious
      }
    } while (touchedLit)

    if (someTouched) {
      // Try 26/5/2014: never increase the age
      val res = (clDemod, true) //  else math.max(cl.age, demodMaxAge)+0
      reporter.detailedSimp("demod", cl, res._1, unitsUsed)
      res
    } else
      (cl, false)

  }

  /** Simplify a clause by removing boolean units of opposite sign to those
    * found in the index.
    * 
    * @param usingNegUnits if true then terms t in the index represent negated predicates.
    * @todo Can't do subsumption here since that allows units to subsume themselves.
    */
  def reduceBool(cl: ConsClause, idx: DiscrTreeIndex[ClInfo], usingNegUnits: Boolean = false): (ConsClause, Boolean) = {

    var touch = false
    var idxRel = cl.idxRelevant
    var newAge = cl.age
    var unitsUsed = List.empty[ConsClause]

    val newLits = cl.lits flatMap { l =>
      if (!l.eqn.isPredEqn)
        List(l)
      else {
        val Lit(isPositive, PredEqn(p)) = l
        val m = idx.findFirstMatch(p)

        if (m.nonEmpty) {
          val MatchResult(mu, t, ClInfo(_, u, _)) = m.get

          if (!usingNegUnits) {
            //negative literals that are instances of positive atoms in idx are filtered
            stats.idxBoolReductions += 1
            touch = true
            idxRel ++= u.idxRelevant
            newAge = (u.age max newAge)
            if (!isPositive) Nil else List(Lit.TrueLit)
          } else if (usingNegUnits && isPositive) {
            //sense is reversed:
            //positive literals that are instances of atoms in idx are filtered
            stats.idxBoolReductions += 1
            touch = true
            idxRel ++= u.idxRelevant
            newAge = (u.age max newAge)
            unitsUsed ::= u
            //if (isPositive) Nil else List(Lit.TrueLit)
            //dont do subsumption by negative units!
            Nil
          } else
            List(l)
        } else //no match in index
          List(l)
      }
    }

    if(!touch) 
      (cl, false)
    /*else if (newLits.contains(Lit.TrueLit)) {
      (cl.modified(
        lits = List(Lit.TrueLit),
        idxRelevant = idxRel,
        inference = if (usingNegUnits) ByNegUnitSimp(unitsUsed :+ cl) else ByDemod(unitsUsed :+ cl),
        deltaConjecture = ((cl :: unitsUsed) map { _.info.deltaConjecture }).min+1,
        age = newAge), 
       true)
    }*/ else {
        val res = cl.modified(
          lits = newLits,
          idxRelevant = idxRel,
          inference = if (usingNegUnits) ByNegUnitSimp(unitsUsed :+ cl) else ByDemod(unitsUsed :+ cl),
          deltaConjecture = ((cl :: unitsUsed) map { _.info.deltaConjecture }).min+1,
          age = newAge)

        if (usingNegUnits) reporter.detailedSimp("neg_unit", cl, res, unitsUsed)
        else reporter.detailedSimp("demod", cl, res, unitsUsed)

        (res, true)
      }
  }

  import fol.unification._

  /** This is a filter function for matching.
    * Given a t such that mu(t) = l.lhs:
    * In the index info.rhs is stored against t (i.e. t=info.rhs was the original equation
    * inserted in the index) and so mu must be extended so that mu'(info.rhs)=rhs
    * in order to get a complete matcher for l.
    */
  private def matchAndGetIndex(lhs: Term, rhs: Term) = (mu: Subst, t: Term, info: ClInfo) => {
    unification.matchTo(List(mu(info.rhs)), List(rhs), mu) flatMap { mu2 =>
      //if a matcher exists return the corresponding unit
      //but only if it does not act on lhs or rhs
      if (mu2(lhs) == lhs && mu2(rhs) == rhs) {
        //println(s"*## dbg matching $t to $lhs, mu was $mu2")
        Option(info.unit)
      } else {
        //println(s"*## dbg matching $t to $lhs, FAILED when mu was $mu2")
        None
      }
    } nonEmpty
  }

  /** Given an index containing negative unit equations simplify `cl` by filtering 
    * any positive units that are instances of units in the index.
    */
  def negUnitSimp(cl: ConsClause, negUnits: DiscrTreeIndex[ClInfo]): (ConsClause, Boolean) = {
    if (cl.isNegative) return (cl, false)

    val (unitsUsed, resLits) = cl.lits.foldLeft((List.empty[ConsClause], List.empty[Lit])) {
      case ((units, lits), l @ Lit(true, eqn)) => {
        var res = negUnits.findFirstMatchFilter(eqn.lhs, matchAndGetIndex(eqn.lhs, eqn.rhs))

        if (res.isEmpty && !eqn.isOrdered) {
          //if failed try for a match of rhs
          //index is over maximal terms only so don't bother if l is ordered.
          res = negUnits.findFirstMatchFilter(eqn.rhs, matchAndGetIndex(eqn.rhs, eqn.lhs))
        }

        if (res.nonEmpty) {
          val u = res.get.data.unit
          //remove this lit
          (u :: units, lits)
        } else //non matching so do not remove
          (units, l :: lits)

      } 
      case ((units, lits), l) => 
        (units, l :: lits)
    }

    if (unitsUsed.nonEmpty) {
      stats.idxNegUnitSimp += 1
      val res = (cl.modified(lits = resLits.reverse,
                             constraint = (cl.constraint ++ (unitsUsed flatMap { _.constraint })).distinct,
	                     idxRelevant = cl.idxRelevant ++ unitsUsed.flatMap(_.idxRelevant),
                             inference = ByNegUnitSimp(unitsUsed.distinct :+ cl),
                             deltaConjecture = ((cl :: unitsUsed) map { _.info.deltaConjecture }).min+1),
	         true)

      reporter.detailedSimp("neg_unit", cl, res._1, unitsUsed)
      res
    } else (cl, false)
  }

  /** Whether any literal in the index matches a clause literal */
  def unitSubsumption(cl: ConsClause, cls: UnitClauseIndex): Boolean = {
    for { l <- cl.lits } l match {
      case Lit(isPositive, PredEqn(p)) => {
        val m =
          if (isPositive) cls.posBoolUnits.findFirstMatch(p)
          else cls.negBoolUnits.findFirstMatch(p)

        if (m.nonEmpty) {
          val MatchResult(_, _, ClInfo(_, subsumingClause, nonUnit)) = m.get
          // there is a pos bool unit which matches some positive literal in cl
          if (!nonUnit) { //do not subsume using unabstracted non-units
            util.stats.idxUnitSubsume += 1
            reporter.onClauseSubsumed(subsumingClause, cl, Nil)
            return true
          }
        }
      }
      case Lit(isPositive, e) => {
        //try for a match of lhs in index
        var res =
          if (isPositive) cls.posUnits.findFirstMatchFilter(e.lhs, matchAndGetIndex(e.lhs, e.rhs))
          else cls.negUnits.findFirstMatchFilter(e.lhs, matchAndGetIndex(e.lhs, e.rhs))

        if (res.isEmpty && !e.isOrdered) {
          //if failed try for a match of rhs
          //index is over maximal terms only so don't bother if l is ordered.
          res =
            if (isPositive) cls.negUnits.findFirstMatchFilter(e.rhs, matchAndGetIndex(e.rhs, e.lhs))
            else cls.negUnits.findFirstMatchFilter(e.rhs, matchAndGetIndex(e.rhs, e.lhs))
        }

        if (res.nonEmpty) {
          if (!res.get.data.byUnabstr) {
            val subsumingClause = res.get.data.unit
            util.stats.idxUnitSubsume += 1
            reporter.onClauseSubsumed(subsumingClause, cl, Nil)
            return true
          }
        }

      }
    } //end match
    //end for
    return false
  }

}
