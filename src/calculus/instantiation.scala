package beagle.calculus

import beagle._
import datastructures._
import fol._
import util._
import bgtheory._
import LIA._

/** Instantiation of < atoms to recover sufficient completeness.
  * 
  * There are two versions: Inter-clause and Intra-clause corresponding to each of the directions
  * of the LIA axiom (c < x & x < d) <=> (c+1 = x v ... v d-1 = x).
  * The => direction applies to a single clause (intra-clause inst.) and produces (d-c-1) new 
  * clause instances as follows:
  * ~(c < t) v ~(t' < d) v C
  * -------------------------  s=mgu(t,t')
  *      ((c+i != t) v C)s
  *  where, d > c and for all 1<= i <= (d-c-1)
  *  
  * The <= direction applies to a pair of clauses and produces a single new clause:
  *  c < t v C            t' < d v D
  *  -------------------------------  s=mgu(t,t')
  *  ((c+1=t v ... v d-1=t) v C v D)s
  *  Given that t and t' are not variables and d > c.
  * 
  * In each of the rules, we could restrict t,t' to be non-variable, or impure BG, or BSFG.
  * Also we can restrict c,d to be domain elements, or to be ground, or at minimum that the
  * expression (d-c-1) evaluates to a positive domain element.
  * 
  * *Inst use only integer bounds while *Sym use symbolic bounds.
  */
object instantiation {

  /* LIA atoms are normalised to Less(a,b) or Neg(Less(a,b)) */

  private def boundsOf(lits: Iterable[(Lit,Int)]): 
      (List[(Int, Term, Int)], List[(Int, Term, Int)]) = {
    //these are (index, ti, bound dom. elem.)
    var lbs: List[(Int, Term, Int)] = Nil
    var ubs: List[(Int, Term, Int)] = Nil

    //collect positive bounds and their indices
    for ((l,i) <- lits)
      l match {
        case Lit(_, LessEqn(DomElemInt(c),t)) if !t.isVar     => lbs ::= (i,t,c)
        //case Lit(true, GreaterEqn(t, DomElemInt(c))) if !t.isVar => lbs ::= (i,t,c)
        case Lit(_, LessEqn(t, DomElemInt(c))) if !t.isVar    => ubs ::= (i,t,c)
        //case Lit(true, GreaterEqn(DomElemInt(c),t)) if !t.isVar  => ubs ::= (i,t,c)
        case _ => ()
      }
    
    (lbs, ubs)
  }

  /** Given a clause of the form (c < t) v C where c is a DE and t is not a variable
    * and a clause (t' < d) v D where d is a DE st d > c+1 and sigma = mgu(t,t') then produce
    * sigma((c+1 = t v ... v d-1 = t) v C v D) for i in [1,d-c].
    * TODO- possiby it is best to restrict one or both of the input clauses to be units.
    */
  def interclauseInst(from: ConsClause, into: ListClauseSet): Iterable[ConsClause] = {
    val (lbs, ubs) = boundsOf(from.iPosLits map { i => (from(i), i) })

    (for {
      inCl <- into.clauses
      (lbInto, ubInto) = boundsOf(inCl.iPosLits map { i => (inCl(i), i) })
    } yield {
      val newIdx = from.idxRelevant ++ inCl.idxRelevant
      val newAge = (from.age max inCl.age)+1

      //for each lower bound of from and upper in into
      val res1 =
      for {
        (idxL, t1, l) <- lbs
        (idxU, t2, u) <- ubInto
        if (u - l > 1)
        //attempt to unify non-DE terms
        sigma <- (t1 mgu t2)
      } yield {

        val litsTail = 
          (for (i <- Range(1, (u-l))) yield Lit(true, Eqn(sigma(t1), DomElemInt(l+i)))) ++ inCl.lits.removeNth(idxU)

        val res = Clause(sigma(from.lits.removeNth(idxL) ++ litsTail),
          ClsInfo(newIdx, newAge, ByChaining(from, inCl), from.info.deltaConjecture+1)).abstr
        println(s"$res by inter-clause inst between $from and $inCl using $t1 and $t2")
        res
      }

      //for each upper bound of from and lower in into
      val res2 = 
      for {
        (idxL, t1, l) <- lbInto
        (idxU, t2, u) <- ubs
        if (u - l > 1)
        //attempt to unify non-DE terms
        sigma <- (t1 mgu t2)
      } yield {

        val litsTail = 
          (for (i <- Range(1, (u-l))) yield Lit(true, Eqn(sigma(t1), DomElemInt(l+i)))) ++ from.lits.removeNth(idxU)

        val res = Clause(sigma(inCl.lits.removeNth(idxL) ++ litsTail),
          ClsInfo(newIdx, newAge, ByChaining(from, inCl), from.info.deltaConjecture+1)).abstr
        println(s"$res by inter-clause inst between $from and $inCl using $t1 and $t2")
        res
        }

      res1 ++ res2
    }).flatten

  }

  /** Given a clause of the form ~(c < t & t' < d) v C where c,d are concrete integers c+1 < d 
    * and s=mgu(t,t') form the clauses (c+1 != t.s) v C.s, (c+2 != t.s) v C.s, ...
    */
  def intraclauseInst(cl: ConsClause): Iterable[ConsClause] = {

    val (lbs, ubs) = boundsOf(cl.iNegLits map { i => (cl(i),i) })

    //for each pair of lower and upper bounds
    (for {
      (idxL, t1, l) <- lbs
      (idxU, t2, u) <- ubs
      //if domain elements are sufficiently separated
      if (u - l > 1)
      //attempt to unify non-DE terms
      sigma <- (t1 mgu t2)
    } yield {
      for (i <- Range(1, (u-l))) yield {
        //if they unify compute the difference in the DE and instantiate C over that range
        //C must drop t1 and t2 and add (t1.sigma != l+i)
        val newLit = Lit(false, Eqn(sigma(t1), DomElemInt(l+i)))
        val res = Clause(sigma(cl.lits.replaceNth(idxL, newLit).removeNth(idxU)), 
          ClsInfo(cl.idxRelevant, cl.age+1, ByChaining(cl, cl), cl.info.deltaConjecture+1)).abstr
        println(s"$res by intra-clause inst. of $t1 and $t2 bounded by $l and $u")
        res
      }
    }).flatten
  }

  /** Intra-clause instantiation using symbolic end points */
  def intraclauseSym(cl: ConsClause) = {

    def intraClauseSingle(l1: Lit, l1Pos: Int, l2: Lit, l2Pos: Int) = {
      val Lit(_, LessEqn(t1, t2)) = l1
      val Lit(_, LessEqn(t3, t4)) = l2
      (for {
        sigma <- (t2 mgu t3)
        k = LIA.Difference(sigma(t4), sigma(t1)).simplifyBG
        if (k.isInstanceOf[DomElemInt] && k.asInstanceOf[DomElemInt].value > 1)
      } yield {
        println(s"ic using $t2 and $t3 where k=$k")
        util.stats.nrIntraInst += 1
        for (i <- Range(1, k.asInstanceOf[DomElemInt].value)) yield {
          val newLit = Lit(false, Eqn(LIA.Sum(t1, DomElemInt(i)), t2))
          Clause(sigma(cl.lits.replaceNth(l1Pos, newLit).removeNth(l2Pos)),
            ClsInfo(cl.idxRelevant, cl.age+1, ByChaining(cl, cl), cl.info.deltaConjecture+1)).simplifyBG.abstr
        }
      }).getOrElse(List[ConsClause]())
    }

    var negLessPos = cl.termIndex(LIA.LessOpInt).map(_._2.head) intersect cl.iNegLits
    //could intersect with iEligible?
    var res = List[ConsClause]()

    while(!negLessPos.isEmpty) {
      val l1Pos = negLessPos.head
      val l1 = cl(l1Pos)
      negLessPos = negLessPos.tail

      for {
        l2Pos <- negLessPos
        l2 = cl(l2Pos)
      } {
        res = res ++ intraClauseSingle(l1, l1Pos, l2, l2Pos) ++ 
          intraClauseSingle(l2, l2Pos, l1, l1Pos)
      }
    }
    res
  }

  def interclauseSym(from: ConsClause, into: ListClauseSet): Iterable[ConsClause] = {
    def interSymInst(from: ConsClause, fromPos: Int, fromLit: Lit, into: ConsClause, intoPos: Int, intoLit: Lit) = {
      var res = List.empty[ConsClause]

      def addToRes(ls: IndexedSeq[Lit], sigma: Subst) {
        res = Clause(sigma(ls.toList ::: from.lits.removeNth(fromPos) ::: into.lits.removeNth(intoPos)),
          ClsInfo(into.idxRelevant ++ from.idxRelevant,
          math.max(into.age, from.age) + 1, ByChaining(from, into), math.min(from.info.deltaConjecture, into.info.deltaConjecture)+1)).abstr :: res
        println(s"interclause inst yields ${res.head} using $from and $into at $fromLit and $intoLit")
      }

      val Lit(true, LIA.LessEqn(s, t)) = fromLit
      val Lit(true, LIA.LessEqn(u, v)) = intoLit

      //s < t = u < v
      if (!t.isVar && !u.isVar) {
        (t mgu u) map { sigma =>
          //check that v - s > 1
          val k = LIA.Difference(v,s).simplifyBG
          if (k.isInstanceOf[DomElemInt] && k.asInstanceOf[DomElemInt].value > 1) {
            util.stats.nrInterInst += 1
            val newLits = for (i <- Range(1, k.asInstanceOf[DomElemInt].value)) yield
              Lit(true, Eqn(LIA.Sum(s,DomElemInt(i)),t))
            addToRes(newLits, sigma)
          }
        }
      }
      if (!s.isVar && !v.isVar) {
        (s mgu v) map { sigma =>
          //check that t - u > 1
          val k = LIA.Difference(v,s).simplifyBG
          if (k.isInstanceOf[DomElemInt] && k.asInstanceOf[DomElemInt].value > 1) {
            util.stats.nrInterInst += 1
            val newLits = for (i <- Range(1, k.asInstanceOf[DomElemInt].value)) yield
              Lit(true, Eqn(LIA.Sum(s,DomElemInt(i)),t))
            addToRes(newLits, sigma)
          }
        }
      }

      res
    }

    // Assume clauses are variable disjoint
    var res = List.empty[ConsClause]

    for {
      // eligible literals are never pure BG, hence no need to test that
      fromPos ← from.iPosLits
      fromLit = from(fromPos)
    }
      fromLit match {
        case Lit(true, LIA.LessEqn(_, _)) =>
          for {
            intoCl <- into.clauses;
            intoPos ← intoCl.iEligible; // todo: use index
            intoLit = intoCl(intoPos)
          }
          intoLit match {
            case Lit(true, LIA.LessEqn(_, _)) =>
              res :::= interSymInst(from, fromPos, fromLit, intoCl, intoPos, intoLit)
            case _ => ()
          }
        case _ => ()
      }
    res

  }

}
