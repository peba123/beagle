package beagle.calculus

import beagle._
import datastructures._
import indexing._
import fol._
import util._
import scala.util.control.Breaks

object simplification {

  /** This heuristic is amazingly effective on SWW problems, esp. SWW573=2, SWW574=2, SWW575=2
    * Basically delay the NLPP demodulation until part-way into the proof
    * Still unsure why this actually works, but hey... */
  private def canDemodNLPP = Timer.mainLoop.sinceStarted() > 10

  /** See if there is non-linear multiplication we can eliminate in the literal `k`.
    * Doing this explictly is better than via general demodulation for two reasons: 
    * (1) it may take a long time until a demodulator like '#nlpp'(20000, X) = $product(20000, X)
    * has been generated, and
    * (2) Only demodulators like '#nlpp'(10, X) = $product(10, X) are generated but not their
    * symmetric forms '#nlpp'(10, X) = $product(10, X). 
    * The code below avoids both issues.
    */
  def nlppDemod(k: Lit): Option[Lit] = {
    import bgtheory._

    if (!canDemodNLPP) return None

    def demodRes(s: Term) = s match {
      case NLPPOp(_: DomElem[_], _) | NLPPOp(_, _: DomElem[_]) => {
        val NLPPOp(l,r) = s
        if (s.sort == LIA.IntSort) Some(LIA.Product(l, r))
        else if (s.sort == LRA.RatSort) Some(LRA.Product(l, r))
        else if (s.sort == LFA.RealSort) Some(LFA.Product(l, r))
        else throw new InternalError("Unexpected sort for NLPP term: "+s)
      }
      case _ => None
    }
    
    for {
      (s, pos) <- k.termIndex(LFA.NLPPOpReal) ++ k.termIndex(LRA.NLPPOpRat) ++ k.termIndex(LIA.NLPPOpInt)
      rhsInst <- demodRes(s)
      // Conditions to check if we're ok to proceed,
      // the nontrivial one is whether demodulation with e gives a smaller clause than cl
      /*if (pos.tail.nonEmpty || // paramodulating into a proper subterm of the lhs or rhs, or
        !k.isPositive || // or a negative literal, or
        (k.eqn.isOrdered && pos.head == 1)) || // demodulating the rhs of an ordered equation
                                              // Coming here means we are demodulating the top-level of the (potentially) bigger side of k.
                                              // Only in this case we to make sure explictly that the demodulator is smaller than the clause to be demodulated.
                                              // For example, demodulating a->b into a->c with a > b > c would violate this, as a->b > a->c.
        (cl.lits exists { _ gtr Lit(true, Eqn(s, rhsInst)) }))*/
                                            //do not need this test because k[nlpp(l,r)] > nlpp(l,r) by subterm prop. and k[nlpp(l,r)] > $prod(l,r) by term precedence
                                            // therefore k > (s = rhsInst) for any cl
      } {
      stats.simpDemod.succeed
      // println("demodulation of %s with instance %s → %s yields %s".format(k, s, rhsInst, kDemod))
      return Option(k.replaceAt(pos, rhsInst))
    }
    return None
  }

  /**
   * Demodulate a clause cl exhaustively with the given positive unit clauses posUnits.
   * The result needs abstraction
   */
  def demodulate(cl: ConsClause, posUnits: Iterable[ConsClause]): (ConsClause, Boolean) = {

    if (flags.nodemod.value) return (cl, false)

    // assume posUnits are all ordered
    var idx = Set.empty[Int] // The indices of the clauses used for demodulation.
    
    var touchedLit = false // whether some literal was touched by simplification
    var demodMaxAge = 0 

    var unitsUsed: List[ConsClause] = Nil

    var clDemod = cl.unabstrCautious // The currently demodulized version of cl

    def makeEquationsForDemod(eqn: Eqn) = {
      var res = List.empty[(Term, Term, Boolean)]
      if (!eqn.isOrdered && !eqn.rhs.isVar) res ::= (eqn.rhs, eqn.lhs, false)
      if (!eqn.lhs.isVar) res ::= (eqn.lhs, eqn.rhs, eqn.isOrdered)
      res
    }
    // println("demod on "+cl)

    def demodulate(l: Lit): Lit = {
      var k = l // the copy of l we work on stepwisely
      var done = false
      val breakInner = new Breaks
      do { 
        stats.simpDemod.tried
        breakInner.breakable {

          // todo: this for-loop could be rather inefficient by always expanding all iteration variables,
          // which can be a lot of overhead, in particular when the inner break is triggered early.
          // Some experiments seem to show, however, that this does not make a big difference
          for {
	    u <- posUnits
            (lhs, rhs, isOrdered) <- makeEquationsForDemod(u(0).eqn)
            (s, pos) <- k.subTermsWithPosForSup(lhs)
            sigma <- lhs matcher s
            eRhsInst = sigma(rhs)
            if isOrdered || (s gtr eRhsInst)
          } {
            val kDemod = k.replaceAt(pos, eRhsInst)
            // Various conditions to check if we're ok to proceed,
            // the nontrivial one is whether demodulation with e gives a smaller clause than cl
            if ( // preprocessing ||
              pos.tail.nonEmpty || // paramodulating into a proper subterm of the lhs or rhs, or
              !k.isPositive || // or a negative literal, or
              kDemod.isTrivialPos || // a trivial literal; clauses with trivial positive literals will be removed anyway
              (k.eqn.isOrdered && pos.head == 1) || // demodulating the rhs of an ordered equation
                                                    // Coming here means we are demodulating the top-level of the (potentially) bigger side of k.
                                                    // In this case we to make sure explictly that the demodulator is smaller than the clause to be demodulated.
                                                    // For example, demodulating a->b into a->c with a > b > c would violate this, as a->b > a->c.
              (clDemod.length > 1) || // Relax the just said: experimental
              (cl existsLit { _ gtr Lit(true, Eqn(s, eRhsInst)) }) ) { // Lit(true, Eqn(s, eRhsInst)) is the instantiated demodulator
              idx ++= u.idxRelevant
              unitsUsed ::= u
              //stats.nrSimpDemod += 1
	      stats.simpDemod.succeed
              // println("demodulation of %s with instance %s → %s yields %s".format(k, s, eRhsInst, kDemod))
              // k = kDemod.simplifyBG // Update k with current result
              k = kDemod // Update k with current result
              touchedLit = true
              demodMaxAge = math.max(demodMaxAge, u.age)
              if (k.isTrivialPos)
                return Lit.TrueLit
              else if (k.isTrivialNeg)
                return Lit.FalseLit
              else 
                breakInner.break() // Continue with outer loop
            }
          }

          nlppDemod(k) map { kDemod =>
            touchedLit = true
            if (kDemod.isTrivialPos)
              return Lit.TrueLit
            else if (kDemod.isTrivialNeg)
              return Lit.FalseLit
            else {
              k = kDemod
              breakInner.break() // Continue with outer loop
            }
          }

          // After both for loops
          // Coming here means that no rule has applied
          done = true
        } // breakInner
      } while (!done)
      k // final result
    }

    // Body of demodulation for clauses

    // println("Unit clauses for demodulation of %s:".format(cl.toString()))
    // posUnits foreach { cl => println(cl) }

    var someTouched = false
    do {
      touchedLit = false
      val clDemodResLits = clDemod.lits map { demodulate(_) }
      if (touchedLit) {
        someTouched = true

/*        clDemod = Clause(clDemodResLits, clDemod.idxRelevant ++ idx,
          math.max(cl.age, demodMaxAge)+0,
          ByDemod(unitsUsed :+ cl), cl.deltaConjecture+1).unabstrCautious*/
        clDemod = cl.modified(lits = clDemodResLits,
                              constraint = (cl.constraint ++ (unitsUsed flatMap { _.constraint })).distinct,
                              idxRelevant = clDemod.idxRelevant ++ idx,
                              age = math.max(cl.age, demodMaxAge),
                              inference = ByDemod(unitsUsed :+ cl), 
                              deltaConjecture = cl.info.deltaConjecture+1).unabstrCautious

        // val hcl = Clause(clDemodResLits, clDemod.idxRelevant ++ idx, math.max(cl.age, demodMaxAge)+0, ByDemod(unitsUsed :+ cl))
        // println(s"hcl = $hcl:" + hcl.id)
        // clDemod = hcl.simplifyBG
        // println(s"clDemod = $clDemod:" +clDemod.id)
        // hcl.simplifyCheap match {
        //   case List() => return (Clause(List(Lit.TrueLit), hcl.idxRelevant, hcl.age+1, BySimple(hcl)), true)
        //   case List(c) => clDemod = c
        // }
        // clDemod = Clause(clDemodResLits, clDemod.idxRelevant ++ idx, math.max(cl.age, demodMaxAge)+1).unabstr
        // Clause(hclResLits, hcl.idxRelevant ++ idx, cl.age+1).simplifyCheap match {
        //   case Nil => hcl = Clause(List(Lit.TrueLit), hcl.idxRelevant ++ idx, cl.age+1) // todo: delete clause straight away
        //   case List(c) => hcl = c
        // }
        // println("Success: " + hcl)
      }
    } while (touchedLit)

    if (someTouched) {
      // Try 26/5/2014: never increase the age
      val res = (clDemod, true) //  else math.max(cl.age, demodMaxAge)+0
      // println(res)
      reporter.detailedSimp("demod", cl, res._1, unitsUsed)
      res
    } else
      (cl, false)

/*
    val h = cl.unabstr.lits map { demodulate(_) }
    if (touchedLit) {
      // Try 26/5/2014: never increase the age
      val res = (Clause(h, cl.idxRelevant ++ idx, cl.age+1), true) //  else math.max(cl.age, demodMaxAge)+0
      reporter.detailedSimp("demod", cl, res._1, unitsUsed)
      res
    } else
      (cl, false)
 */

  }

  /**
   * Simplify a Clause by removing positive literals that are subsumed by some
   * negative unit clause in negUnits. No need to remove negative literals, as
   * this is done by simplification.
   */
  def negUnitSimp(cl: ConsClause, negUnits: Iterable[ConsClause]): (ConsClause, Boolean) = {

    if (cl.isNegative) {
      return (cl, false)
    }

    var touched = false
    var idx = Set.empty[Int]
    //var resLits = List.empty[Lit]
    
    var unitsUsed = List.empty[ConsClause]
    stats.simpNegUnit.tried

    val smallNegUnits = negUnits.filter(_.operators subsetOf cl.operators)
    val resLits = 
      cl.lits.foldLeft(List[Lit]())( (acc, l) =>
        if (l.isPositive)
          smallNegUnits find {
            u => (u(0).eqn matchers l.eqn).nonEmpty
          } match {
            case Some(u) ⇒ {
              idx ++= u.idxRelevant
              unitsUsed ::= u
              touched = true
              acc
            }
            case None ⇒ l :: acc
          }
          else l :: acc
      )

    if (touched) {
      stats.simpNegUnit.succeed
      //val res = (Clause(resLits.reverse, cl.idxRelevant ++ idx, cl.age + 0, ByNegUnitSimp(unitsUsed :+ cl), ((cl :: unitsUsed) map { _.deltaConjecture }).min+1), true)
      val res = (cl.modified(lits = resLits.reverse,
                            constraint = (cl.constraint ++ (unitsUsed flatMap { _.constraint })).distinct,
                            idxRelevant = cl.idxRelevant ++ idx,
                            inference = ByNegUnitSimp(unitsUsed :+ cl),
                            deltaConjecture = ((cl :: unitsUsed) map { _.info.deltaConjecture }).min+1),
                 true)

      reporter.detailedSimp("neg_unit", cl, res._1, unitsUsed)
      res
    } else {
      (cl, false)
    }
  }

  /** If we iterate over the units and use the clause's PMI to find possibly simplified literals this could
    *  be faster.
    * Does not appear to be noticeably faster.
    */
/*  def negUnitSimpAlt(cl: ConsClause, negUnits: Iterable[ConsClause]): (ConsClause, Boolean) = {
    var simpIndices = Vector.empty[Int]
    for {
      un <- negUnits
      Lit(false, Eqn(l, r)) = un(0)
    } {
      //get top-level op from u, both sides of eqn
      l match {
        case FunTerm(op, _) => {
          //check that op is at top level
          //pos = iInClause :: iInLit :: iInTerm :: ...
          cl.termIndex(op) foreach {
            case (_, List(iClause, iLit)) if (cl(iClause).isPositive) => 
              if ((un(0).eqn matchers cl(iClause).eqn).nonEmpty) simpIndices :+= iClause
            case _ => ()
          }
        }
        case x: Var => 
          for { i <- cl.iPosLits } if ((un(0).eqn matchers cl(i).eqn).nonEmpty) simpIndices :+= i
      }
    }

    if (simpIndices.isEmpty)
      return (cl, false)
    else
      return (cl, true)
  }*/

  /** Cheap simplifications: removal of trivial negative equations,
    * tautology detection, unabstraction, removal of extraneous BG variables by QE.
    * Returns an abstracted clause */
  def simplifyCheap(cl: ConsClause): List[ConsClause] = {

    // todo: check which is better:
    // Josh- 31/5 reverted to unabstrCautious as this was killing performance (see SWW598=2)
    // perhaps due to mishandled flag settings?
    // similar for line 320.
    //var cl1 = if (util.flags.bgsimp.value >= 2) cl.unabstrAggressive else  cl.unabstrCautious
    var cl1 = cl.unabstrCautious
    var cl1old: ConsClause = null
    do {
      cl1old = cl1
      cl1 = cl1.simplifyBG.unabstrCautious
      // cl1 = cl1.simplifyBG
      // cl1 = if (util.flags.bgsimp.value >= 2) cl1.unabstrAggressive else  cl1.unabstrCautious
    } while (cl1.length < cl1old.length)

    // Some cheap simplifications that still get unnoticed otherwise, e.g. 2|4 => true
    // no - part of Lit.simplifyBG
    if (cl.sorts(bgtheory.LIA.IntSort)) 
      cl1 = cl1.modified(lits = (cl1.lits map { l =>
        if (l.isGround) bgtheory.solver.simplifyCheap(l) else l
      }))

    // Removal of trivial negative equations
    // TODO- record trivial negative unit deletion
    // The ident number of the result is the same as the given clause
    // val cl2 = Clause(cl1.id, cl1.lits.toListSet filterNot { _.isTrivialNeg }, cl1.idxRelevant, cl1.age)
    // val cl2 = cl1.modified(lits = cl1.lits.toListSet filterNot { _.isTrivialNeg })
    // removal of trivial negative literals and duplicates is part of unabstraction now.
    // Now done eagerly, when a clause is made
    if (cl1.isTautology) {
      //stats.nrSimpTaut += 1
      List.empty
    } else
      List(cl1.abstr)
  }

  /** Reduce the clause cl by all clauses in the given clauses cls
    * This includes rewriting by negative unit clauses.
    * The result is cheaply simplified, but only if the clause was touched
    */
  def reduceWithStatus(cl: ConsClause, cls: ClauseSet): (Iterable[ConsClause], Boolean) = {
    Timer.reduction.start()

    // if (cl.unabstrCautious.isTautology) return (List.empty, true)

    //the unit clauses in cls are extended with any new units produced by aggressive simplification
    //if using indexing those clauses are added automatically to the index
    val newUnits = {
      if (!util.flags.indexing.value)
        cls.clauses collect {
          case cl if (!cl.isUnitClause && cl.unabstrAggressive.isUnitClause) => cl.unabstrAggressive
        }
        else Nil
    }

    Timer.demod.start()

    // Use late check for orderedness (after instantiation)
    val (cl1, touched1) =
      if (util.flags.indexing.value && cls.isInstanceOf[UnitClauseIndex]) {
        IndexSimp.simplifyPos(cl, cls.asInstanceOf[UnitClauseIndex])
      } else { //usual case
        //now filter those units based on whether
        val unitsForDemodulation = (cls.unitClauses ++ newUnits) filter { cl => cl(0) match {
          case l if (!l.isPositive || cl.constraint.nonEmpty) => false
          //a) they are ordered and top operator of LHS appears in operators of cl
          case l @ Lit(_, e @ Eqn(s: FunTerm, _)) if (e.isOrdered) =>
            (cl.operators(s.op))
          //b) unordered and either top term of LHS or RHS appears in operators of cl
          case l @ Lit(_, e @ Eqn(s: FunTerm, t: FunTerm)) =>
            ((cl.operators(s.op)) || (cl.operators(t.op)))
          //c) cases involving variables? x=t, x=y are these ever demodulators? sometimes e.g. f(x) = x
          case l @ Lit(_, Eqn(s, t)) if (s.isVar || t.isVar) =>
            true
          case l =>
            false
        }}
        demodulate(cl, unitsForDemodulation)
      }
    Timer.demod.stop()

    // After demodulation the clause may need abstraction, and cheap simplification could apply
    val cl3 = if (touched1) cl1.abstr else cl1 // cl1.simplifyChea

    val (cl4, touched4) = {
      if (util.flags.indexing.value && cls.isInstanceOf[UnitClauseIndex]) {
        IndexSimp.simplifyNeg(cl3, cls.asInstanceOf[UnitClauseIndex])
      } else {
        negUnitSimp(cl3, (cls.unitClauses ++ newUnits) filter { u => u.constraint.isEmpty && !u(0).isPositive } )
      }
    }

    Timer.reduction.stop()

    Timer.subsumption.start()

    //subsumption requires an abstracted clause
    val cl5 = cl4.abstr

    def isBGS(e: Eqn): Boolean = e match {
      case PredEqn(FunTerm(p, _)) => p.kind == BG
      case eq => eq.sort.kind == BG
    }

    val subRes: Option[List[ConsClause]] = 
      if (util.flags.indexing.value && cls.isInstanceOf[UnitClauseIndex]) {
        if (IndexSimp.unitSubsumption(cl5, cls.asInstanceOf[UnitClauseIndex]))
          Some(Nil) //TODO- indexed subsumption returns a dummy residue here
        else {
          cls.clauses.filter(c => !c.isUnitClause || isBGS(c.lits(0).eqn)).toStream map { _.subsumes(cl5) } find { _.nonEmpty } flatten
        }
      } else {
        cls.clauses.toStream map { _.subsumes(cl5) } find { _.nonEmpty } flatten
      }

    Timer.subsumption.stop()

    if (subRes.nonEmpty) {
      return (subRes.get, true)
    } else if (touched1 || touched4)
      return (cl4.simplifyCheap, true)
  else
    // Have not touched the clause at all
    return (List(cl), false)
  }
}
