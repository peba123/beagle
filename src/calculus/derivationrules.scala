package beagle.calculus

import beagle._
import fol._
import datastructures._
import util._
import bgtheory._
import scala.math._

/**
 * The inference rules Ref, Para, Fact, Define and Split.
 */
object derivationrules {

  /** paper: Equality resolution */
  def infRef(cl: ConsClause) =
    for (
      i ← cl.iEligible intersect cl.iNegLits;
      l = cl(i);
      //if !l.isPositive;
      if (!l.eqn.lhs.isPureBG && !l.eqn.rhs.isPureBG);
      sigma ← (l.eqn.lhs mgu l.eqn.rhs);
      // No! Cannot afford maximality,  as cl(i) could be a selected (non-maximal) literal
      // if sigma(cl).iIsMaximalIn(i)
      sigmaCl = sigma(cl);       //store sigma(cl)
      if (!cl.iEligibleIsMax || sigmaCl.iIsMaximalIn(i));
      // The following test is *not* redundant with the above checks. For example
      // Ref on x+1 /= 1+y is cancelled only with the following test:
      //if (!sigma(l).isPureBG) 
      //if we store sigma(cl), then sigma(l)=sigma(cl)(i)
      if (!sigmaCl(i).isPureBG)
    ) yield {
      stats.nrInfRef += 1
      //Clause(sigma(cl.lits.removeNth(i)), cl.idxRelevant, cl.age + 1, List(cl.id)).abstr
      //use stored sigma(cl)
      sigmaCl
        .removeLit(i)
        .setInfo(cl.info.copy(age = cl.age + 1, inference = ByRef(cl), deltaConjecture = cl.info.deltaConjecture + 1))
        .abstr
    }


  /** paper: equality factoring */
  def infFact(cl: ConsClause) =
    for (
      i1 ← cl.iEligible intersect cl.iPosLits;
      // Scan for the second literal to factorize with
      i2 ← cl.iPosLits;
      if i1 != i2;
      l1 = cl(i1);
      l2 = cl(i2);
      // Need to consider both orientations of both equations.
      // Naming conventions as in the paper
      (l, r) ← List((l1.eqn.lhs, l1.eqn.rhs), (l1.eqn.rhs, l1.eqn.lhs));
      (s, t) ← List((l2.eqn.lhs, l2.eqn.rhs), (l2.eqn.rhs, l2.eqn.lhs));
      if (!l.isPureBG && !s.isPureBG);
      sigma ← l mgu s;
      // if sigma.isSimpleFor(cl.c.vars);
      // experimental
      // if sigma(l1) isMaximalIn sigma(cl.lits);
      if sigma(cl).iIsMaximalIn(i1);
      if !(sigma(r) geq sigma(l));
      if !(sigma(t) geq sigma(s));
      if !sigma(l).isPureBG
    ) yield {
      // Build the factor. First remove l1 and l2
      // println("Factor: " + cl + " on positions " + i1 + " and " + i2)
      //val lits1 = cl.lits.removeNth(i1)
      //val lits2 = lits1.removeNth(if (i2 < i1) i2 else i2 - 1) // Careful here
      //val lits3 = Lit(true, Eqn(l, t)) :: Lit(false, Eqn(r, t)) :: lits2

      // println("*** " + sigma(Clause(OClause(lits3), cl.R, cl.c, cl.idxRelevant)))
      stats.nrInfFact += 1
      //Clause(sigma(lits3), cl.idxRelevant, cl.age + 1, ByFact(cl), cl.deltaConjecture + 1).abstr
      //note that the new info is preserved by later operations
      cl.removeLit(i1 max i2)
        .removeLit(i1 min i2)
        .append(Eqn(l, t).toLit :: Eqn(r, t).toNegLit :: Nil)
        .applySubst(sigma)
        .setInfo(cl.info.copy(age = cl.age + 1, inference = ByFact(cl), deltaConjecture = cl.info.deltaConjecture + 1))
        .abstr
    }

  /**
    * @return all results of one-step superposition from clauses in the clause set cs into the clause cs.
    * Assumes premises are variable-disjoint
    */
  def infSupFromClauses(cs: ListClauseSet, cl: ConsClause) =
    // put in exra file for its size
    paramodulation.supFromClauses(cs, cl)


  /**
    * @return all results of one-step superposition from the clause cs into the clauses in the clause set cs.
    * Assumes premises are variable-disjoint
    */
  def infSupIntoClauses(cl: ConsClause, cs: ListClauseSet) =
    // put in exra file for its size
    paramodulation.supIntoClauses(cl, cs)

  def infChaining(cl: ConsClause, cs: ListClauseSet) =
    // put in exra file for its size
    if (flags.chaining.value)
      chaining.chainingIntoClauses(cl, cs)
    else
      List.empty



  /**
   * @return A new definition for some ground term in the unabstracted version of cl
   * and the given clause demodulated with the new definition, or None if Define is
   * not applicable.
   */

  def infDefine(cl: ConsClause) =
    if (flags.nodefine.value)
      None
    // If we have a GBT Clause set we need to apply Define only to input clauses,
    // which always have an age 0
    else if (main.haveGBTClauseSet && cl.age > 0)
      None
    else if (cl.isDefinition)
      // No need to apply Define on definitions
      None
    else if (util.flags.bgsimp.value >= 2)
      infDefineAggressive(cl)
    else
      infDefineCautious(cl)


  // A cautious Define rule, from the paper
  def infDefineCautious(cl: ConsClause): Option[(ConsClause, Iterable[ConsClause])] = {

    import PMI._

    // Check if the unabstracted version of cl is itself a definition or serves the same purpose,
    // i.e. that the rhs is a pure BG term.
    // Need to avoid the following loop:
    // f(a) = 90
    // --------- Define
    // f(a) = e1
    // e1 = 90
    // --------- Simp
    // f(a) = 90
    // e1 = 90
    // ...
    // That is, must make sure that Define is an instance of Simp, see below
    // This is a useful pre-test but not quite enough:

    // We avoid the loop above by checking if cl.unabstractAggressive is a definition
    // (with the rhs being a pure BG term). This should be good enough to break any loop wrt
    // the implemented simplification techniques.
    // cl.unabstractAggressive._1.lits match {
    //   case List(Lit(true, Eqn(lhs, rhs))) =>
    //     if (lhs.isMinBSFGTerm && rhs.isPureBG)
    //       return None
    //   case _ => ()
    // }

    val sigma = cl.unabstractAggressive._2 // The unabstracting definition

    // In the following for-loop we search for a Define application.
    // It returns after the first application. Simultaneous application is difficult to implement
    // because the positions in the termindex may change be invalidated after an replacement.
    // Specifically the lhs and rhs of an equation might be swapped by a replacement, so that, say, a
    // position in the previous rhs is no linger there because it is the lhs now.

    for ((op, termsAndPos) <- cl.termIndex;
      if op.isBSFGOp;
      (t, pos) <- termsAndPos) {
      // t is a BS-sorted FG term.
      // Instantiate t and see if it is a *minimal* one
      val tsigma = sigma(t)
      if (tsigma.isGround && tsigma.isMinBSFGTerm) {
        //  Make the new definition
        val (Eqn(s, alpha), newOp) = tsigma.asInstanceOf[FunTerm].mkDefinition()
        // The new definition
        //TODO- this might need to inherit the constraint from cl?
        val newDef = Clause(List(Eqn(s, alpha).toLit), ClsInfo(Set.empty, 0, ByDefine(cl), cl.info.deltaConjecture + 1))
        // Check that the clause newDef is to be applied to is greater than newDef:
        // if (cl gtr newDef.abstr) {
        // Unfortunately, the implementation of gtr is not good enough to get this.
        // Although f(X) = c \/ X != 1+1 is gtr than the abstraction of f(1+1) = e
        // this is not obtained by gtr because of the *fresh* variable introduced by abstraction.
        // Hence use the above approximation
        // if (true) {
        // Apply it
        // val newCl = cl.replaceAt(pos, alpha)
        val newCl = cl.modified(inference = ByDefine(cl), deltaConjecture = cl.info.deltaConjecture+1).replaceAt(pos, alpha)
        // Side effect: add new BG domain element to Sigma
        Sigma += newOp
        // touched = true
        // println(s"xx $cl[$pos] -> $t $newCl")

        // If Define is applied to a definition itself we get a tautology:
        stats.nrInfDefine += 1
        // unabstrCautious pays off often, to remove these stray disequations
        // val res = (newCl.unabstrCautious.abstr.modified(age = if (cl.age == 0) 0 else cl.age+1), List(newDef.abstr))
        val res = (newCl.unabstrCautious.abstr, List(newDef.abstr))
        // println("*** Define on " + cl)
        // println("*** Defs      " + res._2)
        // println("*** Clause    " + res._1)
        return Some(res)
        //       }
      }
    }
    // After for loop, didn't find spot to apply define
    return None
  }

// A more aggressive version of Define that destructively applies unabstraction
def infDefineAggressive(cl: ConsClause): Option[(ConsClause, Iterable[ConsClause])] = {

  // def isDefinition(cl: ConsClause) =
  //   // Assume cl is unabstracted
  //   cl.lits match {
  //     case List(Lit(true, Eqn(lhs, rhs))) =>
  //       lhs.isMinBSFGTerm && rhs.isPureBG
  //     case _ => false
  //   }

  // val clUn = cl.unabstrCautious // Aggressive
  val clUn = cl.unabstrAggressive
  val newDefs =
    for (
      t <- clUn.minBSFGTerms;
      // t <- clUn.maxBSFGTerms;
      if t.isGround
      // if !isDefinition(clUn)
    ) yield {
      stats.nrInfDefine += 1
      val (newDefEqn, newOp) = t.asInstanceOf[FunTerm].mkDefinition()
      // Side effect: add new BG domain element to Sigma
      Sigma += newOp
      Clause(List(newDefEqn.toLit), ClsInfo(Set.empty, 0, ByDefine(cl), cl.info.deltaConjecture+1))
    }
  val (newClause, touched) = simplification.demodulate(clUn, newDefs)
  if (touched) {
    val res = (newClause.abstr.modified(age = if (cl.age == 0) 0 else cl.age+1, deltaConjecture = cl.info.deltaConjecture+1), newDefs map { _.abstr })
    // println("*** Define on " + cl)
    // println("*** Defs      " + res._2)
    // println("*** Clause    " + res._1)
    Some(res)
  }
  else
    None
}
 

  /** Splits a clause of the form leftClause \/ rightClause where leftClause and rightClause are
    * variable disjoint into (leftClause, rightClauses).
    * rightClauses = { rightClause, -leftClause } 
    *   if leftClause is ground, otherwise rightClauses = { rightClause } 
    * The rightClauses are returned as a function taking relevant indexes to be set for each one (the
    * relevant indexes cannot be known at splitting time)*/
def infSplit(cl: ConsClause, iDecisionLevel: Int) = {
    if (flags.split.value == "on" ||
      (flags.split.value == "purebgc" && cl.isPureBG) ||
      (flags.split.value == "nopurebgc" && !cl.isPureBG) ||
      // or we have to split because the BG solver needs unit clauses:
      (cl.isBG && solver.needsUnitClauses)) {
      cl.split map { case (leftLits, rightLits) =>
        // println("Split %s into".format(cl))
        stats.nrInfSplit += 1

        val leftClause =
          cl.modified(lits = leftLits,
            idxRelevant = Set(iDecisionLevel),
            inference = BySplitLeft(cl),
            deltaConjecture = cl.info.deltaConjecture + 1)

        //the result function for right hand clauses
        //these are assigned new idxRelevant passed in when the branch is closed
        //leftEmptyClause is used when recording an inference.
        val rightClausesFn = { (idxRelevant: Set[Int], leftEmptyClause: ConsClause) ⇒

          //return the right part of the original split clause,
          //note that it takes the constraint from the empty clause resulting from the last branch
          cl.modified(lits = rightLits, 
                      idxRelevant = idxRelevant ++ cl.idxRelevant,
                      constraint = leftEmptyClause.constraint ++ cl.constraint,
                      inference = BySplitRight(leftEmptyClause, cl), 
                      deltaConjecture = cl.info.deltaConjecture + 1) :: (
            //plus the negated part of the left clause:
            leftLits collect { case l if l.isGround =>
              cl.modified(lits = l.compl :: Nil,
                          idxRelevant = idxRelevant,
                          constraint = leftEmptyClause.constraint ++ cl.constraint,
                          inference = BySplitRight(leftEmptyClause, cl),
                          deltaConjecture = cl.info.deltaConjecture + 1)
              // no abstraction needed
            })
        }

        (List(leftClause), rightClausesFn)
      }
    } else None
}


  /*
def infSplit(cl: ConsClause, iDecisionLevel: Int) = {
    if (flags.split.value == "on" ||
      (flags.split.value == "purebgc" && cl.isPureBG) ||
      (flags.split.value == "nopurebgc" && !cl.isPureBG) ||
      // or we have to split because the BG solver needs unit clauses:
      (cl.isBG && solver.needsUnitClauses))
      cl.split map {
        //case None ⇒ None
        //case Some((leftLits, rightLits)) ⇒ {
	case (leftLits, rightLits) ⇒ {
          // println("Split %s into".format(cl))
          stats.nrInfSplit += 1
          val leftClause = Clause(leftLits, Set(iDecisionLevel), cl.age)
          // fold down all ground literals from rightLits as unit clauses
          val foldDownClauses =
            for (l <- rightLits; if l.isGround) yield
              Clause(List(l.compl), Set(iDecisionLevel), cl.age) // no abstraction needed
          // println("  left clause: %s".format(leftClause))
          val rightClausesFn = {
              (idxRelevant: Set[Int]) ⇒ List(Clause(rightLits, idxRelevant ++ cl.idxRelevant, cl.age))
          }
          (leftClause :: foldDownClauses, rightClausesFn)
        }
      }
    else None
  }
   */

  /* 
def infSplit(cl: ConsClause, iDecisionLevel: Int) = {
    if (flags.split.value == "on" ||
      (flags.split.value == "purebgc" && cl.isPureBG) ||
      (flags.split.value == "nopurebgc" && !cl.isPureBG) ||
      // or we have to split because the BG solver needs unit clauses:
      (cl.isBG && solver.needsUnitClauses))
      cl.split map {
        //case None ⇒ None
        //case Some((leftLits, rightLits)) ⇒ {
	case (leftLits, rightLits) ⇒ {
          // println("Split %s into".format(cl))
          stats.nrInfSplit += 1
          val leftClause = Clause(leftLits, Set(iDecisionLevel), cl.age)
          // println("  left clause: %s".format(leftClause))
          val rightClausesFn = {
            val leftClauseUn = leftClause.unabstrAggressive
            if (leftClauseUn.vars.isEmpty)
              (idxRelevant: Set[Int]) ⇒ Clause(rightLits, idxRelevant ++ cl.idxRelevant, cl.age) ::
                (leftClauseUn.lits map { l ⇒ Clause(List(l.compl), idxRelevant, cl.age).abstr })
            else {
              // println("Split %s into".format(cl))
              // println("  right clause: %s".format(Clause(rightLits, idxRelevant ++ cl.idxRelevant, cl.age)))
              (idxRelevant: Set[Int]) ⇒ List(Clause(rightLits, idxRelevant ++ cl.idxRelevant, cl.age))
            }
          }
          (List(leftClause), rightClausesFn)
        }
      }
    else None
  }
   */




  /**
   * Simple inst rule. Replace a clause with its instances.
   */
  def infInst(c: ConsClause,cs: ConsClause): Option[List[ConsClause]] = {

    throw InternalError("infInst is currently defunct")

    /*
    val cl = c.unabstr
    // Check for flags.formatFlag.value=="tff-fd" as this Instantiation rule applies only for the FD case,
    // but the flag -inst is also use for LIA
    if (!(flags.formatFlag.value=="tff-fd") || !flags.useInst.value || cl.bgVars.size==0 ) None
    //TODO apply QE only to vars which are in BG sorted FG terms
    else Some(bgtheory.FD.ChocoCSP.QE(cl).map(_.makeProtected))
     */

    //Test- one var at a time inst...
    //else Some(bgtheory.FD.ChocoCSP.QE(cl,List(cl.vars.head)).map(_.makeProtected))
    
    //Test2- wild guess, not working :(
    /*else 
      bgtheory.FD.Testing.wildGuess(c, cs) match {
      case Some(newClauseList) if(solver.isConsistent(cs.clauses++(cl::newClauseList))) => Some(cl::newClauseList) 
      //do this as original inst rule is a replacement rule...
      case None => None
    }*/
  }
}
  
