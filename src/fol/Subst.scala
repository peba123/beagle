package beagle.fol

import beagle._
import datastructures.Eqn

final class Subst(val env: Map[Var, Term]) {
  // It might be tempting to mixin collection.Map[Var, Term]
  // however apply and + are just too different to make sense

  def isEmpty = env.isEmpty
  def lookup(v: Var): Option[Term] = env.get(v)
  def actsOn(v: Var) = env.contains(v)// && env(v) != v //it seems subsumption depends on this being defined as 'acted upon'
  def actsOn(vs: Traversable[Var]): Boolean = vs exists { actsOn _ }
  def actsOn[T <: Expression[T]](e: Expression[T]): Boolean = actsOn(e.vars)
  // def getEnv = env

  lazy val dom = env.keys.toSet

  /**
   * Two substitutions are equal if they have the same `env`.
   * This is NOT functional equality, e.g. [X->X,Y->X]!=[Y->X]
   * It may be the case that this does not matter in practise since
   * substitutions of the first form are never made?
   */
/*
  override def equals(any: Any) = any match {
    case that: Subst => 
      (this.env.keys ++ that.env.keys).forall( x => this.env(x)==that.env(x) ) 
    case _ => false
  }
 */

  // The above definition of equals is better, but it is not obvious how
  // to define a hasCode for substitutions, so that two equal substitutions have 
  // the same hashCode
  override def equals(any: Any) = any match {
    case that: Subst => (this.env == that.env)
    case _ => false
  }

  override def hashCode: Int = env.hashCode

  // Use substitutions as functions:
  // Notice that this works for any Expression
  // def apply[T](es: Array[Expression[T]]) = 
  //   if (isEmpty) es.asInstanceOf[Array[T]] else 
  //     es.map(_.applySubst(this))
  def apply[T <: Expression[T]](es: List[Expression[T]]) = 
    if (isEmpty) es.asInstanceOf[List[T]] else 
      es.map(_.applySubst(this))
  def apply[T <: Expression[T]](e: Expression[T]) = 
    if (isEmpty) e.asInstanceOf[T] else 
      e.applySubst(this)

  override def toString = env.mkString("[", ", ", "]")

  /** Extend with one binding - don't check anything */
  def +(xt: (Var, Term)) = new Subst(env + xt)

  /** Composition of substitutions */
  def ++(sigma: Subst) = 
    new Subst((env.mapValues(sigma(_)) // apply sigma to all terms,
      ++
      // extend with all bindings from sigma that env does not act on,
      sigma.env.filter(vt => !env.contains(vt._1)))
      // and delete all trivial bindings
      .filter(vt => vt._1 != vt._2))

  /** Remove bindings for a given list of variables */
  def --(xs: List[Var]) = new Subst(env -- xs)

  /** Whether this substitution is an injective map from variables to variables. */
  def isRenaming: Boolean = {
    env forall {
      case (x,y) =>
      // Check that all terms in the codomain are variables, 
      y.isVar &&
      // and that env is injective,
      (!(env exists { case (x1, y1) => (y == y1) && (x != x1) }))
    }
  }

  /** Combine two unifiers (typically mgus) into one, simultaneous unifier, if possible 
    * Throws Unification.UnifyFail otherwise
    * This is terribly inefficient
    */
  def combine(that: Subst): Subst = {
    me.combineTimer.start()
    // Take this and extend it to a unifier for each binding in that.
    // The assert substitution can be used at most once.
    // This is a consequence of how the assert bindings are made.
    var sigma = this // sigma will be the result
    var binds = that.env.toList
    while (binds.nonEmpty) {
      val bind = binds.head
      binds = binds.tail
      // sigma must also be a unifier of the bindings in that,
      // hence sigma is applied to these bindings before unifying them
      val s = sigma(bind._1)
      val t = sigma(bind._2)
      sigma = sigma ++ unification.unify(s, t)
    }
    me.combineTimer.stop()
    // println(s"$this combine $that = $sigma")
    sigma
  }

  // TYhis is even (much) worse
/*
  def combine(that: Subst): Subst = {
    focdcl.combineTimer.start()
    var ss = List.empty[Term]
    var ts = List.empty[Term]
    optimize {
      for ((v,t) <- this.env) { ss ::= v; ts ::= t }
      for ((v,t) <- that.env) { ss ::= v; ts ::= t }
      }
    unification.unifyList(ss, ts)
  }
 */

  def combineOpt(that: Subst): Option[Subst] =
    try Some(combine(that)) catch { case unification.UnifyFail =>
          // println(s"$this combine $that = FAIL")
      None }

}

object Subst {
  def apply(bindings: (Var, Term)*) = new Subst(bindings.toMap)
  val empty = Subst()

  def combine(σs: Iterable[Subst]) = σs.foldLeft(empty) { _ combine _  }
  def combineOpt(σs: Iterable[Subst]) =
    try Some(combine(σs)) catch { case unification.UnifyFail => None }

  /**
   * Build the cross product of all unifiers in ss1 and ss2
   * by combining their elements
   */
  // def combineSubst(ss1: List[Subst], ss2: List[Subst]): List[Subst] =
  //   for (s1 <- ss1; s2 <- ss2; sigma <- (s1 combine s2)) yield sigma

}

