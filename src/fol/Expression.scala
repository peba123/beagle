package beagle.fol
import beagle._

/**
 * Adds operations which are expected to be applied (and perhaps the same)
 * at many levels different levels; such as formula, term, literal, variable etc.
 */
trait Expression[E <: Expression[E]] {

  import Signature._
  /*
   * Things to be implemented
   */

  /**
   * The free variables in the expression
   */
  val vars: Set[Var]

  /**
   * The application of a substitution to an expression.
   */
  def applySubst(sigma: Subst): E

  /**
   * The sorts of all sub-expressions in this expression not
   * including OSort.
   */
  val sorts: Set[Sort]

  /** The set of all operators in this expression. */
  val operators: Set[Operator]
  /** The depth of this expression */
  val depth: Int
  /** The set of B-sorted functional terms with a top-level foreground symbol */
  val minBSFGTerms: Set[Term]
  /** The set of B-sorted terms with a top-level foreground symbol,
   * which are not subterms of any other BSFG term. */
  val maxBSFGTerms: Set[Term]
  /** The symbolic constants occuring in this */
  val symConsts: Set[SymConst]
  val kind: Expression.Kind // See comment in Expression object

  def isPureBG = kind == Expression.PureBG
  def isImpureBG = kind == Expression.ImpureBG
  def isBG = isPureBG || isImpureBG

  def isFG = kind == Expression.FG

  /**
   * Match this to that under a set of pre-existing substitutions gammas.
   * @return All substitutions that match this to that as an extension
   * of any of the preexisting ones.
   */
  def matchers(that: E, gammas: List[Subst]): List[Subst]

  /**
   * The list of all most general unifiers of this and that.
   * (For terms, the empty set or a singleton, for equations, 0, 1 or 2
   * substitutions.)
   */
  def mgus(that: E): List[Subst]

  def mgu(that: E) = {
    me.mguTimer.start()
    // catch upfront some cheap cases
    if (this.isGround && that.isGround) {
      if (this == that) Some(Subst.empty) else None
    } else (this mgus that) match {
      case Nil          ⇒ me.mguTimer.stop(); me.mguCntNo += 1; None
      case sigma :: Nil ⇒ me.mguTimer.stop(); me.mguCntYes += 1; Some(sigma)
      case _ => throw util.InternalError("mgus: unexpected result")
    }
  }

  override def toString: String

  /*
  * Derived stuff
  */

  lazy val bgVars = vars filter { _.sort.kind == BG }
  lazy val nonBGVars = vars -- bgVars
  lazy val genVars =  vars filter { _.isGenVar }

  lazy val FGOps = (this.operators filter { op => op != TTOp && op.kind == FG })


  def fresh() = applySubst(Term.mkRenaming(vars))
  def freshGenVars() = applySubst(Term.mkRenamingIntoGenVars(vars))

  // The invariant is that all variables in all data structures
  // use indices that are less than or equal to variantCtr.

  /** Skolemize this expression, i.e. replace variables with fresh constants. */
  def sko() = applySubst(Term.mkSkolemSubst(vars))

  /** @return True if there are no free variables. */
  def isGround = (vars.isEmpty)

  // Handy abbreviation.
  def matchers(that: E): List[Subst] = {
    // catch upfront some cheap cases
    if (this.isGround) {
      val res = if (that.isGround && this == that) List(Subst.empty) else List()
      res
  } else matchers(that, List(Subst.empty))
}
/*
  /**
   * Whether this is an expression over linear integer arithmetic, etc
   */
  lazy val isLIA = isBG && (sorts subsetOf Set(IntSort)) && (operators subsetOf liaOps)
  lazy val isLRA = isBG && (sorts subsetOf Set(RatSort)) && (operators subsetOf lraOps)
  lazy val isLFA = isBG && (sorts subsetOf Set(RealSort)) && (operators subsetOf lfaOps)
  lazy val isFD = isBG &&
    bgVars.forall(_.sort.isInstanceOf[IntSubsort]) &&
    (operators forall { o ⇒ (commonIntArithOps(o)) })
  //TODO- an alternative: no equation has Int sort?
*/
  /*
   * Instantiation pre-order 
   */

  /**
   * Variantship. Two Es are variants iff *all* matchers are a renaming.
   * Is this the Right Thing?
   */
  def ~(that: E) = {
    // (!(this matchers that).isEmpty) &&
    // (!(that matchers this).isEmpty)
    (this matchers that) exists { _.isRenaming }
  }

  /** Non-strict instanceship.*/
  def >>~(that: E) = {
    !(this matchers that).isEmpty
  }

  /** Strict instanceship.*/
  def >>(that: E) = {
    //      !(this matchers that).isEmpty &&
    //       (that matchers this).isEmpty 
    // this >~ that && (!that >~ this)

    val sigmas = (this matchers that)
    (!sigmas.isEmpty) && (sigmas forall { !_.isRenaming })
  }

  def standardizeVariables(nextUnused: Int, soFar: Subst): (E, Int, Subst)

  def standardizedVariables: E =
    this.standardizeVariables(0, Subst.empty)._1

}

object Expression {

  /**
   * Kind: The kind of expressions, in particular terms, literals, clauses:
   * One of FG, PureBG, ImpureBG.
   * Used in weak abstraction and indexing.
   * The kinds are linearly ordered as PureBG < ImpureBG < FG. This ordering is used to determine the kind of
   * a term from its subterms by taking their least upper bound (lub) For example, a term containing PureBG and FG terms
   * is itself a FG term.
   */
  sealed abstract class Kind {
    def lub(k: Kind): Kind
  }
  /** Contains some FG operator. */
  case object FG extends Kind {
    def lub(k: Kind) = FG
  }
  /** Only defined BG operators (includes DomElem and parameters) and AbstVar, no GenVar. */
  case object PureBG extends Kind {
    def lub(k: Kind) = k
  }
  /** Over BG signature but may contain GenVars. */
  case object ImpureBG extends Kind {
    def lub(k: Kind) = if (k == FG) FG else ImpureBG
  }

  /**
   * Matching of lists of Expressions. The caller must makes sure that
   * l2 is at least as long as l1, otherwise unexpected thing may happen.
   */
  def matchers[E <: Expression[E]](l1: List[E], l2: List[E], gammas: List[Subst]): List[Subst] = {
    var hl1 = l1 // we loop over l1 and
    var hl2 = l2 // we loop over l2
    var hgammas = gammas
    // the result substitution
    while (!hl1.isEmpty) {
      hgammas = hl1.head.matchers(hl2.head, hgammas)
      hl1 = hl1.tail
      hl2 = hl2.tail
    }
    hgammas
  }

  def lub[E <: Expression[E]](l: List[E]): Kind = l.foldLeft(PureBG: Kind)(_ lub _.kind)

}



 
