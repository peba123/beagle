package beagle.fol

import beagle._
import util._
import datastructures._

abstract class Ordering {

  import Ordering._

  // Ordering need to implement the compare method,
  // which is supposed to return one of { Equal, Greater, Unknown } (but not Less)
  def compare(s: Term, t: Term): OrderingResult

}

object KBO extends Ordering {

  import Ordering._

  val varWeight = 0 // Weight of variables
  val constWeight = 0 // Weight of constants
  val funWeight = 1 // Weight of proper function symbols, must be greater than 0

  /**
   * Weight :
   * w(x) = varWeight, for all variables x
   * w(c) >= varWeight, for all constants c
   * if w(f)=0 for some unary function symbol f then f >=_prec g for all function symbols g
   * where >=_prec is a precedence on the function symbols
   *
   * Definition of KBO:
   *
   * s >_KBO t iff
   * (1) #(x,s) >= #(x,t) for all variables x and w(s) > w(t), or
   * (2) #(x,s) >= #(x,t) for all variables x, w(s) = w(t), and
   *    (a) t = x, s = f^n(x) for some n >= 1, or
   *    (b) s = f(s1,...,sm), t = g(t1,...,tn) and f>g, or
   *    (c) s = f(s1,...,sm), t = f(t1,...,tm) and
   *          (s1,...,sm) (>_KBO)_lex (t1,...,tm)
   *
   * Notice that case (2a) is relevant only if unary function symbols have a weight 0
   */

  // Straight from the definition
  def compare(s: Term, t: Term): OrderingResult =
    // Check variable condition, which is common to both cases
    if (t.vars forall { x ⇒ (s.vars.contains(x)) && s.varsCnt.getOrElse(x, 0) >= t.varsCnt.getOrElse(x, 0) }) {
      val (sw, tw) = (s.weightForKBO, t.weightForKBO)
      if (sw > tw)
        Greater 
      else if (sw == tw) {
        // case (2a) is not needed because unary function symbols have non-zero weight
        (s, t) match {
          case (FunTerm(f, fArgs), FunTerm(g, gArgs)) if (f != g) ⇒
            if (Ordering.gtr_prec(f, g))
              Greater
            else 
              Unknown
          case (FunTerm(f, fArgs), FunTerm(g, gArgs)) if (f == g) ⇒
            compare_lex(fArgs, gArgs)
          // todo: equal variables should result in Equal, no?
          case (_, _) ⇒ Unknown
        }
      } else
        Unknown
    } else
      Unknown

  def compare_lex(ss: List[Term], ts: List[Term]): OrderingResult = {
    // strip off equal prefixes
    var (restSs, restTs) = (ss, ts)
    while (restSs.nonEmpty && (restSs.head == restTs.head)) {
      restSs = restSs.tail
      restTs = restTs.tail
    }
    if (restSs.isEmpty)
      Equal
    else 
      compare(restSs.head, restTs.head)
  }
}

object LPO extends Ordering {

  /**
   * Definition of LPO (similarly as implemented below)
   *
   * s >_LPO t iff
   * (1) t \in vars(s) and t /= s, or
   * (2) s = f(s_1,...,s_m), t = g(t_1,...,t_n) and
   *     (a) s_i >=_LPO t for some i,
   *     (b) f > g and s >_LPO t_j for all j, or
   *     (c) f = g, there is an i such that
   *         (c-1) s_j = t_j for all j < i, 
   *         (c-2) s_i >_LPO t_i, and
   *         (c-3) s > t_j for all j > i
   * 
   * The precedences (see Ordering below) are (must) defined in such a way that all of the following hold:
   * (i)  s >_LPO d for every domain element d and every ground term s that is
   *      not a domain element, and
   * (ii) s >_LPO t for every ground FG term s and every ground BG term t. 
   * We exploit properties (i) and (ii) in the implementation below.
   */

  import Ordering._

  /*
   * Compare two terms with LPO. Returns one of Greater, Equal or Unknown (but not Less)
   */
  def compare(s: Term, t: Term): OrderingResult = {

    // case (a) above:
    def caseA(args: List[Term]) = 
      if (args exists { s =>
        val h = compare(s, t)
        h == Equal || h == Greater
      }) Greater
      else
        Unknown

    // case (i)
    if (t.isDomElem && (s.isFG || s.isInstanceOf[PFunTerm] || s.isInstanceOf[SymConst])) return Greater
    // case (ii)
    if (t.isPureBG && s.isFG) return Greater
    // if t contains a general variable that is not in s then s cannot be greater than t
    if (!(t.genVars subsetOf s.vars)) return Unknown
    return (s, t) match {
      // case 1
      case (s: FunTerm, t: AbstVar) ⇒
        if (s.isFG || (s != t && (t occursIn s)))
          Greater
        else Unknown
// Buggy:
/*        if (s.sort == Signature.OSort || s.isFG || (t occursIn s))
          Greater
        else Unknown
 */
      case (s: FunTerm, t: GenVar) ⇒ 
        if (s != t && (t occursIn s))
          Greater
        else Unknown

      /*
        if ((s.sort == Signature.OSort) || (t occursIn s))
          Greater
        else Unknown
 */
// cases 2
      case (s: FunTerm, t: FunTerm) ⇒ {
        // println("xxx %s\n    %s".format(s, t))
        val (FunTerm(f, fArgs), FunTerm(g, gArgs)) = (s, t)
        if (f == g) {
          // case c
          val m = fArgs.length // == gArgs.length assumed
                               // find the index i
          var (fArgsRest, gArgsRest) = (fArgs, gArgs)
          // (c-1)
          // We use equality (==) to chop of equal arguments
          // instead of calling compare on the pairs of arguments and testing for Equal
          // status, because this is cheaper
          while (fArgsRest.nonEmpty && (fArgsRest.head == gArgsRest.head)) {
            fArgsRest = fArgsRest.tail
            gArgsRest = gArgsRest.tail
          }
          if (fArgsRest.isEmpty)
            Equal
          // println("(c-2) gtr(%s, %s)".format(fArgsRest.head, gArgsRest.head))
          // println("(c-3) gtr(%s, %s)".format(s, gArgsRest.tail))
          else 
            // found the position i
            if (compare(fArgsRest.head, gArgsRest.head) == Greater) { // (c-2)
              if (gArgsRest.tail forall { compare(s, _) == Greater }) // (c-3)
                Greater
              // If f=g, (c-2) holds but (c-3) does not, we don't need to consider case (a),
              // by the same argument as for f>g and (b) below
              else Unknown 
            } else
              // (c-2) and hence (c) does not apply, try case (a)
              caseA(fArgs)
        } else if (Ordering.gtr_prec(f, g)) {
          if (gArgs forall { compare(s, _) == Greater }) // case (b)
            Greater
          else
            Unknown
        }
        // If f>g and case (b) does not apply don't need to consider case (a)
        // Example: f(1,2,3) >? h(1,2,f(1,2,3)) 
        // Suppose f > h and case (b) fails becaise f(1,2,3) /> f(1,2,3)
        // So it is impossible that 1 > f(1,2,3) and same for the other arguments 2 and 3
        // of f(1,2,3)
        else 
          // case (a)
          caseA(fArgs)
      }
      case (_, _) ⇒ Unknown
    }
  }
}

object Ordering {

  sealed abstract class OrderingResult
  case object Equal extends OrderingResult
  case object Less extends OrderingResult
  case object Greater extends OrderingResult
  case object Unknown extends OrderingResult

  /** 
   * Precedence among operators. Let
   * p: predicate symbol
   * f: non-zero arity foreground operator
   * c: foreground constant operator
   * g: non-zero arity background operator
   * a: symbolic background constant operator
   * d: domain element
   * Then p > f > c > g > a > d
   */
  def gtr_prec(op1: Operator, op2: Operator) = {

    // Precedence for BG operators
    def bgPrec = {
      // "Predicates" have precedence over non-predicates:
      if ((op1.sort() == Signature.OSort) && (op1.sort() != Signature.OSort))
        true
      else {
        val (args1len, args2len) = (op1.arity.nrArgs, op2.arity.nrArgs)
        // This emphasizes the replacement of constants by complex terms:
        // DEVELOP
        args1len < args2len || (args1len == args2len && op1.name > op2.name)
        // args1len > args2len || (args1len == args2len && op1.name > op2.name)
      }
    }

    (op1.kind, op2.kind) match {
      case (FG, BG) ⇒ op1 != TTOp // special case: the operator for "true" has to be minimal
      case (BG, FG) ⇒ op2 == TTOp
      // case (FG, FG) ⇒ (op1 != TTOp && op2 == TTOp) || fgPrec 
      case (FG, FG) ⇒ (op1, op2) match {
        case (TTOp, TTOp) => false
        case (_, TTOp) => true
        case (TTOp, _) => false
        case (op1, op2) => Sigma.gtrPrecSimple(op1, op2)
      }
      case (BG, BG) ⇒ (op1.isDomElemOp, op2.isDomElemOp) match {
        case (true, true) ⇒ {
          // just use the names of the domain elements, assuming they are canonical
          // No - can't do that. The canonical ordering on strings is not well-founded
          // op1.name > op2.name
          (op1.name.length > op2.name.length ||
            // break the tie on equal length names by comparing lexicographically
            (op1.name.length == op2.name.length && op1.name > op2.name))
        }
        case (true, false) ⇒ false
        case (false, true) ⇒ true
        case (false, false) ⇒ bgPrec
      }
    }
  }

  /** The global ordering in use */
  private var ordering: Ordering = //{ throw new InternalError("Ordering.ordering not set"); null }
    LPO

  def setOrdering(ord: Ordering) {
    ordering = ord
  }

  // derived methods
  def compare(s: Term, t: Term) = ordering.compare(s, t)
  def gtr(s: Term, t: Term) = ordering.compare(s, t) == Greater
  def geq(s: Term, t: Term) = ordering.compare(s, t) match {
    case Equal | Greater => true
    case _ => false
  }

  // Multiset extension

  def mso_geq(s1: List[Term], s2: List[Term]): Boolean = {
    // assume s1 and s2 are not equal as a multiset

    // How many times does t occur in s ?
    def count(t: Term, s: List[Term]) = s count { _ == t }

    // Straight from the definition
    // Notice that "mso_gtr" would have to test for inequality as well,
    // which is expensive, hence we try to avoid
    s2 forall { y ⇒
      !(count(y, s1) < count(y, s2)) ||
      (s1 exists { x ⇒ (x gtr y) && count(x, s1) > count(x, s2) })
    }
  }


}

