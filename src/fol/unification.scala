package beagle.fol

import beagle._
//import bgtheory.CSP._
import bgtheory.LIA.DomElemInt
import datastructures.Eqn
import Signature._

object unification {

  case object UnifyFail extends Exception
  //case object MatchFail extends Exception


  def unifyList(l1: List[Term], l2: List[Term]): Subst = {
    var hl1 = l1 // we loop over l1 and
    var hl2 = l2 // we loop over l2
    var sigma = Subst.empty // the result substitution
      // invariant: sigma has still to be applied to all terms in hl1 and hl2
    while (!hl1.isEmpty) {
      sigma = sigma ++ unify(sigma(hl1.head), sigma(hl2.head))
      hl1 = hl1.tail
      hl2 = hl2.tail
    }
    sigma
  }

  /** Computes a *simple* mgu if it exists. */
  def unify(s: Term, t: Term): Subst = {

    // Body of unify
    if (s.sort != t.sort)
      // Ill-sorted case. Can come up even in a sortchecked world, when s or t is a variable
      throw UnifyFail
    else
      (s, t) match {
        case (x: AbstVar, y: AbstVar) ⇒ if (x == y) Subst.empty else Subst(x->y)
        case (x: GenVar, y: GenVar)   ⇒ if (x == y) Subst.empty else Subst(x->y)
        case (x: GenVar, y: AbstVar)  ⇒ Subst(x->y)
        case (x: AbstVar, y: GenVar)  ⇒ Subst(y->x)
        case (x: AbstVar, t: FunTerm) ⇒
          if ((!t.isPureBG) || (x occursIn t)) 
            throw UnifyFail
          else {
            // if (!t.isDomElem)
            //   println("Warning: binding a non-domain element pure background term %s to an abstraction variable %s.".format(t, x))
            Subst(x -> t)
          }
        case (x: GenVar, t: FunTerm) ⇒
          if (x occursIn t) throw UnifyFail 
          else Subst(x -> t)
        case (s: FunTerm, y: Var) ⇒ unify(y, s)
        case (s: FunTerm, t: FunTerm) ⇒ {
          val (FunTerm(f, f_args), FunTerm(g, g_args)) = (s, t)
          if (f == g) unifyList(f_args, g_args) else throw UnifyFail
        }
      }
  }
/*
  /**
   * Match s to t under a pre-existing substitution sigma
   * defined so far. sigma is to be extended to the sought matcher.
   */
  def matchTo(s: Term, t: Term, sigma: Subst): Subst = {

    // Matching of lists of terms:
    def matchToList(l1: List[Term], l2: List[Term], sigma: Subst): Subst = {
      var hl1 = l1 // we loop over l1 and
      var hl2 = l2 // we loop over l2
      var hsigma = sigma
      // the result substitution
      while (!hl1.isEmpty) {
        hsigma = matchTo(hl1.head, hl2.head, hsigma)
        hl1 = hl1.tail
        hl2 = hl2.tail
      }
      hsigma
    }

    // Body of matchTo
    if (!(t.sort == s.sort))
      throw MatchFail
    else
      (s, t) match {
        case (x: Var, t: Term) ⇒ {
          if (sigma.actsOn(x))
            // check if the term bound to x is equal to t, if so nothing
            // needs to be done, otherwise fail
            if (sigma(x) == t) sigma else throw MatchFail
          else {              
            // Can do a new binding for x - but make sure it is simple
            x match {
              case _: AbstVar ⇒ if (t.isPureBG) sigma + (x -> t) else throw MatchFail
              case _: GenVar  ⇒ sigma + (x -> t)
            }
          }
        }
        case (s: FunTerm, y: Var) ⇒
          throw MatchFail
        case (s: FunTerm, t: FunTerm) ⇒ {
          val (FunTerm(f, f_args), FunTerm(g, g_args)) = (s, t)
          if (f == g && f_args.length == g_args.length) // allow varyadic
            matchToList(f_args, g_args, sigma)
          else
            throw MatchFail
        }
      }
   }
*/

  def matchTo(s: Term, t: Term, sigma: Subst): Option[Subst] = matchTo(List(s),List(t),sigma)

  @annotation.tailrec
  def matchTo(l1: List[Term], l2: List[Term], sigma: Subst): Option[Subst] =
    if (l1.isEmpty) Some(sigma)
    else if (!(l1.head.sort == l2.head.sort)) None
    else {
      (l1.head, l2.head) match {
        case (x: Var, t: Term) ⇒ {
          if (sigma.actsOn(x))
            // check if the term bound to x is equal to t, if so nothing
            // needs to be done, otherwise fail
            if (sigma(x) == t) matchTo(l1.tail,l2.tail,sigma)
  	    else None
          else {
            // Can do a new binding for x - but make sure it is simple
            x match {
              case _: AbstVar ⇒ if (t.isPureBG) matchTo(l1.tail,l2.tail,sigma + (x -> t))
  				else None
              case _: GenVar  ⇒ matchTo(l1.tail,l2.tail,sigma + (x -> t))
            }
          }
        }
        case (s: FunTerm, y: Var) ⇒ None
        case (s: FunTerm, t: FunTerm) ⇒ {
          val (FunTerm(f, f_args), FunTerm(g, g_args)) = (s, t)
          if (f == g && f_args.length == g_args.length) // allow varyadic
            matchTo(f_args:::l1.tail, g_args:::l2.tail, sigma)
          else
            None
        }
      }
  }

}
