package beagle.fol

import beagle._
import util._
import calculus._
import datastructures._
import Signature._
import PMI._
import bgtheory.LIA._
import collection.mutable.LongMap

//import bgtheory.CSP.BoundedIntSort

abstract class Term extends Expression[Term] with PMI[Term] {
// with Ordered[Term]

  /*
   * Mixin Expression
   */

  import unification._

  def mgus(that: Term) =
    try {
      val sigma = unify(this, that)
      List(sigma)
    } catch {
      case UnifyFail ⇒ List()
    }

  def matchers(that: Term, gammas: List[Subst]) =
    for (
      gamma ← gammas;
      sigma ← matchTo(this,that,gamma)
    ) yield sigma

/*
  def matchers(that: Term, gammas: List[Subst]) =
    for (
      gamma ← gammas;
      sigma ← try {
        Some(matchTo(this, that, gamma))
      } catch {
        case MatchFail ⇒ None
      }
    ) yield sigma
*/
  /*
   * Mixin Ordered
   */

  // def compare(that: Term) = this.toString compareTo that.toString

  /*
   * Abstract members
   */

  def iteExpanded: (Term, List[(Var, ITE)])
  def elimNonLinearMult: Term

  val depth: Int // The depth of the term
  val sort: Sort // The result sort of the term

  /** Preorder traversal of this term as a tree */
  val preorder: List[VarOrOp]

  /** The number of occurrences of a given variable in this */
  val varsCnt: Map[Var, Int]
  /** The weight of this, as used in KBO */
  val weightForKBO: Int

  def replace(lhs: FunTerm, rhs: Term): Term
  // lhs is a flat term over pairwise different variables, something like f(X,Y)
  // replace binds all occurrences of f-terms in this to the lhs via a matcher σ
  // and replaces all these occurrences by the corresponding rhsσ
  def replace(lhs: Atom, rhs: Formula): Term // Only used for ite's 

  /** An iterator over all (improper) subterms of this */
  def subterms: Iterator[Term]

  /**
   * Use cases of mgus and matchers.
   * The result of mgus and matchers is always empty or a singleton
   */
/*  def mgu(that: Term) =
    (this mgus that) match {
      case Nil          ⇒ None
      case sigma :: Nil ⇒ Some(sigma)
      case _ => throw InternalError("mgus: unexpected result")
    }
 */
  
  def matcher(that: Term) =
    (this matchers that) match {
      case Nil          ⇒ None
      case sigma :: Nil ⇒ Some(sigma)
      case _ => throw InternalError("mgus: unexpected result")
    }

  def isFlat = depth <= 1
  /*
  def isFGOperatorTerm = this match {
    case FunTerm(Operator(FG, _, _), _) ⇒ true
    case _                              ⇒ false
  }

  def isBGOperatorTerm = this match {
    case FunTerm(Operator(BG, _, _), _) ⇒ true
    case _                              ⇒ false
  }
*/
  // todo: check which of the following is really needed
  def isVar = isInstanceOf[Var]
  def isAbstVar = isInstanceOf[AbstVar]
  def isGenVar = isInstanceOf[GenVar]
  def isSymConst = isInstanceOf[SymConst]
  def isDomElem = isInstanceOf[DomElem[_]]
  def isDomElemInt = isInstanceOf[bgtheory.LIA.DomElemInt] //|| isInstanceOf[DomElemFD]
  def isDomElemRat = isInstanceOf[bgtheory.LRA.DomElemRat]
  def isDomElemReal = isInstanceOf[bgtheory.LFA.DomElemReal]

  def isBSFGTerm = this match {
    case FunTerm(op, _) ⇒ op.isBSFGOp
    case _                ⇒ false
  }

  def isNLPPTerm = this match {
    case PFunTerm(bgtheory.LIA.NLPPOpInt, _) ⇒ true
    case PFunTerm(bgtheory.LRA.NLPPOpRat, _) ⇒ true
    case PFunTerm(bgtheory.LFA.NLPPOpReal, _) ⇒ true
    case _                ⇒ false
  }

  /**
    * isMinBSFGTerm: this is a minimal BS-sorted FG term:
    * - its toplevel operator is BS-sorted and FG
    * - non of its proper subterms is made with a BS-sorted and FG operator
    */
  def isMinBSFGTerm = this match {
    case FunTerm(op, args) ⇒
      op.isBSFGOp &&
      (args forall { arg => arg.operators forall { op => ! op.isBSFGOp } })
    case _ => false
  }

  // The weight of the term, used for selecting the next clause
  /* private def weightFun: Int = 
    if (isPureBG) 1 // Pure background terms never participate atively in inferences 
    else if (isVar) 1
    else {
      val NonDomElemFunTerm(op,args) = this
      1 + args.foldLeft(0)(_+ _.weight)
    }
    */
  def weightFun: Int = this match {
    case _: Var => 1
    // case d: bgtheory.LIA.DomElemInt => math.abs(d.value) // not good at all ...
    case d: DomElem[_] => {
      val dsize = {
        val h = d.toString
        // Don't like negative numbers
        // (if (h(0) == '-') h.length+2 else h.length)
        h.length
      }
      // dsize
      if (dsize < 3)
        dsize
      else (util.flags.domElemWeightMultiplier.value * dsize)
      // else dsize * 15
    }
    case c: SymConst => 
      c.op.skolemCtr match {
        case None => 1
        case Some(ctr) => ctr / 10 + 1 
      }
      // Penalize nlpp terms
    // case PFunTerm(_:NLPPOp, args) => 1 + args.foldLeft(0)(_+ _.weight)
    case PFunTerm(_, args) => 1 + args.foldLeft(0)(_+ _.weight)
  }
  lazy val weight = weightFun
    
  // Convenience methods
  def gtr(that: Term) = Ordering.gtr(this, that)
  def geq(that: Term) = Ordering.geq(this, that)
  def compare(that: Term) = Ordering.compare(this, that)

  /**
   * Replace a particular subterm everywhere in an expression.
   * TODO- could add this to the PMI interface.
   */
  def replaceSubterm(subterm: Term, replacement: Term): Term = {
    //var working = this
    this.subTermsWithPos
	  .filter(_._1==subterm)
	  .foldLeft(this)((acc,p) => acc.replaceAt(p._2,replacement))
	  //.foreach(p => working=working.replaceAt(p._2,replacement))
    //return working
  }

  /** @return The term at the given position, or None if there is no such position. */
  def get(p: Pos): Term = p match {
    case Nil => this
    case i :: is => this match {
      case PFunTerm(_, args) if (0 <= i && i < args.length) => 
        args(i).get(is)
      case _ => throw new IndexOutOfBoundsException()
    }
  }
  
  import bgtheory.LIA.cooper._

  lazy val asPolynomial: Polynomial = {
    assume { sort == IntSort }
    Polynomial.toPolynomial(this, allowImproper = true)
  }

  lazy val canonical: Term = {
    if (sort == IntSort)
      asPolynomial.toTerm
    else this match {
      case PFunTerm(op, args) =>
        // Can canonicalize the arguments
        PFunTerm(op, args map { _.canonical })
      case t => t // Foreground or non-integer sorted constants
    }
  }

  lazy val toTPOT = me.TPOT(preorder)


  lazy val simplifyBG =
    if (sort == IntSort && flags.bgsimp.value >= 2) {
      if (main.inPreprocessing)
        // we do a little more, to get rid off all these is_int, to_int etc
        bgtheory.solver.simplify(canonical)
      else
        canonical
    } else
      bgtheory.solver.simplify(this)

}
/* 
 * In variables we keep their sort explicitly - makes life so much easier
 */

abstract class Var extends Term with VarOrSymConst with VarOrOp {

  // val subTermsWithPos = PMI.empty
  val termIndex = TermIndex.empty
  // val ops = Set.empty[Operator]
  def replaceAt(pos: Pos, t: Term) = null
  def replace(lhs: FunTerm, rhs: Term) = this
  def replace(lhs: Atom, rhs: Formula) = this

  // Abstract members
  val name: String
  val index: Int

  val depth = 0
  val varsCnt = Map(this -> 1)
  val vars = Set(this)
  val weightForKBO = KBO.varWeight
  val symConsts = Set.empty[SymConst]
  val minBSFGTerms = Set.empty[Term]
  val maxBSFGTerms = Set.empty[Term]
  val sorts = Set(sort)
  val operators = Set.empty[Operator]

  val preorder = List(this)

  //  val tlKind = None

  val thisvar = this
  def subterms = new Iterator[Term] {
    // delivers just this
    var gotit = false; def next() = { gotit = true; thisvar }; def hasNext = !gotit
  }
  def freshVar(): Var
  def freshGenVar(): GenVar
  lazy val iteExpanded = (this, List.empty)
  lazy val elimNonLinearMult = this

  override def fresh() = freshVar() // override fresh() in Expression - gives a variable instead of a term

  // def normalize = this

  // Mixin Expression
  def applySubst(s: Subst) =
    s.lookup(this).getOrElse(this)

  def occursIn(t: Term): Boolean =
    // todo: use this ∈ t.vars instead?
  t match {
      case y @ Var(_, _, _)   ⇒ this == y
      case FunTerm(fun, args) ⇒ args exists { this occursIn (_) }
    }
  
  override def toString = printer.varToString(this) //toStringWithSort
}

object Var {
  def unapply(t: Term) = t match {
    case AbstVar(name, index, sort) ⇒ Some((name, index, sort))
    case GenVar(name, index, sort)  ⇒ Some((name, index, sort))
    case _                          ⇒ None
  }
}

case class AbstVar(name: String, index: Int, sort: Sort) extends Var {
  assume(sort.kind == BG, "AbstVar: sort must be BG")
  val kind = Expression.PureBG
  def freshVar() = AbstVar(name, Term.variantCtr.next(), sort)
  def freshGenVar() = GenVar(name, Term.variantCtr.next(), sort)
  // val isBG = true
  // val isPureBG = true
  // val isPureFG = false // Do we ever need this?

  def standardizeVariables(nextUnused: Int, soFar: Subst) =
    soFar.lookup(this) match {
      case None =>
        // Need a new name for this
        val v = AbstVar("X", nextUnused, sort)
        (v, nextUnused + 1, soFar + ((this -> v)))
      case Some(v) => (v, nextUnused, soFar)
    } 
}

object AbstVar {
  //doesn't make sense
  //def apply(name: String) = new AbstVar(name, 0, Signature.ISort)
  def apply(name: String, sort: Sort) = new AbstVar(name, 0, sort)
}

case class GenVar(name: String, index: Int, sort: Sort) extends Var {
  def freshVar() = GenVar(name, Term.variantCtr.next(), sort)
  def freshGenVar() = freshVar()
  val kind = if (sort.kind == BG) Expression.ImpureBG else Expression.FG
  // val isBG = sort.kind == BG
  // val isPureBG = false
  // val isPureFG = sort.kind == FG // todo: Do we ever need this?

  def standardizeVariables(nextUnused: Int, soFar: Subst) =
    soFar.lookup(this) match {
      case None =>
        // Need a new name for this
        val v = GenVar("X", nextUnused, sort)
        (v, nextUnused + 1, soFar + ((this -> v)))
      case Some(v) => (v, nextUnused, soFar)
    } 

}
object GenVar {
  def apply(name: String) = new GenVar(name, 0, Signature.ISort)
  def apply(name: String, sort: Sort) = new GenVar(name, 0, sort)
}


case class Let(bindings: List[(Var, Term)], body: Term) extends Term {
    // As usual, unintended capturing of variables in the range of sigma by quantified
  // variable must be avoided. Renaming away the quantified variable before applying sigma
  // solves the problem.
  // val letVars = bindings map { _._1 }
  // val letTerms = bindings map { _._2 }
  val (letVars, letTerms) = bindings.unzip
  lazy val vars = (body.vars -- letVars) ++ letTerms.vars // todo: check: is this the Right Thing?

  lazy val preorder = body.preorder
  
  val termIndex = null
  val ops = null
  def replaceAt(pos: Pos, t: Term) = null

  lazy val iteExpanded = {
    // This is a bit cryptic
    val h = bindings map { _._2.iteExpanded }
    val b = body.iteExpanded
    ( Let((bindings map { _._1 }) zip (h map { _._1 }), b._1), 
      (h map { _._2 }).flatten ::: b._2)
  }
  lazy val elimNonLinearMult = 
    Let(letVars zip (letTerms map { _.elimNonLinearMult }), body.elimNonLinearMult)

  // val varsCnt = body.varsCnt
  // todo not correct: need to combine with Term.varsCnt(letTerms)
  // val varsCnt = letVars.foldLeft(Term.combineCnts(body.varsCnt, Term.varsCnt(letTerms)))(_.updated(_, 0))
  // new version without Terms.varsCnt
  lazy val varsCnt = 
    //first combine body and letTerms varsCnt  
    //then set all letVars to be 0
    (body::letTerms).foldLeft(Map[Var,Int]())( 
      (varsCnt,subterm) => varsCnt ++ (subterm.varsCnt.transform((x,c) => varsCnt.getOrElse(x,0)+c))
    ) ++
    letVars.map((_->0))

  lazy val kind = body.kind
  lazy val operators = body.operators ++ letTerms.operators
  val symConsts = body.symConsts ++ 
  { var s = Set.empty[SymConst]
    letTerms foreach { t => s ++= t.symConsts }
    s
  }
  // the following is rather approximative, but we eliminate lets anyway,
  // and so this does not matter
  val depth = body.depth + 1
  val sorts = body.sorts
  def subterms = body.subterms
  val minBSFGTerms = body.minBSFGTerms
  val maxBSFGTerms = body.maxBSFGTerms
  val weightForKBO = body.weightForKBO
  val sort = body.sort
  //TODO- this fits with the above, correct?

  def applySubst(sigma: Subst) = {
    val rho = Term.mkRenaming(letVars)
    //      QuantForm(q, rho(xs), sigma(rho(f)))
    Let(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], sigma(t)) }, sigma(rho(body)))
  }
  def replace(lhs: FunTerm, rhs: Term) = {
    val rho = Term.mkRenaming(letVars)
    Let(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], t.replace(lhs, rhs)) }, rho(body).replace(lhs, rhs))
  }

  def replace(lhs: Atom, rhs: Formula)= {
    val rho = Term.mkRenaming(letVars)
    Let(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], t.replace(lhs, rhs)) }, rho(body).replace(lhs, rhs))
  }

  override def toString = printer.letTermToString(this)

  def standardizeVariables(nextUnused: Int, soFar: Subst) = ??? // never need this

}

/**
 * Has a head operator 'op' and a list of terms 'args' which may be empty.
 */
abstract class FunTerm extends Term {
  // Abstract members
  val op: Operator
  val args: List[Term]

  //derived
  lazy val preorder: List[VarOrOp] = op :: args.flatMap(_.preorder)

  lazy val maxBSFGTerms: Set[Term] =
    if (op.kind==FG && sort.kind==BG) Set(this)
    else {
      val (bsfg,other) = args.partition(_.isBSFGTerm)
      bsfg.toSet ++ (other.map(_.maxBSFGTerms).flatten.toSet)
      // (bsfg ::: (other.map(_.maxBSFGTerms).flatten)).toSet
    }

  // Get the (result) sort of a given term t.
  lazy val sort = op.sort(args.map(_.sort))

  lazy val kind =
    if (op.kind == FG)
      Expression.FG
    else
      // op is a background operator, the lub of the arguments gives the result. 
      // Notice if args.isEmpty we have PureBG
      Expression.lub(args)

  lazy val sorts: Set[Sort] = if (sort == Signature.OSort) args.sorts else (args.sorts + sort)
  lazy val operators = args.operators + op
  override def toString =
    this match {
      case LitToTerm(lit) => lit.toString
      case _ => op.formatFn(args)
    }

  /*
   * Whether we have created a definition for this already
   */
  // This is an utterly unreliable definition: the mere presence of rhsName in defineMap doesn't say anything.
  // The defineMap is global, the definition culd have been inserted only after a left split and we're currently in the right split.
/*  def haveDefinition = {
    assume(vars.isEmpty && op.kind == FG && sort.kind == BG, "haveDefinition: need a ground B-sorted term with an FG-operator")
    val rhsName = printer.skolemEltPref+"'" + toString + "'"
    Term.defineMap.isDefinedAt(rhsName)
  }
 */

  /**
   * Return an equation that says that this is equal to some value;
   * Also return the freshly created operator
   */
  def mkDefinition() = {
    assume(vars.isEmpty && op.kind == FG && sort.kind == BG, "mkDefinition: need a ground B-sorted term with an FG-operator")
    val rhsName = "'#skE_" + toString + "'"
    val ctr = Term.defineMap.get(rhsName) match {
      case Some(ctr) => ctr
      case None => { 
        // have to invent a new short name (an int) and remember it
        val ctr = Term.defineCtr.next()
        Term.defineMap += ((rhsName, ctr))
        ctr
      }
    }  
    val rhsShortName = "#skE_" + ctr
    // val rhs = new SymConst(Operator(sort.kind, "#e_'" + toString + "'", Arity0(sort))) 
    val newOp = new Operator(BG, rhsShortName, Arity0(sort)) {
      override val skolemCtr = Some(ctr)
    }
    (Eqn(this, SymConst(newOp)), newOp)
  }

}

// Sometimes the decomposition comes in handy
object FunTerm {
  def unapply(t: Term) = t match {
    case Const(op)          ⇒ Some((op, List.empty))
    case PFunTerm(op, args) ⇒ Some((op, args))
    case _                  ⇒ None
  }
}

object NonDomElemFunTerm {
  def apply(op: Operator, args: List[Term]) =
    if (args.isEmpty) SymConst(op) 
    else PFunTerm(op, args)

  def unapply(t: Term) = t match {
    case _ if (t==TT)        => None
    case SymConst(op)       ⇒ Some((op, List.empty))
    case PFunTerm(op, args) ⇒ Some((op, args))
    case _                  ⇒ None
  }
}

/** Proper functional terms, i.e. not constants. */
case class PFunTerm(op: Operator, args: List[Term]) extends FunTerm {

  require(!args.isEmpty, println("Attempt to construct a PFunTerm without arguments"))

  // Mixin Expression

  def applySubst(sigma: Subst) = 
    if (sigma actsOn this) PFunTerm(op, sigma(args)) else this

  def replace(lhs: FunTerm, rhs: Term) = {
    val argsReplaced = PFunTerm(op, args map { _.replace(lhs, rhs) })
    lhs matchers argsReplaced match {
      case Nil => 
        if (lhs.asInstanceOf[FunTerm].op == op && lhs.asInstanceOf[FunTerm].args.length == args.length)
          throw GeneralError("let-binder: cannot apply definition %s = %s to atom %s".format(lhs, rhs, this))
        else argsReplaced
      case List(sigma) => sigma(rhs)
      case _ => throw InternalError("Unexpected matching result in replace.")
    }
  }

  lazy val iteExpanded = {
    val h = args map { _.iteExpanded }
    ( PFunTerm(op, h map { _._1 }), (h map { _._2 }).flatten )
  }

  /*
   * Eliminate all non-linear multiplications s * t by #nlpp(s, t)
   * This is done during preprocessing.
   */
  lazy val elimNonLinearMult = {
    import bgtheory._

    // Calling simplifyBG here prevents terms of the form e.g 3(4x) or (3+4)x having multiplication
    // replaced.
    // val argsElim = args map { _.elimNonLinearMult }
    val argsElim = args map { _.simplifyBG.elimNonLinearMult }
    lazy val defaultRes = PFunTerm(op, argsElim)

    argsElim match {
      // Giving a stronger guard here doesn't prevent the case above because
      // weakAbstraction can apply to give a nonlinear term.
      case List(l, r) if (!l.isDomElem && !r.isDomElem) =>
        // need to check if op is multiplication; if so replace
        op match {
          case LIA.ProductOpInt => PFunTerm(LIA.NLPPOpInt, argsElim)
          case LRA.ProductOpRat => PFunTerm(LRA.NLPPOpRat, argsElim)
          case LFA.ProductOpReal => PFunTerm(LFA.NLPPOpReal, argsElim)
          case _ => defaultRes
        }
      case _ => defaultRes
    }
  }


  def replace(lhs: Atom, rhs: Formula) =
    PFunTerm(op, args map { _.replace(lhs, rhs) })

  lazy val varsCnt = //Term.varsCnt(args)
    args.foldLeft(Map[Var,Int]())( 
      (varsCnt,subterm) => varsCnt ++ (subterm.varsCnt.transform((x,c) => varsCnt.getOrElse(x,0)+c))
    )
  lazy val vars = args.vars
  lazy val symConsts = args.symConsts
  lazy val weightForKBO = args.foldLeft(KBO.funWeight)((acc, h) ⇒ acc + h.weightForKBO)

  lazy val minBSFGTerms = {
    // val argsBSFGTerms = Term.minBSFGTerms(args)
    val argsBSFGTerms = args.minBSFGTerms
    if (op.kind == FG && sort.kind == BG && argsBSFGTerms.isEmpty)
      // this is the minimal B-sorted FG Term we are looking form
      Set[Term](this)
    else
      argsBSFGTerms
  }

  // lazy val ops = opsUnion(args map { _.ops }) + op
  lazy val termIndex = {
    val argsIndex = liftIndex(args map { _.termIndex })
    argsIndex + (op, List(this ->  List.empty))
  }

  def replaceAt(pos: Pos, t: Term) = 
    if (pos.isEmpty) t 
    else 
      PFunTerm(op, replaceAtList(args, pos, t))

  def thisterm = this
  // An iterator over just this
  def thisiterator = new Iterator[Term] {
    // delivers just this
    var gotit = false; def next() = { gotit = true; thisterm }; def hasNext = !gotit
  }
  def subterms = args.foldLeft[Iterator[Term]](thisiterator)((i: Iterator[Term], t: Term) ⇒ i ++ t.subterms)
  // The depth of a compound term is the max of its arguments plus 1.
  // That is, we take the tree depth
  lazy val depth = (args map { _.depth }).max + 1

  def standardizeVariables(nextUnused: Int, soFar: Subst) = {
    val (argsStandardized, nu, sf) = args.standardizeVariables(nextUnused, soFar)
    // (PFunTerm(op, argsStandardized), nu, sf)
    (this, nextUnused, soFar)
    // (PFunTerm(op, argsStandardized), nu, sf)
  }
}

case class ITE(cond: Formula, thenTerm: Term, elseTerm: Term) extends Term {

  def applySubst(sigma: Subst) = 
    if (sigma actsOn this) ITE(sigma(cond), sigma(thenTerm), sigma(elseTerm)) else this

  def replace(lhs: FunTerm, rhs: Term) = 
    ITE(cond.replace(lhs, rhs), thenTerm.replace(lhs, rhs), elseTerm.replace(lhs, rhs))
  def replace(lhs: Atom, rhs: Formula) = 
    ITE(cond.replace(lhs, rhs), thenTerm.replace(lhs, rhs), elseTerm.replace(lhs, rhs))

  lazy val iteExpanded = {
    val condEx = cond.iteExpanded
    val thenTermEx = thenTerm.iteExpanded
    val elseTermEx = elseTerm.iteExpanded
    val h = ( ITE(condEx, thenTermEx._1, elseTermEx._1), 
              thenTermEx._2 ::: elseTermEx._2 )
    // Now replace h._1 by a fresh variable and append that binding to h._2
    val x = GenVar("x", thenTerm.sort).fresh()
    (x, (x -> h._1) :: h._2)
  }
  lazy val elimNonLinearMult = ITE(cond.elimNonLinearMult, thenTerm.elimNonLinearMult, elseTerm.elimNonLinearMult)


  lazy val varsCnt = { throw new Exception("ITE.varsCnt not implemented") } // never used
  lazy val vars = cond.vars ++ thenTerm.vars ++ elseTerm.vars
  lazy val weightForKBO = 0 // never used 
  lazy val symConsts = cond.symConsts ++ thenTerm.symConsts ++ elseTerm.symConsts
  val kind = Expression.FG
  // This doe not work, gives an error in parsing. This is, because predicates defined in let-binders
  // appear only temporarily in the signature.
  // val operators = cond.operators ++ thenTerm.operators ++ elseTerm.operators
  val operators = Set.empty[Operator] 
  val sorts = cond.sorts ++ thenTerm.sorts ++ elseTerm.sorts
  val sort = thenTerm.sort
  val preorder = null // never used

  lazy val minBSFGTerms = { throw new Exception("ITE.minBSFGTerms not implemented") } // never used
  lazy val maxBSFGTerms = { throw new Exception("ITE.maxBSFGTerms not implemented") } // never used

  lazy val termIndex = null // never used

  def replaceAt(pos: Pos, t: Term) = { throw new Exception("ITE.replaceAt not implemented") } // never used

  def subterms = null // never used 
  lazy val depth = 0 // never used

  override def toString = "$ite_t(%s, %s, %s)".format(cond, thenTerm, elseTerm)

  def standardizeVariables(nextUnused: Int, soFar: Subst) = ???

}


/** Need to distinguish symbolic constants from domain elements, the latter have a value attached */
abstract class Const extends FunTerm {
  // Abstract members
  val args = List.empty
  val weightForKBO = KBO.constWeight // overridden by domain elements, see below
  // Oops, we had:
  // override lazy val operators = Set.empty[Operator]
  // constants don't count as operators. It would make sense though to distinguish between
  // parameters and originally defined symbolic BG constants

  val varsCnt = Map.empty[Var, Int]
  val vars = Set.empty[Var]
  def applySubst(sigma: Subst) = this
  // def normalize = this

  def standardizeVariables(nextUnused: Int, soFar: Subst) = (this, nextUnused, soFar)

  lazy val iteExpanded = (this, List.empty)
  lazy val elimNonLinearMult = this

  val thisconst = this
  def subterms = new Iterator[Term] {
    // delivers just this
    var gotit = false; def next() = { gotit = true; thisconst }; def hasNext = !gotit
  }

}

// For desctructing FunTerms , comes handy
object Const {
  def unapply(t: Term) = t match {
    case s: SymConst   ⇒ Some(s.op)
    case d: DomElem[_] ⇒ Some(d.op)
    case _             ⇒ None
  }
}

// Symbolic constants
case class SymConst(op: Operator) extends Const with VarOrSymConst {
  
  val symConsts = Set(this)

  val depth = 0

  lazy val minBSFGTerms =
    if (op.kind == FG && sort.kind == BG) Set[Term](this) else Set.empty[Term]

  // lazy val subTermsWithPos = 
  //   if (this == TT) PMI.empty else List(this -> List.empty)
  // lazy val ops = 
  //   if (this == TT) Set.empty[Operator] else Set(op)

  lazy val termIndex = 
    if (this == TT) TermIndex.empty else TermIndex(op -> List((this, List.empty)))

  def replaceAt(pos: Pos, t: Term) = {
    assume(pos == Nil)
    t
  }

  def replace(lhs: FunTerm, rhs: Term) = 
    if (lhs == this) rhs else this

  def replace(lhs: Atom, rhs: Formula) = this

}

// Handy ways to construct symbolic constants
object FGConst {
  def apply(name: String, sort: Sort) = SymConst(Operator(FG, name, Arity0(sort)))
  def apply(op: Operator) = {
    require(op.kind == FG, println("FGConst application with non-FG operator"))
    SymConst(op)
  }
  def unapply(t: Term) = t match {
    case SymConst(op @ Operator(FG, _, _)) ⇒ Some((op))
    case _                                 ⇒ None
  }
}

object BGConst {
  def apply(name: String, sort: Sort) = SymConst(Operator(BG, name, Arity0(sort)))
  def apply(op: Operator) = {
    require(op.kind == BG, println("BGConst application with non-BG operator"))
    SymConst(op)
  }
  def unapply(t: Term) = t match {
    case SymConst(op @ Operator(BG, _, _)) ⇒ Some((op))
    case _                                 ⇒ None
  }
}

abstract class DomElem[T] extends Const {
  // Abstract members
  val op: Operator
  val value: T
  val depth = 0

  // val subTermsWithPos = PMI.empty
  val termIndex = TermIndex.empty
  // val ops = Set.empty[Operator]
  def replaceAt(pos: Pos, t: Term) = null
  def replace(lhs: FunTerm, rhs: Term) = this
  def replace(lhs: Atom, rhs: Formula) = this
  // notice the TPTP sort can be obtained by the sort method
  val symConsts = Set.empty[SymConst]
  val minBSFGTerms = Set.empty[Term]
  override def toString = printer.domElemToString(this) //value.toString
  
}

/**
 * Other view on FunTerm
 */
object FGFunTerm {
  def apply(op: Operator, args: List[Term]) = {
    require(op.kind == FG, println("FGFunTerm application with non-FG operator"))
    if (args.isEmpty) SymConst(op) else PFunTerm(op, args)
  }
  def unapply(t: Term) = t match {
    case FGConst(op)                             ⇒ Some((op, List.empty))
    case PFunTerm(op @ Operator(FG, _, _), args) ⇒ Some((op, args))
    case _                                       ⇒ None
  }
}


/**
 * Contains generic methods for terms
 */
object Term {

  import scala.collection.mutable.HashMap
  val defineCtr = new util.Counter
  val variantCtr = new util.Counter()

  var defineMap = HashMap.empty[String, Int]
  def resetDefineMap() {
    defineMap = HashMap.empty
  }

  /** Create a renaming substition from vars to a set of fresh variables, preserving kind */
  def mkRenaming(vars: Iterable[Var]) =
    new Subst((vars map { x ⇒ (x, x.freshVar()) }).toMap)

  /** Renaming substitution into ordinary variables */
  def mkRenamingIntoGenVars(vars: Iterable[Var]) =
    new Subst((vars map { x ⇒ (x, x.freshGenVar()) }).toMap)

  /*
   * Skolemizing substitutions
   */

  val skoCtr = new util.Counter

  /**
   * Substitution that replaces variables by fresh constants
   * The variables will provide the sort, the kind will be that of the variable.
   * JOSH- now adds the new skolem operator to the signature
   */
  def mkSkolemSubst(vars: Iterable[Var]) =
    new Subst(
      (for(x <- vars) yield {
	val newSko = SymConst(Operator(x.sort.kind, "$sk_" + skoCtr.next(), Arity0(x.sort)))
	//must add the skolem operator to the global signature.
	Sigma+=newSko.op
	(x -> newSko)
      }).toMap)
    //new Subst((vars map { x ⇒ (x -> SymConst(Operator(x.sort.kind, "$sk_" + skoCtr.next(), Arity0(x.sort)))) }).toMap)

  /**
   * @return a Substitution replacing every variable with *the default* Skolem element
   * from the appropriate sort.
   */
  def mkSkolemElementSubst(vars: Iterable[Var]) =
    new Subst((vars map { x ⇒ (x -> x.sort.someSkolemElement) }).toMap)

  /**
   * Compute the varsCnt of the list of terms ts
   * This is now done locally for PFunTerms and LetTerms- otherwise get GCOverflow when making lots of terms.
   */
  // def varsCnt(ts: List[Term]): Map[Var,Int] = {
  //   var res = Map.empty[Var, Int]
  //   // Go over all ts and accumulate the result
  //   for (
  //     t ← ts;
  //     (x, m) ← t.varsCnt //for each tuple (x,m) in varsCnt of t (is varsCnt lazy for t?)
  //   ) {
  //     res.get(x) match {
  //       case None    ⇒ res = res + (x -> m) //if x not already in res, add (x->m)
  //       case Some(n) ⇒ res = res.updated(x, m + n) //otherwise add count to value and store
  //     }
  //   }
  //   res
  // }

  // def combineCnts(m1: Map[Var, Int], m2: Map[Var, Int]) = {
  //   var res = Map.empty[Var, Int]
  //   for (
  //     (x, m) ← m1
  //   ) {
  //     res.get(x) match {
  //       case None    ⇒ res = res + (x -> m)
  //       case Some(n) ⇒ res = res.updated(x, m + n)
  //     }
  //   }
  //   res
  // }

  /** Extend xs with a given list of sortd vars */
  def add(vars: List[Var], vs: Iterable[Var]) = {
    var res = vars
    for (v ← vs) {
      if (res exists { x ⇒ (x.name == v.name) && (x.index == v.index) && (x.sort != v.sort) })
        throw SyntaxError("double declaration of variable " + v)
      else
        res ::= v
    }
    res
  }

  // def pair(s: Term, t: Term) = PFunTerm("$pair", List(s, t))

  /** Decompose a term into a list of (sub)term and position pairs.
    * Note that this includes variables but `subTermsWithPos` defined using `PMI`, does not.
    * @todo possibly just modify the definition of subTermsWithPos?
    */
  def allSubtermsWithPos(t: Term, acc: Pos = Nil): List[(Term,Pos)] = t match {
    case t: FunTerm =>
      (t, acc) :: (t.args.zipWithIndex flatMap {
        si => allSubtermsWithPos(si._1, acc :+ si._2)
      }).toList
    case v: Var =>
      (v, acc) :: Nil
    case _ => throw new InternalError(s"Term $t is not supported")
  }

}

trait VarOrSymConst   {

  def toTerm: Term = this match {
    case x: Var      ⇒ x
    case c: SymConst ⇒ c
    case _           ⇒ throw InternalError("toTerm: unexpected object " + this)
  }
  def isVar: Boolean
  def asVar = this.asInstanceOf[Var]
  def isSymConst: Boolean
  def asSymConst = this.asInstanceOf[SymConst]
  
  def vOrSoccursIn(f: Formula) = this match {
    case x: Var      ⇒ f.vars contains x
    case c: SymConst ⇒ f.symConsts contains c
    case _           ⇒ throw InternalError("vOrSoccursIn: unexpected object " + this)
  } 

  /** The identifier of this */
  def ident = this match {
    case x: Var => x.name
    case c: SymConst => c.op.name
  }

  // Total ordering based on names. Used for iQE.
  // We cannot use Term.gtr because that ordering is not total
  def >(that: VarOrSymConst) =
    if (this.isSymConst && that.isSymConst)
      // use usual ordering, so that can use precedence
      this.asSymConst gtr that.asSymConst
    else 
      this.ident > that.ident

  def >=(that: VarOrSymConst) = (this == that) || (this > that)

  // For some reason this did not compile
  // def compare(a: VarOrSymConst, b: VarOrSymConst) = a.ident compare b.ident
  // def >(that: VarOrSymConst) = (compare(this, that) > 0)

/*
  override def equals(that: Any) = that match {
    case that:VarOrSymConst => 
      if (! (this canEqual that))
        false
      else if (this.isInstanceOf[AbstVar] && that.isInstanceOf[AbstVar])
        AbstVar.equals(this.asInstanceOf[AbstVar], that.asInstanceOf[AbstVar])
      else if (this.isInstanceOf[GenVar] && that.isInstanceOf[GenVar])
        GenVar.equals(this.asInstanceOf[GenVar], that.asInstanceOf[GenVar])
      else if (isSymConst && that.isSymConst)
        SymConst.equals(this.asInstanceOf[SymConst], that.asInstanceOf[SymConst])
      else false
    case _ => false
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[VarOrSymConst]

  override def hashCode: Int = 
    if (this.isInstanceOf[AbstVar])
      this.asInstanceOf[AbstVar].hashCode
    else if (this.isInstanceOf[GenVar])
      this.asInstanceOf[GenVar].hashCode
    else
      this.asInstanceOf[SymConst].hashCode
 */
}


