Notes on new lemmas
===================

Added the following as lemmas:

      - (a * b) = - a * b
      c = b * c ⟷ c = 0 ∨ b = 1
      (0 < a & 0 < b) ⟹ 0 < a * b
      a * b = b * a
      a * (b + c) = (a * b) + (a * c)

(Note, doesn't include associativity, for a few problems this has adverse effects).
This allows the following problems to be solved

* ARI/ARI669=1.p	1.00		12.49
* ARI/ARI670=1.p	1.00		6.26
* ARI/ARI673=1.p	1.00		8.17
* ARI/ARI674=1.p	0.67		8.44
* ARI/ARI675=1.p	1.00		15.11
* ARI/ARI676=1.p	1.00		4.23
* ARI/ARI677=1.p	1.00		3.22
* ARI/ARI680=1.p	1.00		4.41
* ARI/ARI681=1.p	1.00		29.69
* ARI/ARI682=1.p	1.00		2.10
* ARI/ARI691=1.p	0.33		1.67
* ARI/ARI652=1.p	0.50		5.61
* ARI/ARI655=1.p	1.00		6.65
* ARI/ARI656=1.p	1.00		30.11
* ARI/ARI659=1.p	1.00		38.16
* ARI/ARI695=1.p	0.33		2.10
* ARI/ARI706=1.p	0.33		4.52
* SWW/SWW598=2.p	0.00		29.12
* SWW/SWW613=2.p	0.33		32.12
* SWW/SWW650=2.p	0.00		19.74

For this run, delayed NLPP expansion was off, as was strong BG tautology simplification and indexing.
The translation: nlpp(k.l, r) => k.nlpp(l,r) was not compiled in either.

Enabling -x and delayed nlpp improved performance (at least 5s and up to 20s) on
* ARI/ARI656=1.p (26.62) (no-x 25.72) (no nlpp 24.63) hmmm
* ARI/ARI674=1.p
* ARI/ARI655=1.p
* ARI/ARI659=1.p
* SWW/SWW650=2.p

However, performance on SWW/SWW613=2.p was worse (15s).
nlpp delayed: 52.77
nlpp delayed, -x: 39.12
default: 54.70
perhaps due to -x being enabled by default in the main branch?

And ARI/ARI700=1.p produced an error: related to -x, calls cooper on an unsimplified formula with large coeff...
This is just a cooper bug/performance problem.

Using Assoc lemma
-----------------
This kills performance on
* ARI/ARI669=1.p
* ARI/ARI676=1.p
* ARI/ARI680=1.p
* ARI/ARI681=1.p
* ARI/ARI659=1.p
* ARI/ARI659=1.p
* SWW/SWW613=2.p

and possibly others that were solved already without lemmas

It allows solving the following
* ARI/ARI701=1.p
* ARI/ARI707=1.p
* ARI/ARI710=1.p
* NUM/NUM886=1.p

LFA + LRA theories
------------------
Added extra rules based on Isabelle/HOL library for fields.
Note that some of these may allow that $quotient(X,0) = 0, though I have guarded most of these rules.
The ones previously compiled into main.scala were inconsistent for that reason.

It seems we will need parallel rules for both $product and #nlpp terms, since if the input term is linear it
is never converted to an #nlpp term and so the new lemmas never apply.
For example $quotient($product(3, t), 3) = t and the lemma: $quotient(#nlpp(x,y), y) = x.

Some other lemmas could be made simpRules, particularly those for $quotient terms as they don't have the $product/#nlpp
distinction.