BEAGLE - a theorem prover for hierarchic superposition
======================================================

Beagle is an automated theorem prover for first-order logic with equality over
linear integer/rational/real arithmetic. It accepts formulas in the [FOF, TFF, and
TFF-INT formats of the TPTP syntax](http://www.cs.miami.edu/~tptp/TPTP/TR/TPTPTR.shtml). 
It also accepts [SMT-LIB](http://www.smtlib.org/) version 2 input files. See the 
[SMTtoTPTP tool](https://bitbucket.org/peba123/smttotptp) for more details on this.

A more experimental version of Beagle is maintained at https://bitbucket.org/joshbax189/beagle .

Warning
=======
Beagle is currently not suitable for "big" input formulas. In particular the
preprocessing algorithms are not optimized for that case. Beagle is intended
as a testbed for theorem proving modulo arithmetic background theories. For problems
without arithmetics there are better provers than Beagle.

Installation
============

Beagle is available at https://bitbucket.org/peba123/beagle .
The distribution includes a pre-compiled jar archive and the Scala sources.

Running the pre-compiled Java archive
-------------------------------------

Run

    java -jar /PATH/TO/HERE/target/scala-2.12/beagle-0.9.52.jar [OPTION...] FILE

where FILE is an SMT-LIB version 2 or a TPTP TFF file.
See the `examples` subdirectory for some test problems.

Run

    java -jar /PATH/TO/HERE/target/scala-2.12/beagle-0.9.52.jar -help

to obtain basic usage information.

The trivial shell script `beagle` in this directory is a shorthand for
executing java with the above jar file and some java options for more memory.

Installation from sources
-------------------------

Beagle can be built with sbt, the Scala Build Tool.
The sbt tool installs the scala compiler, if needed. Beagle is (currently)
compiled with Scala version 2.12.19.

If Beagle is to be compiled once-and-forall probably the easiest way 
is to create a Java jar-archive and execute that.
Steps:

(1) Install [sbt](http://www.scala-sbt.org/). 

(2) In the current directory invoke `sbt assembly`. .

If successful, this creates the file `target/scala-2.12/beagle-0.9.52.jar`

Caveat: the file `project/plugins.sbt` contains a line like 

	addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "1.1.1")

Please update the specified version string (here: "1.1.1") with
a more recent one from https://github.com/sbt/sbt-assembly
should this version not be available.

(3) Run

    java -jar PATH/TO/HERE/target/scala-2.12/beagle-0.9.52.jar [OPTION...] FILE

as described above.

Background reasoning through SMT solvers
----------------------------------------

Beagle depends on an external SMT solver for the background theories of (linear)
real arithmetics. For linear integer/rational arithmetic no external SMT
solver is required, but one can be used instead of the built-in solvers.
The SMT solver must be installed separately, it is not included in this
distribution.

The SMT solver must comply with the [SMT-LIB](http://www.smtlib.org/)
standard, version 2. Its invocation is specified by the Beagle flag
`-smtsolver` flag, e.g.,

	beagle -smtsolver /usr/local/bin/cvc4 FILE

Beagle calls the SMT solver via an SMT-LIB file and receives its result from
standard output. The SMT solver must accept its input file name as its last
argument. Beagle tests if the SMT solver supports the computation of
unsatisfiable cores and, if so, makes use of them.

Suitable SMT solvers are, e.g., [CVC4](http://cvc4.cs.nyu.edu/web/) and
[Z3](https://github.com/Z3Prover/z3). 

Notice that when specified, the SMT solver is also used for linear
integer/rational arithmetic. This behaviour can be overridden for linear
integer arithmetic by the `-liasolver` flag and specifying either `iQE` or
`cooper` (`iQE` is highly experimental, not recommended).

Example:

	beagle -smtsolver /usr/local/bin/cvc4 -liasolver iQE FILE


Running out of memory
---------------------

Occasionally it is helpful to enable higher ressource limits.
If you are encountering, e.g., heap problems, try adding the option

	-Xms512M -Xmx4G -Xss10M -XX:MaxPermSize=384M

to the java call.

Documentation
=============
Documentation beyond `beagle -help` is not yet available.

Precedences
-----------
Beagle, a superposition prover, works internally with term orderings (LPO if theories are present, KBO otherwise). 
The precedences among operators for LPO can be specified in the input TPTP file as follows:

    %$ :prec f > g > h
    %$ :prec f > k > l

Beagle will linearize the stated precedences into a total ordering on operator symbols.

The corresponding syntax in SMT-LIB input files is as follows:

    (set-option :prec ("f > g > h" "f > k > l"))

Refutations
-----------
Beagle is capable of generating refutations from successful proof attempts.
There are currently three ways to display refutations:
* `beagle -proof` print a plain text refutation to stdout
* `beagle -proof -print tff` print a TFF formatted refutation to stdout
* `beagle -proof-file [file_name]` write both a type signature and refutation in TFF format to the specified file

In addition Beagle will produce a TFF formatted description of a saturated set if found when the `-print tff` flag is set.

Note that these refutations do not typically include detailed descriptions of CNF translations or detailed theory reasoning.

Publications
============

- [Peter Baumgartner, Joshua Bax, Uwe Waldmann. Beagle - A Hierarchic Superposition Theorem Prover](https://peba62.github.io/publications/Beagle.pdf).
This is the main paper on Beagle. Please use the following Bibtex entry for
citations:

		@inproceedings{Baumgartner:Bax:Waldmann:Beagle:CADE:2015,
		  author = {Peter Baumgartner and Joshua Bax and Uwe Waldmann},
		  title = {{Beagle} -- {A} {H}ierarchic {S}uperposition {T}heorem {P}rover},
		  booktitle = {CADE-25 -- 25th International Conference on Automated Deduction},
		  editor = {Amy P. Felty and Aart Middeldorp},
		  year = {2015},
		  series = {LNAI},
		  volume = {9195},
		  pages = {367--377}
		}
  
- [Peter Baumgartner, Uwe Waldmann. Hierarchic superposition with weak
  abstraction](http://dx.doi.org/10.1007/978-3-642-38574-2_3)

- [Peter Baumgartner, Joshua Bax, Uwe Waldmann. Finite Quantification in
  Hierarchic Theorem Proving](https://peba62.github.io/publications/finite-quantification-IJCAR-2014.pdf)

- [Peter Baumgartner, Joshua Bax. Proving Infinite
  Satisfiability](https://peba62.github.io/publications/LPAR-19-final.pdf)

- [Geoff Sutcliffe, Stephan Schulz, Koen Claessen, Peter Baumgartner. The TPTP
  Typed First-order Form and Arithmetic](https://peba62.github.io/publications/TFFArith.pdf)

Contact
=======

[Peter Baumgartner](https://peba62.github.io/)

Email: Peter.Baumgartner@data61.csiro.au

Usage reports, comments and suggestions are gratefully received.
